<?php

namespace Drupal\access_policy_test\Plugin\AccessRule;

use Drupal\access_policy\Plugin\access_policy\AccessRule\AccessRuleBase;
use Drupal\access_policy\Plugin\access_policy\AccessRule\AccessRulePluginInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access Rule Test.
 *
 * This is a test access rule plugin primarily for testing a missing query
 * handler.
 *
 * @AccessRule(
 *   id = "access_rule_test",
 * )
 */
class AccessRuleTest extends AccessRuleBase implements AccessRulePluginInterface {

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityInterface $entity) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(EntityInterface $entity, AccountInterface $account) {
    return TRUE;
  }

}
