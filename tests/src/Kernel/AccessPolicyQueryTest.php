<?php

namespace Drupal\Tests\access_policy\Kernel;

use Drupal\access_policy\AccessPolicyQueryAlter;
use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\Core\Database\Database;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests the Select query builder.
 *
 * @group access_policy
 */
class AccessPolicyQueryTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;

  /**
   * The modules required for this test.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'taxonomy',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
  ];

  /**
   * The database connection for testing.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('taxonomy_term');

    $this->connection = Database::getConnection();

    NodeType::create([
      'type' => 'page',
      'title' => 'Page',
    ])->save();

    // Create the user reference field.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_user_reference',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'user',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_user_reference',
      'bundle' => 'page',
      'translatable' => FALSE,
    ]);
    $field->save();

    // Create the entity reference fields for user and content.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_entity_reference',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'node',
      ],
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_entity_reference',
      'bundle' => 'page',
      'translatable' => FALSE,
    ]);
    $field->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_entity_reference',
      'entity_type' => 'user',
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'node',
      ],
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_entity_reference',
      'bundle' => 'user',
      'translatable' => FALSE,
    ]);
    $field->save();

    AccessPolicy::create([
      'id' => 'basic_policy',
      'label' => 'Basic policy',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ])->save();

    $policy = AccessPolicy::create([
      'id' => 'basic_policy_with_rule',
      'label' => 'Basic policy with field value access rule',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Setup some node references for access rule testing purposes.
    $node_references = [
      'Node reference 1',
      'Node reference 2',
    ];
    foreach ($node_references as $label) {
      $node = $this->createNode([
        'title' => $label,
        'status' => 1,
        'type' => 'page',
      ]);
      $node->save();
    }
  }

  /**
   * Get the access policy query alter class.
   *
   * @return \Drupal\access_policy\AccessPolicyQueryAlter
   *   The access policy query alter handler.
   */
  protected function getQueryAlter() {
    return \Drupal::service('class_resolver')
      ->getInstanceFromDefinition(AccessPolicyQueryAlter::class);
  }

  /**
   * Tests the query alter to ensure it returns the right results if disabled.
   */
  public function testSimpleQueryWithNoPolicies() {
    $node = $this->createNode([
      'title' => 'Node 1',
    ]);
    $node->save();
    $entity_type = \Drupal::entityTypeManager()->getDefinition('node');

    $query = $this->connection->select('node_field_data')
      ->fields('node_field_data', ['nid']);

    $this->getQueryAlter()->queryAlter($query, $entity_type);
    $result = $query->execute()->fetchCol();
    $expected = [
      $this->getNodeByTitle('Node reference 1')->id(),
      $this->getNodeByTitle('Node reference 2')->id(),
      $node->id(),
    ];
    $this->assertEquals($expected, $result);
  }

  /**
   * Tests query alter for queries with different base tables.
   */
  public function testQueryAlterWithDifferentBaseTables() {

    $admin_user = $this->createUser([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view basic_policy content',
      'view basic_policy_with_rule content',
    ]);
    $admin_user->set('field_entity_reference', [
      $this->getNodeByTitle('Node reference 1'),
    ]);
    $admin_user->save();

    $this->setCurrentUser($admin_user);

    $node_1 = $this->createNode([
      'title' => 'Node  (Without policy)',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['basic_policy'],
    ]);
    $node_1->save();

    $node_2 = $this->createNode([
      'title' => 'Node 2 (With policy)',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['basic_policy'],
    ]);

    $node_3 = $this->createNode([
      'title' => 'Node 3 (With policy and rule)',
      'type' => 'page',
      'status' => 1,
      'field_entity_reference' => [
        $this->getNodeByTitle('Node reference 1'),
      ],
      'access_policy' => ['basic_policy_with_rule'],
    ]);

    $entity_type = \Drupal::entityTypeManager()->getDefinition('node');

    // Perform the query with node table.
    $query = $this->connection->select('node')
      ->fields('node', ['nid']);

    $this->getQueryAlter()->queryAlter($query, $entity_type);
    $result = $query->execute()->fetchCol();
    $expected = [
      $this->getNodeByTitle('Node reference 1')->id(),
      $this->getNodeByTitle('Node reference 2')->id(),
      $node_1->id(),
      $node_2->id(),
      $node_3->id(),
    ];
    $this->assertEquals($expected, $result);

    // Perform the query with node_field_data table.
    $query = $this->connection->select('node_field_data')
      ->fields('node_field_data', ['nid']);

    $this->getQueryAlter()->queryAlter($query, $entity_type);
    $result = $query->execute()->fetchCol();
    $expected = [
      $this->getNodeByTitle('Node reference 1')->id(),
      $this->getNodeByTitle('Node reference 2')->id(),
      $node_1->id(),
      $node_2->id(),
      $node_3->id(),
    ];
    $this->assertEquals($expected, $result);

    // Perform the query on the node revisions table.
    $query = $this->connection->select('node_field_revision')
      ->fields('node_field_revision', ['nid']);

    $this->getQueryAlter()->queryAlter($query, $entity_type);
    $result = $query->execute()->fetchCol();
    $expected = [
      $this->getNodeByTitle('Node reference 1')->id(),
      $this->getNodeByTitle('Node reference 2')->id(),
      $node_1->id(),
      $node_2->id(),
      $node_3->id(),
    ];
    $this->assertEquals($expected, $result);

    // Perform the query with a table alias.
    $query = $this->connection->select('node_field_data', 'base_table')
      ->fields('base_table', ['nid']);

    $this->getQueryAlter()->queryAlter($query, $entity_type);
    $result = $query->execute()->fetchCol();
    $expected = [
      $this->getNodeByTitle('Node reference 1')->id(),
      $this->getNodeByTitle('Node reference 2')->id(),
      $node_1->id(),
      $node_2->id(),
      $node_3->id(),
    ];
    $this->assertEquals($expected, $result);

    // Perform the query with conflicting table and alias.
    // This is joining the field table before the plugin, such as what might
    // happen with views.
    $query = $this->connection->select('node_field_data', 'base_table')
      ->fields('base_table', ['nid']);
    $query->join('node__field_entity_reference', 'e', 'e.entity_id = base_table.nid');

    $this->getQueryAlter()->queryAlter($query, $entity_type);
    $result = $query->execute()->fetchCol();
    $expected = [
      $node_3->id(),
    ];
    $this->assertEquals($expected, $result);

  }

  /**
   * Do a basic query alter on node and taxonomy term entity type.
   *
   * Most of the logic is already handled with nodes, this is essentially a
   * smoke test to catch any major issues with multiple entity types.
   */
  public function testQueryAlterWithMultipleEntityTypes() {
    // First create some nodes that are assigned the basic policy.
    $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['basic_policy'],
    ]);

    $this->createNode([
      'title' => 'Node 2',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['basic_policy'],
    ]);

    // Then create some taxonomy terms that are assigned their own policy.
    $vocabulary = Vocabulary::create([
      'name' => 'tags',
      'vid' => 'tags',
    ]);
    $vocabulary->save();

    $policy = AccessPolicy::create([
      'id' => 'group_policy_with_terms',
      'label' => 'Group test policy taxonomy terms',
      'access_rules' => [],
      'target_entity_type_id' => 'taxonomy_term',
    ]);
    $policy->save();

    $policy_2 = AccessPolicy::create([
      'id' => 'group_policy_with_terms_2',
      'label' => 'Group test policy taxonomy terms 2',
      'access_rules' => [],
      'target_entity_type_id' => 'taxonomy_term',
    ]);
    $policy_2->save();

    // Add a term to the vocabulary.
    $term_1 = $this->createTerm($vocabulary, [
      'title' => 'Term 1',
    ]);
    $term_1->save();

    $term_2 = $this->createTerm($vocabulary, [
      'name' => 'Term 2',
    ]);
    $term_2->save();

    $this->contentAccessPolicyManager->assign($term_1, $policy);
    $this->contentAccessPolicyManager->assign($term_2, $policy_2);

    $admin_user = $this->createUser([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view group_policy_with_terms taxonomy term',
    ]);
    $admin_user->save();
    $this->setCurrentUser($admin_user);

    // Perform the query with taxonomy term table.
    $entity_type = \Drupal::entityTypeManager()->getDefinition('taxonomy_term');
    $query = $this->connection->select('taxonomy_term_data')
      ->fields('taxonomy_term_data', ['tid']);

    $this->getQueryAlter()->queryAlter($query, $entity_type);
    $result = $query->execute()->fetchCol();
    $expected = [
      $term_1->id(),
    ];
    $this->assertEquals($expected, $result, 'It should only return a single taxonomy term');
  }

}
