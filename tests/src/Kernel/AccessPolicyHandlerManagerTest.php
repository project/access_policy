<?php

namespace Drupal\Tests\access_policy\Kernel;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests the access policy handler manager service.
 *
 * @group access_policy
 */
class AccessPolicyHandlerManagerTest extends AccessPolicyKernelTestBase {

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);

    FieldStorageConfig::create([
      'field_name' => 'field_test_2',
      'entity_type' => 'node',
      'type' => 'text',
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_test_2',
      'entity_type' => 'node',
      'label' => 'Text field',
      'bundle' => 'page',
    ])->save();

    // Creating a policy is necessary to register the plugins.
    $policy = AccessPolicy::create([
      'id' => 'basic_policy',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();
  }

  /**
   * This tests retrieving a handler that has a numeric field name.
   */
  public function testGetHandlerWithNumericFieldName() {
    // Add two access rules with numeric field names. This will append a count
    // to the end of each one.
    $access_rule_1 = $this->accessRuleManager->getHandler('node', 'field_test_2');
    $policy = $this->entityTypeManager->getStorage('access_policy')->load('basic_policy');
    $policy->addHandler('access_rule', $access_rule_1);

    $access_rule_2 = $this->accessRuleManager->getHandler('node', 'field_test_2');
    $policy->addHandler('access_rule', $access_rule_2);
    $policy->save();

    // Fetch all the access rules and compare their ids.
    $access_rules = $policy->getHandlers('access_rule');
    $ids = array_keys($access_rules);
    $expected = [
      'field_test_2',
      'field_test_2_1',
    ];
    $this->assertEquals($expected, $ids, 'The access rule ids were not correct.');

    // Fetch the access rules from the policy. Make sure that it returns the
    // right plugins.
    $handler = $this->accessRuleManager->getHandlerFromPolicy($policy, 'field_test_2');
    $this->assertEquals('entity_field_string', $handler->getPluginId(), 'The plugin is incorrect');
    $this->assertEquals('field_test_2', $handler->getDefinition()->getId(), 'The id is incorrect.');
    $this->assertEquals('field_test_2', $handler->getDefinition()->getFieldName(), 'The field name is incorrect.');

    $handler = $this->accessRuleManager->getHandlerFromPolicy($policy, 'field_test_2_1');
    $this->assertEquals('entity_field_string', $handler->getPluginId(), 'The plugin is incorrect');
    $this->assertEquals('field_test_2_1', $handler->getDefinition()->getId(), 'The id is incorrect.');
    $this->assertEquals('field_test_2', $handler->getDefinition()->getFieldName(), 'The field name is incorrect.');
  }

  /**
   * This tests retrieving all handlers across policies with filter.
   */
  public function testGetHandlersFromPoliciesWithFilter() {
    $access_rule_1 = $this->accessRuleManager->getHandler('node', 'field_test_2');
    $policy = $this->entityTypeManager->getStorage('access_policy')->load('basic_policy');
    $policy->addHandler('access_rule', $access_rule_1);
    $policy->save();

    // Fetch the access rules from the policy. Make sure that it returns the
    // right plugins.
    $handlers = $this->accessRuleManager->getHandlersFromPolicies('node');
    $handler = $handlers[0];
    $this->assertEquals('entity_field_string', $handler->getPluginId(), 'The plugin is incorrect');
    $this->assertEquals('field_test_2', $handler->getDefinition()->getId(), 'The id is incorrect.');
    $this->assertEquals('field_test_2', $handler->getDefinition()->getFieldName(), 'The field name is incorrect.');

    // Fetch the access rules from the policy filtered.
    $handlers = $this->accessRuleManager->getHandlersFromPolicies('node', function ($handler) {
      if ($handler->getDefinition()->getId() == 'field_test_2') {
        return FALSE;
      }
      return TRUE;
    });

    $this->assertEmpty($handlers);
  }

  /**
   * This tests retrieving all handlers across multiple policies.
   */
  public function testGetHandlersFromMultiplePolicies() {
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_text');
    $policy = $this->entityTypeManager->getStorage('access_policy')->load('basic_policy');
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $policy = AccessPolicy::create([
      'id' => 'basic_policy_2',
      'label' => 'Basic test policy 2',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_text');
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Fetch the access rules from the policy. Make sure that it returns the
    // right plugins.
    $handlers = $this->accessRuleManager->getHandlersFromPolicies('node');
    $this->assertEquals(2, count($handlers), 'Two handlers should have been returned.');
    $handler = $handlers[0];
    $this->assertEquals('entity_field_string', $handler->getPluginId(), 'The plugin is incorrect');
    $this->assertEquals('field_text', $handler->getDefinition()->getId(), 'The id is incorrect.');
    $this->assertEquals('field_text', $handler->getDefinition()->getFieldName(), 'The field name is incorrect.');

    $handler = $handlers[1];
    $this->assertEquals('entity_field_string', $handler->getPluginId(), 'The plugin is incorrect');
    $this->assertEquals('field_text', $handler->getDefinition()->getId(), 'The id is incorrect.');
    $this->assertEquals('field_text', $handler->getDefinition()->getFieldName(), 'The field name is incorrect.');

  }

}
