<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\AccessRule\Broken;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\Core\Site\Settings;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;

/**
 * Tests UserFieldValuesCacheContext.
 *
 * @group access_policy
 */
class UserFieldValuesCacheContextTest extends AccessPolicyKernelTestBase {

  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);

    // Create a list field for user and node.
    FieldStorageConfig::create([
      'field_name' => 'field_list_string',
      'entity_type' => 'node',
      'type' => 'list_string',
      'cardinality' => 1,
      'settings' => [
        'allowed_values' => [
          'foo' => 'Foo',
          'bar' => 'Bar',
        ],
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_list_string',
      'entity_type' => 'node',
      'label' => 'Test options list field',
      'bundle' => 'page',
    ])->save();

    FieldStorageConfig::create([
      'field_name' => 'field_list_string',
      'entity_type' => 'user',
      'type' => 'list_string',
      'cardinality' => 1,
      'settings' => [
        'allowed_values' => [
          'foo' => 'Foo',
          'bar' => 'Bar',
        ],
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_list_string',
      'entity_type' => 'user',
      'label' => 'Test options list field',
      'bundle' => 'user',
    ])->save();

    $this->policy = AccessPolicy::create([
      'id' => 'basic',
      'label' => 'Basic policy',
      'target_entity_type_id' => 'node',
    ]);
    $this->policy->save();
  }

  /**
   * Tests the user field values cache context.
   *
   * @dataProvider providerUserFieldValuesCacheContext
   */
  public function testUserFieldValuesCacheContext(array $handlers, $field_name, array $expected) {

    if ($handlers) {
      foreach ($handlers as $handler) {
        $access_rule = $this->accessRuleManager->getHandler($handler['group'], $handler['name']);
        $this->assertNotInstanceOf(Broken::class, $access_rule);
        if (isset($handler['settings'])) {
          $access_rule->setSettings($handler['settings']);
        }
        $this->policy->addHandler('access_rule', $access_rule);
      }
      $this->policy->save();
    }

    $user = $this->createUser([
      'access content',
      'view basic content',
    ]);
    $user->set('field_list_string', ['Foo']);
    $user->setEmail('foobar@example.com');
    $user->save();

    $this->setCurrentUser($user);
    $cacheContext = \Drupal::service('cache_context.user.field_values');

    // We're hashing the values here so that we can compare the final hash
    // with the hash returned by the service.
    $identifier = serialize($expected['field_values']);
    $expected_hash = hash('sha256', Settings::getHashSalt() . $identifier);
    $context = $cacheContext->getContext($field_name);

    $this->assertEquals($expected_hash, $context);
  }

  /**
   * Provider for user field values cache context.
   */
  public function providerUserFieldValuesCacheContext() {
    return [
      'no fields observed by access rules' => [
        'handlers' => [],
        'field_name' => NULL,
        'expected' => [
          'field_values' => [],
        ],
      ],
      'mail observed by access rules' => [
        'handlers' => [['group' => 'user', 'name' => 'mail']],
        'field_name' => NULL,
        'expected' => [
          'field_values' => [
            'mail' => [['value' => 'foobar@example.com']],
          ],
        ],
      ],
      'multiple fields observed by access rules (without field name argument)' => [
        'handlers' => [
          [
            'group' => 'user',
            'name' => 'mail',
          ],
          [
            'group' => 'node',
            'name' => 'match_field_list_string',
            'settings' => [
              'operator' => 'in',
              'value' => ['field' => 'field_list_string'],
            ],
          ],
        ],
        'field_name' => NULL,
        'expected' => [
          'field_values' => [
            'mail' => [['value' => 'foobar@example.com']],
            'field_list_string' => [['value' => 'Foo']],
          ],
        ],
      ],
      'multiple fields observed by access rules (with single field name argument)' => [
        'handlers' => [
          [
            'group' => 'user',
            'name' => 'mail',
          ],
          [
            'group' => 'node',
            'name' => 'match_field_list_string',
            'settings' => [
              'operator' => 'in',
              'value' => ['field' => 'field_list_string'],
            ],
          ],
        ],
        'field_name' => 'mail',
        'expected' => [
          'field_values' => [
            'mail' => [['value' => 'foobar@example.com']],
          ],
        ],
      ],
      'multiple fields observed by access rules (with multiple field name argument)' => [
        'handlers' => [
          [
            'group' => 'user',
            'name' => 'mail',
          ],
          [
            'group' => 'node',
            'name' => 'match_field_list_string',
            'settings' => [
              'operator' => 'in',
              'value' => ['field' => 'field_list_string'],
            ],
          ],
        ],
        'field_name' => 'mail,field_list_string',
        'expected' => [
          'field_values' => [
            'mail' => [['value' => 'foobar@example.com']],
            'field_list_string' => [['value' => 'Foo']],
          ],
        ],
      ],
    ];
  }

}
