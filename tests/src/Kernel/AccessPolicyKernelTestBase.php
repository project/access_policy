<?php

namespace Drupal\Tests\access_policy\Kernel;

use Drupal\access_policy\AccessPolicyQueryAlter;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Access policy kernel test base.
 */
abstract class AccessPolicyKernelTestBase extends KernelTestBase {

  use NodeCreationTrait;
  use UserCreationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The content access policy manager.
   *
   * @var \Drupal\access_policy\ContentAccessPolicyManager
   */
  protected $contentAccessPolicyManager;

  /**
   * The access rule manager.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $accessRuleManager;

  /**
   * The selection rule manager service.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $selectionRuleManager;

  /**
   * The access policy validator.
   *
   * @var \Drupal\access_policy\AccessPolicyValidatorInterface
   */
  protected $accessPolicyValidator;

  /**
   * The modules required for this test.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
  ];

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system']);
    $this->installConfig(['node']);
    $this->installConfig(['filter']);
    $this->installConfig('access_policy');
    $this->installSchema('system', ['sequences']);
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('access_policy');

    $this->connection = Database::getConnection();
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->contentAccessPolicyManager = \Drupal::service('access_policy.content_policy_manager');
    $this->accessRuleManager = $this->container->get('plugin.manager.access_policy.access_rule');
    $this->selectionRuleManager = \Drupal::service('plugin.manager.access_policy.selection_rule');
    $this->accessPolicyValidator = $this->container->get('access_policy.validator');
  }

  /**
   * Get the access policy query alter class.
   *
   * @return \Drupal\access_policy\AccessPolicyQueryAlter
   *   The access policy query alter handler.
   */
  protected function getQueryAlter() {
    return \Drupal::service('class_resolver')
      ->getInstanceFromDefinition(AccessPolicyQueryAlter::class);
  }

  /**
   * Helper function for assigning an access policy to a node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param mixed $access_policy
   *   The access policy.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function assignAccessPolicy(EntityInterface $entity, $access_policy) {
    if (!is_array($access_policy)) {
      $access_policy = [$access_policy];
    }
    $entity->set('access_policy', $access_policy);
    $entity->save();
  }

  /**
   * Create access policy entity.
   *
   * @param string $name
   *   The name of the policy.
   * @param array $values
   *   The policy vale.
   *
   * @return \Drupal\access_policy\Entity\AccessPolicyInterface
   *   The created access policy.
   */
  protected function createAccessPolicy($name, array $values = []) {
    $values += [
      'id' => $name,
      'label' => $name,
    ];
    $access_policy = $this->entityTypeManager->getStorage('access_policy')
      ->create($values);
    $access_policy->save();

    return $access_policy;
  }

  /**
   * Assert query results.
   *
   * @param string $entity_type
   *   The entity type.
   * @param mixed $expected
   *   The expected results.
   */
  protected function assertQueryResults($entity_type, $expected) {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type);
    $data_table = $entity_type->getDataTable();
    $id_key = $entity_type->getKey('id');
    $query = $this->connection->select($data_table)
      ->fields($data_table, [$id_key]);

    $this->getQueryAlter()->queryAlter($query, $entity_type);
    $result = $query->execute()->fetchCol();
    $this->assertEquals($expected, $result, 'The query did not return the right entities.');
  }

}
