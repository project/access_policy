<?php

namespace Drupal\Tests\access_policy\Kernel;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests validation constraints for ValidReferenceConstraintValidator.
 *
 * @group access_policy
 */
class ValidAccessPolicyReferenceConstraintValidatorTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use StringTranslationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'taxonomy',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('taxonomy_term');
  }

  /**
   * Tests the ValidAccessPolicyReferenceConstraintValidator.
   */
  public function testAccessPolicyReferenceConstraintValidation() {

    $policy = AccessPolicy::create([
      'id' => 'policy_test_1',
      'label' => 'Policy test 1',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'selection_set' => ['restricted'],
    ]);
    $policy->save();

    $policy_2 = AccessPolicy::create([
      'id' => 'policy_test_2',
      'label' => 'Policy test 2',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ]);
    $policy_2->save();

    $policy_3 = AccessPolicy::create([
      'id' => 'policy_test_3',
      'label' => 'Policy test 3',
      'access_rules' => [],
      'target_entity_type_id' => 'taxonomy_term',
    ]);
    $policy_3->save();

    $node = Node::create([
      'title' => 'Test published node',
      'type' => 'page',
      'status' => NodeInterface::PUBLISHED,
    ]);

    // This is not a valid access policy for this entity type.
    $node->set('access_policy', $policy_3);
    $violations = $node->get('access_policy')->validate();
    $this->assertCount(1, $violations);
    $this->assertEquals($this->t('The access policy (@label) cannot be referenced by this entity type.', [
      '@label' => $policy_3->label(),
    ]), $violations[0]->getMessage());
  }

}
