<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests EntityFieldTermReferenceDepthTest plugin features.
 *
 * @group access_policy
 */
class TermReferenceDepthTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use OperatorValidationTrait;

  /**
   * The tags vocabulary.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $vocabulary;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'taxonomy',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);
    $this->installEntitySchema('taxonomy_term');

    // Create a tags vocabulary.
    $this->vocabulary = Vocabulary::create([
      'name' => 'tags',
      'vid' => 'tags',
    ]);
    $this->vocabulary->save();

    $handler_settings = [
      'target_bundles' => ['tags' => 'tags'],
    ];

    // Create the taxonomy term entity reference field for the node and user.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_tags',
      'entity_type' => 'user',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_tags',
      'bundle' => 'user',
      'translatable' => FALSE,
      'settings' => [
        'handler' => 'default',
        'handler_settings' => $handler_settings,
      ],
    ]);
    $field->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_tags',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_tags',
      'bundle' => 'page',
      'translatable' => FALSE,
      'settings' => [
        'handler' => 'default',
        'handler_settings' => $handler_settings,
      ],
    ]);
    $field->save();

    // Creating a policy is necessary to register the plugins.
    $policy = AccessPolicy::create([
      'id' => 'basic_policy',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // The first user is always the admin user.
    $this->createUser();
  }

  /**
   * Tests user field term reference with depth access rule.
   */
  public function testUserFieldTermReferenceWithDepth() {
    // Test entity reference fields.
    $parent_term_1 = $this->createTerm($this->vocabulary, [
      'title' => 'Parent 1',
    ]);
    $parent_term_1->save();

    $child_1 = $this->createTerm($this->vocabulary, [
      'name' => 'Child 1',
      'parent' => $parent_term_1->id(),
    ]);
    $child_1->save();

    $parent_term_2 = $this->createTerm($this->vocabulary, [
      'name' => 'Parent 2',
    ]);
    $parent_term_2->save();

    $policy = AccessPolicy::create([
      'id' => 'term_depth',
      'label' => 'Matching term references with depth',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('user', 'term_depth_field_tags');
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => [
        ['target_id' => $parent_term_1->id()],
      ],
      'depth' => 2,
      'query' => TRUE,
    ]);

    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['term_depth'],
    ]);

    // This user is referencing a parent_1. They should have access.
    $web_user = $this->createUser([
      'access content',
      'view term_depth content',
    ]);
    $web_user->set('field_tags', [$child_1->id()]);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

    // This user is referencing a child of parent_1. They should have access.
    $web_user_2 = $this->createUser([
      'access content',
      'view term_depth content',
    ]);
    $web_user_2->set('field_tags', [$child_1->id()]);
    $web_user_2->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user_2, 'view');
    $this->assertTrue($access, 'The user should have access');

    $this->setCurrentUser($web_user_2);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

    // Create another user with a different parent term. They should not have
    // access.
    $web_user_3 = $this->createUser([
      'access content',
      'view term_depth content',
    ]);
    $web_user_3->set('field_tags', [$parent_term_2]);
    $web_user_3->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user_3, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user_3);
    $this->assertQueryResults('node', []);

  }

  /**
   * Tests entity field term reference with depth access rule.
   */
  public function testEntityFieldTermReferenceWithDepth() {
    // Test entity reference fields.
    $parent_term_1 = $this->createTerm($this->vocabulary, [
      'title' => 'Parent 1',
    ]);
    $parent_term_1->save();

    $child_1 = $this->createTerm($this->vocabulary, [
      'name' => 'Child 1',
      'parent' => $parent_term_1->id(),
    ]);
    $child_1->save();

    $parent_term_2 = $this->createTerm($this->vocabulary, [
      'name' => 'Parent 2',
    ]);
    $parent_term_2->save();

    $policy = AccessPolicy::create([
      'id' => 'term_depth',
      'label' => 'Matching term references with depth',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'term_depth_field_tags');
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => [
        ['target_id' => $parent_term_1->id()],
      ],
      'depth' => 2,
      'query' => TRUE,
    ]);

    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // This is referencing a child term and should be visible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_tags' => [
        $child_1,
      ],
      'access_policy' => ['term_depth'],
    ]);

    // This is a different parent which the user does not have access to and
    // should not be visible.
    $node_2 = $this->createNode([
      'title' => 'Node 2',
      'type' => 'page',
      'status' => 1,
      'field_tags' => [
        $parent_term_2,
      ],
      'access_policy' => ['term_depth'],
    ]);

    // This user is referencing a parent_1. They should have access.
    $web_user = $this->createUser([
      'access content',
      'view term_depth content',
    ]);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $access = $this->accessPolicyValidator->validate($node_2, $web_user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

  }

  /**
   * Tests term reference with depth access rule when comparing user and entity.
   */
  public function testMatchingTermReferenceWithDepth() {
    // Test entity reference fields.
    $parent_term_1 = $this->createTerm($this->vocabulary, [
      'title' => 'Parent 1',
    ]);
    $parent_term_1->save();

    $child_1 = $this->createTerm($this->vocabulary, [
      'name' => 'Child 1',
      'parent' => $parent_term_1->id(),
    ]);
    $child_1->save();

    $parent_term_2 = $this->createTerm($this->vocabulary, [
      'name' => 'Parent 2',
    ]);
    $parent_term_2->save();

    $policy = AccessPolicy::create([
      'id' => 'term_depth',
      'label' => 'Matching term references with depth',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'match_depth_field_tags');
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => ['field' => 'field_tags'],
      'depth' => 1,
      'query' => TRUE,
    ]);

    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // This is referencing a child term and should be visible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_tags' => [
        $child_1,
      ],
      'access_policy' => ['term_depth'],
    ]);

    // This is a different parent which the user does not have access to and
    // should not be visible.
    $node_2 = $this->createNode([
      'title' => 'Node 2',
      'type' => 'page',
      'status' => 1,
      'field_tags' => [
        $parent_term_2,
      ],
      'access_policy' => ['term_depth'],
    ]);

    $web_user = $this->createUser([
      'access content',
      'view term_depth content',
    ]);

    $web_user->set('field_tags', [$parent_term_1]);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $access = $this->accessPolicyValidator->validate($node_2, $web_user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);
  }

  /**
   * Tests term reference access rule for entity reference fields.
   *
   * This ensures that authors cannot assign terms that they do not have access
   * to.
   */
  public function testTermReferenceWithDepthEntityReferenceField() {

    $policy = AccessPolicy::create([
      'id' => 'term_reference',
      'label' => 'Term reference access policy',
      'target_entity_type_id' => 'taxonomy_term',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('taxonomy_term', 'term_reference');
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => ['field' => 'field_tags'],
      'depth' => 1,
      'query' => TRUE,
    ]);

    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Test entity reference fields.
    $parent_term_1 = $this->createTerm($this->vocabulary, [
      'title' => 'Parent 1',
      'access_policy' => ['term_reference'],
    ]);
    $parent_term_1->save();

    $child_1 = $this->createTerm($this->vocabulary, [
      'name' => 'Child 1',
      'parent' => $parent_term_1->id(),
      'access_policy' => ['term_reference'],
    ]);
    $child_1->save();

    $parent_term_2 = $this->createTerm($this->vocabulary, [
      'name' => 'Parent 2',
      'access_policy' => ['term_reference'],
    ]);
    $parent_term_2->save();

    $web_user = $this->createUser([
      'view term_reference taxonomy term',
    ]);

    $web_user->set('field_tags', [$parent_term_1]);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($parent_term_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $access = $this->accessPolicyValidator->validate($child_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $access = $this->accessPolicyValidator->validate($parent_term_2, $web_user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('taxonomy_term', [
      $parent_term_1->id(),
      $child_1->id(),
    ]);
  }

}
