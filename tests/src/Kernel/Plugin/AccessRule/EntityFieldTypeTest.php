<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;

/**
 * Tests EntityFieldList plugin features.
 *
 * @group access_policy
 */
class EntityFieldTypeTest extends AccessPolicyKernelTestBase {

  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);
  }

  /**
   * Tests list string with multiple access rules.
   *
   * @dataProvider providerEntityFieldType
   */
  public function testEntityFieldType($settings, $bundle, $expected, $expected_query) {

    $policy = AccessPolicy::create([
      'id' => 'policy_bundle',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'type');
    $access_rule->setSettings($settings);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Create a basic node.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => $bundle,
      'status' => 1,
      'access_policy' => ['policy_bundle'],
    ]);

    $user = $this->createUser([
      'access content',
      'view policy_bundle content',
    ]);
    $user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $user, 'view');
    $this->assertEquals($expected, $access);

    $this->setCurrentUser($user);
    $this->assertQueryResults('node', $expected_query);
  }

  /**
   * Provides access rule settings.
   */
  public function providerEntityFieldType() {
    return [
      'bundle is true' => [
        ['operator' => 'or' , 'value' => ['page' => 'page']],
        'page',
        TRUE,
        [1],
      ],
      'bundle is not true' => [
        ['operator' => 'not' , 'value' => ['page' => 'page']],
        'page',
        FALSE,
        [],
      ],
    ];
  }

}
