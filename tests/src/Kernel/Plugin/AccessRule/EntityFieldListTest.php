<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests EntityFieldList plugin features.
 *
 * @group access_policy
 */
class EntityFieldListTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use OperatorValidationTrait;

  /**
   * The tags vocabulary.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $vocabulary;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);

    // Create two field entities.
    FieldStorageConfig::create([
      'field_name' => 'field_list_string',
      'entity_type' => 'node',
      'type' => 'list_string',
      'cardinality' => 1,
      'settings' => [
        'allowed_values' => [
          'foo' => 'Foo',
          'bar' => 'Bar',
          'test' => 'Test',
        ],
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_list_string',
      'entity_type' => 'node',
      'label' => 'Test options list field',
      'bundle' => 'page',
    ])->save();

    // Creating a policy is necessary to register the plugins.
    $policy = AccessPolicy::create([
      'id' => 'basic_policy',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();
  }

  /**
   * Tests list string with multiple access rules.
   *
   * @dataProvider providerEntityFieldList
   */
  public function testEntityFieldList($settings, $value, $expected, $expected_query) {

    $policy = AccessPolicy::create([
      'id' => 'policy_list_string',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_list_string');
    $access_rule->setSettings($settings);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Create a basic node.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_list_string' => $value,
      'access_policy' => ['policy_list_string'],
    ]);

    $user = $this->createUser([
      'access content',
      'view policy_list_string content',
    ]);
    $user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $user, 'view');
    $this->assertEquals($expected, $access);

    $this->setCurrentUser($user);
    $this->assertQueryResults('node', $expected_query);
  }

  /**
   * Provides access rule settings.
   */
  public function providerEntityFieldList() {
    return [
      'list field has matching value is true' => [
        ['operator' => 'or', 'value' => ['foo' => 'foo']],
        ['foo'],
        TRUE,
        [1],
      ],
      'list field has matching value is not true' => [
        ['operator' => 'or', 'value' => ['foo' => 'foo']],
        ['bar'],
        FALSE,
        [],
      ],
      'list field has no matching value is true' => [
        ['operator' => 'not', 'value' => ['foo' => 'foo']],
        ['bar'],
        TRUE,
        [1],
      ],
      'list field has no matching value is false' => [
        ['operator' => 'not', 'value' => ['foo' => 'foo']],
        ['foo'],
        FALSE,
        [],
      ],
    ];
  }

  /**
   * Tests list string with multiple access rules.
   */
  public function testEntityFieldListWithMultipleValues() {

    $policy = AccessPolicy::create([
      'id' => 'policy_list_string',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_list_string');
    $access_rule->setSettings([
      'value' => [
        'foo' => 'foo',
        'bar' => 'bar',
      ],
      'operator' => 'or',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Create a basic node.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_list_string' => [
        'foo',
        'test',
      ],
      'access_policy' => ['policy_list_string'],
    ]);
    // Create a basic node.
    $node_2 = $this->createNode([
      'title' => 'Node 2',
      'type' => 'page',
      'status' => 1,
      'field_list_string' => [
        'bar',
        'test',
      ],
      'access_policy' => ['policy_list_string'],
    ]);
    // Create a basic node.
    $node_3 = $this->createNode([
      'title' => 'Node 3',
      'type' => 'page',
      'status' => 1,
      'field_list_string' => [
        'test',
      ],
      'access_policy' => ['policy_list_string'],
    ]);

    $user = $this->createUser([
      'access content',
      'view policy_list_string content',
    ]);
    $user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $access = $this->accessPolicyValidator->validate($node_2, $user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $access = $this->accessPolicyValidator->validate($node_3, $user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($user);
    $this->assertQueryResults('node', [
      $node_1->id(),
      $node_2->id(),
    ]);
  }

  /**
   * Tests list string with multiple access rules.
   */
  public function testEntityFieldListWithMultipleRules() {

    $policy = AccessPolicy::create([
      'id' => 'policy_with_multiple_rules',
      'label' => 'Basic test policy with multiple rules',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_list_string');
    $access_rule->setSettings([
      'value' => [
        'foo' => 'foo',
      ],
      'operator' => 'or',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);

    $access_rule_2 = $this->accessRuleManager->getHandler('node', 'field_list_string');
    $access_rule_2->setSettings([
      'value' => [
        'bar' => 'bar',
      ],
      'operator' => 'not',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule_2);
    $policy->save();

    // Create a basic node.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_list_string' => [
        'foo',
      ],
      'access_policy' => ['policy_with_multiple_rules'],
    ]);

    $user = $this->createUser([
      'access content',
      'view policy_with_multiple_rules content',
    ]);
    $user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $this->setCurrentUser($user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

    $node_1->set('field_list_string', [
      'foo',
      'bar',
    ])->save();

    $this->accessPolicyValidator->resetCache();
    $access = $this->accessPolicyValidator->validate($node_1, $user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    // @todo there is a bug here.
    // This is also an edge case. Maybe address it during beta?
    // $this->setCurrentUser($user);
    // $this->assertQueryResults('node', []);
  }

}
