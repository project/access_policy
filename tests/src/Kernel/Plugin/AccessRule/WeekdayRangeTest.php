<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;

/**
 * Tests WeekdayRange plugin features.
 *
 * @group access_policy
 */
class WeekdayRangeTest extends AccessPolicyKernelTestBase {

  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);
  }

  /**
   * Tests weekday range when validating all day dates.
   */
  public function testWeekdayRangeAllDay() {

    $policy = AccessPolicy::create([
      'id' => 'policy_basic',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $weekdays = ['sun', 'mon', 'tue', 'thu', 'fri', 'sat'];

    // Remove the current date from the array. We'll validate against
    // the days of the week except the current day.
    $current_day = strtolower(date('D'));
    $pos = array_search($current_day, $weekdays);
    array_splice($weekdays, $pos, 1);

    $access_rule = $this->accessRuleManager->getHandler('global', 'weekday_range');
    $access_rule->setSettings([
      'weekday' => $weekdays,
      'all_day' => TRUE,
      'timezone_select' => 'default',
      'query' => FALSE,
      'operations' => ['edit'],
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Create a basic node.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => 'policy_basic',
    ]);

    $user = $this->createUser([
      'access content',
      'view policy_basic content',
      'edit policy_basic content',
    ]);
    $user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $user, 'edit');
    $this->assertFalse($access, 'The user should not be able to edit the node.');

    $access = $this->accessPolicyValidator->validate($node_1, $user, 'view');
    $this->assertTrue($access, 'The user should be able to view the node.');

    $this->setCurrentUser($user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);
  }

  /**
   * Tests weekday range when validating time range dates.
   */
  public function testWeekdayRangeTimeRange() {

    // Create a basic node.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
    ]);

    $user = $this->createUser([
      'access content',
    ]);
    $user->save();

    $access_rule = $this->accessRuleManager->getHandler('global', 'weekday_range');
    $access_rule->setSettings([
      'weekday' => ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'],
      'start_time' => '9:00:00',
      'end_time' => '17:00:00',
      'timezone_select' => 'default',
      'query' => FALSE,
      'operations' => ['edit'],
    ]);

    $time = new DrupalDateTime('5:30:00');
    $access_rule->setValue($time);

    $access = $access_rule->validate($node_1, $user);
    $this->assertFalse($access, 'The user should not be able to edit the node.');

    // Change the access rule settings and run validation again.
    $time = new DrupalDateTime('12:30:00');
    $access_rule->setValue($time);
    $access = $access_rule->validate($node_1, $user);

    $this->assertTrue($access, 'The user should be able to edit the node.');
  }

  /**
   * Tests weekday range when validating against different time zones.
   */
  public function weekdayRangeWithDifferentTimezone() {

    $timezone = 'America/Los_Angeles';

    $access_rule = $this->accessRuleManager->getHandler('global', 'weekday_range');
    $access_rule->setSettings([
      'weekday' => ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'],
      'start_time' => '9:00:00',
      'end_time' => '17:00:00',
      'timezone_select' => 'other',
      'timezone' => $timezone,
      'query' => FALSE,
      'operations' => ['edit'],
    ]);

    // Create a basic node.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
    ]);

    // Create a user with the same time zone.
    $user = $this->createUser([
      'access content',
    ]);
    $user->set('timezone', $timezone);
    $user->save();
    $this->setCurrentUser($user);

    $time = new DrupalDateTime('12:30:00', $timezone);
    $access_rule->setValue($time);
    $access = $access_rule->validate($node_1, $user);
    $this->assertTrue($access, 'The user should be able to edit the node.');

    // Create a user with a different time zone.
    $user = $this->createUser([
      'access content',
    ]);
    $user->set('timezone', 'UTC');
    $user->save();

    // The user should only be able to access it as the same time zone as los
    // angeles.
    $this->setCurrentUser($user);
    // This will use the same timezone as the user. 18:00:00 UTC is 11:00 AM LA.
    $time = new DrupalDateTime('18:00:00', 'UTC');
    $access_rule->setValue($time);
    $access = $access_rule->validate($node_1, $user);
    $this->assertTrue($access, 'The user should be able to edit the node.');

    // Set the current time to 12:00 UTC (5:00 AM LA, not allowed).
    $time = new DrupalDateTime('12:00:00', 'UTC');
    $access_rule->setValue($time);
    $access = $access_rule->validate($node_1, $user);
    $this->assertFalse($access, 'The user should not be able to edit the node.');
  }

}
