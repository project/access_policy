<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests ModerationStateTest access rule plugin.
 *
 * @group access_policy
 */
class ModerationStateTest extends AccessPolicyKernelTestBase {

  use ContentModerationTestTrait;
  use TaxonomyTestTrait;
  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'system',
    'text',
    'user',
    'datetime',
    'content_moderation',
    'workflows',
    'access_policy',
    'access_policy_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);
    $this->installEntitySchema('content_moderation_state');
    $this->installConfig(['content_moderation', 'system']);

    // Articles are not using content moderation.
    NodeType::create([
      'type' => 'article',
      'name' => 'Article',
    ])->save();

    $nodeType = NodeType::load('page');
    $nodeType->setNewRevision(TRUE);
    $nodeType->save();

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'page');
    $workflow->save();
  }

  /**
   * Tests moderation state access rule.
   *
   * @dataProvider providerModerationState
   */
  public function testModerationState($settings, $moderation_state, $expected, $expected_query) {
    $policy = AccessPolicy::create([
      'id' => 'archived',
      'label' => 'Archived policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();
    $access_rule = $this->accessRuleManager->getHandler('node', 'moderation_state');
    $access_rule->setSettings($settings);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Create a basic node.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'moderation_state' => $moderation_state,
      'access_policy' => ['archived'],
    ]);

    $user = $this->createUser([
      'access content',
      'view archived content',
      'view any archived unpublished content',
    ]);

    $user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $user, 'view');
    $this->assertEquals($expected, $access);

    $this->setCurrentUser($user);
    $this->assertQueryResults('node', $expected_query);
  }

  /**
   * Provides access rule settings.
   */
  public function providerModerationState() {
    return [
      'moderation state is archived is true' => [
        [
          'operator' => 'or',
          'value' => ['editorial-archived' => 'editorial-archived'],
        ],
        'archived',
        TRUE,
        [1],
      ],
      'moderation state is archived is false' => [
        [
          'operator' => 'or',
          'value' => ['editorial-archived' => 'editorial-archived'],
        ],
        'draft',
        FALSE,
        [],
      ],
      'moderation state is not archived is true' => [
        [
          'operator' => 'not',
          'value' => ['editorial-archived' => 'editorial-archived'],
        ],
        'draft',
        TRUE,
        [1],
      ],
      'moderation state is not archived is false' => [
        [
          'operator' => 'not',
          'value' => ['editorial-archived' => 'editorial-archived'],
        ],
        'archived',
        FALSE,
        [],
      ],
    ];
  }

}
