<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests EntityField base plugin features.
 *
 * @group access_policy
 */
class EntityFieldStringTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);
  }

  /**
   * Tests entity field string equals.
   *
   * @dataProvider providerEntityFieldString
   */
  public function testEntityFieldString($settings, $value, $expected, $expected_query) {
    $policy = AccessPolicy::create([
      'id' => 'string_equals',
      'label' => 'Access policy string equals',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Add an equals operator.
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_text');
    $access_rule->setSettings($settings);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_text' => [
        $value,
      ],
      'access_policy' => ['string_equals'],
    ]);

    $admin_user = $this->createUser([
      'access content',
      'view string_equals content',
    ]);
    $admin_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $admin_user, 'view');
    $this->assertEquals($expected, $access);

    $this->setCurrentUser($admin_user);
    $this->assertQueryResults('node', $expected_query);
  }

  /**
   * Provides access rule settings.
   */
  public function providerEntityFieldString() {
    return [
      'string equals is true' => [
        ['operator' => '=', 'value' => 'Foo'],
        'Foo',
        TRUE,
        [1],
      ],
      'string equals with two values is true' => [
        ['operator' => '=', 'value' => 'Foo'],
        ['Foo', 'Bar'],
        TRUE,
        [1],
      ],
      'string not equals is true' => [
        ['operator' => '!=', 'value' => 'Bar'],
        'Bar',
        FALSE,
        [],
      ],
      'string not equals is false' => [
        ['operator' => '!=', 'value' => 'Bar'],
        'Foo',
        TRUE,
        [1],
      ],
      'string contains is true' => [
        ['operator' => 'contains', 'value' => 'Foo'],
        'Foo',
        TRUE,
        [1],
      ],
      'string contains is false' => [
        ['operator' => 'contains', 'value' => 'Bar'],
        'Foo',
        FALSE,
        [],
      ],
      'string not contains is true' => [
        ['operator' => 'not contains', 'value' => 'Bar'],
        'Foo',
        TRUE,
        [1],
      ],
      'string not contains is false' => [
        ['operator' => 'not contains', 'value' => 'Foo'],
        'Foo',
        FALSE,
        [],
      ],
      'string starts with is true' => [
        ['operator' => 'starts with', 'value' => 'Foo'],
        'Foo with text',
        TRUE,
        [1],
      ],
      'string starts with is false' => [
        ['operator' => 'starts with', 'value' => 'Foo'],
        'Bar with text',
        FALSE,
        [],
      ],
      'string not starts with is true' => [
        ['operator' => 'not starts with', 'value' => 'Bar'],
        'Foo with text',
        TRUE,
        [1],
      ],
      'string not starts with is false' => [
        ['operator' => 'not starts with', 'value' => 'Bar'],
        'Bar with text',
        FALSE,
        [],
      ],
      'string ends with is true' => [
        ['operator' => 'ends with', 'value' => 'Foo'],
        'Text with Foo',
        TRUE,
        [1],
      ],
      'string ends with is false' => [
        ['operator' => 'ends with', 'value' => 'Foo'],
        'Text without bar',
        FALSE,
        [],
      ],
      'string not ends with is true' => [
        ['operator' => 'not ends with', 'value' => 'Foo'],
        'Text without bar',
        TRUE,
        [1],
      ],
      'string not ends with is false' => [
        ['operator' => 'not ends with', 'value' => 'Foo'],
        'Text without Foo',
        FALSE,
        [],
      ],
    ];
  }

}
