<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\link\LinkItemInterface;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests EntityField boolean plugin features.
 *
 * @group access_policy
 */
class EntityFieldEmptyTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
    'link',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);

    // The link field itself doesn't make much sense in terms of access so we
    // just check to see whether it's empty or not.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_link',
      'entity_type' => 'node',
      'type' => 'link',
      'settings' => [],
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'page',
      'settings' => [
        'title' => DRUPAL_OPTIONAL,
        'link_type' => LinkItemInterface::LINK_GENERIC,
      ],
    ]);
    $field->save();
  }

  /**
   * Tests entity field standard equals.
   *
   * @dataProvider providerEntityFieldEmpty
   */
  public function testEntityFieldEmpty($settings, $value, $expected, $expected_query) {

    $policy = AccessPolicy::create([
      'id' => 'policy_basic',
      'label' => 'Basic policy',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Add an equals operator.
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_link');
    $access_rule->setSettings($settings);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Node 1 has the link and should be accessible.
    $node_1 = $this->createNode([
      'nid' => 1,
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_link' => $value,
      'access_policy' => ['policy_basic'],
    ]);

    $admin_user = $this->createUser([
      'access content',
      'view policy_basic content',
    ]);
    $admin_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $admin_user, 'view');
    $this->assertEquals($expected, $access);

    $this->setCurrentUser($admin_user);
    $this->assertQueryResults('node', $expected_query);
  }

  /**
   * Provides access rule operators.
   */
  public function providerEntityFieldEmpty() {
    return [
      'field is not empty is true' => [
        ['operator' => 'not empty'],
        'https://foo.org/',
        TRUE,
        [1],
      ],
      'field is not empty is false' => [
        ['operator' => 'not empty'],
        '',
        FALSE,
        [],
      ],
      'field is empty is true' => [
        ['operator' => 'empty'],
        '',
        TRUE,
        [1],
      ],
      'field is empty is false' => [
        ['operator' => 'empty'],
        'https://foo.org/',
        FALSE,
        [],
      ],
    ];
  }

}
