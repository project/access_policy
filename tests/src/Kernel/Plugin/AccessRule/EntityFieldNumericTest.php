<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests EntityField base plugin features.
 *
 * @group access_policy
 */
class EntityFieldNumericTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);

    // Add an integer field to the node.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_integer',
      'entity_type' => 'node',
      'type' => 'integer',
      'cardinality' => -1,
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_integer',
      'bundle' => 'page',
      'translatable' => FALSE,
    ]);
    $field->save();
  }

  /**
   * Tests the numeric entity field access rule.
   *
   * @dataProvider providerEntityFieldNumeric
   */
  public function testEntityFieldNumeric($settings, $value, $expected, $expected_query) {

    $policy = AccessPolicy::create([
      'id' => 'numeric',
      'label' => 'Access policy with numeric values',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Add a greater than operator.
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_integer');
    $access_rule->setSettings($settings);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // 75 is between 50 and 100 and should be visible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_integer' => [
        $value,
      ],
      'access_policy' => ['numeric'],
    ]);

    $admin_user = $this->createUser([
      'access content',
      'view numeric content',
    ]);
    $admin_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $admin_user, 'view');
    $this->assertEquals($expected, $access);

    $this->setCurrentUser($admin_user);
    $this->assertQueryResults('node', $expected_query);
  }

  /**
   * Provides access rule operators.
   */
  public function providerEntityFieldNumeric() {
    return [
      'integer is equal to is true' => [
        ['operator' => '=', 'value' => 50],
        50,
        TRUE,
        [1],
      ],
      'integer is not equal to is true' => [
        ['operator' => '!=', 'value' => 50],
        75,
        TRUE,
        [1],
      ],
      'integer greater than is true' => [
        ['operator' => '>', 'value' => 50],
        75,
        TRUE,
        [1],
      ],
      'integer greater than is false' => [
        ['operator' => '>', 'value' => 50],
        25,
        FALSE,
        [],
      ],
      'integer greater than or equal to is true' => [
        ['operator' => '>=', 'value' => 50],
        50,
        TRUE,
        [1],
      ],
      'integer less than is true' => [
        ['operator' => '<', 'value' => 50],
        25,
        TRUE,
        [1],
      ],
      'integer less than is false' => [
        ['operator' => '<', 'value' => 50],
        75,
        FALSE,
        [],
      ],
      'integer less than or equal to is true' => [
        ['operator' => '<=', 'value' => 50],
        50,
        TRUE,
        [1],
      ],
    ];
  }

  /**
   * Tests a node with numeric field values between values.
   */
  public function testEntityFieldNumericWithMultipleRules() {
    $policy = AccessPolicy::create([
      'id' => 'integer_between',
      'label' => 'Access policy with between values.',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Add a less than operator.
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_integer');
    $access_rule->setSettings([
      'value' => 100,
      'operator' => '<',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Add a greater than operator.
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_integer');
    $access_rule->setSettings([
      'value' => 50,
      'operator' => '>',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Add a greater than operator.
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_integer');
    $access_rule->setSettings([
      'value' => 101,
      'operator' => '!=',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // 75 is between 50 and 100 and should be visible.
    $node_1 = $this->createNode([
      'title' => 'Node 1 (With int: 75)',
      'type' => 'page',
      'status' => 1,
      'field_integer' => [
        75,
      ],
      'access_policy' => ['integer_between'],
    ]);

    // 25 is less than 50 and should not be visible.
    $node_2 = $this->createNode([
      'title' => 'Node 2 (With int: 25)',
      'type' => 'page',
      'status' => 1,
      'field_integer' => [
        25,
      ],
      'access_policy' => ['integer_between'],
    ]);

    // 125 is greater than 100 and should not be visible.
    $node_3 = $this->createNode([
      'title' => 'Node 3 (With int: 125)',
      'type' => 'page',
      'status' => 1,
      'field_integer' => [
        125,
      ],
      'access_policy' => ['integer_between'],
    ]);

    // 101 is not allowed and should not be visible.
    $node_4 = $this->createNode([
      'title' => 'Node 4 (With int: 101)',
      'type' => 'page',
      'status' => 1,
      'field_integer' => [
        101,
      ],
      'access_policy' => ['integer_between'],
    ]);

    // We're not comparing field values with a user so it's not necessary to set
    // it.
    $admin_user = $this->createUser([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view integer_between content',
    ]);
    $admin_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $admin_user, 'view');
    $this->assertTrue($access, 'The user should have access.');

    $access = $this->accessPolicyValidator->validate($node_2, $admin_user, 'view');
    $this->assertFalse($access, 'The user should not have access.');

    $access = $this->accessPolicyValidator->validate($node_3, $admin_user, 'view');
    $this->assertFalse($access, 'The user should not have access.');

    $access = $this->accessPolicyValidator->validate($node_4, $admin_user, 'view');
    $this->assertFalse($access, 'The user should not have access.');

    $this->setCurrentUser($admin_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

  }

}
