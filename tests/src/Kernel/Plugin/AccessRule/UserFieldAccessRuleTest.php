<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests entity field plugins against the current user.
 *
 * @group access_policy
 */
class UserFieldAccessRuleTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use NodeCreationTrait;
  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'taxonomy',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setup();

    $this->installConfig(['access_policy_test']);
    $this->installEntitySchema('taxonomy_term');

    // Creating a policy is necessary to register the plugins.
    $policy = AccessPolicy::create([
      'id' => 'basic_policy',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $this->accessPolicyValidator = $this->container->get('access_policy.validator');

  }

  /**
   * Tests viewing a node that is referenced by the user.
   */
  public function testViewEntityWithUserFieldEquals() {

    $policy = AccessPolicy::create([
      'id' => 'user_email_equals',
      'label' => 'Access policy with user email.',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();
    $access_rule = $this->accessRuleManager->getHandler('user', 'mail');
    $access_rule->setSettings([
      'operator' => '=',
      'value' => 'user@foo.com',
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['user_email_equals'],
    ]);

    $rid = $this->createRole([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view user_email_equals content',
    ]);

    // This user does not have a matching email and should not be granted
    // access.
    $web_user_2 = $this->createUser();
    $web_user_2->setEmail('user@bar.com');
    $web_user_2->addRole($rid);
    $web_user_2->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user_2, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user_2);
    $this->assertQueryResults('node', []);
  }

  /**
   * Tests viewing a node that is referenced by the user.
   */
  public function testUserFieldStringValueStartsWithAndNotStartsWith() {
    $policy = AccessPolicy::create([
      'id' => 'string_starts_with',
      'label' => 'Access policy starts with',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('user', 'mail');
    $access_rule->setSettings([
      'value' => 'foo',
      'operator' => 'starts with',
    ]);

    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('user', 'mail');
    $access_rule->setSettings([
      'value' => 'bar',
      'operator' => 'not starts with',
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Because this has one value that matches it should be visible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['string_starts_with'],
    ]);

    $rid = $this->createRole([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view string_starts_with content',
    ]);

    // This user has an email that starts with foo and should be granted access.
    $web_user = $this->createUser();
    $web_user->setEmail('foo@foo.com');
    $web_user->addRole($rid);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

    // This user has an email that starts with bar and should not be granted
    // access.
    $web_user = $this->createUser();
    $web_user->setEmail('bar@foo.com');
    $web_user->addRole($rid);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', []);

  }

  /**
   * Tests viewing a node that is referenced by the user.
   */
  public function testUserFieldStringValueEndsWithAndNotEndsWith() {
    $policy = AccessPolicy::create([
      'id' => 'string_ends_with',
      'label' => 'Access policy starts with',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('user', 'mail');
    $access_rule->setSettings([
      'value' => 'foo.com',
      'operator' => 'ends with',
    ]);

    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('user', 'mail');
    $access_rule->setSettings([
      'value' => 'bar.com',
      'operator' => 'not ends with',
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Because this has one value that matches it should be visible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['string_ends_with'],
    ]);

    $rid = $this->createRole([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view string_ends_with content',
    ]);

    // This user has an email that starts with foo and should be granted access.
    $web_user = $this->createUser();
    $web_user->setEmail('foo@foo.com');
    $web_user->addRole($rid);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

    // This user has an email that starts with bar and should not be granted
    // access.
    $web_user = $this->createUser();
    $web_user->setEmail('foo@bar.com');
    $web_user->addRole($rid);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', []);
  }

  /**
   * Tests boolean user field.
   */
  public function testUserFieldBooleanValue() {
    // Create the boolean field on the user.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_is_active',
      'entity_type' => 'user',
      'type' => 'boolean',
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_is_active',
      'bundle' => 'user',
      'translatable' => FALSE,
    ]);
    $field->save();

    $policy = AccessPolicy::create([
      'id' => 'user_boolean',
      'label' => 'Access policy user boolean field',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('user', 'field_is_active');
    $access_rule->setSettings([
      'value' => 'true',
      'operator' => '=',
    ]);

    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Because this has one value that matches it should be visible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['user_boolean'],
    ]);

    $rid = $this->createRole([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view user_boolean content',
    ]);

    // This user is active and should have access.
    $web_user = $this->createUser();
    $web_user->addRole($rid);
    $web_user->set('field_is_active', TRUE);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

    // This user is not active and should not have access.
    $web_user = $this->createUser();
    $web_user->addRole($rid);
    $web_user->set('field_is_active', FALSE);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', []);
  }

  /**
   * Tests a user with a numeric field value.
   */
  public function testUserFieldIntegerValueWithLessThanAndGreaterThan() {
    // Add an integer field to the node.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_integer',
      'entity_type' => 'user',
      'type' => 'integer',
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_integer',
      'bundle' => 'user',
      'translatable' => FALSE,
    ]);
    $field->save();

    $policy = AccessPolicy::create([
      'id' => 'integer_between',
      'label' => 'Access policy with between values.',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Add a less than operator.
    $access_rule = $this->accessRuleManager->getHandler('user', 'field_integer');
    $access_rule->setSettings([
      'value' => 100,
      'operator' => '<',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Add a greater than operator.
    $access_rule = $this->accessRuleManager->getHandler('user', 'field_integer');
    $access_rule->setSettings([
      'value' => 50,
      'operator' => '>',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Add a greater than operator.
    $access_rule = $this->accessRuleManager->getHandler('user', 'field_integer');
    $access_rule->setSettings([
      'value' => 101,
      'operator' => '!=',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // 75 is between 50 and 100 and should be visible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['integer_between'],
    ]);

    // User with field integer 75 should have access.
    $web_user = $this->createUser([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view integer_between content',
    ]);
    $web_user->set('field_integer', 75);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

    // Create a user with value below 25, they should not have access.
    $web_user = $this->createUser([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view integer_between content',
    ]);
    $web_user->set('field_integer', 25);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', []);
  }

  /**
   * Tests a user with a field list value.
   */
  public function testUserFieldListValueIsOneOf() {
    // Add an integer field to the node.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_rating',
      'entity_type' => 'user',
      'type' => 'list_integer',
      'cardinality' => 1,
      'settings' => [
        'allowed_values' => [
          1 => 'One',
          2 => 'Two',
          3 => 'Three',
          4 => 'Four',
          5 => 'Five',
        ],
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_rating',
      'bundle' => 'user',
      'translatable' => FALSE,
    ]);
    $field->save();

    $policy = AccessPolicy::create([
      'id' => 'list_integer_rating',
      'label' => 'Access policy with list integer values.',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('user', 'field_rating');
    $access_rule->setSettings([
      'value' => [4 => 4, 5 => 5],
      'operator' => 'or',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['list_integer_rating'],
    ]);

    // User with a rating of 4 can view the content.
    $web_user = $this->createUser([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view list_integer_rating content',
    ]);
    $web_user->set('field_rating', 4);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

    // Create a user with a rating of 3, they can't view the content.
    $web_user = $this->createUser([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view list_integer_rating content',
    ]);
    $web_user->set('field_rating', 3);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', []);
  }

  /**
   * Tests role entity reference access rule.
   */
  public function testUserFieldRoleEntityReference() {
    // Private access policies don't generate any permissions to view, so we
    // don't need to add any here.
    $this->createRole([], 'content admin');
    $this->createRole([], 'content editor');

    // Making this private since this is the scenario where it makes the most
    // sense with role-based access rules.
    $policy = AccessPolicy::create([
      'id' => 'role_entity_reference',
      'label' => 'Access policy with role entity reference',
      'target_entity_type_id' => 'node',
      'access_rule_operator' => 'OR',
      'operations' => [
        'view' => [
          'access_rules' => TRUE,
        ],
        'view_all_revisions' => [
          'access_rules' => TRUE,
        ],
        'update' => [
          'access_rules' => TRUE,
        ],
        'delete' => [
          'access_rules' => TRUE,
        ],
        'view_unpublished' => [
          'access_rules' => TRUE,
        ],
        'manage_access' => [],
      ],
    ]);

    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'is_own');
    $policy->addHandler('access_rule', $access_rule);

    $access_rule = $this->accessRuleManager->getHandler('user', 'roles');
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => [
        'content_editor' => 'content_editor',
      ],
    ]);
    $policy->addHandler('access_rule', $access_rule);

    $selection_rule = $this->selectionRuleManager->getHandler('node', 'is_own');
    $policy->addHandler('selection_rule', $selection_rule);

    $policy->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['role_entity_reference'],
    ]);

    $web_user = $this->createUser();
    $web_user->addRole('content editor');
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertEquals(TRUE, $access, 'This user should have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

    $web_user = $this->createUser();
    $web_user->addRole('content admin');
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertEquals(FALSE, $access, 'This user should not have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', []);

  }

  /**
   * Tests term entity reference access rule.
   */
  public function testUserFieldTermEntityReference() {
    // Then create some taxonomy terms that are assigned their own policy.
    $vocabulary = Vocabulary::create([
      'name' => 'department',
      'vid' => 'department',
    ]);
    $vocabulary->save();

    // Create the entity reference field on the user.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_entity_reference',
      'entity_type' => 'user',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_entity_reference',
      'bundle' => 'user',
      'translatable' => FALSE,
    ]);
    $field->save();

    // This term will be evaluated against.
    $term_foo = $this->createTerm($vocabulary, [
      'tid' => 1,
      'name' => 'Foo',
      'status' => 1,
    ]);

    $term_bar = $this->createTerm($vocabulary, [
      'tid' => 2,
      'name' => 'Bar',
      'status' => 1,
    ]);

    $policy = AccessPolicy::create([
      'id' => 'term_entity_reference',
      'label' => 'Access policy with role entity reference',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Require this user to have access to term Foo.
    $access_rule = $this->accessRuleManager->getHandler('user', 'field_entity_reference');
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => [
        ['target_id' => 1],
      ],
    ]);
    $policy->addHandler('access_rule', $access_rule);
    // They should not have term Bar.
    $access_rule->setSettings([
      'operator' => 'not in',
      'value' => [
        ['target_id' => 2],
      ],
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Create a basic node.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['term_entity_reference'],
    ]);

    // Create a user who has the taxonomy term assigned to them. They should
    // have access.
    $web_user = $this->createUser([
      'view term_entity_reference content',
    ]);
    $web_user->set('field_entity_reference', $term_foo);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertEquals(TRUE, $access, 'This user should have access');

    // Create a user with both terms assigned to them. They should not
    // have access.
    $web_user = $this->createUser([
      'view term_entity_reference content',
    ]);
    $terms = [
      $term_foo,
      $term_bar,
    ];
    $web_user->set('field_entity_reference', $terms);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertEquals(FALSE, $access, 'This user should not have access');
  }

  /**
   * Tests user field date.
   */
  public function testUserFieldDate() {
    $policy = AccessPolicy::create([
      'id' => 'policy_date',
      'label' => 'Access policy with created date',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Only grant access if they've registered within the last month.
    $access_rule = $this->accessRuleManager->getHandler('user', 'created');
    $access_rule->setSettings([
      'operator' => '>',
      'type' => 'relative',
      'value' => '-30 days',
    ]);
    $policy->addHandler('access_rule', $access_rule);
    // And only grant access if they've logged in within the last 2 days.
    $access_rule = $this->accessRuleManager->getHandler('user', 'access');
    $access_rule->setSettings([
      'operator' => '>',
      'type' => 'relative',
      'value' => '-2 days',
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // 75 is between 50 and 100 and should be visible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['policy_date'],
    ]);

    // This is an old user and should not have access.
    $web_user = $this->createUser([
      'view policy_date content',
    ]);
    $web_user->set('created', strtotime('-45 days'));
    $web_user->save();
    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertEquals(FALSE, $access, 'This user should not have access');

    $web_user = $this->createUser([
      'view policy_date content',
    ]);

    // They haven't logged in yet so they shouldn't have access.
    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertEquals(FALSE, $access, 'This user should not have access');

    // The user has logged in so they should have access.
    $web_user->setLastAccessTime(time());
    $web_user->save();
    $this->accessPolicyValidator->resetCache();
    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertEquals(TRUE, $access, 'This user should have access');
  }

}
