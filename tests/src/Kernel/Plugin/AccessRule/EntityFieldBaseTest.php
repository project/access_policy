<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests EntityField base plugin features.
 *
 * @group access_policy
 */
class EntityFieldBaseTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use OperatorValidationTrait;

  /**
   * The tags vocabulary.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $vocabulary;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'taxonomy',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);
    $this->installEntitySchema('taxonomy_term');

    // Create a tags vocabulary.
    $this->vocabulary = Vocabulary::create([
      'name' => 'tags',
      'vid' => 'tags',
    ]);
    $this->vocabulary->save();

    // Create the taxonomy term entity reference field for the node and user.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_tags',
      'entity_type' => 'user',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_tags',
      'bundle' => 'user',
      'translatable' => FALSE,
    ]);
    $field->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_tags',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_tags',
      'bundle' => 'page',
      'translatable' => FALSE,
    ]);
    $field->save();

    // Creating a policy is necessary to register the plugins.
    $policy = AccessPolicy::create([
      'id' => 'basic_policy',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();
  }

  /**
   * Tests access for entities with empty values.
   *
   * @dataProvider providerEntityFieldBaseWithEmptyValue
   */
  public function testEntityFieldBaseWithEmptyValue($settings, $value, $expected, $expected_query) {
    $policy = AccessPolicy::create([
      'id' => 'policy_empty_fields',
      'label' => 'Policy with empty fields',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Access rule from a hard coded value.
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_text');
    $access_rule->setSettings($settings);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Node 1 does not have any field values.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_text' => $value,
      'access_policy' => ['policy_empty_fields'],
    ]);

    $web_user = $this->createUser([
      'access content',
      'view policy_empty_fields content',
    ]);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertEquals($expected, $access);

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', $expected_query);
  }

  /**
   * Provides access rule settings.
   */
  public function providerEntityFieldBaseWithEmptyValue() {
    return [
      'empty behavior allowed is true' => [
        [
          'operator' => 'contains',
          'value' => 'Foo',
          'empty_behavior' => 'allow',
        ],
        '',
        TRUE,
        [1],
      ],
      'empty behavior allowed is false' => [
        ['operator' => 'contains', 'value' => 'Foo', 'empty_behavior' => 'deny'],
        '',
        FALSE,
        [],
      ],
    ];
  }

  /**
   * Tests access for entities with empty values.
   */
  public function testEntityFieldBaseWithIgnoredEmptyValue() {
    $policy = AccessPolicy::create([
      'id' => 'policy_ignored_field',
      'label' => 'Policy with ignored fields',
      'target_entity_type_id' => 'node',
      'access_rule_operator' => 'OR',
    ]);
    $policy->save();

    // The field_text field should contain Bar. If its empty though, ignore it.
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_text');
    $access_rule->setSettings([
      'value' => 'Bar',
      'operator' => 'contains',
      'query' => TRUE,
      'empty_behavior' => 'ignore',
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Node 1 does not have any field values and the title does not match.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['policy_ignored_field'],
    ]);

    $web_user = $this->createUser([
      'access content',
      'view policy_ignored_field content',
    ]);
    $web_user->save();

    // The user should have access because the field value is empty and
    // validation is ignored.
    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'This user should have access');

    $this->setCurrentUser($web_user);
    $expected = [
      $node_1->id(),
    ];
    $this->assertQueryResults('node', $expected);

    // Add an access rule. This will change the behavior of the query.
    // The title should contain Foo, this will not match.
    $access_rule = $this->accessRuleManager->getHandler('node', 'title');
    $access_rule->setSettings([
      'value' => 'Foo',
      'operator' => 'contains',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // The user should not have access because the title is set to Foo, the
    // node does not match and field_text is ignored. This means only the title
    // rule is validated.
    $this->accessPolicyValidator->resetCache();
    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertFalse($access, 'This user should not have access');

    $expected = [];
    $this->assertQueryResults('node', $expected);
  }

  /**
   * Tests access rule validation for overridden operations.
   */
  public function testEntityFieldBaseWithSpecificOperations() {
    $policy = AccessPolicy::create([
      'id' => 'policy_op_override',
      'label' => 'Policy with overridden operations',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Access rule that validates during the "update" operation. This means that
    // All other operations should pass except for edit.
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_text');
    $access_rule->setSettings([
      'value' => 'Foo',
      'operator' => '=',
      'admin_label' => '',
      'operations' => ['edit'],
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Node 1 does not have any field values.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_text' => 'Bar',
      'access_policy' => ['policy_op_override'],
    ]);

    $web_user = $this->createUser([
      'access content',
      'view policy_op_override content',
    ]);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'This user should have access');

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'edit');
    $this->assertFalse($access, 'This user should not have access');
  }

  /**
   * Tests access rule validation for multiple access policies.
   */
  public function testAssignMultipleAccessPolicies() {
    $policy = AccessPolicy::create([
      'id' => 'policy_1',
      'label' => 'Policy 1',
      'target_entity_type_id' => 'node',
      'selection_set' => ['restricted'],
    ]);
    $policy->save();

    $policy = AccessPolicy::create([
      'id' => 'policy_2',
      'label' => 'Policy 2',
      'target_entity_type_id' => 'node',
      'selection_set' => ['restricted'],
    ]);
    $policy->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_text' => 'Bar',
      'access_policy' => ['policy_1', 'policy_2'],
    ]);

    $web_user = $this->createUser([
      'access content',
      'view policy_2 content',
    ]);
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'This user should have access');

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'edit');
    $this->assertFalse($access, 'This user should not have access');
  }

  /**
   * Test error when attempting to assign access policies from different sets.
   */
  public function testAssignMultipleAccessPoliciesFromDifferentSets() {
    $policy = AccessPolicy::create([
      'id' => 'policy_1',
      'label' => 'Policy 1',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $policy = AccessPolicy::create([
      'id' => 'policy_2',
      'label' => 'Policy 1',
      'target_entity_type_id' => 'node',
      'selection_set' => ['test_set'],
    ]);
    $policy->save();

    $policy = AccessPolicy::create([
      'id' => 'policy_3',
      'label' => 'Policy 3',
      'target_entity_type_id' => 'node',
      'selection_set' => ['restricted'],
    ]);
    $policy->save();

    // Node 1 does not have any field values.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_text' => 'Bar',
    ]);

    // It shouldn't be possible to assign access policies with mismatched sets.
    $node_1->set('access_policy', ['policy_1', 'policy_2']);
    $this->assertEmpty($node_1->get('access_policy')->isEmpty(), 'The policies should not have been assigned');

    $node_1->set('access_policy', ['policy_2', 'policy_3']);
    $this->assertEmpty($node_1->get('access_policy')->isEmpty(), 'The policies should not have been assigned');

  }

  /**
   * Basic test for testing access_policy_data_alter.
   */
  public function testAccessPolicyDataAlter() {
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_text');

    // Check the access rule description, confirm that hook_access_rule_alter()
    // works.
    // @see access_policy_test.module.
    $description = $access_rule->getDefinition()->getDescription();
    $this->assertEquals('Foo bar', $description);
  }

}
