<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests EntityField boolean plugin features.
 *
 * @group access_policy
 */
class EntityFieldBooleanTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_boolean',
      'entity_type' => 'node',
      'type' => 'boolean',
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_name' => 'field_boolean',
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => "boolean",
      'required' => TRUE,
      'settings' => [
        'on_label' => 'On',
        'off_label' => 'Off',
      ],
    ]);
    $field->save();
  }

  /**
   * Tests entity field standard equals.
   *
   * @dataProvider providerEntityFieldBoolean
   */
  public function testEntityFieldBoolean($settings, $value, $expected, $results) {

    $policy = AccessPolicy::create([
      'id' => 'boolean',
      'label' => 'Access policy boolean',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    // Add an equals operator.
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_boolean');
    $access_rule->setSettings($settings);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Node 1 has foo and should be accessible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_boolean' => $value,
      'access_policy' => ['boolean'],
    ]);

    // We're not comparing field values with a user so it's not necessary to set
    // it.
    $admin_user = $this->createUser([
      'access content',
      'view boolean content',
    ]);
    $admin_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $admin_user, 'view');
    $this->assertEquals($expected, $access);

    $this->setCurrentUser($admin_user);
    $this->assertQueryResults('node', $results);
  }

  /**
   * Provides access rule settings.
   */
  public function providerEntityFieldBoolean() {
    return [
      'true operator' => [
        ['value' => 'true', 'operator' => '='],
        TRUE,
        TRUE,
        [1],
      ],
      'not true operator' => [
        ['value' => 'true', 'operator' => '!='],
        FALSE,
        TRUE,
        [1],
      ],
      'false operator' => [
        ['value' => 'false', 'operator' => '='],
        FALSE,
        TRUE,
        [1],
      ],
      'not false operator' => [
        ['value' => 'false', 'operator' => '!='],
        TRUE,
        TRUE,
        [1],
      ],
    ];
  }

}
