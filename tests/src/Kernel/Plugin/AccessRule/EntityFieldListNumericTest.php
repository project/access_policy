<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests EntityFieldListNumeric plugin features.
 *
 * @group access_policy
 */
class EntityFieldListNumericTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use OperatorValidationTrait;

  /**
   * The tags vocabulary.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $vocabulary;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);

    FieldStorageConfig::create([
      'field_name' => 'field_list_integer',
      'entity_type' => 'node',
      'type' => 'list_integer',
      'cardinality' => 1,
      'settings' => [
        'allowed_values' => [
          0 => 'Public',
          1 => 'Confidential',
          2 => 'Secret',
        ],
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_list_integer',
      'entity_type' => 'node',
      'label' => 'Security level',
      'bundle' => 'page',
    ])->save();

    FieldStorageConfig::create([
      'field_name' => 'field_list_integer',
      'entity_type' => 'user',
      'type' => 'list_integer',
      'cardinality' => 1,
      'settings' => [
        'allowed_values' => [
          0 => 'Public',
          1 => 'Confidential',
          2 => 'Secret',
        ],
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_list_integer',
      'entity_type' => 'user',
      'label' => 'Security clearance',
      'bundle' => 'user',
    ])->save();

    // Creating a policy is necessary to register the plugins.
    $policy = AccessPolicy::create([
      'id' => 'security',
      'label' => 'Security level',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();
  }

  /**
   * Tests numeric field lists with numeric operators.
   *
   * @dataProvider providerFieldListNumeric
   */
  public function testEntityFieldListNumeric($handler, $settings, $values, $expected, $expected_query) {
    $policy = $this->entityTypeManager->getStorage('access_policy')->load('security');
    $access_rule = $this->accessRuleManager->getHandler('node', $handler);
    $access_rule->setSettings($settings);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Create a basic node.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_list_integer' => $values['node'],
      'access_policy' => ['security'],
    ]);

    $user = $this->createUser([
      'access content',
      'view security content',
    ]);

    if (isset($values['user'])) {
      $user->set('field_list_integer', $values['user']);
      $user->save();
    }

    $access = $this->accessPolicyValidator->validate($node_1, $user, 'view');
    $this->assertEquals($expected, $access);

    $this->setCurrentUser($user);
    $this->assertQueryResults('node', $expected_query);

  }

  /**
   * Provides numeric field list data.
   */
  public function providerFieldListNumeric() {
    return [
      'integer value is greater than is true' => [
        'numeric_field_list_integer',
        ['operator' => '>', 'value' => '1'],
        ['node' => [2]],
        TRUE,
        [1],
      ],
      'integer value is greater than is false' => [
        'numeric_field_list_integer',
        ['operator' => '>', 'value' => '1'],
        ['node' => [0]],
        FALSE,
        [],
      ],
      'compare with user integer value <= is true' => [
        'numeric_match_field_list_integer',
        ['operator' => '<=', 'value' => ['field' => 'field_list_integer']],
        [
          'node' => [2],
          'user' => [2],
        ],
        TRUE,
        [1],
      ],
      'compare with user integer value <= is not true' => [
        'numeric_match_field_list_integer',
        ['operator' => '<=', 'value' => ['field' => 'field_list_integer']],
        [
          'node' => [2],
          'user' => [1],
        ],
        FALSE,
        [],
      ],
    ];
  }

}
