<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\AccessRule\Broken;
use Drupal\access_policy\Plugin\access_policy\AccessRule\EntityFieldEntityReference;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests FieldMatchEntityReference plugin features.
 *
 * @group access_policy
 */
class FieldMatchEntityReferenceTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;
  use NodeCreationTrait;
  use OperatorValidationTrait;

  /**
   * The tags vocabulary.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $vocabulary;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'taxonomy',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);
    $this->installEntitySchema('taxonomy_term');

    // Create a tags vocabulary.
    $this->vocabulary = Vocabulary::create([
      'name' => 'tags',
      'vid' => 'tags',
    ]);
    $this->vocabulary->save();

    // Create the taxonomy term entity reference field for the node and user.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_tags',
      'entity_type' => 'user',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_tags',
      'bundle' => 'user',
      'translatable' => FALSE,
      'settings' => [
        'handler_settings' => [
          'target_bundles' => ['tags' => 'tags'],
        ],
      ],
    ]);
    $field->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_tags',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_tags',
      'bundle' => 'page',
      'translatable' => FALSE,
      'settings' => [
        'handler_settings' => [
          'target_bundles' => ['tags' => 'tags'],
        ],
      ],
    ]);
    $field->save();

    // Creating a policy is necessary to register the plugins.
    $policy = AccessPolicy::create([
      'id' => 'basic_policy',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

  }

  /**
   * This tests which plugins are available for entity reference fields.
   */
  public function testFieldMatchEntityReferenceDiscovery() {

    // The bundles match so it should be discovered.
    $access_rule = $this->accessRuleManager->getHandler('node', 'match_field_tags');
    $this->assertInstanceOf(EntityFieldEntityReference::class, $access_rule);

    $department = Vocabulary::create([
      'name' => 'department',
      'vid' => 'department',
    ]);
    $department->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_department',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_department',
      'bundle' => 'page',
      'translatable' => FALSE,
      'settings' => [
        'handler_settings' => [
          'target_bundles' => ['department' => 'department'],
        ],
      ],
    ]);
    $field->save();

    // This field does not have matching bundles so it shouldn't be discovered.
    $access_rule = $this->accessRuleManager->getHandler('node', 'match_field_department');
    $this->assertInstanceOf(Broken::class, $access_rule);

    // Confirm that it can find handlers for fields configured with views
    // reference method.
    $this->setupVocabularyWithFields('group', 'field_group', 'views', [
      'view' => [
        'view_name' => 'group_view',
        'display_name' => 'group_view_1',
      ],
    ]);
    $access_rule = $this->accessRuleManager->getHandler('node', 'match_field_group');
    $this->assertInstanceOf(EntityFieldEntityReference::class, $access_rule, 'The handler was not found for term reference with view reference.');

  }

  /**
   * Tests the field value access rule with not in operator.
   */
  public function testFieldMatchEntityReference() {
    // Test entity reference fields.
    $term_1 = $this->createTerm($this->vocabulary, [
      'title' => 'Term 1',
    ]);
    $term_1->save();

    $term_2 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 2',
    ]);
    $term_2->save();

    $policy = AccessPolicy::create([
      'id' => 'match_reference_value',
      'label' => 'Matching entity reference between node and user',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'match_field_tags');
    $this->assertInstanceOf(EntityFieldEntityReference::class, $access_rule);

    $access_rule->setSettings([
      'operator' => 'in',
      'value' => ['field' => 'field_tags'],
      // This should work with both TRUE and 1.
      // Do not change, this provides coverage for a rare bug.
      'query' => 1,
    ]);

    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // This is referencing term 1 and should  visible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_tags' => [
        $term_1,
      ],
      'access_policy' => ['match_reference_value'],
    ]);

    // This is referencing term 2 and should not be visible.
    $node_2 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_tags' => [
        $term_2,
      ],
      'access_policy' => ['match_reference_value'],
    ]);

    $web_user = $this->createUser([
      'access content',
      'view match_reference_value content',
    ], NULL, FALSE, ['field_tags' => $term_1]);

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $access = $this->accessPolicyValidator->validate($node_2, $web_user, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $result = $node_2->access('view', $web_user, TRUE);
    // Verify the cache contexts and cache tags.
    $contexts = $result->getCacheContexts();
    $this->assertEquals(['user.field_values', 'user.permissions'], $contexts);

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);
  }

  /**
   * Tests that user has access if they have a reference to the original author.
   *
   * @dataProvider providerCurrentUserHasReferenceToOriginalAuthor
   */
  public function testCurrentUserHasReferenceToOriginalAuthor($has_reference, $expected) {
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_teacher',
      'entity_type' => 'user',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'user',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_teacher',
      'bundle' => 'user',
      'translatable' => FALSE,
    ]);
    $field->save();

    $policy = $this->createAccessPolicy('author_reference', [
      'target_entity_type_id' => 'node',
    ]);

    $access_rule = $this->accessRuleManager->getHandler('node', 'author_reference');
    $access_rule->setSettings([
      'value' => ['field' => 'field_teacher'],
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $teacher = $this->createUser();
    $teacher->setUsername('author');
    $teacher->save();

    $node_1 = $this->createNode([
      'title' => 'Node authored by teacher',
      'uid' => $teacher->id(),
      'access_policy' => ['author_reference'],
    ]);

    $student = $this->createUser([
      'view author_reference content',
    ]);

    if ($has_reference) {
      $student->set('field_teacher', $teacher);
    }
    $student->save();

    $access = $this->accessPolicyValidator->validate($node_1, $student, 'view');
    $this->assertEquals($expected['access'], $access);

    $this->setCurrentUser($student);
    $this->assertQueryResults('node', $expected['query_results']);
  }

  /**
   * Data provider for current user has reference to original author.
   */
  public function providerCurrentUserHasReferenceToOriginalAuthor() {
    return [
      'user has reference to author' => [
        'has_reference_to_author' => TRUE,
        'expected' => [
          'access' => TRUE,
          'query_results' => [1],
        ],
      ],
      'user does not have reference to author' => [
        'has_reference_to_author' => FALSE,
        'expected' => [
          'access' => FALSE,
          'query_results' => [],
        ],
      ],
    ];
  }

  /**
   * Tests that a user has access to a content if referencing it directly.
   *
   * @dataProvider providerCurrentUserHasReferenceToContentItem
   */
  public function testCurrentUserHasReferenceToContentItem($has_reference, $expected) {
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_node',
      'entity_type' => 'user',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'node',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_node',
      'bundle' => 'user',
      'translatable' => FALSE,
    ]);
    $field->save();

    $policy = $this->createAccessPolicy('node_reference', [
      'target_entity_type_id' => 'node',
    ]);

    $access_rule = $this->accessRuleManager->getHandler('node', 'entity_reference');
    $access_rule->setSettings([
      'value' => ['field' => 'field_node'],
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'access_policy' => ['node_reference'],
    ]);

    $web_user = $this->createUser([
      'view node_reference content',
    ]);

    if ($has_reference) {
      $web_user->set('field_node', [
        $node_1,
      ]);
    }
    $web_user->save();

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertEquals($expected['access'], $access);

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', $expected['query_results']);
  }

  /**
   * Data provider for current user has reference to content item.
   */
  public function providerCurrentUserHasReferenceToContentItem() {
    return [
      'user has reference to content item' => [
        'has_reference_to_content_item' => TRUE,
        'expected' => [
          'access' => TRUE,
          'query_results' => [1],
        ],
      ],
      'user does not have reference to content item' => [
        'has_reference_to_content_item' => FALSE,
        'expected' => [
          'access' => FALSE,
          'query_results' => [],
        ],
      ],
    ];
  }

  /**
   * Tests that a user has access to a content if referencing it directly.
   */
  public function testContentItemHasReferenceToCurrentUser() {
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_user',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'user',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_user',
      'bundle' => 'page',
      'translatable' => FALSE,
    ]);
    $field->save();

    $policy = $this->createAccessPolicy('user_reference', [
      'target_entity_type_id' => 'node',
    ]);

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_user_user_reference');
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $web_user = $this->createUser([
      'view user_reference content',
    ]);
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'field_user' => [
        $web_user,
      ],
      'access_policy' => ['user_reference'],
    ]);

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'The user should have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_1->id(),
    ]);

    $web_user_2 = $this->createUser([
      'view user_reference content',
    ]);
    $access = $this->accessPolicyValidator->validate($node_1, $web_user_2, 'view');
    $this->assertFalse($access, 'The user should not have access');

    $this->setCurrentUser($web_user_2);
    $this->assertQueryResults('node', []);
  }

  /**
   * Tests entity reference field with multiple access rules.
   *
   * @dataProvider providerFieldMatchEntityReferenceWithMultiplePolicies
   */
  public function testFieldMatchEntityReferenceWithMultiplePolicies($nodes, $user, $expected) {

    $policy = AccessPolicy::create([
      'id' => 'policy_tags',
      'label' => 'Basic policy with tags',
      'target_entity_type_id' => 'node',
      'selection_set' => ['restricted'],
    ]);
    $policy->save();

    $this->createTerm($this->vocabulary, [
      'name' => 'tag1',
    ]);
    $this->createTerm($this->vocabulary, [
      'name' => 'tag2',
    ]);

    $access_rule = $this->accessRuleManager->getHandler('node', 'match_field_tags');
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => ['field' => 'field_tags'],
      'empty_behavior' => 'ignore',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $department_vocab = $this->setupVocabularyWithFields('department', 'field_department');
    $this->createTerm($department_vocab, [
      'name' => 'department1',
    ]);
    $this->createTerm($department_vocab, [
      'name' => 'department2',
    ]);

    $policy = AccessPolicy::create([
      'id' => 'policy_department',
      'label' => 'Basic policy with department',
      'target_entity_type_id' => 'node',
      'selection_set' => ['restricted'],
    ]);
    $access_rule = $this->accessRuleManager->getHandler('node', 'match_field_department');
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => ['field' => 'field_department'],
      'empty_behavior' => 'ignore',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    foreach ($nodes as $label => $data) {
      $data += [
        'title' => $label,
        'type' => 'page',
        'status' => 1,
      ];
      // Prepare the taxonomy term fields.
      $term_fields = ['field_tags', 'field_department'];
      foreach ($term_fields as $field_name) {
        if (isset($data[$field_name])) {
          foreach ($data[$field_name] as $delta => $term_name) {
            $data[$field_name][$delta] = $this->getTermByName($term_name);
          }
        }
      }

      $node = $this->createNode($data);

      if (isset($data['access_policy'])) {
        $node->set('access_policy', $data['access_policy']);
      }
    }

    // This user should have access to node_1 and node_3.
    $web_user = $this->createUser($user['permissions']);
    foreach ($user as $field_name => $term_names) {
      $values = [];
      if ($web_user->hasField($field_name)) {
        foreach ($term_names as $name) {
          $values[] = $this->getTermByName($name);
        }
        $web_user->set($field_name, $values);
      }
    }
    $web_user->save();

    $expected_query_results = [];
    foreach ($expected as $node_name => $expected_access) {
      $node = $this->getNodeByTitle($node_name);
      $access = $this->accessPolicyValidator->validate($node, $web_user, 'view');
      $this->assertEquals($expected_access, $access, 'Access test failed for ' . $node_name);
      if ($expected_access) {
        $expected_query_results[] = $node->id();
      }
    }
    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', $expected_query_results);

  }

  /**
   * Get taxonomy term by name.
   *
   * @param string $name
   *   The term name.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The taxonomy term entity or NULL if not found.
   */
  protected function getTermByName($name) {
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties(['name' => $name]);
    if (!empty($terms)) {
      $term = reset($terms);
      return $term->id();
    }
  }

  /**
   * Data provider for field match entity reference with multiple policies.
   */
  public function providerFieldMatchEntityReferenceWithMultiplePolicies() {
    return [
      'user has access to tag terms' => [
        'nodes' => [
          'node1' => [
            'field_tags' => ['tag1', 'tag2'],
            'access_policy' => ['policy_tags', 'policy_department'],
          ],
          'node2' => [
            'field_tags' => ['tag2'],
            'field_department' => ['department2'],
            'access_policy' => ['policy_tags', 'policy_department'],
          ],
          'node3' => [],
        ],
        'user' => [
          'permissions' => [
            'view policy_tags content',
            'view policy_department content',
          ],
          'field_tags' => ['tag1'],
        ],
        'expected' => [
          'node1' => TRUE,
          'node2' => FALSE,
          'node3' => TRUE,
        ],
      ],
      'user has access to department terms' => [
        'nodes' => [
          'node1' => [
            'field_tags' => ['tag1', 'tag2'],
            'field_department' => ['department2'],
            'access_policy' => ['policy_tags', 'policy_department'],
          ],
          'node2' => [
            'field_tags' => ['tag2'],
            'field_department' => ['department2'],
            'access_policy' => ['policy_tags', 'policy_department'],
          ],
          'node3' => [],
        ],
        'user' => [
          'permissions' => [
            'view policy_tags content',
            'view policy_department content',
          ],
          'field_department' => ['department1'],
        ],
        'expected' => [
          'node1' => FALSE,
          'node2' => FALSE,
          'node3' => TRUE,
        ],
      ],
    ];
  }

  /**
   * Tests entity reference field with multiple user fields.
   *
   * This tests an edge case bug where multiple access rules are set to ignore
   * but the user has no field values. Content was appearing in queries when it
   * should not.
   *
   * @dataProvider providerFieldMatchEntityReferenceWithMultipleFields
   */
  public function testFieldMatchEntityReferenceWithMultipleUserFields($nodes, $user, $expected) {
    $department_vocab = $this->setupVocabularyWithFields('department', 'field_department');

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_department_2',
      'entity_type' => 'user',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_department_2',
      'bundle' => 'user',
      'translatable' => FALSE,
      'settings' => [
        'handler_settings' => [
          'target_bundles' => ['department' => 'department'],
        ],
      ],
    ]);
    $field->save();

    $this->createTerm($department_vocab, [
      'name' => 'department1',
    ]);
    $this->createTerm($department_vocab, [
      'name' => 'department2',
    ]);

    $policy = AccessPolicy::create([
      'id' => 'policy_department',
      'label' => 'Basic policy with department',
      'target_entity_type_id' => 'node',
      'access_rule_operator' => 'OR',
    ]);
    $access_rule = $this->accessRuleManager->getHandler('node', 'match_field_department');
    $this->assertInstanceOf(EntityFieldEntityReference::class, $access_rule);
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => ['field' => 'field_department'],
      'empty_behavior' => 'ignore',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);

    $access_rule = $this->accessRuleManager->getHandler('node', 'match_field_department');
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => ['field' => 'field_department_2'],
      'empty_behavior' => 'ignore',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);

    $policy->save();

    foreach ($nodes as $label => $data) {
      $data += [
        'title' => $label,
        'type' => 'page',
        'status' => 1,
      ];
      // Prepare the taxonomy term fields.
      if (isset($data['field_department'])) {
        foreach ($data['field_department'] as $delta => $term_name) {
          $data['field_department'][$delta] = $this->getTermByName($term_name);
        }
      }

      $node = $this->createNode($data);

      if (isset($data['access_policy'])) {
        $node->set('access_policy', $data['access_policy']);
      }
    }

    // This user should have access to node_1 and node_3.
    $web_user = $this->createUser($user['permissions']);
    foreach ($user as $field_name => $term_names) {
      $values = [];
      if ($web_user->hasField($field_name)) {
        foreach ($term_names as $name) {
          $values[] = $this->getTermByName($name);
        }
        $web_user->set($field_name, $values);
      }
    }
    $web_user->save();

    $expected_query_results = [];
    foreach ($expected as $node_name => $expected_access) {
      $node = $this->getNodeByTitle($node_name);
      $access = $this->accessPolicyValidator->validate($node, $web_user, 'view');
      $this->assertEquals($expected_access, $access, 'Access test failed for ' . $node_name);
      if ($expected_access) {
        $expected_query_results[] = $node->id();
      }
    }
    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', $expected_query_results);

  }

  /**
   * Data provider for field match entity reference with multiple fields.
   */
  public function providerFieldMatchEntityReferenceWithMultipleFields() {
    return [
      'user has values in department fields.' => [
        'nodes' => [
          'node1' => [
            'field_department' => ['department2'],
            'access_policy' => ['policy_department'],
          ],
          'node2' => [
            'field_department' => ['department1'],
            'access_policy' => ['policy_department'],
          ],
          // @todo re-enable this.
          /*'node3' => [
            'access_policy' => ['policy_department'],
          ],*/
          'node4' => [],
        ],
        'user' => [
          'permissions' => [
            'view policy_department content',
          ],
          'field_department' => ['department2'],
        ],
        'expected' => [
          'node1' => TRUE,
          'node2' => FALSE,
          'node4' => TRUE,
        ],
      ],
      'user has missing department fields' => [
        'nodes' => [
          'node1' => [
            'field_department' => ['department2'],
            'access_policy' => ['policy_department'],
          ],
          'node2' => [
            'field_department' => ['department1'],
            'access_policy' => ['policy_department'],
          ],
          'node3' => [
            'access_policy' => ['policy_department'],
          ],
          'node4' => [],
        ],
        'user' => [
          'permissions' => [
            'view policy_department content',
          ],
        ],
        'expected' => [
          'node1' => FALSE,
          'node2' => FALSE,
          'node3' => TRUE,
          'node4' => TRUE,
        ],
      ],
    ];
  }

  /**
   * Tests entity reference field with ignored field.
   *
   * This tests a use case where content has a value in one field and no value
   * in another and is ignored. This addresses a bug where content could appear
   * in search results when it shouldn't.
   *
   * @dataProvider providerFieldMatchEntityReferenceWithIgnoredField
   */
  public function testFieldMatchEntityReferenceWithIgnoredField($nodes, $user, $expected) {
    $department_vocab = $this->setupVocabularyWithFields('department', 'field_department');

    $this->createTerm($department_vocab, [
      'name' => 'department1',
    ]);
    $this->createTerm($department_vocab, [
      'name' => 'department2',
    ]);
    $this->createTerm($this->vocabulary, [
      'name' => 'tag1',
    ]);

    $policy = AccessPolicy::create([
      'id' => 'policy_department',
      'label' => 'Basic policy with department',
      'target_entity_type_id' => 'node',
      'access_rule_operator' => 'OR',
    ]);
    $access_rule = $this->accessRuleManager->getHandler('node', 'match_field_department');
    $this->assertInstanceOf(EntityFieldEntityReference::class, $access_rule);
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => ['field' => 'field_department'],
      'empty_behavior' => 'deny',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);

    // Tags should be ignored if there is no value.
    $access_rule = $this->accessRuleManager->getHandler('node', 'match_field_tags');
    $access_rule->setSettings([
      'operator' => 'in',
      'value' => ['field' => 'field_tags'],
      'empty_behavior' => 'ignore',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);

    $policy->save();

    foreach ($nodes as $label => $data) {
      $data += [
        'title' => $label,
        'type' => 'page',
        'status' => 1,
      ];
      // Prepare the taxonomy term fields.
      if (isset($data['field_department'])) {
        foreach ($data['field_department'] as $delta => $term_name) {
          $data['field_department'][$delta] = $this->getTermByName($term_name);
        }
      }

      if (isset($data['field_tags'])) {
        foreach ($data['field_tags'] as $delta => $term_name) {
          $data['field_tags'][$delta] = $this->getTermByName($term_name);
        }
      }

      $node = $this->createNode($data);

      if (isset($data['access_policy'])) {
        $node->set('access_policy', $data['access_policy']);
      }
    }

    // This user should have access to node_1 and node_3.
    $web_user = $this->createUser($user['permissions']);
    foreach ($user as $field_name => $term_names) {
      $values = [];
      if ($web_user->hasField($field_name)) {
        foreach ($term_names as $name) {
          $values[] = $this->getTermByName($name);
        }
        $web_user->set($field_name, $values);
      }
    }
    $web_user->save();

    $expected_query_results = [];
    foreach ($expected as $node_name => $expected_access) {
      $node = $this->getNodeByTitle($node_name);
      $access = $this->accessPolicyValidator->validate($node, $web_user, 'view');
      $this->assertEquals($expected_access, $access, 'Access test failed for ' . $node_name);
      if ($expected_access) {
        $expected_query_results[] = $node->id();
      }
    }
    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', $expected_query_results);

  }

  /**
   * Data provider for testFieldMatchEntityReferenceWithIgnoredField.
   */
  public function providerFieldMatchEntityReferenceWithIgnoredField() {
    return [
      'user has values in department fields.' => [
        'nodes' => [
          'node1' => [
            'field_department' => ['department'],
            'access_policy' => ['policy_department'],
          ],
          'node2' => [
            'field_department' => ['department2'],
            'access_policy' => ['policy_department'],
          ],
        ],
        'user' => [
          'permissions' => [
            'view policy_department content',
          ],
          'field_department' => ['department2'],
        ],
        'expected' => [
          'node1' => FALSE,
          'node2' => TRUE,
        ],
      ],
      'user has values in tags field.' => [
        'nodes' => [
          'node1' => [
            'field_tags' => ['tag1'],
            'access_policy' => ['policy_department'],
          ],
        ],
        'user' => [
          'permissions' => [
            'view policy_department content',
          ],
          'field_tags' => ['tag1'],
        ],
        'expected' => [
          'node1' => TRUE,
        ],
      ],
    ];
  }

  /**
   * This creates a new vocabulary and adds a corresponding fields.
   *
   * @param string $name
   *   The name of the vocabulary.
   * @param string $field_name
   *   The field name.
   * @param string $handler
   *   The selection handler.
   * @param array $handler_settings
   *   The selection handler settings.
   */
  protected function setupVocabularyWithFields($name, $field_name, $handler = 'default', array $handler_settings = []) {
    $vocabulary = Vocabulary::create([
      'name' => $name,
      'vid' => $name,
    ]);
    $vocabulary->save();

    if (empty($handler_settings)) {
      $handler_settings = [
        'target_bundles' => [$name => $name],
      ];
    }

    // Create the taxonomy term entity reference field for the node and user.
    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'user',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => $field_name,
      'bundle' => 'user',
      'translatable' => FALSE,
      'settings' => [
        'handler' => $handler,
        'handler_settings' => $handler_settings,
      ],
    ]);
    $field->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => $field_name,
      'bundle' => 'page',
      'translatable' => FALSE,
      'settings' => [
        'handler' => $handler,
        'handler_settings' => $handler_settings,
      ],
    ]);
    $field->save();

    return $vocabulary;
  }

}
