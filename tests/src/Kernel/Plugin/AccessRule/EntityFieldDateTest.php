<?php

namespace Drupal\Tests\access_policy\Kernel\Plugin\AccessRule;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;

/**
 * Tests EntityField base plugin features.
 *
 * @group access_policy
 */
class EntityFieldDateTest extends AccessPolicyKernelTestBase {

  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);
  }

  /**
   * Tests the relative date entity field access rule.
   */
  public function testEntityFieldRelativeDateAccessRule() {
    $policy = AccessPolicy::create([
      'id' => 'policy_date',
      'label' => 'Access policy with created date',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'created');
    $access_rule->setSettings([
      'operator' => '<',
      'type' => 'relative',
      'value' => '-30 days',
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'created' => strtotime('-15 days'),
      'access_policy' => ['policy_date'],
    ]);
    $node_2 = $this->createNode([
      'title' => 'Node 2',
      'type' => 'page',
      'status' => 1,
      'created' => strtotime('-45 days'),
      'access_policy' => ['policy_date'],
    ]);

    $web_user = $this->createUser([
      'view policy_date content',
    ]);
    // Node 1 was created less than 30 days ago and should not be available.
    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertFalse($access, 'This user should have access');

    // Node 2 was created more than 30 days ago and should be available.
    $access = $this->accessPolicyValidator->validate($node_2, $web_user, 'view');
    $this->assertTrue($access, 'This user should not have access');

    // Perform the query. Note that it requires a current user.
    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_2->id(),
    ]);
  }

  /**
   * Tests the date entity field access rule.
   */
  public function testEntityFieldDateAccessRule() {
    $policy = AccessPolicy::create([
      'id' => 'policy_date',
      'label' => 'Access policy with created date',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'created');
    $access_rule->setSettings([
      'operator' => '>',
      'type' => 'date',
      'value' => '2023-02-20 2:00:00',
    ]);
    $policy->addHandler('access_rule', $access_rule);

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_date');
    $access_rule->setSettings([
      'operator' => '>',
      'type' => 'date',
      'value' => '2023-02-20 2:00:00',
    ]);
    $policy->addHandler('access_rule', $access_rule);

    $policy->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'created' => strtotime('2023-01-01 5:00:00'),
      'access_policy' => ['policy_date'],
    ]);
    $node_2 = $this->createNode([
      'title' => 'Node 2',
      'type' => 'page',
      'status' => 1,
      'created' => strtotime('2023-02-27 3:00:00'),
      'access_policy' => ['policy_date'],
    ]);

    $web_user = $this->createUser([
      'view policy_date content',
    ]);
    // Node 1 before the date in the access rule and should not have access.
    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertFalse($access, 'This user should not have access');

    // Node 2 was created after the date in the access rule and should have
    // access.
    $access = $this->accessPolicyValidator->validate($node_2, $web_user, 'view');
    $this->assertTrue($access, 'This user should have access');

    $this->setCurrentUser($web_user);
    $this->assertQueryResults('node', [
      $node_2->id(),
    ]);

  }

  /**
   * Tests the date entity field access rule with the datetime field.
   */
  public function entityFieldDateAccessRuleWithDatetimeField() {
    // The datetime field stores the dates as a varchar in the format
    // YYYY-MM-DD H:I:S. The is different than the normal integer timestamp.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_date',
      'entity_type' => 'node',
      'type' => 'datetime',
      'cardinality' => -1,
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_date',
      'bundle' => 'page',
      'translatable' => FALSE,
    ]);
    $field->save();

    $policy = AccessPolicy::create([
      'id' => 'policy_date',
      'label' => 'Access policy with created date',
      'target_entity_type_id' => 'node',
      'access_rule_operator' => 'OR',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_date');
    $access_rule->setSettings([
      'operator' => '<',
      'type' => 'date',
      'value' => '2023-02-25 3:00:00',
    ]);
    $policy->addHandler('access_rule', $access_rule);

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_date');
    $access_rule->setSettings([
      'operator' => '<',
      'type' => 'relative',
      'value' => '-1 day',
    ]);
    $policy->addHandler('access_rule', $access_rule);

    $policy->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_date' => '2022-02-25 3:00:00',
      'access_policy' => ['policy_date'],
    ]);

    $node_2 = $this->createNode([
      'title' => 'Node 2',
      'type' => 'page',
      'status' => 1,
      'field_date' => '2024-07-10 3:00:00',
      'access_policy' => ['policy_date'],
    ]);

    // These use a relative date in MYSQL format.
    $node_3 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_date' => date('Y-m-d H:i:s', time()),
      'access_policy' => ['policy_date'],
    ]);

    // This node has a date two days before the relative requirement.
    $node_4 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_date' => date('Y-m-d H:i:s', time() - (86400 * 2)),
      'access_policy' => ['policy_date'],
    ]);

    $web_user = $this->createUser([
      'view policy_date content',
    ]);
    // Node 1 before the date in the access rule and should have access.
    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'This user should have access');

    // Node 2 after the date in the access rule and should not have access.
    $access = $this->accessPolicyValidator->validate($node_2, $web_user, 'view');
    $this->assertFalse($access, 'This user should not have access');

    $access = $this->accessPolicyValidator->validate($node_3, $web_user, 'view');
    $this->assertFalse($access, 'This user should not have access');

    $access = $this->accessPolicyValidator->validate($node_4, $web_user, 'view');
    $this->assertTrue($access, 'This user should have access');

    // Perform the query. Note that it requires a current user.
    $this->setCurrentUser($web_user);

    $this->assertQueryResults('node', [
      $node_1->id(),
      $node_4->id(),
    ]);
  }

  /**
   * Tests the date entity field access rule with the datetime field.
   */
  public function testEntityFieldDateAccessRuleWithCurrentTime() {
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_launch_date',
      'entity_type' => 'node',
      'type' => 'datetime',
      'cardinality' => -1,
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_launch_date',
      'bundle' => 'page',
      'translatable' => FALSE,
    ]);
    $field->save();

    $policy = AccessPolicy::create([
      'id' => 'policy_date',
      'label' => 'Access policy with created date',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_launch_date');
    $access_rule->setSettings([
      'operator' => '<',
      'type' => 'now',
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // This has a launch date that happened in the past. It should be
    // accessible.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_launch_date' => date('Y-m-d H:i:s', time() - (86400 * 2)),
      'access_policy' => ['policy_date'],
    ]);

    // This has a launch date in the future. It should not be accessible.
    $node_2 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_launch_date' => date('Y-m-d H:i:s', time() + (86400 * 2)),
      'access_policy' => ['policy_date'],
    ]);

    $web_user = $this->createUser([
      'view policy_date content',
    ]);

    $access = $this->accessPolicyValidator->validate($node_1, $web_user, 'view');
    $this->assertTrue($access, 'This user should have access');

    $access = $this->accessPolicyValidator->validate($node_2, $web_user, 'view');
    $this->assertFalse($access, 'This user should not have access');
  }

}
