<?php

namespace Drupal\Tests\access_policy\Kernel;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests selection rule plugins.
 *
 * @group access_policy
 */
class AccessPolicySelectionRuleTest extends AccessPolicyKernelTestBase {

  use TaxonomyTestTrait;

  /**
   * The tags vocabulary.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $vocabulary;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'taxonomy',
    'options',
    'access_policy',
    'access_policy_test',
    'system',
    'text',
    'user',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);
    $this->installEntitySchema('taxonomy_term');

    // Create a tags vocabulary.
    $this->vocabulary = Vocabulary::create([
      'name' => 'tags',
      'vid' => 'tags',
    ]);
    $this->vocabulary->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_tags',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_tags',
      'bundle' => 'page',
      'translatable' => FALSE,
    ]);
    $field->save();

    // Creating a policy is necessary to register the plugins.
    $policy = AccessPolicy::create([
      'id' => 'basic_policy',
      'label' => 'Basic test policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    NodeType::create(['type' => 'page', 'name' => 'Page']);
    NodeType::create(['type' => 'article', 'name' => 'Article']);

  }

  /**
   * Test the standard selection rule plugin.
   */
  public function testStringSelectionRule() {

    $node = $this->createNode([
      'type' => 'page',
      'title' => 'Page',
    ]);

    $web_user = $this->createUser();

    // Test empty.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_text');
    $handler->setSettingValue('operator', 'empty');
    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result, 'field_text is empty and should return true.');

    // Test equal to.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_text');
    $handler->setSettingValue('operator', '=');
    $handler->setSettingValue('value', 'Foo bar');

    $node->set('field_text', 'Foo bar');
    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result, 'field_text is set to Foo bar and should return true');

    // Test not equal to.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_text');
    $handler->setSettingValue('operator', '!=');
    $handler->setSettingValue('value', 'Foo bar');

    $node->set('field_text', 'Bar foo');
    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result, 'The value should return true.');

    // Test not empty.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_text');
    $handler->setSettingValue('operator', 'not empty');

    $node->set('field_text', 'Foo bar');
    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result, 'field_text is not empty and should return true.');

    // Test is applicable.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_text');
    $handler->setSettingValue('operator', 'is applicable');

    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result, 'field_text exists on the node and should return true.');

    $node_article = $this->createNode([
      'title' => 'Article',
      'type' => 'article',
    ]);
    $result = $handler->validate($node_article, $web_user);
    $this->assertFalse($result, 'field_text does not exist on the node and should return false.');
  }

  /**
   * Test the boolean selection rule.
   */
  public function testBooleanSelectionRule() {

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_boolean',
      'entity_type' => 'node',
      'type' => 'boolean',
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_name' => 'field_boolean',
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => "boolean",
      'required' => TRUE,
      'settings' => [
        'on_label' => 'On',
        'off_label' => 'Off',
      ],
    ]);
    $field->save();

    $node = $this->createNode([
      'type' => 'page',
      'title' => 'Page',
    ]);

    $web_user = $this->createUser();

    // Test equal to true.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_boolean');
    $handler->setSettingValue('operator', '=');
    $handler->setSettingValue('value', 'true');

    $node->set('field_boolean', TRUE);
    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result);

    // Test equal to false.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_boolean');
    $handler->setSettingValue('operator', '=');
    $handler->setSettingValue('value', 'false');

    $node->set('field_boolean', FALSE);
    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result);

    // Test not equal to true.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_boolean');
    $handler->setSettingValue('operator', '!=');
    $handler->setSettingValue('value', 'true');

    $node->set('field_boolean', FALSE);
    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result);

    // Test not equal to false.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_boolean');
    $handler->setSettingValue('operator', '!=');
    $handler->setSettingValue('value', 'false');

    $node->set('field_boolean', TRUE);
    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result);

  }

  /**
   * Validate the entity reference selection rule.
   */
  public function testEntityReferenceSelectionRule() {
    // Test entity reference fields.
    $term_1 = $this->createTerm($this->vocabulary, [
      'title' => 'Term 1',
    ]);
    $term_1->save();

    $term_2 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 2',
    ]);
    $term_2->save();

    // This is matching the value and should return false.
    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_tags' => [
        $term_1,
      ],
    ]);

    $node_2 = $this->createNode([
      'title' => 'Node 2',
      'type' => 'page',
      'status' => 1,
      'field_tags' => [
        $term_2,
      ],
    ]);

    $node_3 = $this->createNode([
      'title' => 'Node 3',
      'type' => 'page',
      'status' => 1,
    ]);

    $web_user = $this->createUser();

    // Test not in operator.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_tags');
    $handler->setSettings([
      'value' => [
        ['target_id' => 1],
      ],
      'operator' => 'not in',
    ]);

    // This is matching the value and should return false.
    $result = $handler->validate($node_1, $web_user);
    $this->assertFalse($result, 'Not in: Node matches term value and should return false.');

    // This is referencing a different node and should be visible.
    $result = $handler->validate($node_2, $web_user);
    $this->assertTrue($result, 'Not in: Node has different term value and should return true.');

    // Test IN operator.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_tags');
    $handler->setSettings([
      'value' => [
        ['target_id' => 1],
      ],
      'operator' => 'in',
    ]);

    $result = $handler->validate($node_1, $web_user);
    $this->assertTrue($result, 'In: Node matches term value and should return false.');

    $result = $handler->validate($node_2, $web_user);
    $this->assertFalse($result, 'In: Node does not term value and should return false.');

    // Test empty operator.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_tags');
    $handler->setSettings([
      'operator' => 'empty',
    ]);
    $result = $handler->validate($node_3, $web_user);
    $this->assertTrue($result, 'Empty: Node is empty and should return true.');

    $result = $handler->validate($node_2, $web_user);
    $this->assertFalse($result, 'Empty: Node is not empty and should return false.');

    // Test not empty operator.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_tags');
    $handler->setSettings([
      'operator' => 'not empty',
    ]);
    $result = $handler->validate($node_3, $web_user);
    $this->assertFalse($result, 'Not empty: Node is empty and should return false.');

    $result = $handler->validate($node_2, $web_user);
    $this->assertTrue($result, 'Not empty: Node is not empty and should return true.');

    // Test is applicable.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_tags');
    $handler->setSettingValue('operator', 'is applicable');

    $result = $handler->validate($node_1, $web_user);
    $this->assertTrue($result, 'field_tags exists on the node and should return true.');

    $node_article = $this->createNode([
      'title' => 'Article',
      'type' => 'article',
    ]);

    $result = $handler->validate($node_article, $web_user);
    $this->assertFalse($result, 'field_tags does not exist on the node and should return false.');

  }

  /**
   * Test the list field selection rule plugin.
   */
  public function testListFieldSelectionRule() {
    // Create two field entities.
    FieldStorageConfig::create([
      'field_name' => 'field_list_string',
      'entity_type' => 'node',
      'type' => 'list_string',
      'cardinality' => 1,
      'settings' => [
        'allowed_values' => [
          'foo' => 'Foo',
          'bar' => 'Bar',
          'test' => 'Test',
        ],
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_list_string',
      'entity_type' => 'node',
      'label' => 'Test options list field',
      'bundle' => 'page',
    ])->save();

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_list_string' => [
        'foo',
      ],
    ]);

    $node_2 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_list_string' => [
        'bar',
      ],
    ]);

    $node_3 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
    ]);

    $web_user = $this->createUser();

    // Test OR operator.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_list_string');
    $handler->setSettings([
      'value' => [
        'foo' => 'foo',
      ],
      'operator' => 'or',
    ]);

    $result = $handler->validate($node_1, $web_user);
    $this->assertTrue($result, 'OR: Node matches list value and should return true.');

    $result = $handler->validate($node_2, $web_user);
    $this->assertFalse($result, 'OR: Node does not match list value and should return false.');

    // Test NOT operator.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_list_string');
    $handler->setSettings([
      'value' => [
        'foo' => 'foo',
      ],
      'operator' => 'not',
    ]);

    $result = $handler->validate($node_1, $web_user);
    $this->assertFalse($result, 'NOT: Node matches list value and should return false.');

    $result = $handler->validate($node_2, $web_user);
    $this->assertTrue($result, 'NOT: Node does not match list value and should return true.');

    // Test empty operator.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_list_string');
    $handler->setSettings([
      'operator' => 'empty',
    ]);

    $result = $handler->validate($node_1, $web_user);
    $this->assertFalse($result, 'Empty: Node is not empty and should return false.');

    $result = $handler->validate($node_3, $web_user);
    $this->assertTrue($result, 'NOT: Node is empty and should return true.');

    // Test is applicable.
    $handler = $this->selectionRuleManager->getHandler('node', 'field_list_string');
    $handler->setSettingValue('operator', 'is applicable');

    $result = $handler->validate($node_1, $web_user);
    $this->assertTrue($result, 'field_list_string exists on the node and should return true.');

    $node_article = $this->createNode([
      'title' => 'Article',
      'type' => 'article',
    ]);

    $result = $handler->validate($node_article, $web_user);
    $this->assertFalse($result, 'field_list_string does not exist on the node and should return false.');
  }

  /**
   * Test the bundle selection rule.
   */
  public function testBundleSelectionRule() {

    $node_1 = $this->createNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_list_string' => [
        'foo',
      ],
    ]);

    $node_2 = $this->createNode([
      'title' => 'Node 2',
      'type' => 'article',
      'status' => 1,
    ]);

    $web_user = $this->createUser();

    // Test OR operator.
    $handler = $this->selectionRuleManager->getHandler('node', 'type');
    $handler->setSettings([
      'value' => [
        'page' => 'page',
      ],
      'operator' => 'or',
    ]);

    $result = $handler->validate($node_1, $web_user);
    $this->assertTrue($result, 'OR: Node matches bundle and should return true.');

    $result = $handler->validate($node_2, $web_user);
    $this->assertFalse($result, 'OR: Node does match bundle and should return false.');

    // Test NOT operator.
    $handler = $this->selectionRuleManager->getHandler('node', 'type');
    $handler->setSettings([
      'value' => [
        'page' => 'page',
      ],
      'operator' => 'not',
    ]);

    $result = $handler->validate($node_1, $web_user);
    $this->assertFalse($result, 'NOT: Node matches bundle and should return false.');

    $result = $handler->validate($node_2, $web_user);
    $this->assertTrue($result, 'NOT: Node does not match bundle and should return true.');
  }

}
