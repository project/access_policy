<?php

namespace Drupal\Tests\access_policy\Kernel;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests moderation state selection rule plugin.
 *
 * @group access_policy
 */
class ModerationStateSelectionRuleTest extends AccessPolicyKernelTestBase {


  use ContentModerationTestTrait;
  use TaxonomyTestTrait;
  use OperatorValidationTrait;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'system',
    'text',
    'user',
    'datetime',
    'content_moderation',
    'workflows',
    'access_policy',
    'access_policy_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['access_policy_test']);
    $this->installEntitySchema('content_moderation_state');
    $this->installConfig(['content_moderation', 'system']);

    // Articles are not using content moderation.
    NodeType::create([
      'type' => 'article',
      'name' => 'Article',
    ])->save();

    $nodeType = NodeType::load('page');
    $nodeType->setNewRevision(TRUE);
    $nodeType->save();

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'page');
    $workflow->save();
  }

  /**
   * Test the moderation state selection rule plugin.
   */
  public function testModerationStateSelectionRule() {

    $policy = AccessPolicy::create([
      'id' => 'archived',
      'label' => 'Archived policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $node = $this->createNode([
      'type' => 'page',
      'title' => 'Page',
      'moderation_state' => 'archived',
    ]);

    $node_article = $this->createNode([
      'type' => 'article',
      'title' => 'Article',
    ]);

    $web_user = $this->createUser();

    $handler = $this->selectionRuleManager->getHandler('node', 'moderation_state');
    $handler->setSettingValue('value', [
      'editorial-archived' => 'editorial-archived',
    ]);

    // Test OR operator.
    $handler->setSettingValue('operator', 'or');
    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result, 'The content is archived and should return true.');

    // Test NOT operator.
    $node->set('moderation_state', 'published');
    $node->save();
    $handler->setSettingValue('operator', 'not');
    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result, 'The content is published and shot return true.');

    // Test empty operator.
    $handler = $this->selectionRuleManager->getHandler('node', 'moderation_state');
    $handler->setSettings([
      'operator' => 'empty',
    ]);

    $result = $handler->validate($node, $web_user);
    $this->assertFalse($result, 'Empty: Node is not empty and should return false.');

    $result = $handler->validate($node_article, $web_user);
    $this->assertTrue($result, 'NOT: Node is empty and should return true.');

    // Test is applicable.
    $handler = $this->selectionRuleManager->getHandler('node', 'moderation_state');
    $handler->setSettingValue('operator', 'is applicable');

    $result = $handler->validate($node, $web_user);
    $this->assertTrue($result, 'moderation_state exists on the node and should return true.');

    $result = $handler->validate($node_article, $web_user);
    $this->assertFalse($result, 'moderation_state does not exist on the node and should return false.');
  }

}
