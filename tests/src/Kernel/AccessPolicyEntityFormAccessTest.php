<?php

namespace Drupal\Tests\access_policy\Kernel;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\access_policy\Form\AccessPolicyEntityForm;
use Drupal\Core\Routing\RouteMatch;
use Drupal\node\Entity\NodeType;
use Symfony\Component\Routing\Route;

/**
 * Tests the access policy entity form access.
 *
 * @group access_policy
 */
class AccessPolicyEntityFormAccessTest extends AccessPolicyKernelTestBase {

  /**
   * The entity type settings service.
   *
   * @var \Drupal\access_policy\EntityTypeSettingsInterface
   */
  protected $entityTypeSettings;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();

    NodeType::create(['type' => 'page', 'name' => 'Page']);

    $this->entityTypeSettings = $this->container->get('access_policy.entity_type_settings');

    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'manual');
    $settings->set('selection_strategy_settings', [
      'allow_empty' => TRUE,
      'default_policy' => 'first_available',
      'show_operations_link' => FALSE,
    ]);
    $settings->save();

    // Set access policies as first accessible.
    AccessPolicy::create([
      'id' => 'policy_test_1',
      'label' => 'Policy test 1',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'weight' => 0,
    ])->save();
  }

  /**
   * Tests the access policy form access.
   *
   * Note that this only tests against correctly and incorrectly formed route
   * parameters. Other conditions are covered with functional tests.
   */
  public function testAccessPolicyEntityFormAccessParameters() {
    $nid = 1;
    $node = $this->createNode([
      'nid' => $nid,
      'type' => 'page',
      'title' => 'Page',
    ]);
    $node->save();
    $defaults = [
      '_entity_form' => 'node.manage access',
      'entity_type_id' => 'node',
    ];
    $route = new Route('/node/{node}/access', $defaults);
    $parameters = ['entity_type_id' => 'node', 'node' => $node];
    $raw_parameters = ['entity_type_id' => 'node', 'node' => $nid];
    $route_match = new RouteMatch('entity.node.access_policy_form', $route, $parameters, $raw_parameters);

    $form = \Drupal::service('class_resolver')->getInstanceFromDefinition(AccessPolicyEntityForm::class);
    $result = $form->checkAccess($route_match);
    $this->assertTrue($result->isAllowed(), 'Access is allowed');

    // Test access against a route with parameters that are not upcasted.
    $parameters = ['entity_type_id' => 'node', 'node' => $nid];
    $raw_parameters = ['entity_type_id' => 'node', 'node' => $nid];
    $route_match = new RouteMatch('entity.node.access_policy_form', $route, $parameters, $raw_parameters);
    $result = $form->checkAccess($route_match);
    $this->assertTrue($result->isAllowed(), 'Access is allowed');
  }

}
