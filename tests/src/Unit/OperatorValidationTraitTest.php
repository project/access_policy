<?php

namespace Drupal\Tests\access_policy\Unit;

use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\Tests\UnitTestCase;

/**
 * Access rule operator trait test.
 *
 * @group access_policy
 */
class OperatorValidationTraitTest extends UnitTestCase {

  use OperatorValidationTrait;

  /**
   * Tests the access rule operator trait.
   */
  public function testAccessRuleOperatorTrait() {

    $this->assertTrue($this->equals('Foo', 'Foo'));

    $this->assertTrue($this->notEquals('Test', 'Foo'));

    $string = 'Foo bar with text';
    $this->assertTrue($this->contains('Foo', $string));

    $this->assertTrue($this->notContains('Test', $string));

    $this->assertTrue($this->startsWith('Foo', $string));

    $this->assertTrue($this->notStartsWith('Test', $string));

    $this->assertTrue($this->endsWith('text', $string));

    $this->assertTrue($this->notEndsWith('test', $string));

    $this->assertTrue($this->isLessThan(5, 1));

    $this->assertTrue($this->isLessThanOrEqualTo(4, 4));
    $this->assertTrue($this->isLessThanOrEqualTo(4, 3));

    $this->assertTrue($this->isGreaterThan(10, 25));

    $this->assertTrue($this->isGreaterThanOrEqualTo(1, 1));
    $this->assertTrue($this->isGreaterThanOrEqualTo(1, 2));

    $set = [1, 2, 3, 4];
    $this->assertTrue($this->isOneOf($set, 3));

    $this->assertTrue($this->isNoneOf($set, 10));
  }

}
