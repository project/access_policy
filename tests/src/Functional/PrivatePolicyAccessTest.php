<?php

namespace Drupal\Tests\access_policy\Functional;

use Drupal\access_policy\Entity\AccessPolicy;

/**
 * Ensures that private access policy assignment works as expected.
 *
 * The private access policy is unique in that it doesn't have any permissions.
 * Instead, all logic is handled through rules. This tests to ensure that
 * assigning a private access policy is also enforced by rules.
 *
 * @see \Drupal\access_policy\Plugin\access_policy\AccessPolicyType\PrivateAccess
 *
 * @group access_policy
 */
class PrivatePolicyAccessTest extends AccessPolicyTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'access_policy',
    'access_policy_test',
    'filter',
    'node',
    'datetime',
    'field',
    'user',
    'datetime',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setup();

    $policy = AccessPolicy::create([
      'id' => 'private_policy',
      'label' => 'Private',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();
    $access_rule = $this->accessRuleManager->getHandler('node', 'is_own');
    $access_rule->setSettings([
      'operations' => [
        'view',
        'update',
        'delete',
        'view_unpublished',
        'view_all_revisions',
      ],
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->addHandler('access_rule', $this->accessRuleManager->getHandler('node', 'matching_text'));
    $policy->save();

    AccessPolicy::create([
      'id' => 'public_policy',
      'label' => 'public policy',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ])->save();

    \Drupal::service('router.builder')->rebuildIfNeeded();
  }

  /**
   * Tests assigning a private access policy to a node from a form.
   */
  public function testAssignPrivateAccessPolicy() {
    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'manual');
    $settings->save();

    $author = $this->drupalCreateUser([
      'set entity access policy',
      'assign public_policy access policy',
      'assign private_policy access policy',
    ]);
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
      'uid' => $author->id(),
    ]);

    $this->drupalLogin($author);

    // Allow the original author to set the content private.
    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->optionExists('access_policy', 'Private');

    // Don't allow other users to set the content private. They are not the
    // original author.
    $web_user = $this->drupalCreateUser([
      'set entity access policy',
      'assign public_policy access policy',
    ]);
    $this->drupalLogin($web_user);
    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->optionNotExists('access_policy', 'Private');

  }

  /**
   * Tests assigning a private access policy with one failing rule.
   *
   * Tests assigning a private access policy to a node when one of the other
   * rules assigned to it would normally fail. It should pass because the
   * other access rules do not fire during "assign access policy".
   */
  public function testAssignPrivateAccessPolicyWithOneFailingRule() {
    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'manual');
    $settings->save();

    $author = $this->drupalCreateUser([
      'set entity access policy',
      'assign private_policy access policy',
    ]);
    $author->set('field_text', 'Some text');
    $author->save();
    $this->drupalLogin($author);

    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
      'uid' => $author->id(),
      'field_text' => 'Some text does not match',
    ]);

    // Allow the original author to set the content private.
    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->optionExists('access_policy', 'Private');
  }

  /**
   * Testing the "Me only" private access policy on a listing page.
   */
  public function testContentListingWithPrivatePolicy() {
    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'manual');
    $settings->save();

    $policy = AccessPolicy::create([
      'id' => 'private_basic',
      'label' => 'Private',
      'target_entity_type_id' => 'node',
      'operations' => [
        'view' => [
          'access_rules' => TRUE,
        ],
        'view_all_revisions' => [
          'access_rules' => TRUE,
        ],
        'update' => [
          'access_rules' => TRUE,
        ],
        'delete' => [
          'access_rules' => TRUE,
        ],
        'view_unpublished' => [
          'access_rules' => TRUE,
        ],
        'manage_access' => [],
      ],
    ]);
    $access_rule = $this->accessRuleManager->getHandler('node', 'is_own');
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $author = $this->drupalCreateUser([
      'access administration pages',
      'access content overview',
      'administer nodes',
    ]);

    $node = $this->drupalCreateNode([
      'title' => 'Private policy (visible)',
      'type' => 'page',
      'status' => 1,
      'uid' => $author->id(),
      'access_policy' => 'private_content',
    ]);

    $this->drupalLogin($author);

    $node->set('access_policy', ['private_basic']);
    $node->save();

    $this->drupalGet('admin/content');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Private policy (visible)');

    $web_user = $this->drupalCreateUser([
      'access administration pages',
      'access content overview',
      'administer nodes',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('admin/content');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextNotContains('Private policy (visible)');

  }

}
