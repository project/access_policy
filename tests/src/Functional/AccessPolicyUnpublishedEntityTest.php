<?php

namespace Drupal\Tests\access_policy\Functional;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\system\Functional\Cache\AssertPageCacheContextsAndTagsTrait;
use Drupal\user\Entity\Role;

/**
 * Ensures that access policy entity permissions work correctly.
 *
 * @group access_policy
 */
class AccessPolicyUnpublishedEntityTest extends AccessPolicyTestBase {

  use ContentModerationTestTrait;
  use AssertPageCacheContextsAndTagsTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'access_policy',
    'access_policy_test',
    'content_moderation',
    'filter',
    'node',
    'datetime',
  ];

  /**
   * Admin permissions.
   *
   * @var string[]
   */
  protected $adminPermissions = [
    'administer workflows',
    'access administration pages',
    'administer content types',
    'administer nodes',
    'view latest version',
    'view any unpublished content',
    'access content overview',
    'bypass node access',
    'use editorial transition create_new_draft',
    'use editorial transition publish',
    'use editorial transition archive',
    'use editorial transition archived_draft',
    'use editorial transition archived_published',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setup();
    AccessPolicy::create([
      'id' => 'test_policy',
      'label' => 'Test policy',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ])->save();

    AccessPolicy::create([
      'id' => 'test_policy_2',
      'label' => 'Test policy 2',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ])->save();

    $nodeType = NodeType::load('page');
    $nodeType->setNewRevision(TRUE);
    $nodeType->save();

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'page');
    $workflow->save();

    $this->entityTypeSettings = $this->container->get('access_policy.entity_type_settings');
    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'manual');
    $settings->set('selection_strategy_settings', [
      'allow_empty' => TRUE,
      'default_policy' => 'empty',
      'show_operations_link' => FALSE,
    ]);
    $settings->save();
  }

  /**
   * Tests view own unpublished entity with access policy.
   *
   * Tests to ensure that original owners will no longer have access to the
   * content that they created if it's assigned to an access policy they don't
   * have access too.
   */
  public function testViewOwnUnpublishedEntityWithAccessPolicy() {
    $author = $this->drupalCreateUser();

    $rid = $this->createRole([
      'view own unpublished content',
      'view test_policy content',
    ]);
    $role = Role::load($rid);
    $author->addRole($rid);
    $author->save();

    $this->drupalLogin($author);

    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 0,
      'uid' => $author->id(),
      'access_policy' => ['test_policy'],
    ]);

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);

    // Add permission to view unpublished content. Confirm that you can see it.
    $this->grantPermissions($role, [
      'view any test_policy unpublished content',
    ]);

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests viewing unpublished entity with access policy.
   *
   * Tests view permissions for unpublished entity when assigned to the same
   * access policy as the user.
   */
  public function testViewAnyUnpublishedEntityWithAccessPolicy() {
    // Create a published page. Under normal circumstances this will always
    // be visible.
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 0,
      'access_policy' => ['test_policy'],
    ]);

    // User with access to a different policy can't view it.
    $web_user = $this->drupalCreateUser([
      'view latest version',
      'view any unpublished content',
      'view all revisions',
      'view test_policy_2 content',
      'view any test_policy_2 unpublished content',
    ]);
    $this->drupalLogin($web_user);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);

    $web_user = $this->drupalCreateUser();

    // Set through the permissions to confirm that they can't access it.
    $permissions = [
      'view any unpublished content',
      'view all revisions',
      'view test_policy content',
    ];
    $rid = $this->createRole($permissions);
    $role = Role::load($rid);
    $web_user->addRole($rid);
    $web_user->save();

    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);

    // Make sure that another user with the same access policy can view it.
    // Even though they're not the owner.
    $permissions = [
      'view any test_policy unpublished content',
    ];
    $this->grantPermissions($role, $permissions);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

  }

  /**
   * Tests viewing unpublished entity as an anonymous user.
   */
  public function testViewAnyUnpublishedEntityAsAnonymous() {
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 0,
    ]);

    // View the node as an anonymous user. They should not have access.
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);

    $node->set('access_policy', ['test_policy']);
    $node->save();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests view permissions for latest unpublished entity with access policy.
   */
  public function testViewLatestRevisionEntityWithAccessPolicy() {
    $author = $this->drupalCreateUser([
      'view own unpublished content',
      'view latest version',
      'view all revisions',
      'view test_policy content',
      'view any test_policy unpublished content',
    ]);

    // Create a published page. Under normal circumstances this will always
    // be visible.
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
      'uid' => $author->id(),
      'access_policy' => ['test_policy'],
    ]);

    // Create a pending revision for the 'Latest revision' tab.
    $admin_user = $this->createUser($this->adminPermissions);
    $this->drupalLogin($admin_user);

    // First publish the node.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->submitForm(['moderation_state[0][state]' => 'published'], 'Save');

    // Create a pending revision for the 'Latest revision' tab.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->submitForm([
      'title[0][value]' => 'moderated content revised',
      'moderation_state[0][state]' => 'draft',
    ], 'Save');

    // Make sure the original author can view the latest revision.
    $this->drupalLogin($author);
    $this->drupalGet('node/' . $node->id() . '/latest');
    $this->assertSession()->statusCodeEquals(200);

    // User with access to a different policy can't view it.
    $web_user = $this->drupalCreateUser([
      'view latest version',
      'view all revisions',
      'view any unpublished content',
      'view test_policy_2 content',
      'view any test_policy_2 unpublished content',
    ]);

    $this->drupalLogin($web_user);
    $this->drupalGet('node/' . $node->id() . '/latest');
    $this->assertSession()->statusCodeEquals(403);

    // The user must always have both permissions in order to see the latest
    // revision.
    $rid = $this->createRole([
      'view latest version',
      'view all revisions',
      'view test_policy content',
      'view any test_policy unpublished content',
    ]);
    $role = Role::load($rid);

    $web_user = $this->drupalCreateUser();
    $web_user->addRole($rid);
    $web_user->save();

    $this->drupalLogin($web_user);
    $this->drupalGet('node/' . $node->id() . '/latest');
    $this->assertSession()->statusCodeEquals(403);

    $permissions = [
      'view any unpublished content',
    ];
    $this->grantPermissions($role, $permissions);
    $this->drupalGet('node/' . $node->id() . '/latest');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests that a listing does not display unpublished entities.
   */
  public function testViewListingOfUnpublishedContent() {
    $policy = AccessPolicy::create([
      'id' => 'test_policy_with_rules',
      'label' => 'Test Access Policy with rule',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_text');
    $policy->addHandler('access_rule', $access_rule);

    AccessPolicy::create([
      'id' => 'test_policy_with_no_rules',
      'label' => 'Test Access Policy with no rules',
      'target_entity_type_id' => 'node',
    ])->save();

    $node_1 = $this->drupalCreateNode([
      'title' => 'Node with rules',
      'type' => 'page',
      'status' => 0,
      'field_text' => 'Matching text',
      'access_policy' => ['test_policy_with_rules'],
    ]);

    $node_2 = $this->drupalCreateNode([
      'title' => 'Node without rules',
      'type' => 'page',
      'status' => 0,
      'access_policy' => ['test_policy_with_no_rules'],
    ]);

    // This user can view unpublished content but can't view unpublished
    // policy content.
    $user = $this->drupalCreateUser([
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view any unpublished content',
      'view test_policy_with_rules content',
      'view test_policy_with_no_rules content',
    ]);
    $user->set('field_text', 'Matching text');
    $user->save();
    $this->drupalLogin($user);

    $this->drupalGet('node/' . $node_1->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('node/' . $node_2->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('admin/content');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextNotContains('Node with rules');
    $this->assertSession()->pageTextNotContains('Node without rules');

    // This user can view unpublished content and some unpublished policy
    // content.
    $user = $this->drupalCreateUser([
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view any unpublished content',
      'view test_policy_with_rules content',
      'view any test_policy_with_rules unpublished content',
      'view test_policy_with_no_rules content',
    ]);
    $user->set('field_text', 'Matching text');
    $user->save();
    $this->drupalLogin($user);

    $this->drupalGet('node/' . $node_1->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('node/' . $node_2->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('admin/content');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContainsOnce('Node with rules');
    $this->assertSession()->pageTextNotContains('Node without rules');
  }

  /**
   * Tests that the access policy gets updated properly across revisions.
   */
  public function testUpdatePolicyRevisions() {
    /** @var \Drupal\Core\Entity\RevisionableStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('node');

    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'dynamic');
    $settings->save();

    AccessPolicy::create([
      'id' => 'public',
      'label' => 'Public',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ])->save();

    $policy = AccessPolicy::create([
      'id' => 'test_policy_with_selection_rules',
      'label' => 'Access Policy with selection rules',
      'target_entity_type_id' => 'node',
      'weight' => -10,
    ]);
    $policy->save();

    $selection_rule = $this->selectionRuleManager->getHandler('node', 'field_text');
    $selection_rule->setSettings([
      'operator' => 'not empty',
    ]);
    $policy->addHandler('selection_rule', $selection_rule);
    $policy->save();

    // The access policy should only be updated on the current revision.
    $author = $this->drupalCreateUser([
      'access administration pages',
      'view latest version',
      'view any unpublished content',
      'view all revisions',
      'view own unpublished content',
      'access content',
      'administer nodes',
      'access administration pages',
      'access content overview',
      'edit any page content',
      'create page content',
      'assign public access policy',
      'view public content',
      'edit public content',
      'assign test_policy_with_selection_rules access policy',
      'edit test_policy_with_selection_rules content',
      'view any test_policy_with_selection_rules unpublished content',
      'use editorial transition create_new_draft',
      'use editorial transition publish',
      'use editorial transition archive',
      'use editorial transition archived_draft',
      'use editorial transition archived_published',
    ]);

    $this->drupalLogin($author);

    // Create a draft page.
    $node = $this->drupalCreateNode([
      'title' => 'Test node',
      'type' => 'page',
      'status' => 0,
      'uid' => $author->id(),
      'field_text' => 'Foo bar',
    ]);

    $access_policy = $this->contentAccessPolicyManager->getAccessPolicy($node);
    $access_policy = reset($access_policy);
    $this->assertEquals('test_policy_with_selection_rules', $access_policy->id());

    // Publish the node.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $edit = [
      'moderation_state[0][state]' => 'published',
    ];
    $this->submitForm($edit, 'Save');

    $node = $this->drupalGetNodeByTitle($node->label(), TRUE);

    $access_policy = $this->contentAccessPolicyManager->getAccessPolicy($node);
    $access_policy = reset($access_policy);
    $this->assertEquals('test_policy_with_selection_rules', $access_policy->id());

    // Remove the field text and save it as draft. This should change the
    // latest revision to Public, but not the default revision.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $edit = [
      'field_text[0][value]' => NULL,
      'moderation_state[0][state]' => 'draft',
    ];
    $this->submitForm($edit, 'Save');

    // Check the latest revision, this should be set to Public.
    $vid = $storage->getLatestRevisionId($node->id());
    $revision = $storage->loadRevision($vid);

    $access_policy = $this->contentAccessPolicyManager->getAccessPolicy($revision);
    $access_policy = reset($access_policy);
    $this->assertEquals('public', $access_policy->id());

    // Check the default revision, this should be set to test policy.
    $node = $this->drupalGetNodeByTitle($node->label(), TRUE);

    // Confirm that the latest revision and current revision are not the same.
    $this->assertNotSame($node->getRevisionId(), $revision->id(), 'The revisions ids should not be the same');

    $access_policy = $this->contentAccessPolicyManager->getAccessPolicy($node);
    $access_policy = reset($access_policy);
    $this->assertEquals('test_policy_with_selection_rules', $access_policy->id());
  }

}
