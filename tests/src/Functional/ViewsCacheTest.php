<?php

namespace Drupal\Tests\access_policy\Functional;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\views\Views;

/**
 * Tests that caches are updated correctly when users are updated.
 *
 * This is particularly important for policies with access rules because they
 * can't rely on the user.permission cache context.
 *
 * @group access_policy
 */
class ViewsCacheTest extends AccessPolicyTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'access_policy',
    'access_policy_test',
    'filter',
    'node',
    'system',
    'field',
    'user',
    'datetime',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // This is necessary to register the access rules.
    AccessPolicy::create([
      'id' => 'test_policy',
      'label' => 'Test policy',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ])->save();
  }

  /**
   * Tests listing pages when a user is updated.
   */
  public function testViewCacheWithUserUpdate() {

    $policy = AccessPolicy::create([
      'id' => 'matching_text',
      'label' => 'Matching Text Access Policy',
      'target_entity_type_id' => 'node',
    ]);
    // You should audit the rules  and see which ones are worth removing.
    $access_rule = $this->accessRuleManager->getHandler('user', 'field_text');
    $access_rule->setSettings([
      'operator' => '=',
      'value' => 'Foo',
      'empty_behavior' => 'deny',
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $web_user = $this->drupalCreateUser([
      'access content overview',
      'access content',
      'view matching_text content',
    ]);
    $web_user->set('field_text', 'Foo');
    $web_user->save();

    $this->drupalCreateNode([
      'title' => 'First',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['matching_text'],
    ]);

    $view = Views::getView('content_access');
    $context = [
      'languages:language_content',
      'languages:language_interface',
      'url',
      'url.query_args',
      'user.node_grants:view',
      'user.permissions',
      // This gets added with hook_views_post_render().
      'user',
    ];

    $content_access = $view->render('page_1');
    $this->assertEquals($context, $content_access['#cache']['contexts']);

    $this->drupalLogin($web_user);

    // Note that views can add filters which handle view access. In this case,
    // all those filters have been removed so we need to let access policy
    // handle it.
    $this->drupalGet('admin/content/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('First');

    // Change the user's field value and re-test.
    $web_user->set('field_text', 'Bar');
    $web_user->save();

    $this->drupalGet('admin/content/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextNotContains('First');
  }

}
