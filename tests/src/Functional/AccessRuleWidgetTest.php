<?php

namespace Drupal\Tests\access_policy\Functional;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\Core\Entity\EntityInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Ensures that access policy entity permissions work correctly.
 *
 * @group access_policy
 */
class AccessRuleWidgetTest extends AccessPolicyTestBase {

  use TaxonomyTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'access_policy',
    'access_policy_test',
    'taxonomy',
    'filter',
    'node',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The vocabulary.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $vocabulary;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setup();

    // Then create some taxonomy terms that are assigned their own policy.
    $this->vocabulary = Vocabulary::create([
      'name' => 'department',
      'vid' => 'department',
    ]);
    $this->vocabulary->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_term_reference',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_term_reference',
      'bundle' => 'page',
      'translatable' => FALSE,
    ]);
    $field->save();

    AccessPolicy::create([
      'id' => 'test_policy',
      'label' => 'Test policy',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'weight' => 0,
    ])->save();

    $this->entityTypeSettings = $this->container->get('access_policy.entity_type_settings');

    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'manual');
    $settings->set('selection_strategy_settings', [
      'allow_empty' => TRUE,
      'default_policy' => 'empty',
      'show_confirmation_form' => TRUE,
      'show_operations_link' => FALSE,
    ]);
    $settings->save();

    \Drupal::service('router.builder')->rebuildIfNeeded();
  }

  /**
   * Tests that the form renders correctly with multiple rules.
   */
  public function testAccessRuleWidgetWithMultipleAccessRules() {

    $policy = AccessPolicy::create([
      'id' => 'test_policy_with_form',
      'label' => 'Test policy with form',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'weight' => -10,
    ]);

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_text');
    $access_rule->setSettings([
      'value' => 'Some text',
      'operator' => '=',
      'widget' => [
        'show' => TRUE,
        'settings' => [
          'hide_original_field' => TRUE,
        ],
      ],
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);

    $term_1 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 1',
      'status' => 1,
    ]);
    $access_rule_2 = $this->accessRuleManager->getHandler('node', 'field_term_reference');
    $access_rule_2->setSettings([
      'value' => [
        ['target_id' => $term_1->id()],
      ],
      'operator' => 'in',
      'widget' => [
        'show' => TRUE,
      ],
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule_2);
    $policy->save();

    // Create a published page. Under normal circumstances this will always
    // be visible.
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['test_policy_with_form'],
    ]);

    // Having edit any page content and edit test_policy content should give
    // you access to the content.
    $web_user = $this->drupalCreateUser([
      'edit any page content',
      'set entity access policy',
      'assign test_policy access policy',
      'assign test_policy_with_form access policy',
      'edit test_policy_with_form content',
    ]);
    $this->drupalLogin($web_user);

    // Confirm that the field widget is not visible on the edit form.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldNotExists('field_text[0][value]');
    $this->assertSession()->fieldNotExists('field_term_reference[0][target_id]');

    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldExists('field_text[0][value]');
    $this->assertSession()->fieldExists('field_term_reference[0][target_id]');

    $edit = [
      'field_text[0][value]' => 'Some text',
    ];
    $this->submitForm($edit, 'Save');
    $this->submitForm([], 'Save');
    $this->assertSession()->fieldValueEquals('field_text[0][value]', 'Some text');

    // Reload the node and all its values.
    $node = $this->drupalGetNodeByTitle($node->label(), TRUE);
    $this->assertEquals('Some text', $node->get('field_text')->getString(), 'Value not saved on the entity');

    // Change to a different access policy. Confirm that it also clears the
    // access rule values.
    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $edit = [
      'access_policy' => 'test_policy',
    ];
    $this->submitForm($edit, 'Save');
    $this->submitForm([], 'Save');
    $this->assertSession()->fieldValueEquals('access_policy', 'test_policy');
    $node = $this->drupalGetNodeByTitle($node->label(), TRUE);
    $this->assertEquals('', $node->get('field_text')->getString(), 'The access rule field should have been removed.');
  }

  /**
   * Tests an access rule with entity reference field widget.
   */
  public function testAccessRuleWithEntityReferenceWidget() {

    $term_1 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 1',
      'status' => 1,
    ]);

    $term_2 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 2',
      'status' => 1,
    ]);

    $term_3 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 3',
      'status' => 1,
    ]);

    $policy = AccessPolicy::create([
      'id' => 'test_policy_with_form',
      'label' => 'Test policy with form',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'weight' => -10,
    ]);

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_term_reference');
    $access_rule->setSettings([
      'value' => [
        ['target_id' => $term_1->id()],
        ['target_id' => $term_2->id()],
      ],
      'operator' => 'in',
      'widget' => [
        'show' => TRUE,
      ],
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    // Create a published page. Under normal circumstances this will always
    // be visible.
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
    ]);

    // Assign test_policy access policy to the node.
    $this->contentAccessPolicyManager->assign($node, $policy);

    // Having edit any page content and edit test_policy content should give
    // you access to the content.
    $web_user = $this->drupalCreateUser([
      'set entity access policy',
      'assign test_policy_with_form access policy',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);

    $value = $term_1->label() . ' (' . $term_1->id() . ')';
    $edit = [
      'field_term_reference[0][target_id]' => $value,
    ];
    $this->submitForm($edit, 'Save');
    $this->submitForm([], 'Save');

    // Reload the form and confirm that it was saved successfully.
    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->fieldValueEquals('field_term_reference[0][target_id]', $value);
    $this->assertEquals([$term_1->id()], $this->getNodeTerms($node), 'The taxonomy terms were not saved correctly');

    // Change the widget to entity reference auto complete. and confirm that it
    // still renders and saves successfully.
    $rule = $policy->getAccessRule('field_term_reference');
    $settings = $rule['settings'];
    $settings['widget']['settings'] = [
      'field_widget' => 'entity_reference_autocomplete_tags',
    ];
    $policy->updateAccessRule('field_term_reference', $settings);
    $policy->save();

    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);

    $value = $term_2->label() . ' (' . $term_2->id() . ')';
    $edit = [
      'field_term_reference[target_id]' => $value,
    ];
    $this->submitForm($edit, 'Save');
    $this->submitForm([], 'Save');

    // Reload the form and confirm that it was saved successfully.
    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->fieldValueEquals('field_term_reference[target_id]', $value);
    $this->assertEquals([$term_1->id()], $this->getNodeTerms($node), 'The taxonomy terms were not saved correctly');

    // Term 3 is not allowed in the access policy, therefore it should trigger
    // a 403 page response.
    $value = $term_3->label() . ' (' . $term_3->id() . ')';
    $edit = [
      'field_term_reference[target_id]' => $value,
    ];
    $this->submitForm($edit, 'Save');
    $this->submitForm([], 'Save');
    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests the access rule widget when multiple policies can be assigned.
   */
  public function testAccessRuleWidgetWithMultipleAccessPolicies() {
    // Even if allow empty is FALSE it should still set it to empty if no
    // applicable policy was found.
    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'manual');
    $settings->set('selection_strategy_settings', [
      'allow_empty' => TRUE,
      'default_policy' => 'empty',
      'show_operations_link' => FALSE,
    ]);
    $settings->set('selection_sets', [
      'restricted' => [
        'id' => 'restricted',
        'label' => 'Restricted',
      ],
    ]);
    $settings->save();

    $term_1 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 1',
      'status' => 1,
    ]);

    $policy_1 = AccessPolicy::create([
      'id' => 'test_policy_1',
      'label' => 'Test policy 1',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'selection_set' => ['restricted'],
    ]);

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_term_reference');
    $access_rule->setSettings([
      'value' => [
        ['target_id' => $term_1->id()],
      ],
      'operator' => 'in',
      'widget' => [
        'show' => TRUE,
      ],
      'query' => TRUE,
    ]);
    $policy_1->addHandler('access_rule', $access_rule);
    $policy_1->save();

    AccessPolicy::create([
      'id' => 'test_policy_2',
      'label' => 'Test policy 2',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'selection_set' => ['restricted'],
    ])->save();

    AccessPolicy::create([
      'id' => 'group',
      'label' => 'Group',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ])->save();

    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
    ]);

    // Having edit any page content and edit test_policy content should give
    // you access to the content.
    $web_user = $this->drupalCreateUser([
      'set entity access policy',
      'assign test_policy_1 access policy',
      'assign test_policy_2 access policy',
      'assign group access policy',
    ]);
    $this->drupalLogin($web_user);

    // Reload the form and confirm that it was saved successfully.
    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->optionExists('access_policy', 'Restricted');
    $this->assertSession()->optionExists('access_policy', 'Group');

    $node->set('access_policy', ['test_policy_1']);
    $node->save();

    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldValueEquals('access_policy', 'selection_set-restricted');
    $this->assertSession()->checkboxChecked('policies_multiple[test_policy_1]');
    $this->assertSession()->checkboxNotChecked('policies_multiple[test_policy_2]');
    $this->assertSession()->fieldExists('field_term_reference[0][target_id]');

    // Attempt to submit the form without an access policy selected.
    $edit = [
      'access_policy' => 'selection_set-restricted',
      'policies_multiple[test_policy_1]' => 0,
      'policies_multiple[test_policy_2]' => 0,
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Please select an access policy.');

    $value = $term_1->label() . ' (' . $term_1->id() . ')';
    $edit = [
      'policies_multiple[test_policy_1]' => 1,
      'field_term_reference[0][target_id]' => $value,
    ];
    $this->submitForm($edit, 'Save');
    $this->submitForm([], 'Save');

    // Reload the form and confirm that it was saved successfully.
    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldValueEquals('field_term_reference[0][target_id]', $value);
    $this->assertEquals([$term_1->id()], $this->getNodeTerms($node), 'The taxonomy terms were not saved correctly');
    $this->assertSession()->checkboxChecked('policies_multiple[test_policy_1]');
  }

  /**
   * Tests the null fallback selection set.
   *
   * In some cases, a node can be assigned multiple access policies that are not
   * part of a set. This can happen if a policy is removed from a set. This
   * groups all these unassigned policies into an internal null fallback set.
   */
  public function testNullFallbackSelectionSet() {
    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'manual');
    $settings->set('selection_strategy_settings', [
      'allow_empty' => TRUE,
      'default_policy' => 'empty',
      'show_operations_link' => FALSE,
    ]);
    $settings->set('selection_sets', [
      'restricted' => [
        'id' => 'restricted',
        'label' => 'Restricted',
      ],
    ]);
    $settings->save();

    // This is not part of a set, it's still assigned to the node. This
    // and test_policy_2 will be added to the "Other" selection set.
    $policy_1 = AccessPolicy::create([
      'id' => 'test_policy_1',
      'label' => 'Test policy 1',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'selection_set' => ['restricted'],
    ]);
    $policy_1->save();

    AccessPolicy::create([
      'id' => 'test_policy_2',
      'label' => 'Test policy 2',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'selection_set' => ['restricted'],
    ])->save();

    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['test_policy_1', 'test_policy_2'],
    ]);

    // Remove the set from policy 1. This will trigger the "null" set edge case.
    $policy_1->setSelectionSet([]);
    $policy_1->save();

    // Having edit any page content and edit test_policy content should give
    // you access to the content.
    $web_user = $this->drupalCreateUser([
      'set entity access policy',
      'assign test_policy_1 access policy',
      'assign test_policy_2 access policy',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->optionExists('access_policy', 'Test policy 1');
    $this->assertSession()->optionExists('access_policy', 'Restricted');
    $this->assertSession()->optionExists('access_policy', 'Other');
    $this->assertSession()->fieldValueEquals('access_policy', 'selection_set-null_set');
    $this->assertSession()->checkboxChecked('policies_multiple[test_policy_1]');
    $this->assertSession()->checkboxChecked('policies_multiple[test_policy_2]');

    // Uncheck Test policy 1, this should "fix" the null set.
    $edit = [
      'policies_multiple[test_policy_1]' => FALSE,
    ];
    $this->submitForm($edit, 'Save');
    $this->submitForm([], 'Save');
    $this->drupalGet('node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->optionExists('access_policy', 'Test policy 1');
    $this->assertSession()->optionExists('access_policy', 'Restricted');
    $this->assertSession()->optionNotExists('access_policy', 'Other');
    $this->assertSession()->fieldValueEquals('access_policy', 'selection_set-restricted');
    $this->assertSession()->checkboxChecked('policies_multiple[test_policy_2]');
  }

  /**
   * Load the terms on the node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   Array of term ids.
   */
  protected function getNodeTerms(EntityInterface $entity) {
    $node = $this->drupalGetNodeByTitle($entity->label());
    $terms = $node->get('field_term_reference')->referencedEntities();
    $items = [];
    foreach ($terms as $term) {
      $items[] = $term->id();
    }
    return $items;
  }

}
