<?php

namespace Drupal\Tests\access_policy\Functional;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests assigning access policies on translated entities.
 *
 * @group access_policy
 */
class AccessPolicyEntityAssignTranslationTest extends AccessPolicyTestBase {

  use TaxonomyTestTrait;

  /**
   * The tags vocabulary.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $vocabulary;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'access_policy',
    'access_policy_test',
    'language',
    'content_translation',
    'filter',
    'node',
    'taxonomy',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setup();

    // Create a tags vocabulary.
    $this->vocabulary = Vocabulary::create([
      'name' => 'tags',
      'vid' => 'tags',
    ]);
    $this->vocabulary->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_tags',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_tags',
      'bundle' => 'page',
      'translatable' => TRUE,
    ]);
    $field->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_tags',
      'entity_type' => 'user',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    // Make sure that the tags field is available on the node edit form.
    $form_display = EntityFormDisplay::load('node.page.default');
    $form_display->setComponent('field_tags', [
      'type' => 'entity_reference_autocomplete',
    ])->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_tags',
      'bundle' => 'user',
      'translatable' => TRUE,
    ]);
    $field->save();

    AccessPolicy::create([
      'id' => 'public',
      'label' => 'Public',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'weight' => 0,
    ])->save();

    $tags_policy = AccessPolicy::create([
      'id' => 'tags',
      'label' => 'Tags',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'weight' => -10,
    ]);
    $handler = $this->selectionRuleManager->getHandler('node', 'field_tags');
    $tags_policy->addHandler('selection_rule', $handler);

    $handler = $this->accessRuleManager->getHandler('node', 'field_tags');
    $tags_policy->addHandler('access_rule', $handler);
    $tags_policy->save();

    $this->entityTypeSettings = $this->container->get('access_policy.entity_type_settings');

    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'dynamic');

    \Drupal::service('router.builder')->rebuildIfNeeded();

    // Add a new language.
    ConfigurableLanguage::createFromLangcode('fr')->save();

    ContentLanguageSettings::create([
      'target_entity_type_id' => 'node',
      'target_bundle' => 'page',
    ])
      ->setThirdPartySetting('content_translation', 'enabled', TRUE)
      ->save();
  }

  /**
   * Tests changing a field on a translated entity observed by selection rules.
   *
   * This is to ensure that the access policy does not change if the field
   * value changes on that translated field.
   */
  public function testChangePolicyOnTranslationWithSelectionRule() {
    $web_user = $this->drupalCreateUser([
      'access content',
      'access administration pages',
      'access content overview',
      'translate any entity',
      'translate editable entities',
      'create content translations',
      'bypass node access',
      'assign public access policy',
      'assign tags access policy',
    ]);
    $this->drupalLogin($web_user);

    $term_1 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 1',
    ]);

    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
      'field_tags' => $term_1,
    ]);

    // Because this entity has a term it should assign the Tags access policy.
    $policy = $this->contentAccessPolicyManager->getAccessPolicy($node);
    $policy = reset($policy);
    $this->assertEquals('tags', $policy->id());

    $this->drupalGet('/fr/node/' . $node->id() . '/translations/add/en/fr');
    $this->assertSession()->statusCodeEquals(200);

    // Remove the value for the tags on the translation. This should not change
    // the access policy.
    $edit = [
      'field_tags[0][target_id]' => '',
    ];
    $this->submitForm($edit, 'Save');

    $node = $this->drupalGetNodeByTitle($node->label(), TRUE);
    $policy = $this->contentAccessPolicyManager->getAccessPolicy($node);
    $policy = reset($policy);
    $this->assertEquals('tags', $policy->id());
  }

  /**
   * Tests using the Access tab on a translated entity.
   */
  public function testAttemptToUseAccessTabOnTranslatedEntity() {
    $settings = $this->entityTypeSettings->load('node');
    $settings->set('selection_strategy', 'manual');
    $settings->set('selection_strategy_settings', [
      'allow_empty' => FALSE,
      'default_policy' => 'first_available',
      'show_operations_link' => FALSE,
    ]);
    $settings->save();

    $policy = $this->entityTypeManager->getStorage('access_policy')->load('tags');
    $rule = $policy->getAccessRule('field_tags');
    $settings = $rule['settings'];
    $settings['widget'] = [
      'show' => TRUE,
      'settings' => [
        'field_widget' => 'entity_reference_autocomplete_tags',
      ],
    ];
    $policy->updateAccessRule('field_tags', $settings);
    $policy->save();

    $web_user = $this->drupalCreateUser([
      'access content',
      'access administration pages',
      'access content overview',
      'translate any entity',
      'translate editable entities',
      'create content translations',
      'bypass node access',
      'set entity access policy',
      'assign public access policy',
      'assign tags access policy',
    ]);
    $this->drupalLogin($web_user);

    $term_1 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 1',
    ]);

    $term_2 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 2',
    ]);

    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
      'field_tags' => $term_1,
    ]);

    $node->addTranslation('fr', [
      'title' => 'Foo bar (fr)',
      'field_tags' => $term_2,
    ]);
    $node->save();

    $this->entityTypeManager->getStorage('node')->resetCache();

    // View the access tab while translating. Confirm that the fields are
    // disabled and that it's showing the original language.
    $this->drupalGet('/fr/node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldDisabled('access_policy');
    $this->assertSession()->fieldDisabled('field_tags');
    $this->assertSession()->fieldValueEquals('field_tags', $term_1->label() . ' (' . $term_1->id() . ')');
    $this->assertSession()->statusMessageContains('Access can only be changed from the original language');
    $this->assertSession()->buttonNotExists('Save');

    // View it in original language.
    $this->drupalGet('/node/' . $node->id() . '/access');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldEnabled('access_policy');
    $this->assertSession()->fieldExists('field_tags');
    $this->assertSession()->fieldValueEquals('field_tags', $term_1->label() . ' (' . $term_1->id() . ')');
    $this->assertSession()->statusMessageNotExists();
    $this->assertSession()->buttonExists('Save');
  }

}
