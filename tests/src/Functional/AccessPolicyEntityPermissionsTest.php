<?php

namespace Drupal\Tests\access_policy\Functional;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\node\Entity\NodeType;

/**
 * Ensures that access policy entity permissions work correctly.
 *
 * @group access_policy
 */
class AccessPolicyEntityPermissionsTest extends AccessPolicyTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'access_policy',
    'filter',
    'node',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setup();
    AccessPolicy::create([
      'id' => 'test_policy',
      'label' => 'Test policy',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ])->save();
    AccessPolicy::create([
      'id' => 'test_policy_2',
      'label' => 'Test policy 2',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ])->save();
    NodeType::create([
      'type' => 'page',
      'name' => 'Page',
    ])->save();

    \Drupal::service('router.builder')->rebuildIfNeeded();

  }

  /**
   * Tests view permissions restricted by access policy.
   */
  public function testViewEntityWithAccessPolicy() {
    // Create a published page. Under normal circumstances this will always
    // be visible.
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
    ]);

    $web_user = $this->drupalCreateUser();
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    // Assign an access policy to the node. This now adds in a restriction
    // to users.
    $node->set('access_policy', ['test_policy']);
    $node->save();

    // Attempt to view the content that is restricted by access policy.
    // This should not be allowed.
    $web_user = $this->drupalCreateUser();
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);

    // Grant view test_policy content. The user should be able to view the
    // content.
    $web_user = $this->drupalCreateUser([
      'view test_policy content',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

  }

  /**
   * Tests view permissions for content with an empty access policy.
   */
  public function testPermissionsWithEmptyAccessPolicy() {
    // Create a published page. Under normal circumstances this will always
    // be visible.
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
    ]);

    // Create user with no access policy permissions. They should have normal
    // drupal permissions because access policy is empty.
    $web_user = $this->drupalCreateUser();
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Create user with access policy permissions and other permissions. They
    // should have normal drupal access as well.
    $web_user = $this->drupalCreateUser([
      'delete any page content',
      'edit any page content',
      'view test_policy content',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);

  }

  /**
   * Tests permissions for editing content restricted by access policy.
   */
  public function testEditEntityWithAccessPolicy() {
    // Create a published page. Under normal circumstances this will always
    // be visible.
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
    ]);

    // Assign an access policy to the node. This now adds in a restriction
    // to users.
    $node->set('access_policy', ['test_policy']);
    $node->save();

    // Normal edit permissions alone won't give you the ability to edit content
    // that is assigned an access policy.
    $web_user = $this->drupalCreateUser([
      'edit any page content',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Just having the edit test_policy also shouldn't give you access. You
    // need to have both.
    $web_user = $this->drupalCreateUser([
      'edit test_policy content',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Having edit any page content and edit test_policy content should give
    // you access to the content.
    $web_user = $this->drupalCreateUser([
      'edit any page content',
      'edit test_policy content',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests permissions for deleting content restricted by access policy.
   */
  public function testDeleteEntityWithAccessPolicy() {
    // Create a published page. Under normal circumstances this will always
    // be visible.
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
    ]);

    // Assign an access policy to the node. This now adds in a restriction
    // to users.
    $node->set('access_policy', ['test_policy']);
    $node->save();

    // Normal delete permissions alone won't give you the ability to edit
    // content that is assigned an access policy.
    $web_user = $this->drupalCreateUser([
      'delete any page content',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Just having delete test_policy also shouldn't give you access. You
    // need to have both.
    $web_user = $this->drupalCreateUser([
      'delete test_policy content',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Having delete any page content and delete test_policy content should
    // allow you to delete the content.
    $web_user = $this->drupalCreateUser([
      'delete any page content',
      'delete test_policy content',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

}
