<?php

namespace Drupal\Tests\access_policy\Functional;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Base class for access policy functional tests.
 *
 * This is primarily added to address an issue with drupalLogin.
 */
abstract class AccessPolicyTestBase extends BrowserTestBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The access policy entity storage.
   *
   * @var \Drupal\access_policy\ContentAccessPolicyManager
   */
  protected $contentAccessPolicyManager;

  /**
   * The access rule plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $accessRuleManager;

  /**
   * The selection rule plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $selectionRuleManager;

  /**
   * The access policy entity type settings service.
   *
   * @var \Drupal\access_policy\EntityTypeSettingsInterface
   */
  protected $entityTypeSettings;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setup();

    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->contentAccessPolicyManager = \Drupal::service('access_policy.content_policy_manager');
    $this->accessRuleManager = \Drupal::service('plugin.manager.access_policy.access_rule');
    $this->selectionRuleManager = \Drupal::service('plugin.manager.access_policy.selection_rule');
    $this->entityTypeSettings = $this->container->get('access_policy.entity_type_settings');
  }

  /**
   * Helper function for assigning an access policy to a node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param mixed $access_policy
   *   The access policy.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function assignAccessPolicy(EntityInterface $entity, $access_policy) {
    if (!is_array($access_policy)) {
      $access_policy = [$access_policy];
    }
    $entity->set('access_policy', $access_policy);
    $entity->save();
  }

}
