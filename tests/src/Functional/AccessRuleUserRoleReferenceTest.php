<?php

namespace Drupal\Tests\access_policy\Functional;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\Entity\Role;

/**
 * Ensures that listings only display entities that a user is allowed to view.
 *
 * @group access_policy
 */
class AccessRuleUserRoleReferenceTest extends AccessPolicyTestBase {

  use UserCreationTrait;
  use TaxonomyTestTrait;
  use EntityReferenceTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'access_policy',
    'access_policy_test',
    'filter',
    'node',
    'taxonomy',
    'field',
    'user',
    'datetime',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The vocabulary.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $vocabulary;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setup();

    // Then create some taxonomy terms that are assigned their own policy.
    $this->vocabulary = Vocabulary::create([
      'name' => 'department',
      'vid' => 'department',
    ]);
    $this->vocabulary->save();

    // Create the user reference field.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_role_reference',
      'entity_type' => 'taxonomy_term',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'user_role',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_role_reference',
      'bundle' => 'department',
      'translatable' => FALSE,
    ]);
    $field->save();

    $this->vocabulary->save();

    // Access rules data is created based on the access controlled entity types.
    // In order to add access rules you need to create at least one access
    // policy first.
    $policy = AccessPolicy::create([
      'id' => 'user_role_reference',
      'label' => 'User role reference Access Policy',
      'target_entity_type_id' => 'taxonomy_term',
    ]);
    $policy->save();
    $policy->addHandler('access_rule', $this->accessRuleManager->getHandler('taxonomy_term', 'field_role_reference'));
    $policy->save();

  }

  /**
   * Tests viewing a node that contains matching entity references.
   */
  public function testViewEntityWithUserRoleReferenceAccessRule() {
    $rid = $this->createRole([
      'access content',
      'access administration pages',
      'access taxonomy overview',
      'administer taxonomy',
    ], 'taxonomy_admin');
    $taxonomy_admin = Role::load($rid);

    $rid = $this->createRole([
      'access content',
      'access administration pages',
      'access taxonomy overview',
      'administer taxonomy',
      'view user_role_reference taxonomy term',
    ], 'department_admin');
    $dept_admin = Role::load($rid);

    $term_1 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 1',
      'field_role_reference' => [
        $dept_admin,
      ],
      'status' => 1,
      'access_policy' => ['user_role_reference'],
    ]);

    $term_2 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 2',
      'status' => 1,
      'access_policy' => ['user_role_reference'],
    ]);

    // Only the department admin can view Term 1.
    $web_user_1 = $this->createUser();
    $web_user_1->addRole($dept_admin->id());
    $web_user_1->save();

    $this->drupalLogin($web_user_1);
    $this->drupalGet('taxonomy/term/' . $term_1->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('taxonomy/term/' . $term_2->id());
    $this->assertSession()->statusCodeEquals(404);

    $this->drupalGet('admin/structure/taxonomy/manage/' . $this->vocabulary->id() . '/overview');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContainsOnce('Term 1');
    $this->assertSession()->pageTextNotContains('Term 2');

    // Normally the taxonomy admin can edit all terms, in this case it can't
    // edit Term 1 or Term 2 because they have an access policy.
    $web_user_2 = $this->createUser();
    $web_user_2->addRole($taxonomy_admin->id());
    $web_user_2->save();

    $this->drupalLogin($web_user_2);
    $this->drupalGet('taxonomy/term/' . $term_1->id());
    $this->assertSession()->statusCodeEquals(404);

    $this->drupalGet('taxonomy/term/' . $term_2->id());
    $this->assertSession()->statusCodeEquals(404);

    $this->drupalGet('admin/structure/taxonomy/manage/' . $this->vocabulary->id() . '/overview');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextNotContains('Term 1');
    $this->assertSession()->pageTextNotContains('Term 2');
  }

}
