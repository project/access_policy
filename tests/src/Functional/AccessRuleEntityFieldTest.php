<?php

namespace Drupal\Tests\access_policy\Functional;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Ensures that listings only display entities that a user is allowed to view.
 *
 * @group access_policy
 */
class AccessRuleEntityFieldTest extends AccessPolicyTestBase {

  use UserCreationTrait;
  use EntityReferenceTestTrait;
  use TaxonomyTestTrait;

  /**
   * The tags vocabulary.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $vocabulary;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'access_policy',
    'access_policy_test',
    'filter',
    'node',
    'taxonomy',
    'field',
    'user',
    'datetime',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setup();

    // Create a tags vocabulary.
    $this->vocabulary = Vocabulary::create([
      'name' => 'tags',
      'vid' => 'tags',
    ]);
    $this->vocabulary->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_tags',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'field_name' => 'field_tags',
      'bundle' => 'page',
      'translatable' => FALSE,
    ]);
    $field->save();

    \Drupal::service('router.builder')->rebuildIfNeeded();
  }

  /**
   * Tests viewing a node that has hardcoded values in the access rule.
   */
  public function testEntityFieldStandardWithValue() {
    $policy = AccessPolicy::create([
      'id' => 'matching_string',
      'label' => 'Matching String Access Policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_text');
    $access_rule->setSettings([
      'value' => 'Foo',
      'operator' => '=',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $node_1 = $this->drupalCreateNode([
      'title' => 'Node 1 (With text: Foo)',
      'type' => 'page',
      'status' => 1,
      'field_text' => [
        'Foo',
      ],
      'access_policy' => ['matching_string'],
    ]);

    $node_2 = $this->drupalCreateNode([
      'title' => 'Node 2 (With text: Bar)',
      'type' => 'page',
      'status' => 1,
      'field_text' => [
        'Bar',
      ],
      'access_policy' => ['matching_string'],
    ]);

    $node_3 = $this->drupalCreateNode([
      'title' => 'Node 3 (Without text)',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['matching_string'],
    ]);

    // We're not comparing field values with a user so it's not necessary to set
    // it.
    $admin_user = $this->drupalCreateUser([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view matching_string content',
    ]);
    $admin_user->save();

    $this->drupalLogin($admin_user);
    $this->drupalGet('node/' . $node_1->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('node/' . $node_2->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('node/' . $node_3->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('admin/content');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContainsOnce('Node 1 (With text: Foo)');
    $this->assertSession()->pageTextNotContains('Node 2 (With text: Bar)');
    $this->assertSession()->pageTextContainsOnce('Node 3 (Without text)');
  }

  /**
   * Tests the field value access rule with entity reference fields.
   */
  public function testEntityFieldEntityReferenceWithValue() {
    $this->createEntityReferenceField('node', 'page', 'field_entity_reference', 'Entity Reference', 'node', 'default', [], -1);

    $policy = AccessPolicy::create([
      'id' => 'match_reference_value',
      'label' => 'Matching entity reference with value',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    $access_rule = $this->accessRuleManager->getHandler('node', 'field_entity_reference');
    $access_rule->setSettings([
      'value' => [
        ['target_id' => 1],
      ],
      'operator' => 'in',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $referenced_node_1 = $this->drupalCreateNode([
      'nid' => 1,
      'title' => 'Referenced Node #1',
      'type' => 'page',
      'status' => 1,
    ]);

    $referenced_node_2 = $this->drupalCreateNode([
      'title' => 'Referenced Node #1',
      'type' => 'page',
      'status' => 1,
    ]);

    // This is referencing node 1 in the access policy and should be visible.
    $node_1 = $this->drupalCreateNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_entity_reference' => [
        $referenced_node_1,
      ],
      'access_policy' => ['match_reference_value'],
    ]);

    // This is referencing a different node and should not be visible.
    $node_2 = $this->drupalCreateNode([
      'title' => 'Node 2',
      'type' => 'page',
      'status' => 1,
      'field_entity_reference' => [
        $referenced_node_2,
      ],
      'access_policy' => ['match_reference_value'],
    ]);

    // This has no value and will therefore be visible.
    $node_3 = $this->drupalCreateNode([
      'title' => 'Node 3',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['match_reference_value'],
    ]);

    $web_user = $this->drupalCreateUser([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view match_reference_value content',
    ]);

    // We're not matching values on the user, so we don't need to assign it.
    $this->drupalLogin($web_user);
    $this->drupalGet('node/' . $node_1->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('node/' . $node_2->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('node/' . $node_3->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('admin/content');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContainsOnce('Node 1');
    $this->assertSession()->pageTextNotContains('Node 2');
    $this->assertSession()->pageTextContains('Node 3');

  }

  /**
   * Tests the entity reference field with multiple values.
   *
   * Entities with multiple field values can cause duplicates on listing pages.
   */
  public function testEntityFieldEntityReferenceWithMultipleValues() {
    $term_1 = $this->createTerm($this->vocabulary, [
      'title' => 'Term 1',
    ]);

    $term_2 = $this->createTerm($this->vocabulary, [
      'title' => 'Term 2',
    ]);

    $term_3 = $this->createTerm($this->vocabulary, [
      'title' => 'Term 3',
    ]);

    $policy = AccessPolicy::create([
      'id' => 'tags',
      'label' => 'Tags access policy',
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();
    $access_rule = $this->accessRuleManager->getHandler('node', 'field_tags');
    $access_rule->setSettings([
      'value' => [
        ['target_id' => $term_1->id()],
        ['target_id' => $term_2->id()],
      ],
      'operator' => 'in',
      'query' => TRUE,
    ]);
    $policy->addHandler('access_rule', $access_rule);
    $policy->save();

    $node_1 = $this->drupalCreateNode([
      'title' => 'Node 1',
      'type' => 'page',
      'status' => 1,
      'field_tags' => [
        $term_1,
        $term_2,
      ],
      'access_policy' => ['tags'],
    ]);

    // This is assigned the tags access policy but doesn't have any tags.
    // Empty value is allowed, so it should still be visible.
    $this->drupalCreateNode([
      'title' => 'Node 2',
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['tags'],
    ]);

    // This is not assigned an access policy and should still be visible.
    $this->drupalCreateNode([
      'title' => 'Node 3',
      'type' => 'page',
      'status' => 1,
    ]);

    // This is assigned a tag that is not supported and therefore should not be
    // visible.
    $this->drupalCreateNode([
      'title' => 'Node 4',
      'type' => 'page',
      'status' => 1,
      'field_tags' => [
        $term_3,
      ],
      'access_policy' => ['tags'],
    ]);

    $web_user = $this->drupalCreateUser([
      'access content',
      'access administration pages',
      'access content overview',
      'administer nodes',
      'view tags content',
    ]);

    $this->drupalLogin($web_user);

    $this->drupalGet('admin/content');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContainsOnce('Node 1');
    $this->assertSession()->pageTextContainsOnce('Node 2');
    $this->assertSession()->pageTextContainsOnce('Node 3');
    $this->assertSession()->pageTextNotContains('Node 4');

    // Change the cardinality to one. This should also never cause duplicates.
    $node_1->set('field_tags', [
      $term_1,
    ]);
    $node_1->save();

    $storage = FieldStorageConfig::loadByName('node', 'field_tags');
    $storage->setCardinality(1);
    $storage->save();

    $this->drupalGet('admin/content');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContainsOnce('Node 1');
    $this->assertSession()->pageTextContainsOnce('Node 2');
    $this->assertSession()->pageTextContainsOnce('Node 3');
    $this->assertSession()->pageTextNotContains('Node 4');
  }

}
