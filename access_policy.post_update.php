<?php

/**
 * @file
 * Post update functions for access policy.
 */

/**
 * Add support for revisions.
 */
function access_policy_post_update_revision_support(&$sandbox) {
  $connection = \Drupal::database();
  $entityTypeManager = \Drupal::service('entity_type.manager');
  $entity_types = $entityTypeManager->getDefinitions();

  // Initialize the batch for each entity type.
  // Because we're doing raw mysql updates they are much faster and lighter
  // than going through the entity api. Batching per entity type should be
  // sufficient.
  if (!isset($sandbox['total'])) {

    $supported_entity_types = [];
    foreach ($entity_types as $entity_type) {
      $old_table = $entity_type->id() . '_access_policy';
      $old_table_exists = $connection->schema()->tableExists($old_table);
      if (\Drupal::service('access_policy.information')->isSupportedEntityType($entity_type) && $old_table_exists) {
        $old_storage_exists = $connection->select($old_table)
          ->countQuery()
          ->execute()
          ->fetchField() > 0;
        if ($old_storage_exists) {
          $supported_entity_types[] = $entity_type;
        }
      }
    }

    $sandbox['total'] = count($supported_entity_types);
    $sandbox['current'] = 0;
    $sandbox['entity_types'] = $supported_entity_types;
  }

  if (!empty($sandbox['entity_types'])) {
    $current = $sandbox['current'];
    $entity_type = $sandbox['entity_types'][$current];

    $old_table = $entity_type->id() . '_access_policy';
    $table = $entity_type->id() . '__access_policy';

    // First update the deltas of the original table.
    $select = $connection->select($old_table, 'cap');
    $select->addField('cap', 'entity_id');
    $select->addField('cap', 'access_policy');
    $results = $select->execute();

    $data = $results->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($data)) {
      // Reset the deltas.
      $deltas = [];
      foreach ($data as $index => $row) {
        $entity_id = $row['entity_id'];
        if (!isset($deltas[$entity_id])) {
          $deltas[$entity_id] = 0;
        }
        $data[$index]['delta'] = (int) $deltas[$entity_id];
        $deltas[$entity_id]++;
      }

      foreach ($data as $row) {
        $query = $connection->update($old_table)
          ->fields([
            'delta' => $row['delta'],
          ])
          ->condition('entity_id', $row['entity_id'])
          ->condition('access_policy', $row['access_policy']);
        $query->execute();
      }
    }

    // Only copy the content over if the original storage is not empty and the
    // new storage is empty.
    $new_storage_empty = $connection->select($table)
      ->countQuery()
      ->execute()
      ->fetchField() == 0;
    if ($new_storage_empty) {
      $base_table = $entity_type->getBaseTable();
      $bundle_key = $entity_type->getKey('bundle');
      $id_key = $entity_type->getKey('id');
      $revision_key = $entity_type->getKey('revision');
      $langcode_key = $entity_type->getKey('langcode');

      // Do a bulk insert of values into the new table.
      $query = $connection->select($old_table, 'cap');
      $query->addExpression("[et].[{$bundle_key}]", 'bundle');
      $query->addExpression(0, 'deleted');
      $query->addExpression('[cap].[entity_id]', 'entity_id');
      $query->addExpression("[et].[{$revision_key}]", 'revision_id');
      $query->addExpression("[et].[{$langcode_key}]", 'langcode');
      $query->addExpression('[cap].delta', 'delta');
      $query->addExpression('[cap].[access_policy]', 'access_policy_target_id');

      $query->condition('cap.access_policy', NULL, 'IS NOT NULL');
      $query->join($base_table, 'et', "et.{$id_key} = cap.entity_id");

      // Insert into data table.
      $connection->insert($table)
        ->from($query)
        ->execute();

      // Insert into revisions table.
      $connection->insert($entity_type->id() . '_revision__access_policy')
        ->from($query)
        ->execute();
    }

    $sandbox['current']++;
    \Drupal::messenger()->addStatus('Updated ' . $entity_type->id());

    // Drop the old table.
    $old_table = $entity_type->id() . '_access_policy';
    $old_table_exists = $connection->schema()->tableExists($old_table);
    if (\Drupal::service('access_policy.information')->isSupportedEntityType($entity_type) && $old_table_exists) {
      $connection->schema()->dropTable($old_table);
    }
  }

  if ($sandbox['current'] >= $sandbox['total']) {
    $sandbox['#finished'] = 1;
  }
  else {
    $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);
  }
}

/**
 * Drop all deprecated tables.
 */
function access_policy_post_update_table_drop_deprecated(&$sandbox) {
  $entityTypeManager = \Drupal::service('entity_type.manager');
  $entity_types = $entityTypeManager->getDefinitions();
  $connection = \Drupal::database();

  foreach ($entity_types as $entity_type) {
    $old_table = $entity_type->id() . '_access_policy';
    $old_table_exists = $connection->schema()->tableExists($old_table);
    if (\Drupal::service('access_policy.information')->isSupportedEntityType($entity_type) && $old_table_exists) {
      $connection->schema()->dropTable($old_table);
    }
  }
}
