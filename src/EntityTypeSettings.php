<?php

namespace Drupal\access_policy;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Handles the access policy settings for specific entity types.
 */
class EntityTypeSettings implements EntityTypeSettingsInterface {

  use StringTranslationTrait;

  /**
   * Access policy settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The entity type we're updating settings for.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The entity type settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * Fallback entity type settings.
   *
   * Normally the default settings are provided in access_policy.settings.
   * However, in the event that that data is missing we can fall back to these
   * settings.
   *
   * @var array
   */
  protected $fallbackSettings = [
    'selection_strategy' => 'dynamic',
    'selection_strategy_settings' => [],
    'selection_sets' => [],
  ];

  /**
   * Constructs a new EntityTypeSettings service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->getEditable('access_policy.settings');
    $this->settings = $this->config->get('entity_type_settings');
  }

  /**
   * {@inheritdoc}
   */
  public function load($entity_type) {
    $this->entityType = $entity_type;
    // This ensures that the entity type has the same settings on
    // initialization. Note that it only gets saved if the settings are
    // different from default.
    if (empty($this->settings[$this->entityType])) {
      $this->settings[$this->entityType] = $this->getDefaultSettings();
    }
    return $this;
  }

  /**
   * Get default settings.
   *
   * If, for whatever reason, the default settings do not exist in config then
   * it falls back to the fallback settings.
   *
   * @return array
   *   Array of entity type settings.
   */
  protected function getDefaultSettings() {
    if (isset($this->settings['default'])) {
      return $this->settings['default'];
    }

    return $this->fallbackSettings;
  }

  /**
   * {@inheritdoc}
   */
  public function get($key) {
    if (isset($this->settings[$this->entityType][$key])) {
      return $this->settings[$this->entityType][$key];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function set($key, $value) {
    $this->settings[$this->entityType][$key] = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    foreach ($this->settings as $entity_type => $settings) {
      if ($entity_type == 'default') {
        continue;
      }
      // If the settings are identical then do not save unique settings.
      if ($this->isEntityTypeSameAsDefault($settings)) {
        unset($this->settings[$entity_type]);
      }
    }

    $this->config->set('entity_type_settings', $this->settings);
    $this->config->save();

    return $this;
  }

  /**
   * Determine whether the settings being saved are the same as default.
   *
   * @param array $settings
   *   Array of entity type settings.
   *
   * @return bool
   *   TRUE if the settings are different; FALSE otherwise.
   */
  protected function isEntityTypeSameAsDefault(array $settings) {
    $default = $this->getDefaultSettings();
    foreach ($settings as $key => $value) {
      if (!array_key_exists($key, $default)) {
        return FALSE;
      }
      if ($value != $default[$key]) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
