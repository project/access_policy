<?php

namespace Drupal\access_policy;

/**
 * Class to manage and lazy load cached access policy data.
 */
interface AccessPolicyDataInterface {

  /**
   * Gets all access policy data.
   *
   * @return array
   *   An array of access policy handler data.
   */
  public function getAll();

  /**
   * Get all the handlers in a particular group.
   *
   * @param string $group
   *   The group.
   *
   * @return array
   *   The array of access policy handler data.
   */
  public function getGroup($group);

  /**
   * Gets data for a particular handler.
   *
   * @param string $group
   *   The group.
   * @param string $key
   *   The key of the cache entry to retrieve.
   *
   * @return array
   *   An array of access policy data.
   *
   * @throws \Exception
   *   It will throw an exception if no data is found.
   */
  public function getHandler($group, $key);

  /**
   * Clears the class storage and cache.
   */
  public function clear();

}
