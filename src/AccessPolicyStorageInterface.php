<?php

namespace Drupal\access_policy;

/**
 * Interface for storage of "access policy" configuration entities.
 */
interface AccessPolicyStorageInterface {

  /**
   * Get the access policies enabled for an entity type.
   *
   * This is a helper method for finding all the access policies that have
   * been enabled for a given entity type. (e.g node, media etc.)
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of access policies.
   */
  public function loadByEntityType($entity_type_id);

}
