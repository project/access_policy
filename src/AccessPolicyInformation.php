<?php

namespace Drupal\access_policy;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Service for easily getting information about access policy support.
 *
 * @package Drupal\access_policy
 */
class AccessPolicyInformation {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Static cache of access policies; keyed by entity type.
   *
   * @var array
   */
  protected $cache = [];

  /**
   * Constructs a new Access Policy Information service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Determine whether this entity is under access policy access control.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return bool
   *   TRUE if this entity supports access policies; FALSE otherwise.
   */
  public function isAccessControlledEntity(EntityInterface $entity) {
    if ($this->hasEnabledForEntitiesOfEntityType($entity->getEntityType())) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine whether this entity type is under access policy access control.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return bool
   *   TRUE if this entity type supports access policies; FALSE otherwise.
   */
  public function isAccessControlledEntityType(EntityTypeInterface $entity_type) {
    $policies = $this->getEnabledForEntitiesOfEntityType($entity_type);
    if (!empty($policies)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Determine whether an entity type has any enabled access policies.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   An entity type object.
   *
   * @return bool
   *   TRUE if this entity type has access policies enabled, FALSE otherwise.
   */
  public function hasEnabledForEntitiesOfEntityType(EntityTypeInterface $entity_type) {
    $enabled = $this->getEnabledForEntitiesOfEntityType($entity_type);
    if (!empty($enabled)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Get enabled access policies for entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return array
   *   Array of access policy entities.
   */
  public function getEnabledForEntitiesOfEntityType(EntityTypeInterface $entity_type) {
    if (!isset($this->cache[$entity_type->id()])) {
      $this->cache[$entity_type->id()] = [];
      $policies = $this->entityTypeManager->getStorage('access_policy')->loadMultiple();
      foreach ($policies as $policy) {
        $this->cache[$policy->getTargetEntityTypeId()][] = $policy;
      }
    }

    return $this->cache[$entity_type->id()];
  }

  /**
   * Get enabled access policies for entity type.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $policy
   *   The access policy entity.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *   The bundle.
   *
   * @return bool
   *   TRUE if it's enabled for an entity type; FALSE otherwise.
   */
  public function isEnabledForEntitiesOfEntityType(AccessPolicyInterface $policy, EntityTypeInterface $entity_type) {
    if ($policy->getTargetEntityTypeId() != $entity_type->id()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns all the entity types with access policies.
   *
   * @return array
   *   Array of entity types.
   */
  public function getAllEnabledEntityTypes() {
    $entity_types = [];
    $entities = $this->entityTypeManager->getStorage('access_policy')->loadMultiple();
    foreach ($entities as $entity) {
      if (!isset($entity_types[$entity->getTargetEntityTypeId()])) {
        $entity_types[$entity->getTargetEntityTypeId()] = $this->entityTypeManager->getDefinition($entity->getTargetEntityTypeId());
      }
    }

    return $entity_types;
  }

  /**
   * Determine whether the entity type is supported.
   *
   * @return bool
   *   TRUE if the entity type is supported; FALSE otherwise.
   */
  public function isSupportedEntityType(EntityTypeInterface $entity_type) {
    $supports_bundles = $entity_type->getBundleEntityType();
    $has_view_builder = ($entity_type->get('field_ui_base_route') && $entity_type->hasViewBuilderClass());
    $id_valid = $this->isIdKeyValid($entity_type);
    if ($entity_type instanceof ContentEntityTypeInterface && !$entity_type->isInternal() && $supports_bundles && $has_view_builder && $id_valid && !$this->isExcluded($entity_type)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Exclude certain core entity types.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The current entity type.
   *
   * @return bool
   *   TRUE if the entity type is supported; FALSE otherwise.
   */
  private function isExcluded(EntityTypeInterface $entity_type) {
    // The comment entity does not yet support proper access control. There is
    // ongoing work to get it supported. Once that lands this will be removed.
    // @see https://www.drupal.org/project/drupal/issues/2879087
    $excluded = ['comment'];

    return in_array($entity_type->id(), $excluded);
  }

  /**
   * Determine whether the entity id key is valid.
   *
   * This is a workaround for some contrib modules which, for some reason,
   * don't use an integer for content entity type unique ids. Ideally we should
   * check the field type but that's not possible without causing an infinite
   * loop.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return bool
   *   TRUE if the id is valid; FALSE otherwise.
   */
  private function isIdKeyValid(EntityTypeInterface $entity_type) {
    return (substr_count($entity_type->getKey('id'), 'id') > 0);
  }

  /**
   * Reset the static cache.
   */
  public function resetCache() {
    $this->cache = [];
  }

}
