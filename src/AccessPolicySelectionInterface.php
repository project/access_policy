<?php

namespace Drupal\access_policy;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a listing of AccessPolicy type entities.
 */
interface AccessPolicySelectionInterface {

  /**
   * Validate the manage access operation.
   *
   * Assigning an access policy uses permissions and selection rules instead
   * of permissions and access rules. Therefore, this is part of the selection
   * handler and not the validator.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current logged in user.
   *
   * @return bool
   *   TRUE if the user is allowed to assign an access policy; FALSE otherwise.
   */
  public function validateAssignAccessPolicy(EntityInterface $entity, AccountInterface $account);

  /**
   * Get access policies available to the user.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current logged in user.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of permitted access policies.
   */
  public function getAllowedAccessPolicies(EntityInterface $entity, AccountInterface $account);

  /**
   * Load the default access policy.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current logged in user.
   *
   * @return bool|\Drupal\access_policy\Entity\AccessPolicyInterface
   *   The default policy or FALSE if none exists.
   */
  public function getDefaultPolicy(EntityInterface $entity, AccountInterface $account);

  /**
   * Get all applicable policies for assignment.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current logged in user.
   *
   * @return bool|array
   *   The applicable policies or FALSE if none exists.
   */
  public function getApplicablePolicies(EntityInterface $entity, AccountInterface $account);

  /**
   * Get the current selection set from the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return string|bool
   *   The current selection set or FALSE if not applicable. It can also return
   *   '_null' if more than on policy was found but it doesn't match an existing
   *   set.
   */
  public function getSelectionSetFromEntity(EntityInterface $entity);

  /**
   * Get the access policies in a selection set.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $name
   *   The selection set name.
   *
   * @return array
   *   The array of access policies in the selection set.
   */
  public function getPoliciesInSelectionSet($entity_type, $name);

  /**
   * Get all selection sets for a given entity type.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return array
   *   The selection sets array.
   */
  public function getSelectionSets($entity_type);

  /**
   * Get a specific selection set.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $name
   *   The name of the selection set.
   *
   * @return array|false
   *   The selection set array or FALSE if not found.
   */
  public function getSelectionSet($entity_type, $name);

}
