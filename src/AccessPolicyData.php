<?php

namespace Drupal\access_policy;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class to manage and lazy load cached access policy data.
 */
class AccessPolicyData implements AccessPolicyDataInterface {

  use StringTranslationTrait;

  /**
   * The base cache ID to use.
   *
   * @var string
   */
  protected $baseCid = 'access_policy_data';

  /**
   * The hook name.
   *
   * @var string
   */
  protected $hookName = 'access_policy_data';

  /**
   * The cache backend to use.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The access policy data storage.
   *
   * @var array
   */
  protected $storage = [];

  /**
   * Stores a module manager to invoke hooks.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs this AccessPolicyData object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler class to use for invoking hooks.
   */
  public function __construct(CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $this->cacheBackend = $cache_backend;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function getAll() {
    if (empty($this->storage)) {
      $this->storage = $this->getData();
    }

    return $this->storage;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup($group) {
    if (empty($this->storage)) {
      $this->storage = $this->getData();
    }
    $storage = [];
    foreach ($this->storage as $id => $item) {
      if (substr_count($id, $group . ':') > 0) {
        $storage[$id] = $item;
      }
    }

    return $storage;
  }

  /**
   * {@inheritdoc}
   */
  public function getHandler($group, $key) {
    if (!isset($this->storage[$group . ':' . $key])) {
      $this->storage = $this->getData();
    }
    if ($this->dataExists($group, $key)) {
      return $this->storage[$group . ':' . $key];
    }

    return [];
  }

  /**
   * Determine whether the data exists.
   *
   * @param string $group
   *   The group.
   * @param string $key
   *   The key of the cache entry.
   *
   * @return bool
   *   TRUE if the data exists; an exception if it does not.
   */
  private function dataExists($group, $key) {
    if (empty($this->storage[$group . ':' . $key])) {
      $all_keys = array_keys($this->storage);
      $available = implode(', ', $all_keys);
      throw new \Exception(sprintf('Access policy data not found for "%1$s" in "%2$s". Available definitions include: %3$s.', $key, $group, $available));
    }
    return TRUE;
  }

  /**
   * Gets all data invoked by hook_views_data().
   *
   * This is requested from the cache before being rebuilt.
   *
   * @return array
   *   An array of all data.
   */
  protected function getData() {
    if ($data = $this->cacheGet($this->baseCid)) {
      return $data->data;
    }

    $data = [];
    $this->moduleHandler->invokeAllWith($this->hookName, function (callable $hook, string $module) use (&$data) {
      $access_policy_data = $hook();

      // Make sure that each access rule has the provider.
      foreach ($access_policy_data as $type => $access_data) {
        foreach ($access_data as $id => $rule) {
          if (!isset($rule['provider'])) {
            $access_policy_data[$type][$id]['provider'] = $module;
          }
        }
      }

      if (is_array($access_policy_data)) {
        $data = NestedArray::mergeDeep($data, $access_policy_data);
      }
    });

    // Allow other modules to manipulate the data.
    $this->moduleHandler->alter($this->hookName, $data);

    // Normalize the data into a flattened array.
    $storage = [];
    foreach ($data as $group => $items) {
      foreach ($items as $id => $item) {
        $storage[$group . ':' . $id] = $item;
      }
    }

    $this->cacheSet($this->baseCid, $storage);

    $this->storage = $storage;

    return $this->storage;
  }

  /**
   * Gets data from the cache backend.
   *
   * @param string $cid
   *   The cache ID to return.
   *
   * @return mixed
   *   The cached data, if any.
   */
  protected function cacheGet($cid) {
    return $this->cacheBackend->get($cid);
  }

  /**
   * Sets data to the cache backend.
   *
   * @param string $cid
   *   The cache ID to set.
   * @param mixed $data
   *   The data that will be cached.
   */
  protected function cacheSet($cid, $data) {
    return $this->cacheBackend->set($cid, $data, Cache::PERMANENT, [
      $this->baseCid,
      'config:core.extension',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function clear() {
    $this->storage = [];
    $this->cacheBackend->delete($this->baseCid);
  }

}
