<?php

namespace Drupal\access_policy\Session;

use Drupal\Core\Session\AccountInterface;

/**
 * Defines the UserFieldValuesHashGenerator service.
 *
 * This service inspects any fields that are being observed by access rules and
 * fetches and hashes the values so that we can use them with the cache
 * context.
 */
interface UserFieldValuesHashGeneratorInterface {

  /**
   * Generate the hashed user field values.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param string $field_name
   *   The field name.
   *
   * @return string
   *   The hashed user field values.
   */
  public function generate(AccountInterface $account, $field_name = NULL);

}
