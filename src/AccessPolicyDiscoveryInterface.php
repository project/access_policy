<?php

namespace Drupal\access_policy;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Handles the discovery of access policies for a user.
 */
interface AccessPolicyDiscoveryInterface {

  /**
   * Get access policies available to the user.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current logged in user.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of permitted access policies.
   */
  public function getAllowedAccessPolicies(EntityInterface $entity, AccountInterface $account);

  /**
   * Get all applicable policies for assignment.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current logged in user.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The applicable policies or FALSE if none exists.
   */
  public function getApplicablePolicies(EntityInterface $entity, AccountInterface $account);

}
