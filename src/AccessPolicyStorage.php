<?php

namespace Drupal\access_policy;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Storage controller class for "access policy" configuration entities.
 */
class AccessPolicyStorage extends ConfigEntityStorage implements AccessPolicyStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByEntityType($entity_type_id) {
    // See if access policy is supported.
    $access_policies = $this->loadMultiple();
    return array_filter($access_policies, function ($policy) use ($entity_type_id) {
      if ($policy->getTargetEntityTypeId() == $entity_type_id) {
        return TRUE;
      }
      return FALSE;
    });
  }

}
