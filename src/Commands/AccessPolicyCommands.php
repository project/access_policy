<?php

namespace Drupal\access_policy\Commands;

use Drupal\access_policy\AccessPolicyOperationPluginManager;
use Drupal\access_policy\AccessPolicyValidatorInterface;
use Drupal\access_policy\ContentAccessPolicyManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Access policy drush commands.
 *
 * Class AccessPolicyCommands.
 *
 * @package Drupal\access_policy\Commands
 */
class AccessPolicyCommands extends DrushCommands {

  /**
   * Status severity -- Status is ok.
   */
  const STATUS_OK = 0;

  /**
   * Status severity -- Warning condition.
   */
  const STATUS_WARNING = 1;

  /**
   * Status severity -- Error condition.
   */
  const STATUS_ERROR = 2;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The content access policy manager service.
   *
   * @var \Drupal\access_policy\ContentAccessPolicyManager
   */
  protected $contentAccessPolicyManager;

  /**
   * The access policy validator service.
   *
   * @var \Drupal\access_policy\AccessPolicyValidatorInterface
   */
  protected $accessPolicyValidator;

  /**
   * The operation plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyOperationPluginManager
   */
  protected $operationPluginManager;

  /**
   * Constructs a AccessPolicyCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\ContentAccessPolicyManager $content_access_policy_manager
   *   The ContentAccessPolicyManager service.
   * @param \Drupal\access_policy\AccessPolicyValidatorInterface $access_policy_validator
   *   The access policy validator service.
   * @param \Drupal\access_policy\AccessPolicyOperationPluginManager $operation_plugin_manager
   *   The operation plugin manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ContentAccessPolicyManager $content_access_policy_manager, AccessPolicyValidatorInterface $access_policy_validator, AccessPolicyOperationPluginManager $operation_plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->contentAccessPolicyManager = $content_access_policy_manager;
    $this->accessPolicyValidator = $access_policy_validator;
    $this->operationPluginManager = $operation_plugin_manager;
  }

  /**
   * Check a user's access against an entity.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   The entity id.
   * @param string $username
   *   The user name.
   *
   * @command access-policy:check-access
   * @usage access-policy:check-access node 123 jsmith
   *   Check the user's access of an entity.
   *
   * @aliases apca
   */
  public function checkAccess($entity_type, $entity_id, $username) {
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    if (empty($entity)) {
      $this->logger()->error('That entity does not exist.');
      return;
    }

    // Get the user account.
    $users = $this->entityTypeManager->getStorage('user')->loadByProperties([
      'name' => $username,
    ]);

    if (empty($users)) {
      $this->logger()->error('That user does not exist.');
      return;
    }

    $account = reset($users);

    $access_policies = $this->contentAccessPolicyManager->getAccessPolicy($entity);
    if (!empty($access_policies)) {
      $policy_labels = array_map(function ($policy) {
        return $policy->label();
      }, $access_policies);
      $labels = implode(', ', $policy_labels);

      $this->io()->block(sprintf('Access policies assigned to this entity: %s', $labels));
    }
    else {
      $this->io()->block(sprintf('No access policies are assigned to this entity.'));
    }

    // It's checked all the prerequisites, now it's checking the access.
    // Note that it doesn't include all the operations observed by access policy
    // (e.g. view unpublished) because those aren't technically operations.
    $operation_definitions = $this->operationPluginManager->getApplicableDefinitions($entity->getEntityType());
    $operations = array_map(function ($definition) {
      return $definition['operation'];
    }, $operation_definitions);
    $operations = array_unique($operations);

    $rows = [];
    foreach ($operations as $operation) {
      $result = $entity->access($operation, $account, TRUE);
      if ($result->isAllowed()) {
        $violations = "";
        $has_access = 'Yes';
        $severity = 0;
      }
      else {
        $violations = $result->getReason();
        $has_access = 'No';
        $severity = 1;
      }

      $rows[] = [
        'operation' => $operation,
        'has_access' => self::styleRow($has_access, $severity),
        'violations' => $violations,
      ];

    }
    $this->io()->table(['Operation', 'Access', 'Message'], $rows);
  }

  /**
   * Style the row.
   *
   * @param string $content
   *   The content.
   * @param int $severity
   *   The severity.
   *
   * @return string
   *   The styled row.
   */
  private static function styleRow($content, $severity) {
    switch ($severity) {
      case self::STATUS_OK:
        return '<info>' . $content . '</>';

      case self::STATUS_WARNING:
        return '<comment>' . $content . '</>';

      case self::STATUS_ERROR:
        return '<fg=red>' . $content . '</>';

      default:
        return $content;
    }
  }

}
