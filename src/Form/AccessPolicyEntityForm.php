<?php

namespace Drupal\access_policy\Form;

use Drupal\access_policy\AccessPolicyHandlerManager;
use Drupal\access_policy\AccessPolicyInformation;
use Drupal\access_policy\AccessPolicySelection;
use Drupal\access_policy\AccessPolicySelectionInterface;
use Drupal\access_policy\AccessRuleWidgetPluginManager;
use Drupal\access_policy\ContentAccessPolicyManager;
use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\access_policy\Plugin\access_policy\AccessRule\AccessRulePluginInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to select an active Access Policy.
 */
class AccessPolicyEntityForm extends EntityForm {

  /**
   * The ContentAccessPolicyManager service.
   *
   * @var \Drupal\access_policy\ContentAccessPolicyManager
   */
  protected $contentAccessPolicyManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The AccessPolicyHandlerManager service.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $accessRulePluginManager;

  /**
   * The AccessRuleWidgetPluginManager service.
   *
   * @var \Drupal\access_policy\AccessRuleWidgetPluginManager
   */
  protected $accessRuleWidgetPluginManager;

  /**
   * Access policy selection service.
   *
   * @var \Drupal\access_policy\AccessPolicySelectionInterface
   */
  protected $policySelection;

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $accessPolicyInfo;

  /**
   * Constructs a AccessPolicyEntityForm object.
   *
   * @param \Drupal\access_policy\ContentAccessPolicyManager $content_access_policy_manager
   *   The ContentAccessPolicyManager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\AccessPolicyHandlerManager $access_rule_manager
   *   The access rule manager service.
   * @param \Drupal\access_policy\AccessRuleWidgetPluginManager $widget_plugin_manager
   *   The AccessRuleWidgetPluginManager service.
   * @param \Drupal\access_policy\AccessPolicySelectionInterface $selection
   *   The access policy selection service.
   * @param \Drupal\access_policy\AccessPolicyInformation $access_policy_info
   *   The access policy information service.
   */
  public function __construct(ContentAccessPolicyManager $content_access_policy_manager, EntityTypeManagerInterface $entity_type_manager, AccessPolicyHandlerManager $access_rule_manager, AccessRuleWidgetPluginManager $widget_plugin_manager, AccessPolicySelectionInterface $selection, AccessPolicyInformation $access_policy_info) {
    $this->contentAccessPolicyManager = $content_access_policy_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->accessRulePluginManager = $access_rule_manager;
    $this->accessRuleWidgetPluginManager = $widget_plugin_manager;
    $this->policySelection = $selection;
    $this->accessPolicyInfo = $access_policy_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('access_policy.content_policy_manager'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.access_policy.access_rule'),
      $container->get('plugin.manager.access_rule_widget'),
      $container->get('access_policy.selection'),
      $container->get('access_policy.information'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return $this->entity->getEntityTypeId() . '_access_policy_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'access_policy_entity_form';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    if ($this->isConfirming($form, $form_state)) {
      $this->buildConfirmForm($form, $form_state);
      return $form;
    }

    $entity = $this->getEntity();

    if (!$entity->isDefaultTranslation()) {
      $url = $entity->getUntranslated()->toUrl('access-policy-form')->toString();
      $this->messenger()->addWarning($this->t('Access can only be changed from the original language. <a href=":url">Edit it on the original language access form</a>.', [':url' => $url]));
    }

    $form['access_policy_selection'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'access-policy-settings'],
    ];

    $current_policies = $this->getCurrentAccessPolicies($entity);

    // Only display Access Policies permitted for this Content Entity Type.
    $access_policies = $this->policySelection->getAllowedAccessPolicies($entity, $this->currentUser());

    $all_policies = $this->mergeCurrentIfMissing($current_policies, $access_policies);
    $options = $this->getAccessPolicyOptions($all_policies);

    // If the current selected entities do not match a set then add the "Other"
    // option to the array of options.
    $selection_set = $this->policySelection->getSelectionSetFromEntity($entity);
    if ($selection_set == AccessPolicySelection::NULL_SET) {
      $options[$this->getSelectionSetKey($selection_set)] = $this->t('Other');
    }

    if (empty($options)) {
      $form['access_policy_selection']['empty'] = $this->buildFormEmpty($form, $form_state);
      return $form;
    }

    $current_option = $this->getCurrentOption($entity);

    // If the form is submitted then use that value instead.
    if ($this->isAjaxSubmitted($form, $form_state) || $this->isFormSubmitted($form, $form_state)) {
      $current_option = $this->getSubmittedOption($form, $form_state);
    }

    $form['access_policy_selection']['access_policy'] = [
      '#type' => 'select',
      '#title' => $this->t('Set access to:'),
      '#default_value' => ($current_option instanceof AccessPolicyInterface) ? $current_option->id() : $current_option,
      '#options' => $options,
      '#ajax' => [
        'callback' => '::loadAccessRuleSettings',
        'wrapper' => 'access-policy-settings',
        'event' => 'change',
      ],
      '#disabled' => !$this->isAllowedOrEmpty($entity, $this->currentUser()),
    ];

    if ($this->policySelection->emptyPoliciesAllowed($entity->getEntityTypeId()) || $this->contentAccessPolicyManager->isEmpty($entity)) {
      $form['access_policy_selection']['access_policy']['#empty_option'] = $this->t('- Unrestricted -');
    }

    $form['access_policy_selection']['access_policy_settings'] = [
      '#type' => 'container',
    ];

    if (!$this->isSelectionSetSelected($current_option)) {
      $selected_policy = $this->getSelectedAccessPolicies($entity, $form, $form_state);
      $this->buildFormSingle($selected_policy, $access_policies, $form['access_policy_selection'], $form_state);
    }
    else {
      $selection_set = explode('-', $current_option)[1];
      $selected_policy = $this->getSelectedAccessPolicies($entity, $form, $form_state);

      if ($selection_set == AccessPolicySelection::NULL_SET) {
        $access_policies = $selected_policy;
      }
      else {
        $access_policies = $this->policySelection->getPoliciesInSelectionSet($entity->getEntityTypeId(), $selection_set);
      }
      $this->buildFormMultiple($current_option, $selected_policy, $access_policies, $form['access_policy_selection'], $form_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    // Don't show the save button if users are not allowed to assign policies
    // from the access tab.
    if (!$this->isAllowedOrEmpty($this->getEntity(), $this->currentUser())) {
      return [];
    }

    if ($this->isConfirming($form, $form_state)) {
      $actions['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
        '#submit' => ['::submitForm', '::save'],
      ];
      $actions['cancel'] = [
        '#type' => 'submit',
        '#value' => $this->t('Cancel'),
        '#submit' => ['::cancelForm'],
      ];
    }
    else {
      $actions['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
        '#submit' => $this->isConfirmFormEnabled() ? ['::confirmForm'] : ['::submitForm', '::save'],
      ];
    }

    return $actions;
  }

  /**
   * Confirm form submit callback.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function confirmForm(array $form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $storage = [
      'confirm' => TRUE,
      'values' => $form_state->getValues(),
    ];
    $form_state->setStorage($storage);
  }

  /**
   * Cancel form submit callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function cancelForm(array $form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $form_state->setStorage([]);
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    $access_policies = $this->getSelectedAccessPolicies($entity, $form, $form_state);

    // If we're submitting the confirmation form then copy those values to the
    // entity.
    if ($this->isConfirming($form, $form_state)) {
      $storage = $form_state->getStorage();
      $form_state->setValues($storage['values']);
    }

    $policies = [];
    foreach ($access_policies as $policy) {
      $policies[] = $policy->id();
    }
    $entity->set('access_policy', $policies);

    $all_policies = $this->entityTypeManager->getStorage('access_policy')->loadByProperties([
      'target_entity_type_id' => $this->getEntity()->getEntityTypeId(),
    ]);
    foreach ($all_policies as $policy) {
      $plugins = $this->accessRulePluginManager->getApplicableHandlers($policy, $entity);
      foreach ($plugins as $plugin) {
        if ($this->isWidgetEnabled($plugin)) {
          $widget = $this->createWidgetInstance($entity, $plugin);
          $widget->buildEntity($entity, $form, $form_state);
        }
      }
    }
  }

  /**
   * Merge the current selected access policies if they are missing.
   *
   * @param array $current_policies
   *   The array of current selected policies.
   * @param array $allowed_policies
   *   The array of allowed policies.
   *
   * @return array
   *   Array of merged policies.
   */
  private function mergeCurrentIfMissing(array $current_policies, array $allowed_policies) {
    $allowed_keys = array_map(function ($policy) {
      return $policy->id();
    }, $allowed_policies);

    foreach ($current_policies as $policy) {
      if (!in_array($policy->id(), $allowed_keys)) {
        array_unshift($allowed_policies, $policy);
      }
    }

    return $allowed_policies;
  }

  /**
   * Build the form for when a single access policy can be selected.
   *
   * @param array $selected_policies
   *   The current selected policy.
   * @param array $access_policies
   *   Array of allowed access policies.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  private function buildFormSingle(array $selected_policies, array $access_policies, array &$form, FormStateInterface $form_state) {
    $selected_policy = array_pop($selected_policies) ?? [];

    // Build form elements defined by Access Rules for selected Access Policy.
    if ($selected_policy) {
      $form['access_policy']['#description'] = $selected_policy->getDescription();
      $this->buildAccessRuleSettingsForm($selected_policy, $form, $form_state);
    }
  }

  /**
   * Build the form for when multiple access policies can be selected.
   *
   * @param string $selection_set
   *   The selection set.
   * @param array $selected_policies
   *   The current selected policy.
   * @param array $access_policies
   *   Array of allowed access policies.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  private function buildFormMultiple($selection_set, array $selected_policies, array $access_policies, array &$form, FormStateInterface $form_state) {
    $form['access_policy_settings']['policies_multiple'] = [
      '#tree' => TRUE,
    ];

    $options = [];
    foreach ($access_policies as $policy) {
      $options[$policy->id()] = $policy->label();

      $is_selected = FALSE;
      foreach ($selected_policies as $selected) {
        if ($selected->id() == $policy->id()) {
          $is_selected = TRUE;
        }
      }

      $form['access_policy_settings']['policies_multiple'][$policy->id()] = [
        '#type' => 'checkbox',
        '#title' => $policy->label(),
        '#description' => $policy->getDescription(),
        '#default_value' => $is_selected,
        '#disabled' => !$this->isAllowedOrEmpty($this->getEntity(), $this->currentUser()),
      ];
    }

    foreach ($access_policies as $policy) {
      $this->buildAccessRuleSettingsForm($policy, $form, $form_state);
      if (isset($form['access_policy_settings'][$policy->id()])) {
        $form['access_policy_settings'][$policy->id()]['#states'] = [
          'visible' => [
            ':input[name="policies_multiple[' . $policy->id() . ']"]' => ['checked' => TRUE],
          ],
        ];
      }
    }
  }

  /**
   * Build confirmation form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  protected function buildConfirmForm(array &$form, FormStateInterface $form_state) {
    $policy = $this->getSelectedAccessPolicies($this->getEntity(), $form, $form_state);
    if (!empty($policy)) {
      $label = $this->getAccessPolicyLabels($policy);
    }
    else {
      $label = $this->t('Unrestricted');
    }
    $form['#title'] = $this->t('Are you sure you want to set access to @label?', ['@label' => $label]);
  }

  /**
   * Build the access rule settings form.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  private function buildAccessRuleSettingsForm(AccessPolicyInterface $access_policy, array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $applicable_plugins = $this->accessRulePluginManager->getApplicableHandlers($access_policy, $entity);
    foreach ($applicable_plugins as $plugin_name => $plugin) {
      if ($this->isWidgetEnabled($plugin)) {
        $widget = $this->createWidgetInstance($entity, $plugin);

        if (!isset($form['access_policy_settings'][$access_policy->id()])) {
          $form['access_policy_settings'][$access_policy->id()] = [
            '#type' => 'container',
          ];
        }
        $form['access_policy_settings'][$access_policy->id()][$plugin_name] = [];
        $subform_state = SubformState::createForSubform($form['access_policy_settings'][$access_policy->id()][$plugin_name], $form, $form_state);
        $form['access_policy_settings'][$access_policy->id()][$plugin_name] = $widget->buildForm($form['access_policy_settings'][$access_policy->id()][$plugin_name], $subform_state);
      }
    }
  }

  /**
   * Determine whether the access rule widget is enabled.
   *
   * @param \Drupal\access_policy\Plugin\access_policy\AccessRule\AccessRulePluginInterface $plugin
   *   The access rule plugin.
   *
   * @return bool
   *   TRUE if the access rule widget is enabled; FALSE otherwise.
   */
  private function isWidgetEnabled(AccessRulePluginInterface $plugin) {
    $config = $plugin->getSettings();
    if ($plugin->getDefinition()->getWidget() && !empty($config['widget']['show'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Create a widget instance for this context.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\access_policy\Plugin\access_policy\AccessRule\AccessRulePluginInterface $plugin
   *   The access rule plugin.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessRuleWidget\AccessRuleWidgetPluginInterface
   *   The access rule widget plugin interface.
   */
  private function createWidgetInstance(EntityInterface $entity, AccessRulePluginInterface $plugin) {
    $widget = $this->accessRuleWidgetPluginManager->createInstance($plugin->getDefinition()->getWidget());
    $widget->initialize($plugin->getDefinition());

    $config = $plugin->getSettings();
    $widget->setEntity($entity);
    $settings = $config['widget']['settings'] ?? [];
    $widget->setWidgetSettings($settings);
    return $widget;
  }

  /**
   * Render a message if no access policies are available.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form.
   */
  public function buildFormEmpty(array $form, FormStateInterface $form_state) {
    return [
      '#markup' => $this->t('No access policies are currently available.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();

    $access_policy_id = $form_state->getValue('access_policy');

    // If a selection set is chosen, make sure that one of the policies are
    // chosen as well.
    if ($this->isSelectionSetSelected($access_policy_id)) {
      $selected = $form_state->getValue('policies_multiple') ?? [];
      $multiple_policies = array_filter($selected);
      if (empty($multiple_policies)) {
        $form_state->setErrorByName('policies_multiple', $this->t('Please select an access policy.'));
      }
    }

    // Note: This is not calling getSubmittedValues(); because we don't want to
    // validate submitted ajax values.
    if (!empty($access_policy_id) && !$this->isSelectionSetSelected($access_policy_id)) {
      $access_policy = $this->loadAccessPolicy($access_policy_id);

      $plugins = $this->accessRulePluginManager->getApplicableHandlers($access_policy, $entity);
      foreach ($plugins as $plugin) {
        if ($this->isWidgetEnabled($plugin)) {
          $widget = $this->createWidgetInstance($entity, $plugin);
          $widget->validateForm($form, $form_state);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $access_policies = $this->getSelectedAccessPolicies($entity, $form, $form_state);

    if (!empty($access_policies)) {
      foreach ($access_policies as $access_policy) {
        $plugins = $this->accessRulePluginManager->getApplicableHandlers($access_policy, $entity);
        foreach ($plugins as $plugin) {
          if ($this->isWidgetEnabled($plugin)) {
            $widget = $this->createWidgetInstance($entity, $plugin);
            $widget->submitForm($form, $form_state);
          }
        }
      }

      $this->messenger()->addMessage($this->t('Access has been changed to @policy.', ['@policy' => $this->getAccessPolicyLabels($access_policies)]));
    }
    else {
      $this->contentAccessPolicyManager->remove($entity);
      $this->messenger()->addMessage($this->t('Access policy has been removed.'));
    }

  }

  /**
   * Get the access policies labels.
   *
   * @param array $access_policies
   *   The selected access policies.
   *
   * @return string
   *   The formatted string of access policy labels.
   */
  protected function getAccessPolicyLabels(array $access_policies) {
    $labels = [];
    foreach ($access_policies as $policy) {
      $labels[] = $policy->label();
    }

    return implode(', ', $labels);
  }

  /**
   * Get the selected access policies.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   *   Array of selected access policies.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSelectedAccessPolicies(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    if ($values = $this->getSubmittedValues($form, $form_state)) {
      $field = $this->getPolicyField($form, $form_state);
      $value = $values[$field];

      if (!empty($value)) {
        if (is_array($value)) {
          $policy_ids = array_keys(array_filter($value));
        }
        else {
          $policy_ids = [$value];
        }
      }
    }
    else {
      $policies = $this->contentAccessPolicyManager->getAccessPolicy($entity);
      if (!empty($policies)) {
        $policy_ids = array_map(function ($policy) {
          return $policy->id();
        }, $policies);
      }
    }

    if (!empty($policy_ids)) {
      return $this->entityTypeManager->getStorage('access_policy')->loadMultiple($policy_ids);
    }
    return [];
  }

  /**
   * Get the submitted form values.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The submitted form values.
   */
  protected function getSubmittedValues(array $form, FormStateInterface $form_state) {
    // Check to see if an ajax submission has occurred. This is necessary to
    // work with multi-value fields, especially those using ajax.
    if ($this->isAjaxSubmitted($form, $form_state)) {
      return $form_state->getUserInput();
    }
    elseif ($this->isFormSubmitted($form, $form_state)) {
      return $form_state->getValues();
    }

    return [];
  }

  /**
   * Determine whether an ajax callback has submitted the form.
   *
   * This is primarily to address a bug where a javascript error occurs when
   * the "Add more" field is clicked on the access rule widget.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface.
   *
   * @return bool
   *   TRUE if the form was submitted via ajax; FALSE otherwise.
   */
  protected function isAjaxSubmitted(array $form, FormStateInterface $form_state) {
    // Ajax callbacks do not seem to respect the limit_validation_errors
    // property which can cause the form to be submitted when interacting with
    // an ajax field widget.
    // @see https://www.drupal.org/project/drupal/issues/2476569
    $input = $form_state->getUserInput();
    if (!empty($input['_drupal_ajax'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine whether the form was submitted.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if the form was submitted; FALSE otherwise.
   */
  protected function isFormSubmitted(array $form, FormStateInterface $form_state) {
    // If a value is submitted then load that.
    $option = $form_state->getValue('access_policy');
    if (isset($option)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Get the current access policies on an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]|false
   *   The current access policy.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getCurrentAccessPolicies(EntityInterface $entity) {
    $policies = $this->contentAccessPolicyManager->getAccessPolicy($entity);
    if (!empty($policies)) {
      $policy_ids = array_map(function ($policy) {
        return $policy->id();
      }, $policies);
    }

    if (!empty($policy_ids)) {
      return $this->entityTypeManager->getStorage('access_policy')->loadMultiple($policy_ids);
    }

    return [];
  }

  /**
   * Get the field to load the access policies from.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return string
   *   The access policy field name.
   */
  public function getPolicyField(array $form, FormStateInterface $form_state) {
    $values = $this->getSubmittedValues($form, $form_state);

    if ($this->isSelectionSetSelected($values['access_policy'])) {
      return 'policies_multiple';
    }

    return 'access_policy';
  }

  /**
   * Ajax callback for updating Access Rules.
   *
   * @param array &$form
   *   The built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The updated content.
   */
  public static function loadAccessRuleSettings(array &$form, FormStateInterface $form_state) {
    return $form['access_policy_selection'];
  }

  /**
   * Return Access Policy form options.
   *
   * This includes options for both single and multiple access policy selection.
   *
   * @param array $entities
   *   An array of Access Policy entity ids or the special selection set id.
   *
   * @return array
   *   The Access Policy form options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAccessPolicyOptions(array $entities) {
    $options = [];
    foreach ($entities as $entity) {
      if (!$entity->isMultiple()) {
        $options[$entity->id()] = $entity->label();
      }
      else {
        $selection_set = $entity->getSelectionSet();
        foreach ($selection_set as $set_name) {
          $set = $this->policySelection->getSelectionSet($entity->getTargetEntityTypeId(), $set_name);
          $key = $this->getSelectionSetKey($set['id']);
          $options[$key] = $set['label'];
        }
      }

    }
    return $options;
  }

  /**
   * Get the selection set key.
   *
   * @param string $id
   *   The selection set id.
   *
   * @return string
   *   The selection set key.
   */
  protected function getSelectionSetKey($id) {
    return 'selection_set-' . $id;
  }

  /**
   * Determine whether the option selected is the type.
   *
   * @param string $option
   *   The selected access policy.
   *
   * @return bool
   *   TRUE if the selected option is type and not access policy.
   */
  public function isSelectionSetSelected($option) {
    if (is_string($option) && substr_count($option, 'selection_set-') > 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine whether we're showing the confirmation form.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE if we're showing confirmation form; FALSE otherwise.
   */
  protected function isConfirming(array $form, FormStateInterface $form_state) {
    if ($storage = $form_state->getStorage()) {
      return isset($storage['confirm']);
    }
    return FALSE;
  }

  /**
   * Determine whether the confirmation form is enabled.
   *
   * @return bool
   *   TRUE if the form is enabled; FALSE otherwise..
   */
  protected function isConfirmFormEnabled() {
    $strategy = $this->policySelection->getSelectionStrategy($this->entity->getEntityTypeId());
    return $strategy->getSetting('show_confirmation_form') ?? FALSE;
  }

  /**
   * Get the submitted option.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return \Drupal\access_policy\Entity\AccessPolicyInterface|false
   *   The option or FALSE if not found.
   */
  protected function getSubmittedOption(array $form, FormStateInterface $form_state) {
    $values = $this->getSubmittedValues($form, $form_state);
    $option = $values['access_policy'] ?? FALSE;

    return $option;
  }

  /**
   * Get the current Access Policy.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool|string
   *   The current access policy or FALSE if unset.
   */
  protected function getCurrentOption(EntityInterface $entity) {
    $current_set = $this->policySelection->getSelectionSetFromEntity($entity);
    if ($current_set) {
      return $this->getSelectionSetKey($current_set);
    }

    $policies = $this->contentAccessPolicyManager->getAccessPolicy($entity);
    $policy_ids = array_map(function ($policy) {
      return $policy->id();
    }, $policies);

    if (!empty($policy_ids)) {
      return $policy_ids;
    }

    if (!$this->contentAccessPolicyManager->isEmpty($entity)) {
      $default_policy = $this->policySelection->getDefaultPolicy($entity, $this->currentUser()) ?? '';
      if ($default_policy) {
        if (is_array($default_policy)) {
          return array_map(function ($policy) {
            return $policy->id();
          }, $default_policy);
        }
        else {
          return $default_policy->id();
        }
      }
    }

    return FALSE;
  }

  /**
   * Determine whether this access policy is allowed or empty.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return bool
   *   TRUE if allowed or empty; FALSE otherwise.
   */
  private function isAllowedOrEmpty(EntityInterface $entity, AccountInterface $account) {
    if (!$this->policySelection->isPolicyFieldEnabled($entity->getEntityTypeId())) {
      return FALSE;
    }

    if (!$entity->isDefaultTranslation()) {
      return FALSE;
    }

    // If the user does not have permission to assign this access policy then
    // disable the field.
    $policies = $this->getCurrentAccessPolicies($entity);
    if (!empty($policies)) {
      foreach ($policies as $policy) {
        if (!$account->hasPermission('assign ' . $policy->id() . ' access policy')) {
          return FALSE;
        }
      }
      return TRUE;
    }

    if ($this->contentAccessPolicyManager->isEmpty($entity)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Load an Access Policy.
   *
   * @param string $access_policy_id
   *   The Access Policy id.
   *
   * @return \Drupal\access_policy\Entity\AccessPolicyInterface
   *   The Access Policy.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function loadAccessPolicy(string $access_policy_id) {
    /** @var \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy */
    return $this->entityTypeManager->getStorage('access_policy')->load($access_policy_id);
  }

  /**
   * Checks access for the access tab form.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkAccess(RouteMatchInterface $route_match) {
    $entity_type_id = $route_match->getParameter('entity_type_id');
    $entity = $this->getEntityFromRouteMatch($route_match, $entity_type_id);

    if ($entity) {
      // Check to see if this is page is enabled.
      if (!$this->policySelection->isSelectionPageEnabled($entity->getEntityTypeId())) {
        return AccessResult::forbidden();
      }

      // Check to see if this bundle supports access policy. If it doesn't then
      // do not show the Access tab.
      if (!$this->accessPolicyInfo->isAccessControlledEntity($entity)) {
        return AccessResult::forbidden();
      }

      return AccessResult::allowed()->addCacheableDependency($entity);
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    $entity = parent::getEntityFromRouteMatch($route_match, $entity_type_id);

    // In some cases the entity route might not be properly upcasted. Such as
    // with other contrib modules that are using a similar route path. This
    // makes sure that the entity is properly loaded and doesn't cause an error.
    if (is_numeric($entity)) {
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity);
    }

    return $entity;
  }

}
