<?php

namespace Drupal\access_policy;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Access rule argument plugin manager.
 *
 * @package Drupal\access_policy
 */
class AccessRuleArgumentPluginManager extends DefaultPluginManager {

  /**
   * Access rule widget plugin manager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/access_policy/AccessRuleArgument', $namespaces, $module_handler, 'Drupal\access_policy\Plugin\access_policy\AccessRuleArgument\AccessRuleArgumentPluginInterface', 'Drupal\access_policy\Annotation\AccessRuleArgument');

    $this->alterInfo('access_rule_argument');
    $this->setCacheBackend($cache_backend, 'access_rule_argument');
  }

}
