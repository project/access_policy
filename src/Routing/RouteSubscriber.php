<?php

namespace Drupal\access_policy\Routing;

use Drupal\access_policy\AccessPolicyInformation;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for access policy routes.
 *
 * This is the route for the "Access" tab on content entities.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $accessPolicyInformation;

  /**
   * Constructs a RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\AccessPolicyInformation $access_policy_information
   *   The access policy information service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccessPolicyInformation $access_policy_information) {
    $this->entityTypeManager = $entity_type_manager;
    $this->accessPolicyInformation = $access_policy_information;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($this->accessPolicyInformation->isAccessControlledEntityType($entity_type)) {
        $path = $entity_type->getLinkTemplate('access-policy-form');
        $defaults = [
          '_entity_form' => $entity_type_id . '.manage access',
          'entity_type_id' => $entity_type_id,
          '_title' => 'Access',
        ];
        $requirements = [
          '_permission' => 'set entity access policy',
          '_entity_access' => $entity_type_id . '.manage access',
          '_custom_access' => '\Drupal\access_policy\Form\AccessPolicyEntityForm::checkAccess',
          $entity_type_id => '\d+',
        ];

        $options = [
          'parameters' => [
            $entity_type_id => [
              'type' => 'entity:' . $entity_type_id,
            ],
          ],
          '_admin_route' => TRUE,
        ];
        $route = new Route(
          $path,
          $defaults,
          $requirements,
          $options,
        );
        $collection->add("entity.$entity_type_id.access_policy_form", $route);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -210];
    return $events;
  }

}
