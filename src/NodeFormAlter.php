<?php

namespace Drupal\access_policy;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for altering the node form.
 */
class NodeFormAlter implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity storage handler.
   *
   * @var \Drupal\access_policy\ContentAccessPolicyManager
   */
  protected $storage;

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $information;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new NodeFormAlter object.
   *
   * @param \Drupal\access_policy\ContentAccessPolicyManager $storage
   *   The access policy content entity handler.
   * @param \Drupal\access_policy\AccessPolicyInformation $information
   *   The access policy information service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(ContentAccessPolicyManager $storage, AccessPolicyInformation $information, AccountInterface $current_user) {
    $this->storage = $storage;
    $this->information = $information;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('access_policy.content_policy_manager'),
      $container->get('access_policy.information'),
      $container->get('current_user'),
    );
  }

  /**
   * This alters the node form and adds some access policy metadata.
   *
   * @see hook_form_alter()
   */
  public function nodeFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $entity = $form_state->getFormObject()->getEntity();
    if ($this->information->isAccessControlledEntityType($entity->getEntityType()) && isset($form['meta'])) {
      if (!$this->storage->isEmpty($entity)) {
        $form['meta']['access'] = [
          '#type' => 'item',
          '#title' => $this->t('Access'),
          '#markup' => $this->renderAccessPolicyLabels($entity),
          '#wrapper_attributes' => ['class' => ['entity-meta__access container-inline']],
        ];
      }
    }
  }

  /**
   * Render the current access policy labels.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   The access policy labels.
   */
  public function renderAccessPolicyLabels(EntityInterface $entity) {
    $access_policy = $this->storage->getAccessPolicy($entity);
    $labels = array_map(function ($policy) {
      return $policy->label();
    }, $access_policy);

    return implode(', ', $labels);
  }

}
