<?php

namespace Drupal\access_policy;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The access policy selection strategy plugin manager.
 */
class SelectionStrategyPluginManager extends DefaultPluginManager {

  /**
   * AccessPolicyQueryPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/access_policy/SelectionStrategy', $namespaces, $module_handler, 'Drupal\access_policy\Plugin\access_policy\SelectionStrategy\SelectionStrategyInterface', 'Drupal\access_policy\Annotation\SelectionStrategy');

    $this->alterInfo('selection_strategy');
    $this->setCacheBackend($cache_backend, 'selection_strategy');
  }

  /**
   * Get the applicable selection strategy definitions.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return array
   *   Array of selection strategy definitions.
   */
  public function getApplicableDefinitions(EntityTypeInterface $entity_type) {
    $definitions = $this->getDefinitions();
    return array_filter($definitions, function ($definition) use ($entity_type) {
      $class = $definition['class'];
      if ($class::isApplicable($entity_type)) {
        return TRUE;
      }
      return FALSE;
    });
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    // Fetch and sort definitions by weight.
    $definitions = $this->getCachedDefinitions();
    if (empty($definitions)) {
      $definitions = $this->findDefinitions();
      uasort($definitions, '\Drupal\Component\Utility\SortArray::sortByWeightElement');

      $this->setCachedDefinitions($definitions);
    }

    return $definitions;
  }

}
