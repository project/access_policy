<?php

namespace Drupal\access_policy\EventSubscriber;

use Drupal\access_policy\AccessPolicyValidatorInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\EventSubscriber\DefaultExceptionHtmlSubscriber;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;

/**
 * Exception subscriber for handling access policy 403 HTML error pages.
 */
class AccessPolicyExceptionHtmlSubscriber extends DefaultExceptionHtmlSubscriber {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The access policy validator service.
   *
   * @var \Drupal\access_policy\AccessPolicyValidatorInterface
   */
  protected $accessPolicyValidator;

  /**
   * Constructs a new AccessPolicyExceptionHtmlSubscriber.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The HTTP Kernel service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Symfony\Component\Routing\Matcher\UrlMatcherInterface $access_unaware_router
   *   A router implementation which does not check access.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\AccessPolicyValidatorInterface $access_policy_validator
   *   The access policy validator service.
   */
  public function __construct(HttpKernelInterface $http_kernel, LoggerInterface $logger, RedirectDestinationInterface $redirect_destination, UrlMatcherInterface $access_unaware_router, EntityTypeManagerInterface $entity_type_manager, AccessPolicyValidatorInterface $access_policy_validator) {
    parent::__construct($http_kernel, $logger, $redirect_destination, $access_unaware_router);
    $this->entityTypeManager = $entity_type_manager;
    $this->accessPolicyValidator = $access_policy_validator;
  }

  /**
   * {@inheritdoc}
   */
  protected static function getPriority() {
    return -25;
  }

  /**
   * {@inheritdoc}
   */
  public function on403(ExceptionEvent $event) {
    $entity = $this->getEntity($event);
    if ($entity) {
      $policy = $this->getAccessPolicyFromViolations($entity);
      if ($policy && !$policy->isDefaultHttp403Response()) {
        $this->makeSubrequest($event, '/access_policy/403/' . $policy->id(), Response::HTTP_FORBIDDEN);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function on404(ExceptionEvent $event) {
    // Intentionally left blank so that we do nothing with 404 pages.
  }

  /**
   * Get the current entity.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The exception event.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity object if found.
   */
  public function getEntity(ExceptionEvent $event) {
    $params = $event->getRequest()->attributes;
    if ($params->count() > 0) {
      foreach ($params as $parameter) {
        if ($parameter instanceof ContentEntityInterface) {
          return $parameter;
        }
      }
    }
  }

  /**
   * Get the access policy that triggered the 403 error.
   *
   * @return \Drupal\Core\Entity\EntityInterface|false|null
   *   The access policy entity or FALSE if not found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAccessPolicyFromViolations(EntityInterface $entity) {
    $violations = $this->accessPolicyValidator->getViolations($entity);
    if (!empty($violations)) {
      $failed_policies = array_keys($violations);
      $policy_id = reset($failed_policies);
      return $this->entityTypeManager->getStorage('access_policy')->load($policy_id);
    }
    return FALSE;
  }

}
