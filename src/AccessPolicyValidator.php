<?php

namespace Drupal\access_policy;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\access_policy\Validation\ExecutionContext;
use Drupal\access_policy\Validation\ExecutionContextInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Service for validating access policies on entities.
 */
class AccessPolicyValidator implements AccessPolicyValidatorInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The content access policy manager service.
   *
   * @var \Drupal\access_policy\ContentAccessPolicyManager
   */
  protected $contentAccessPolicyManager;

  /**
   * The access rule plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $accessRulePluginManager;

  /**
   * The operation plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyOperationPluginManager
   */
  protected $operationPluginManager;

  /**
   * Array of violations.
   *
   * @var array
   */
  protected $violations = [];

  /**
   * The validation cache.
   *
   * Validation results are stored in a static cache for later retrieval.
   * This can be reset using self::resetCache().
   *
   * @var array
   */
  protected $validationCache = [];

  /**
   * Constructs an AccessPolicyValidator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\ContentAccessPolicyManager $content_access_policy_manager
   *   The content access policy manager.
   * @param \Drupal\access_policy\AccessPolicyHandlerManager $access_rule_manager
   *   The access rule plugin manager.
   * @param \Drupal\access_policy\AccessPolicyOperationPluginManager $operation_plugin_manager
   *   The operation plugin manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ContentAccessPolicyManager $content_access_policy_manager, AccessPolicyHandlerManager $access_rule_manager, AccessPolicyOperationPluginManager $operation_plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->contentAccessPolicyManager = $content_access_policy_manager;
    $this->accessRulePluginManager = $access_rule_manager;
    $this->operationPluginManager = $operation_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(EntityInterface $entity, AccountInterface $account, string $operation) {
    $cid = $this->getCid($entity);
    if ($this->isCached($cid, $account, $operation)) {
      return $this->getCache($cid, $account, $operation);
    }

    // Make sure the violations are reset.
    $access_policies = $this->contentAccessPolicyManager->getAccessPolicy($entity);

    if (empty($access_policies)) {
      return $this->setCache(TRUE, $cid, $account, $operation);
    }

    foreach ($access_policies as $access_policy) {
      // If any policy passes then they should have access.
      $access = $this->validateAccessPolicy($access_policy, $entity, $account, $operation);
      if ($access) {
        return $this->setCache(TRUE, $cid, $account, $operation);
      }
    }
    return $this->setCache(FALSE, $cid, $account, $operation);
  }

  /**
   * {@inheritdoc}
   */
  public function validateAccessPolicy(AccessPolicyInterface $access_policy, EntityInterface $entity, AccountInterface $account, $operation) {
    $this->violations[$entity->id()][$access_policy->id()] = [];

    $operations_handler = $access_policy->getOperationsHandler();

    if ($operations_handler->shouldValidatePermission($operation)) {
      $permission = $this->getAccessPolicyPermission($access_policy, $entity->getEntityType(), $operation);
      if (!$account->hasPermission($permission)) {
        $this->addViolation($entity, $access_policy->id(), sprintf("The '%s' permission is required.", $permission));
        return FALSE;
      }
    }

    if ($operations_handler->shouldValidateAccessRules($operation)) {
      if (!$account->hasPermission('bypass ' . $access_policy->id() . ' access rules')) {
        $access_rules_check = $this->validateAccessRules($access_policy, $entity, $account, $operation);
        if (!$access_rules_check) {
          return FALSE;
        }
      }
    }

    return TRUE;
  }

  /**
   * Get the validation cache contexts for the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   The cache contexts.
   */
  public function getCacheContexts(EntityInterface $entity) {
    $access_policies = $this->contentAccessPolicyManager->getAccessPolicy($entity);

    if (empty($access_policies)) {
      return ['user.permissions'];
    }

    $contexts = [];
    foreach ($access_policies as $policy) {
      $access_rules = $this->accessRulePluginManager->getApplicableHandlers($policy, $entity);
      foreach ($access_rules as $handler) {
        $contexts = array_merge($handler->getCacheContexts(), $contexts);
      }

    }

    return array_unique($contexts);
  }

  /**
   * Get the properly formed access policy permission.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy entity.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param string $op
   *   The operation.
   *
   * @return string
   *   The permission string.
   */
  private function getAccessPolicyPermission(AccessPolicyInterface $access_policy, EntityTypeInterface $entity_type, $op) {
    $operation = $this->operationPluginManager->createInstance($op);
    return $operation->getPermission($access_policy, $entity_type);
  }

  /**
   * Validate Access Rules in a policy.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $policy
   *   The access policy.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The content entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param string $operation
   *   The current operation.
   *
   * @return bool
   *   Return TRUE if valid, FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function validateAccessRules(AccessPolicyInterface $policy, EntityInterface $entity, AccountInterface $account, $operation) {
    $access_rules = $this->accessRulePluginManager->getApplicableHandlers($policy, $entity);

    $default_status = TRUE;
    $access_rules = $this->filterDisabledOperations($access_rules, $operation, $default_status);

    $user = $this->entityTypeManager->getStorage('user')->load($account->id());
    $context = ExecutionContext::create($operation, [
      'entity' => $entity,
      'user' => $user,
    ]);
    return $this->validateAccessRulePlugins($access_rules, $context, $policy->getAccessRuleOperator());
  }

  /**
   * Validate Access Rules plugins.
   *
   * @param array $access_rules
   *   Array of access rule plugins.
   * @param \Drupal\access_policy\Validation\ExecutionContextInterface $context
   *   The execution context.
   * @param string $operator
   *   The current operator; default is AND.
   *
   * @return bool
   *   Return TRUE if valid, FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validateAccessRulePlugins(array $access_rules, ExecutionContextInterface $context, $operator = 'AND') {
    $access = TRUE;

    foreach ($access_rules as $plugin) {
      // @todo add some other kind of initialization.
      $plugin->setContext($context);
      // Compute the final value that will be validated against.
      $plugin->getValue();

      // @todo deprecate passing arguments to validate().
      $entity = $context->get('entity');
      $user = $context->get('user');
      $rule_access = $plugin->validate($entity, $user);

      $access_policy = $plugin->getDefinition()->getAccessPolicy();
      // If rule_access is null then ignore this access rule and skip to the
      // next one.
      if ($rule_access === NULL) {
        continue;
      }

      $access = $rule_access;
      switch ($operator) {
        // If all rules are required then fail immediately if any return false.
        case 'AND':
          if (!$access) {
            $this->addViolation($entity, $access_policy, sprintf("The '%s' access rule failed.", $plugin->adminLabel()));
            return FALSE;
          }
          break;

        // If any rules are required then pass immediately if any return true.
        case 'OR':
          if ($access) {
            return TRUE;
          }
          break;
      }

      // Note that access rules with OR can still trigger a violation though
      // the final access is still granted.
      if (!$access) {
        $this->addViolation($entity, $access_policy, sprintf("The '%s' access rule failed.", $plugin->adminLabel()));
      }

    }

    return $access;
  }

  /**
   * Filter out any rules that are not configured to execute for this operation.
   *
   * If the access rule does not have any operation overrides then it will
   * fall back to using the policy type operation settings.
   *
   * @param array $rules
   *   Array of access rules.
   * @param string $operation
   *   The current operation.
   * @param bool $default
   *   The default operation setting from the policy type.
   *
   * @return array
   *   Array of filtered access rule plugins.
   */
  private function filterDisabledOperations(array $rules, $operation, $default) {
    return array_filter($rules, function ($rule) use ($operation, $default) {
      $settings = $rule->getSettings();
      // If operations is not set then fallback to the default operation setting
      // from the policy type.
      if (!isset($settings['operations'])) {
        return $default;
      }

      if (in_array($operation, $settings['operations'])) {
        return TRUE;
      }
      return FALSE;
    });
  }

  /**
   * Add a violation message.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity being validated.
   * @param string $access_policy
   *   The access policy id with the violation.
   * @param string $message
   *   The violation message.
   */
  protected function addViolation(EntityInterface $entity, $access_policy, $message) {
    $this->violations[$entity->id()][$access_policy][] = $message;
  }

  /**
   * Return any violation messages.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being validated.
   *
   * @return array
   *   The array of violation messages.
   */
  public function getViolations(EntityInterface $entity) {
    if (isset($this->violations[$entity->id()])) {
      return $this->violations[$entity->id()];
    }
    return [];
  }

  /**
   * Determine whether there are any violations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being validated.
   *
   * @return bool
   *   TRUE if there are violations; FALSE otherwise.
   */
  public function hasViolations(EntityInterface $entity) {
    return !empty($this->violations[$entity->id()]);
  }

  /**
   * Set the validation cache.
   *
   * @param bool $result
   *   The validation result.
   * @param string $cid
   *   The cache id.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current account.
   * @param string $operation
   *   The operation.
   */
  protected function setCache($result, $cid, AccountInterface $account, $operation) {
    return $this->validationCache[$account->id()][$cid][$operation] = $result;
  }

  /**
   * Get the validation cache result.
   *
   * @param string $cid
   *   The cache id.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param string $operation
   *   The operation.
   */
  protected function getCache($cid, AccountInterface $account, $operation) {
    if (isset($this->validationCache[$account->id()][$cid][$operation])) {
      return $this->validationCache[$account->id()][$cid][$operation];
    }
  }

  /**
   * Determine whether a validation cache exists.
   *
   * @param string $cid
   *   The cache id.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param string $operation
   *   The operation.
   *
   * @return bool
   *   TRUE if a cache exists; FALSE otherwise.
   */
  protected function isCached($cid, AccountInterface $account, $operation) {
    if (isset($this->validationCache[$account->id()][$cid][$operation])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the cache id.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return string
   *   The unique cache cid.
   */
  protected function getCid(EntityInterface $entity) {
    return $entity->uuid() ?: $entity->getEntityTypeId() . ':' . $entity->id();
  }

  /**
   * Reset the validation cache.
   */
  public function resetCache() {
    $this->validationCache = [];
  }

}
