<?php

namespace Drupal\access_policy;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\Container;

/**
 * Access policy handler plugin manager.
 */
class AccessPolicyHandlerManager extends DefaultPluginManager {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The access policy data service.
   *
   * @var \Drupal\access_policy\AccessPolicyDataInterface
   */
  protected $accessPolicyData;

  /**
   * The handler type.
   *
   * @var string
   */
  protected $handlerType;

  /**
   * Array of instantiated handlers from policies.
   *
   * Keyed by entity type and policy id.
   *
   * @var array
   */
  protected $handlers = [];

  /**
   * AccessPolicyHandlerManager constructor.
   *
   * @param string $handler_type
   *   The handler type.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\AccessPolicyDataInterface $access_policy_data
   *   The module handler to invoke the alter hook with.
   */
  public function __construct($handler_type, \Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, AccessPolicyDataInterface $access_policy_data) {
    $handler_camel_case = Container::camelize($handler_type);
    $plugin_definition_annotation_name = 'Drupal\access_policy\Annotation\\' . $handler_camel_case;
    $plugin_interface = 'Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface';
    parent::__construct('Plugin/access_policy/' . $handler_camel_case, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);

    $this->entityTypeManager = $entity_type_manager;
    $this->accessPolicyData = $access_policy_data;
    $this->handlerType = $handler_type;

    $this->alterInfo($handler_type);
    $this->setCacheBackend($cache_backend, $handler_type);
  }

  /**
   * Get handlers from access policies that match specific properties.
   *
   * @param string $entity_type
   *   The entity type.
   * @param callable|null $filter
   *   The filter callback. This gets passed to array_filter with $handler as
   *   an argument.
   */
  public function getHandlersFromPolicies($entity_type, callable|null $filter = NULL) {
    $access_policies = $this->entityTypeManager->getStorage('access_policy')->loadMultiple();
    // Filter out any policies that don't match the entity type.
    $access_policies = array_filter($access_policies, function ($policy) use ($entity_type) {
      if ($policy->getTargetEntityTypeId() == $entity_type) {
        return TRUE;
      }
      return FALSE;
    });

    $all_handlers = [];
    foreach ($access_policies as $access_policy) {
      // This method returns an associative array with the plugin name as
      // the key. There can be duplicate plugins names across policies. Use
      // indexed array instead.
      $handlers = array_values($this->getHandlersFromPolicy($access_policy));
      $all_handlers = array_merge($all_handlers, $handlers);
    }

    // Filter the handlers (if necessary).
    if ($filter) {
      $all_handlers = array_filter($all_handlers, $filter);
    }

    return $all_handlers;
  }

  /**
   * Get handler plugins on a policy.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $policy
   *   The current access policy.
   *
   * @return array
   *   Array of handler plugins.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getHandlersFromPolicy(AccessPolicyInterface $policy) {
    $key = $policy->getTargetEntityTypeId() . ':' . $policy->id();
    if (!isset($this->handlers[$key])) {
      $this->handlers[$key] = [];
      $rules = $policy->getHandlers($this->handlerType);
      foreach ($rules as $id => $plugin_config) {
        $this->handlers[$key][$id] = $this->getHandlerFromPolicy($policy, $id);
      }
    }

    return $this->handlers[$key];
  }

  /**
   * Get all the handlers plugins for a particular group.
   *
   * @param string $group
   *   The group.
   *
   * @return array
   *   Array of instantiated handlers.
   */
  public function getHandlersByGroup($group) {
    $data = $this->accessPolicyData->getGroup($group);
    $handlers = [];
    foreach ($data as $name => $item) {
      [$group, $id] = explode(':', $name);
      if ($this->hasHandler($group, $id)) {
        $handlers[$name] = $this->getHandler($group, $id);
      }
    }
    return $handlers;
  }

  /**
   * Get a handler plugin from a policy.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $policy
   *   The access policy.
   * @param string $id
   *   The unique access rule id.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface
   *   The access policy handler plugin.
   */
  public function getHandlerFromPolicy(AccessPolicyInterface $policy, $id) {
    if ($this->hasInvalidData($policy, $id)) {
      return $this->getDeprecatedHandler($policy, $id);
    }

    $config = $policy->getHandler($this->handlerType, $id);
    // Pass the current access policy to the definition.
    $config['access_policy'] = $policy->id();

    if (empty($config['group'])) {
      return $this->getBrokenHandler($config);
    }

    return $this->getHandler($config['group'], $id, $config);
  }

  /**
   * Get the deprecated handler.
   *
   * This attempts to load a plugin based on deprecated data. If it fails it
   * falls back to the broken handler. This is only expected to be used to ease
   * the transition between releases. Especially preventing exposing content.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $policy
   *   The access policy.
   * @param string $id
   *   The unique access rule id.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface
   *   The access policy handler plugin.
   */
  public function getDeprecatedHandler(AccessPolicyInterface $policy, $id) {
    $config = $policy->getHandler($this->handlerType, $id);
    $definition = $this->getDefinition($config['plugin_id']);
    $label = $definition['label'] ?? $id;
    $config['id'] = $id;
    $config['access_policy'] = $policy->id();
    $config['label'] = $this->t('@label (Warning: invalid data detected, please update or replace immediately)', ['@label' => $label]);
    $config['widget'] = $definition['access_rule_widget'] ?? '';
    $config['group'] = $policy->getTargetEntityTypeId();
    $config['entity_type'] = $policy->getTargetEntityTypeId();
    $config['field'] = $this->getOriginalId($policy->getEntityTypeId(), $id);

    try {
      $instance = $this->createInstance($config['plugin_id'], $config);
      if (method_exists($instance, 'getFieldName')) {
        $config['field'] = $instance->getFieldName();
      }
      $instance->initialize(AccessPolicyHandlerDefinition::create($this->handlerType, $config));
      return $instance;
    }
    // If the plugin doesn't exist then return a broken handler.
    catch (\Exception $e) {
      return $this->getBrokenHandler($config);
    }
  }

  /**
   * Get the applicable handler plugins.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $policy
   *   The current access policy.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return array
   *   Array of handler plugins.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getApplicableHandlers(AccessPolicyInterface $policy, EntityInterface $entity) {
    $applicable_plugins = [];
    $rules = $policy->getHandlers($this->handlerType);
    foreach ($rules as $id => $plugin_config) {
      $plugin = $this->getHandlerFromPolicy($policy, $id);
      if ($plugin->isApplicable($entity)) {
        $applicable_plugins[$id] = $plugin;
      }
    }

    return $applicable_plugins;
  }

  /**
   * Determine whether this has invalid data.
   *
   * If the plugin exists but the data is not valid then this is considered
   * an invalid access rule.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $policy
   *   The access policy.
   * @param string $id
   *   The unique access rule id.
   *
   * @return bool
   *   TRUE if it is deprecated; FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function hasInvalidData(AccessPolicyInterface $policy, $id) {
    $config = $policy->getHandler($this->handlerType, $id);
    $required_properties = [
      'id',
      'group',
      'plugin_id',
      'entity_type',
      'field',
    ];
    foreach ($required_properties as $name) {
      if (!isset($config[$name])) {
        $definition = $this->getDefinition($config['plugin_id'], FALSE);
        if (!empty($definition)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Fetches the handler from the data cache.
   *
   * @param string $group
   *   The group.
   * @param string $name
   *   The name.
   * @param array $override
   *   Default configuration override.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface|null
   *   The fully instantiated plugin utilizing the access policy data.
   */
  public function getHandler($group, $name, array $override = []) {
    $id = $this->getOriginalId($group, $name);

    try {
      $config = $this->accessPolicyData->getHandler($group, $id);
      if (!empty($override)) {
        foreach ($override as $key => $value) {
          $config[$key] = $value;
        }
      }

      // Never allow the id or group to be overridden.
      $config['id'] = $name;
      $config['group'] = $group;

      if ($this->hasHandler($group, $name)) {
        $plugin_id = $this->getHandlerPluginId($config);
        return $this->initializeInstance($plugin_id, $config);
      }
    }
    // If the data does not exist then return a broken handler.
    catch (\Exception $e) {
      return $this->getBrokenHandler($override);
    }
  }

  /**
   * Determine whether a handler exists.
   *
   * @param string $group
   *   The group.
   * @param string $name
   *   The name.
   *
   * @return bool
   *   TRUE if the handler exists; FALSE otherwise.
   */
  public function hasHandler($group, $name) {
    $id = $this->getOriginalId($group, $name);
    $config = $this->accessPolicyData->getHandler($group, $id);
    if (empty($config)) {
      return FALSE;
    }

    $plugin_id = $this->getHandlerPluginId($config);
    if (empty($plugin_id)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get the handler plugin id.
   *
   * @param array $config
   *   The configuration array.
   *
   * @return string
   *   The handler plugin id or FALSE if it does not exist.
   */
  protected function getHandlerPluginId(array $config) {
    // @todo put access rule in its own sub-section.
    if ($this->handlerType == 'access_rule') {
      if (isset($config['access_rule'])) {
        return $config['access_rule']['plugin_id'];
      }
      // This is for backwards compatibility.
      return $config['plugin_id'];
    }

    if (isset($config[$this->handlerType])) {
      return $config[$this->handlerType]['plugin_id'];
    }

    return FALSE;
  }

  /**
   * Initialize the access rule plugin.
   *
   * @param string $plugin_id
   *   The access rule plugin id.
   * @param array $config
   *   The access rule configuration.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface
   *   The access policy handler plugin.
   */
  private function initializeInstance($plugin_id, array $config) {
    try {
      $plugin = $this->createInstance($plugin_id, $config);
      $plugin->initialize(AccessPolicyHandlerDefinition::create($this->handlerType, $config));
      return $plugin;
    }
    // If the plugin doesn't exist then return a broken handler.
    catch (\Exception $e) {
      return $this->getBrokenHandler($config);
    }
  }

  /**
   * Get the broken handler if an exception is thrown.
   *
   * @param array $config
   *   The original configuration.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface
   *   The access policy handler plugin.
   */
  private function getBrokenHandler(array $config) {
    $configuration = [
      'original_configuration' => $config,
    ];
    return $this->getHandler('access_rule', 'broken', $configuration);
  }

  /**
   * Get the original id by parsing the string.
   *
   * When ever duplicate access rules are added to a policy, it will append a
   * _n to the end of it. This removes that number to find the original rule
   * data definition.
   *
   * @param string $group
   *   The group.
   * @param string $id
   *   The current unique id.
   *
   * @return string
   *   The original id.
   */
  private function getOriginalId($group, $id) {
    if ($this->isOriginalId($group, $id)) {
      return $id;
    }

    $original_id = $id;
    $match = preg_match('/_[1-9]$/', $original_id, $matches);
    if ($match) {
      $string = reset($matches);
      $original_id = str_replace($string, '', $id);
    }

    return $original_id;
  }

  /**
   * Determine whether this is the original id.
   *
   * Fields can have numbers appended to the end of them, this ensures that we
   * don't trim that number.
   *
   * @param string $group
   *   The group.
   * @param string $id
   *   The current unique id.
   *
   * @return bool
   *   TRUE if it's the original id; FALSE otherwise.
   */
  private function isOriginalId($group, $id) {
    try {
      $this->accessPolicyData->getHandler($group, $id);
      return TRUE;
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

}
