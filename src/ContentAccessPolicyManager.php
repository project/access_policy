<?php

namespace Drupal\access_policy;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Service for handling Access Policy database content.
 *
 * @package Drupal\access_policy
 */
class ContentAccessPolicyManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Access policy information.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $accessPolicyInformation;

  /**
   * Constructs a ContentAccessPolicyManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\access_policy\AccessPolicyInformation $policy_information
   *   Access policy information.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccessPolicyInformation $policy_information) {
    $this->entityTypeManager = $entity_type_manager;
    $this->accessPolicyInformation = $policy_information;
  }

  /**
   * Set the Access Policy on an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param mixed $access_policy
   *   The access policy.
   *
   * @throws \Drupal\facets\Exception\Exception
   */
  public function set(EntityInterface $entity, $access_policy) {
    if (!is_array($access_policy)) {
      $access_policy = [$access_policy];
    }

    foreach ($access_policy as $policy) {
      $this->validateAccessPolicy($policy, $entity);
    }

    $policy_ids = array_map(function ($policy) {
      return $policy->id();
    }, $access_policy);

    $entity->policyUpdated = TRUE;
    $entity->set('access_policy', $policy_ids);
  }

  /**
   * Remove the access policy without saving it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function clear(EntityInterface $entity) {
    $entity->policyUpdated = TRUE;
    $entity->set('access_policy', NULL);
  }

  /**
   * Determine whether the policy has already been updated.
   *
   * This helps with automatic assignment behavior especially in entity
   * presave which can cause an infinite loop.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return bool
   *   TRUE if the policy has been updated; FALSE otherwise.
   */
  public function isPolicyUpdated(EntityInterface $entity) {
    if (isset($entity->policyUpdated)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Assign an Access Policy to a content entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The Access Policy.
   *
   * @throws \Exception
   */
  public function assign(EntityInterface $entity, AccessPolicyInterface $access_policy) {
    $this->set($entity, $access_policy);
    $entity->save();
  }

  /**
   * Determine whether the access policy is empty for this entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE if the access policy is empty; FALSE otherwise.
   */
  public function isEmpty(EntityInterface $entity) {
    if ($entity->get('access_policy')->isEmpty()) {
      return TRUE;
    }

    // Sometimes entity reference fields store an empty string. Need to check
    // the raw values to ensure it's actually empty.
    $definition = $entity->get('access_policy')->getFieldDefinition();
    $property = $definition->getFieldStorageDefinition()->getMainPropertyName();

    $value = $entity->get('access_policy')->getValue();
    if (!empty($value)) {
      foreach ($value as $item) {
        if (empty($item[$property])) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Remove an Access Policy from a content entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function remove(EntityInterface $entity) {
    $entity->set('access_policy', NULL);
    $entity->save();
  }

  /**
   * Retrieve the Access Policy assigned to a content entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\access_policy\Entity\AccessPolicyInterface[]
   *   The array of assigned Access Policies
   */
  public function getAccessPolicy(EntityInterface $entity) {
    if ($entity->hasField('access_policy') && !$entity->get('access_policy')->isEmpty()) {
      return $entity->get('access_policy')->referencedEntities();
    }
    return [];
  }

  /**
   * Validate the access policy.
   *
   * Don't allow entities to be assigned access policies that are not enabled
   * for that entity type.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $policy
   *   The access policy.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @throws \Drupal\facets\Exception\Exception
   */
  private function validateAccessPolicy(AccessPolicyInterface $policy, EntityInterface $entity) {
    $enabled = $this->accessPolicyInformation->isEnabledForEntitiesOfEntityType($policy, $entity->getEntityType());
    if (!$enabled) {
      throw new \Exception(sprintf('The "%1$s" Access Policy is not enabled for "%2$s".', $policy->id(), $entity->getEntityTypeId()));
    }
  }

}
