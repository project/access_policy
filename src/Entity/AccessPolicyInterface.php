<?php

namespace Drupal\access_policy\Entity;

use Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an AccessPolicy entity.
 *
 * @package Drupal\access_policy\Entity
 */
interface AccessPolicyInterface extends ConfigEntityInterface {

  /**
   * Returns label value.
   *
   * @return string
   *   The label value.
   */
  public function label();

  /**
   * Gets the access policy description.
   *
   * @return string
   *   The description.
   */
  public function getDescription();

  /**
   * Sets the access policy description.
   *
   * @param string $description
   *   The description.
   */
  public function setDescription($description);

  /**
   * Get the handlers of a specific type from the access policy.
   *
   * @param string $type
   *   The handler type.
   *
   * @return array
   *   Array of handler data.
   */
  public function getHandlers($type);

  /**
   * Get an access policy handler.
   *
   * @param string $type
   *   The handler type.
   * @param string $id
   *   The unique id of the handler.
   *
   * @return array
   *   The access policy handler.
   */
  public function getHandler($type, $id);

  /**
   * Update the access policy handler settings.
   *
   * @param string $type
   *   The handler type.
   * @param string $id
   *   The unique id of the handler.
   * @param array $settings
   *   The settings.
   */
  public function updateHandler($type, $id, array $settings);

  /**
   * Set the handlers of a specific type.
   *
   * @param string $type
   *   The handler type.
   * @param array $handlers
   *   Array of handler data.
   */
  public function setHandlers($type, array $handlers);

  /**
   * Add a new access policy handler plugin to the policy.
   *
   * @param string $type
   *   The handler type.
   * @param \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface $handler
   *   The handler plugin.
   *
   * @return string|null
   *   The new unique handler id or null if type is not supported.
   */
  public function addHandler($type, AccessPolicyHandlerInterface $handler);

  /**
   * Remove a handler from the access policy.
   *
   * @param string $type
   *   The handler type.
   * @param string $id
   *   The unique id of the handler.
   */
  public function removeHandler($type, $id);

  /**
   * Get the selection rule operator.
   *
   * @return string
   *   The validation operator. Default value is AND.
   */
  public function getSelectionRuleOperator();

  /**
   * Get the current selection sets.
   *
   * @return array
   *   Array of selection sets.
   */
  public function getSelectionSet();

  /**
   * Set the selection sets.
   *
   * @param array $selection_set
   *   The selection sets.
   */
  public function setSelectionSet(array $selection_set);

  /**
   * Determine whether this access policy can be assigned with other policies.
   *
   * @return bool
   *   TRUE if the policy is multiple; FALSE otherwise.
   */
  public function isMultiple();

  /**
   * Set the selection rule operator.
   *
   * @param string $operator
   *   The operator. Accepted values are AND and OR.
   */
  public function setSelectionRuleOperator($operator);

  /**
   * Get the access rule operator.
   *
   * @return string
   *   The validation operator. Default value is AND.
   */
  public function getAccessRuleOperator();

  /**
   * Set the access rule operator.
   *
   * @param string $operator
   *   The operator. Accepted values are AND and OR.
   */
  public function setAccessRuleOperator($operator);

  /**
   * Get the weight.
   *
   * @return int
   *   The weight.
   */
  public function getWeight();

  /**
   * Set the weight.
   *
   * @param int $weight
   *   The weight.
   */
  public function setWeight(int $weight);

  /**
   * Determine whether the policy should be applied to queries.
   *
   * @return bool
   *   TRUE if the policy is enabled; FALSE otherwise.
   */
  public function isQueryEnabled();

  /**
   * Set the policy as enabled or disabled for queries.
   *
   * In some cases you may only want to restrict the access policy to view,
   * edit, delete etc. This allows you to disable the policy on query pages
   * entirely.
   *
   * @param bool $query
   *   TRUE if the policy should be enabled; FALSE otherwise.
   */
  public function setQueryEnabled($query = TRUE);

  /**
   * Determine whether the http 403 response is the system default.
   *
   * @return bool
   *   TRUE if the response is default; FALSE otherwise.
   */
  public function isDefaultHttp403Response();

  /**
   * Get the access denied behavior plugin.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\Http403Response\Http403ResponseInterface
   *   The access denied plugin or FALSE if not set.
   */
  public function getHttp403Response();

  /**
   * Set the access denied behavior plugin and settings.
   *
   * @param string $id
   *   The access denied behavior plugin id.
   * @param array $settings
   *   The plugin settings.
   */
  public function setHttp403Response($id, array $settings = []);

  /**
   * Set the http 403 response to the system default.
   */
  public function resetHttp403Response();

  /**
   * Get the target entity type id.
   *
   * @return string
   *   The target entity type id
   */
  public function getTargetEntityTypeId();

  /**
   * Get the target entity type.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The target entity type.
   */
  public function getTargetEntityType();

  /**
   * Set the target entity type id.
   *
   * @param string $entity_type_id
   *   The entity type id.
   */
  public function setTargetEntityTypeId(string $entity_type_id);

}
