<?php

namespace Drupal\access_policy\Entity;

use Drupal\access_policy\AccessPolicyOperations;
use Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Represents an AccessPolicy.
 *
 * @ConfigEntityType(
 *   id = "access_policy",
 *   label = @Translation("Access Policy"),
 *   label_collection = @Translation("Access policies"),
 *   label_singular = @Translation("access policy"),
 *   label_plural = @Translation("access policies"),
 *   label_count = @PluralTranslation(
 *     singular = "@count access policy",
 *     plural = "@count access policies",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\access_policy\AccessPolicyStorage",
 *     "access" = "Drupal\access_policy\AccessPolicyAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     },
 *   },
 *   config_prefix = "access_policy",
 *   admin_permission = "administer access policy entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "weight",
 *     "access_rules",
 *     "access_rule_operator",
 *     "query",
 *     "http_403_response",
 *     "selection_rules",
 *     "selection_rule_operator",
 *     "selection_set",
 *     "target_entity_type_id",
 *     "type",
 *     "operations",
 *   }
 * )
 */
class AccessPolicy extends ConfigEntityBase implements AccessPolicyInterface {

  /**
   * Unique machine name of the access policy.
   *
   * @var string
   */
  protected $id;

  /**
   * The access policy label.
   *
   * @var string
   */
  protected $label;

  /**
   * The access policy description.
   *
   * @var string
   */
  protected $description;

  /**
   * Weight used to determine the order of access policies.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The enabled access rule plugins.
   *
   * @var array
   */
  protected $access_rules = [];

  /**
   * The access rule validation operator.
   *
   * @var string
   */
  protected $access_rule_operator = 'AND';

  /**
   * Flag whether this policy should apply to listing pages.
   *
   * @var bool
   */
  protected $query = TRUE;

  /**
   * The access denied behavior.
   *
   * @var array
   */
  protected $http_403_response = [];

  /**
   * The selection rule plugins.
   *
   * @var array
   */
  protected $selection_rules = [];

  /**
   * The selection rule validation operator.
   *
   * @var string
   */
  protected $selection_rule_operator = 'AND';

  /**
   * The selection set.
   *
   * @var array
   */
  protected $selection_set = [];

  /**
   * The target bundle.
   *
   * @var string
   */
  protected $target_entity_type_id;

  /**
   * The target bundles.
   *
   * @var array
   */
  protected $target_bundles = [];

  /**
   * The access policy validation type.
   *
   * @var string
   */
  protected $type = 'group';

  /**
   * The default access policy operations.
   *
   * @var array
   */
  protected $operations = [];

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getHandlers($type) {
    $type = $this->getHandlerTypeKey($type);
    return $this->$type;
  }

  /**
   * {@inheritdoc}
   */
  public function getHandler($type, $id) {
    $type = $this->getHandlerTypeKey($type);
    return $this->$type[$id];
  }

  /**
   * {@inheritdoc}
   */
  public function updateHandler($type, $id, array $settings) {
    $type = $this->getHandlerTypeKey($type);
    $this->$type[$id]['settings'] = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function setHandlers($type, $handlers) {
    $type = $this->getHandlerTypeKey($type);
    $this->$type = $handlers;
  }

  /**
   * {@inheritdoc}
   */
  public function addHandler($type, AccessPolicyHandlerInterface $handler) {
    switch ($type) {
      case 'access_rule':
        return $this->addAccessRule($handler);

      case 'selection_rule':
        return $this->addSelectionRule($handler);

    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeHandler($type, $id) {
    $type = $this->getHandlerTypeKey($type);
    unset($this->$type[$id]);
  }

  /**
   * Get access rule plugins.
   *
   * @return array
   *   The access rule plugins.
   */
  public function getAccessRules() {
    return $this->access_rules;
  }

  /**
   * Get access rule plugin by id.
   *
   * @param string $id
   *   The access rule id. Note that this shouldn't be confused with plugin_id.
   *   This is the unique id that gets generated when added to a policy.
   *
   * @return array
   *   The access rule plugin.
   */
  public function getAccessRule($id) {
    return $this->access_rules[$id];
  }

  /**
   * Update an access rule's settings.
   *
   * @param string $id
   *   The unique id of the access rule.
   * @param array $settings
   *   The access rule settings.
   */
  public function updateAccessRule($id, array $settings) {
    $this->access_rules[$id]['settings'] = $settings;
  }

  /**
   * Set access rule plugins.
   *
   * @param array $plugins
   *   The access rule plugins.
   */
  public function setAccessRules(array $plugins) {
    $this->access_rules = $plugins;
  }

  /**
   * Add an access rule plugin.
   *
   * @param \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface $access_rule
   *   The full instantiated access rule plugin.
   *
   * @return string
   *   The new unique access rule id.
   */
  public function addAccessRule(AccessPolicyHandlerInterface $access_rule) {
    $definition = $access_rule->getDefinition();
    $name = $this->createHandlerUniqueId('access_rule', $definition->getId(), $definition->getPluginId());
    $settings = ['query' => TRUE];
    if (!empty($access_rule->getSettings())) {
      $settings = $access_rule->getSettings();
    }
    $this->access_rules[$name] = [
      'id' => $name,
      'group' => $definition->getGroup(),
      'plugin_id' => $definition->getPluginId(),
      'field' => $definition->getFieldName(),
      'entity_type' => $definition->getEntityType(),
      'settings' => $settings,
      'required' => $definition->isRequired(),
    ];

    return $name;
  }

  /**
   * Remove an access rule plugin.
   *
   * @param string $id
   *   The unique id of the plugin.
   */
  public function removeAccessRule($id) {
    unset($this->access_rules[$id]);
  }

  /**
   * Add a new selection rule.
   *
   * @param \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface $handler
   *   The selection rule plugin.
   *
   * @return string
   *   The new unique selection rule id.
   */
  protected function addSelectionRule(AccessPolicyHandlerInterface $handler) {
    $definition = $handler->getDefinition();
    $name = $this->createHandlerUniqueId('selection_rule', $definition->getId(), $definition->getPluginId());
    $settings = [];
    if (!empty($handler->getSettings())) {
      $settings = $handler->getSettings();
    }
    $this->selection_rules[$name] = [
      'id' => $name,
      'group' => $definition->getGroup(),
      'plugin_id' => $handler->getPluginId(),
      'field' => $definition->getFieldName(),
      'entity_type' => $definition->getEntityType(),
      'settings' => $settings,
      'required' => $definition->isRequired(),
    ];

    return $name;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectionRuleOperator() {
    return $this->selection_rule_operator;
  }

  /**
   * {@inheritdoc}
   */
  public function setSelectionRuleOperator($operator) {
    $this->selection_rule_operator = $operator;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectionSet() {
    return $this->selection_set;
  }

  /**
   * {@inheritdoc}
   */
  public function setSelectionSet(array $selection_set) {
    return $this->selection_set = $selection_set;
  }

  /**
   * {@inheritdoc}
   */
  public function isMultiple() {
    if (!empty($this->selection_set)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessRuleOperator() {
    return $this->access_rule_operator;
  }

  /**
   * {@inheritdoc}
   */
  public function setAccessRuleOperator($operator) {
    $this->access_rule_operator = $operator;
  }

  /**
   * Generate a new unique id for the access rule plugin.
   *
   * If the plugin already exists it will append a number to the id.
   * For example: plugin_name_1, plugin_name_2 etc.
   *
   * @param string $type
   *   The handler type.
   * @param string $id
   *   The original id. This correlates with the id defined in
   *   hook_access_policy_data().
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return string
   *   The access rule unique id.
   */
  private function createHandlerUniqueId($type, $id, $plugin_id) {
    $handler_key = $this->getHandlerTypeKey($type);
    $handlers = $this->$handler_key;
    $existing = array_filter($handlers, function ($rule) use ($plugin_id) {
      if ($rule['plugin_id'] == $plugin_id) {
        return TRUE;
      }
      return FALSE;
    });

    // If any currently exist then append a count to the name.
    if (!empty($existing)) {
      return $id . '_' . count($existing);
    }

    return $id;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight');
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
  }

  /**
   * {@inheritdoc}
   */
  public function isQueryEnabled() {
    return $this->query;
  }

  /**
   * {@inheritdoc}
   */
  public function setQueryEnabled($query = TRUE) {
    $this->query = $query;
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultHttp403Response() {
    if (empty($this->http_403_response['plugin_id'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getHttp403Response() {
    if (!empty($this->http_403_response['plugin_id'])) {
      $plugin_id = $this->http_403_response['plugin_id'];
      $settings = $this->http_403_response['settings'] ?? [];
      return \Drupal::service('plugin.manager.http_403_response')->createInstance($plugin_id, $settings);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setHttp403Response($id, array $settings = []) {
    $this->http_403_response = [
      'plugin_id' => $id,
      'settings' => $settings,
    ];
  }

  /**
   * Set the http 403 response to the system default.
   */
  public function resetHttp403Response() {
    $this->http_403_response = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId() {
    return $this->target_entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityType() {
    return \Drupal::service('entity_type.manager')->getDefinition($this->target_entity_type_id);
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetEntityTypeId($entity_type_id) {
    $this->target_entity_type_id = $entity_type_id;
  }

  /**
   * Set the access policy operations.
   *
   * @param array $operations
   *   The array of operations.
   */
  public function setOperations(array $operations) {
    $this->operations = $operations;
  }

  /**
   * Get the operations.
   *
   * @return array
   *   Array of supported operations.
   */
  public function getOperations() {
    return $this->operations;
  }

  /**
   * Get the validation type plugin name.
   *
   * @return string
   *   The validation type plugin id.
   *
   * @deprecated in access_policy:1.0.0-beta7 and is removed from access_policy:1.0.0. There is no
   *   replacement.
   *
   * @see https://www.drupal.org/project/access_policy/issues/3419014
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Get the access policy operations handler.
   *
   * @return \Drupal\access_policy\AccessPolicyOperations
   *   The access policy operations handler.
   */
  public function getOperationsHandler() {
    return AccessPolicyOperations::create($this);
  }

  /**
   * Get the handler type key.
   *
   * @param string $handler_type
   *   The handler type.
   *
   * @return string
   *   The handler type key name.
   */
  private function getHandlerTypeKey($handler_type) {
    return $handler_type . 's';
  }

}
