<?php

namespace Drupal\access_policy;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manipulates entity type information.
 *
 * This class contains primarily bridged hooks for compile-time or
 * cache-clear-time hooks. Runtime hooks should be placed in EntityOperations.
 */
class EntityTypeInfo implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $information;

  /**
   * Constructs a new EntityTypeInfo object.
   *
   * @param \Drupal\access_policy\AccessPolicyInformation $information
   *   The access policy information service.
   */
  public function __construct(AccessPolicyInformation $information) {
    $this->information = $information;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('access_policy.information'),
    );
  }

  /**
   * Provides custom base field definitions for a content entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions, keyed by field name.
   *
   * @see hook_entity_base_field_info()
   */
  public function entityBaseFieldInfo(EntityTypeInterface $entity_type) {
    if (!$this->information->isSupportedEntityType($entity_type)) {
      return [];
    }

    $fields = [];
    $fields['access_policy'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel($this->t('Access policy'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDescription($this->t('The access policy assigned to this entity.'))
      ->setSetting('target_type', 'access_policy')
      ->setRevisionable($entity_type->isRevisionable())
      ->setTranslatable(FALSE)
      ->addConstraint('ValidAccessPolicyReference');

    return $fields;
  }

  /**
   * Alter entity types and access policy access form.
   *
   * @param array $entity_types
   *   Array of entity types.
   *
   * @see hook_entity_type_alter()
   */
  public function entityTypeAlter(array &$entity_types) {
    foreach ($entity_types as $entity_type) {
      if ($this->information->isSupportedEntityType($entity_type)) {
        $path = $this->getPath($entity_type);
        $entity_type->setLinkTemplate('access-policy-form', $path . '/access');
        $entity_type->setFormClass('manage access', 'Drupal\access_policy\Form\AccessPolicyEntityForm');
      }
    }
  }

  /**
   * Create the path for the access tab.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The current entity type.
   *
   * @return string
   *   The path to the access tab.
   */
  public function getPath(EntityTypeInterface $entity_type) {
    $link_template = $entity_type->getLinkTemplate('canonical');

    // Find the last occurrence of {variable} in the path and trim off the rest.
    // This prevents creating paths like /media/{media}/edit/access.
    $pos = strrpos($link_template, '}') + 1;
    if (is_numeric($pos)) {
      return substr($link_template, 0, $pos);
    }

    return $link_template;
  }

}
