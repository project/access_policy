<?php

namespace Drupal\access_policy\AccessRuleQueryHandler;

/**
 * Is own query handler.
 */
class IsOwn extends AccessRuleQueryHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $entity_type = $this->getEntityType();
    $user_field = $this->getTableAlias() . '.' . $entity_type->getKey('owner');

    $this->query->condition($user_field, $this->currentUser->id());
  }

  /**
   * {@inheritdoc}
   */
  public function ensureMyTable() {
    $base_field_placeholder = $this->query->getBaseTable() . '.' . $this->query->getBaseField();

    $entity_type = $this->getEntityType();
    $id = $entity_type->getKey('id');
    $this->query->join($this->tableAlias, $this->tableAlias, $this->tableAlias . "." . $id . " = " . $base_field_placeholder);
  }

  /**
   * Get the entity type.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The current entity type.
   */
  private function getEntityType() {
    $entity_type_id = $this->definition->getEntityType();
    return $this->entityTypeManager->getDefinition($entity_type_id);
  }

}
