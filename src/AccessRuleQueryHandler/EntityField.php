<?php

namespace Drupal\access_policy\AccessRuleQueryHandler;

/**
 * Entity field access rule query handler.
 */
class EntityField extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function operators() {
    return [
      'in' => [
        'method' => 'opIn',
      ],
      'not in' => [
        'method' => 'opNotIn',
      ],
      'or' => [
        'method' => 'opIn',
      ],
      'and' => [
        'method' => 'opEquals',
      ],
      'not' => [
        'method' => 'opNotIn',
      ],
      '=' => [
        'method' => 'opSimple',
      ],
      '!=' => [
        'method' => 'opSimple',
      ],
      '<' => [
        'method' => 'opSimple',
      ],
      '<=' => [
        'method' => 'opSimple',
      ],
      '>=' => [
        'method' => 'opSimple',
      ],
      '>' => [
        'method' => 'opSimple',
      ],
      'empty' => [
        'method' => 'opEmpty',
      ],
      'not empty' => [
        'method' => 'opNotEmpty',
      ],
      'contains' => [
        'method' => 'opContains',
      ],
      'not contains' => [
        'method' => 'opNotContains',
      ],
      'starts with' => [
        'method' => 'opStartsWith',
      ],
      'not starts with' => [
        'method' => 'opNotStartsWith',
      ],
      'ends with' => [
        'method' => 'opEndsWith',
      ],
      'not ends with' => [
        'method' => 'opNotEndsWith',
      ],
    ];
  }

  /**
   * IN operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opIn(array $values, $query) {
    $query->condition($this->realField, $values, 'IN');
  }

  /**
   * NOT IN operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opNotIn(array $values, $query) {
    $query->condition($this->realField, $values, 'NOT IN');
  }

  /**
   * Equals operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opEquals(array $values, $query) {
    foreach ($values as $value) {
      $query->condition($this->realField, $value, '=');
    }
  }

  /**
   * Simple operator for handling numeric values.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opSimple(array $values, $query) {
    $operator = $this->getOperator();
    foreach ($values as $value) {
      $query->condition($this->realField, $value, $operator);
    }
  }

  /**
   * Empty operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opEmpty(array $values, $query) {
    $query->condition($this->realField, NULL, 'IS NULL');
  }

  /**
   * Is not empty operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opNotEmpty(array $values, $query) {
    $query->condition($this->realField, NULL, 'IS NOT NULL');
  }

  /**
   * Contains operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opContains(array $values, $query) {
    foreach ($values as $value) {
      $query->condition($this->realField, '%' . $this->query->getConnection()->escapeLike($value) . '%', 'LIKE');
    }
  }

  /**
   * Not contains operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opNotContains(array $values, $query) {
    foreach ($values as $value) {
      $query->condition($this->realField, '%' . $this->query->getConnection()->escapeLike($value) . '%', 'NOT LIKE');
    }
  }

  /**
   * Starts with operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opStartsWith(array $values, $query) {
    foreach ($values as $value) {
      $query->condition($this->realField, $this->query->getConnection()->escapeLike($value) . '%', 'LIKE');
    }
  }

  /**
   * Not starts with operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opNotStartsWith(array $values, $query) {
    foreach ($values as $value) {
      $query->condition($this->realField, $this->query->getConnection()->escapeLike($value) . '%', 'NOT LIKE');
    }
  }

  /**
   * Ends with operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opEndsWith(array $values, $query) {
    foreach ($values as $value) {
      $query->condition($this->realField, '%' . $this->query->getConnection()->escapeLike($value), 'LIKE');
    }
  }

  /**
   * Not ends with operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opNotEndsWith(array $values, $query) {
    foreach ($values as $value) {
      $query->condition($this->realField, '%' . $this->query->getConnection()->escapeLike($value), 'NOT LIKE');
    }
  }

}
