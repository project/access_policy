<?php

namespace Drupal\access_policy\AccessRuleQueryHandler;

/**
 * Moderation state query handler.
 */
class ModerationState extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function operators() {
    return [
      'or' => [
        'method' => 'opIn',
      ],
      'not' => [
        'method' => 'opNotIn',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $value = $this->value;
    $formatted_value = [];
    foreach ($value as $item) {
      [$workflow, $moderation_state] = explode('-', $item);
      $formatted_value[$workflow][] = $moderation_state;
    }
    return $formatted_value;
  }

  /**
   * IN operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opIn(array $values, $query) {
    foreach ($values as $workflow => $moderation_states) {
      $andGroup = $query->andConditionGroup();
      $andGroup->condition($this->tableAlias . '.workflow', $workflow, '=');
      $andGroup->condition($this->realField, $moderation_states, 'IN');
      $query->condition($andGroup);
    }
  }

  /**
   * NOT IN operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opNotIn(array $values, $query) {
    foreach ($values as $workflow => $moderation_states) {
      $andGroup = $query->andConditionGroup();
      $andGroup->condition($this->tableAlias . '.workflow', $workflow, '=');
      $andGroup->condition($this->realField, $moderation_states, 'NOT IN');
      $query->condition($andGroup);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function ensureMyTable() {
    $base_field_placeholder = $this->query->getBaseTable() . '.' . $this->query->getBaseField();

    $entity_type = $this->entityTypeManager->getDefinition('content_moderation_state');
    $id = $entity_type->getKey('id');
    $this->query->leftJoin($this->tableAlias, $this->tableAlias, $this->tableAlias . "." . $id . " = " . $base_field_placeholder);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldTableName($entity_type_id, $field_name) {
    return 'content_moderation_state_field_data';
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldColumnName($entity_type_id, $field_name, $property = NULL) {
    return 'moderation_state';
  }

}
