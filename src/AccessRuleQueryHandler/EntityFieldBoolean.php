<?php

namespace Drupal\access_policy\AccessRuleQueryHandler;

/**
 * Entity field boolean access rule query handler.
 */
class EntityFieldBoolean extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function operators() {
    return [
      '=' => [
        'method' => 'opEquals',
      ],
      '!=' => [
        'method' => 'opNotEquals',
      ],
    ];
  }

  /**
   * Equals operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opEquals(array $values, $query) {
    foreach ($values as $value) {
      $query->condition($this->realField, boolval($value), '=');
    }
  }

  /**
   * Equals operator.
   *
   * @param array $values
   *   The current values.
   * @param \Drupal\access_policy\QueryInterface|\Drupal\Core\Database\Query\ConditionInterface $query
   *   The query or condition object.
   */
  public function opNotEquals(array $values, $query) {
    foreach ($values as $value) {
      $query->condition($this->realField, boolval($value), '!=');
    }
  }

}
