<?php

namespace Drupal\access_policy\AccessRuleQueryHandler;

use Drupal\access_policy\AccessRuleHandlerInterface;

/**
 * Access rule query handler interface.
 *
 * Access rule query handler allow you to restrict content on listing pages by
 * modifying the query. They are designed to be used with AccessRule plugins.
 * To use them, add the handler to the access rule plugin annotation.
 *
 * handlers = {
 *   "query_alter" = "\Drupal\access_policy\AccessRuleQueryHandler\MyHandler
 * }
 */
interface AccessRuleQueryHandlerInterface extends AccessRuleHandlerInterface {

  /**
   * Alter the query.
   */
  public function query();

}
