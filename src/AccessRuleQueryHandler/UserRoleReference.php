<?php

namespace Drupal\access_policy\AccessRuleQueryHandler;

/**
 * Use role reference query handler.
 */
class UserRoleReference extends AccessRuleQueryHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $roles = $this->currentUser->getRoles();
    $this->query->condition($this->realField, $roles, 'IN');
  }

  /**
   * {@inheritdoc}
   */
  public function ensureMyTable() {
    $base_field_placeholder = $this->query->getBaseTable() . '.' . $this->query->getBaseField();
    $this->query->leftJoin($this->tableAlias, $this->tableAlias, $this->tableAlias . ".entity_id = " . $base_field_placeholder);
  }

}
