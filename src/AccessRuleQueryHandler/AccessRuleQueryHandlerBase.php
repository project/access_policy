<?php

namespace Drupal\access_policy\AccessRuleQueryHandler;

use Drupal\access_policy\AccessPolicyHandlerDefinition;
use Drupal\access_policy\Plugin\access_policy\AccessRule\AccessRuleFieldTrait;
use Drupal\access_policy\QueryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access rule query handler base class.
 */
abstract class AccessRuleQueryHandlerBase implements ContainerInjectionInterface, AccessRuleQueryHandlerInterface {

  use AccessRuleFieldTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The table alias.
   *
   * @var string
   */
  protected $tableAlias;

  /**
   * The field to join the table on.
   *
   * Most of the time this will be entity_id but if we're joining against the
   * base table then it could be nid, tid etc.
   *
   * @var string
   */
  protected $joinField;

  /**
   * The real field name.
   *
   * @var string
   */
  protected $realField;

  /**
   * The current operator.
   *
   * @var string
   */
  protected $operator;

  /**
   * The current actual values.
   *
   * @var array
   */
  protected $value = NULL;

  /**
   * The access policy query.
   *
   * @var \Drupal\access_policy\QueryInterface
   */
  protected $query;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $currentUser;

  /**
   * The access policy handler definition object.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerDefinition
   *   The access policy handler definition.
   */
  protected $definition;

  /**
   * Constructs an access rule query handler.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $this->entityTypeManager->getStorage('user')->load($current_user->id());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
    );
  }

  /**
   * Initialize the access rule query handler.
   *
   * @param \Drupal\access_policy\AccessPolicyHandlerDefinition $definition
   *   The access policy handler definition.
   * @param string $operator
   *   The current operator.
   * @param mixed $value
   *   The current actual values.
   */
  public function initialize(AccessPolicyHandlerDefinition $definition, $operator, $value) {
    $this->definition = $definition;
    $this->operator = $operator;
    $this->value = $value;
    $this->realField = $this->getRealField();
    $this->tableAlias = $this->getTableAlias();
    $this->joinField = $this->getJoinField();
  }

  /**
   * Get the current values.
   *
   * @return array
   *   The current actual values.
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * Get the current operator.
   *
   * @return string
   *   The current operator.
   */
  public function getOperator() {
    return $this->operator;
  }

  /**
   * Set the query object that the access rule with act against.
   *
   * @param \Drupal\access_policy\QueryInterface $query
   *   The query object.
   */
  public function setQuery(QueryInterface $query) {
    $this->query = $query;
  }

  /**
   * Get the query object.
   *
   * @return \Drupal\access_policy\QueryInterface
   *   The manipulated query object.
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function query();

  /**
   * Get the table alias of a field.
   *
   * @return string
   *   The table alias.
   */
  public function getTableAlias() {
    $entity_type_id = $this->definition->getEntityType();
    $entity_field = $this->definition->getFieldName();
    return $this->getFieldTableName($entity_type_id, $entity_field);
  }

  /**
   * Get the real field name.
   *
   * @return string
   *   The field name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getRealField() {
    $entity_type_id = $this->definition->getEntityType();
    $entity_field = $this->definition->getFieldName();
    return $this->getFieldTableName($entity_type_id, $entity_field) . '.' . $this->getFieldColumnName($entity_type_id, $entity_field);
  }

  /**
   * Get the field to join against.
   *
   *  Most of the time this will be entity_id but if we're joining against the
   *  data table then it could be nid, tid etc.
   *
   * @return string
   *   The field name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getJoinField() {
    $entity_type_id = $this->definition->getEntityType();
    $entity_field = $this->definition->getFieldName();
    $definition = $this->entityTypeManager->getDefinition($entity_type_id);
    $table_name = $this->getFieldTableName($entity_type_id, $entity_field);
    // If we're joining against the data table then join against the id column.
    // E.g nid or tid. Otherwise, it will always be entity_id.
    if ($table_name == $definition->getDataTable()) {
      $id_key = $definition->getKey('id');
      return $this->getFieldColumnName($entity_type_id, $id_key);
    }

    return 'entity_id';
  }

}
