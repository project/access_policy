<?php

namespace Drupal\access_policy\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a AccessPolicyQuery annotation object.
 *
 * @Annotation
 */
class AccessPolicyQuery extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * An integer to determine the weight of this query plugin.
   *
   * Different query plugins perform differently based on certain conditions.
   * It's recommended to set weight based on most to least performant. For
   * example, joins are faster than sub-queries.
   *
   * @var int
   */
  public $weight = 0;

}
