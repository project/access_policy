<?php

namespace Drupal\access_policy\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a AccessRule annotation object.
 *
 * @Annotation
 */
class AccessRule extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The entity types that this access rule is for.
   *
   * Leave blank to support all entity types.
   *
   * @var array
   */
  public $entity_types = [];

}
