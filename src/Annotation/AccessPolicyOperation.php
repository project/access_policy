<?php

namespace Drupal\access_policy\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an AccessPolicyOperation annotation object.
 *
 * @Annotation
 */
class AccessPolicyOperation extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The entity operation that this evaluates as (e.g view, view all revisions).
   *
   * This is primarily used for drush. Use isCurrent() instead.
   *
   * @var string
   */
  public $operation;

  /**
   * The weight that controls the order of the operation during evaluation.
   *
   * It's possible to have two plugins for the same operation (e.g view and view
   * unpublished), this sets the order so that they are evaluated correctly.
   *
   * @var int
   */
  public $weight;

  /**
   * Flag indicating if permissions are supported.
   *
   * @var bool
   */
  public $permission;

  /**
   * Flag indicating if access rules are supported.
   *
   * @var bool
   */
  public $access_rules;

  /**
   * Flag indicating if this operation should show in the table by default.
   *
   * @var bool
   */
  public $show_column;

}
