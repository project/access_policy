<?php

namespace Drupal\access_policy\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a AccessRule argument object.
 *
 * @Annotation
 */
class AccessRuleArgument extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
