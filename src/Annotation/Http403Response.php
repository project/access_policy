<?php

namespace Drupal\access_policy\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Http403Response plugin for access policy.
 *
 * @Annotation
 */
class Http403Response extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The plugin settings.
   *
   * @var array
   */
  public $settings = [];

}
