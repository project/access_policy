<?php

namespace Drupal\access_policy;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\AlterableInterface;

/**
 * Access policy sql query stub.
 */
class SqlQuery implements QueryInterface, AlterableInterface {

  /**
   * Array of joined tables.
   *
   * @var array
   */
  protected $tables = [];

  /**
   * Array of conditions.
   *
   * @var array
   */
  protected $conditions = [];

  /**
   * Flag indicating whether the query is distinct.
   *
   * @var bool
   */
  protected $distinct = FALSE;

  /**
   * The base table.
   *
   * @var string
   */
  protected $baseTable;

  /**
   * The base field.
   *
   * @var string
   */
  protected $baseField;

  /**
   * The query metadata for alter purposes.
   */
  public array $alterMetaData;

  /**
   * The query tags.
   */
  public array $alterTags;

  /**
   * Initialize the sql query.
   *
   * @param string $base_table
   *   The base table.
   * @param string $base_field
   *   The base field.
   */
  public function init($base_table, $base_field) {
    $this->baseTable = $base_table;
    $this->baseField = $base_field;
  }

  /**
   * {@inheritdoc}
   */
  public function setDistinct() {
    return $this->distinct = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isDistinct() {
    return $this->distinct;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseTable() {
    return $this->baseTable;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseField() {
    return $this->baseField;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnection() {
    return Database::getConnection();
  }

  /**
   * {@inheritdoc}
   */
  public function getTables() {
    return $this->tables;
  }

  /**
   * {@inheritdoc}
   */
  public function join($table, $alias = NULL, $condition = NULL, $arguments = []) {
    return $this->addJoin('INNER', $table, $alias, $condition, $arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function leftJoin($table, $alias = NULL, $condition = NULL, $arguments = []) {
    return $this->addJoin('LEFT OUTER', $table, $alias, $condition, $arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function addJoin($type, $table, $alias = NULL, $condition = NULL, $arguments = []) {
    if (empty($alias)) {
      $alias = $table;
    }

    $alias_candidate = $alias;
    $count = 2;
    while (!empty($this->tables[$alias_candidate])) {
      $alias_candidate = $alias . '_' . $count++;
    }
    $alias = $alias_candidate;

    if (is_string($condition)) {
      $condition = str_replace('%alias', $alias, $condition);
    }

    $this->tables[$alias] = [
      'join type' => $type,
      'table' => $table,
      'alias' => $alias,
      'condition' => $condition,
      'arguments' => $arguments,
    ];

    return $alias;
  }

  /**
   * {@inheritdoc}
   */
  public function conditions() {
    return $this->conditions;
  }

  /**
   * {@inheritdoc}
   */
  public function condition($field, $value = NULL, $operator = NULL) {
    $this->conditions[] = [
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function andConditionGroup() {
    return $this->getConnection()->condition('AND');
  }

  /**
   * {@inheritdoc}
   */
  public function orConditionGroup() {
    return $this->getConnection()->condition('OR');
  }

  /**
   * {@inheritdoc}
   */
  public function addTag($tag) {
    $this->alterTags[$tag] = 1;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasTag($tag) {
    return isset($this->alterTags[$tag]);
  }

  /**
   * {@inheritdoc}
   */
  public function hasAllTags() {
    return !(boolean) array_diff(func_get_args(), array_keys($this->alterTags));
  }

  /**
   * {@inheritdoc}
   */
  public function hasAnyTag() {
    return (boolean) array_intersect(func_get_args(), array_keys($this->alterTags));
  }

  /**
   * {@inheritdoc}
   */
  public function addMetaData($key, $object) {
    $this->alterMetaData[$key] = $object;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetaData($key) {
    return $this->alterMetaData[$key] ?? NULL;
  }

}
