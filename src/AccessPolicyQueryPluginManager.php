<?php

namespace Drupal\access_policy;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The access policy query plugin manager.
 */
class AccessPolicyQueryPluginManager extends DefaultPluginManager {

  /**
   * AccessPolicyQueryPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/access_policy/AccessPolicyQuery', $namespaces, $module_handler, 'Drupal\access_policy\Plugin\access_policy\AccessPolicyQuery\AccessPolicyQueryInterface', 'Drupal\access_policy\Annotation\AccessPolicyQuery');

    $this->alterInfo('access_policy_query');
    $this->setCacheBackend($cache_backend, 'access_policy_query');
  }

  /**
   * Get the first applicable query plugin.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The query object.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyQuery\AccessPolicyQueryInterface|null
   *   The access policy query plugin or NULL if none found.
   */
  public function getApplicableQueryPlugin(AlterableInterface $query, EntityTypeInterface $entity_type) {
    $definitions = $this->getDefinitions();
    foreach ($definitions as $id => $definition) {
      $plugin = $this->createInstance($id);
      if ($plugin->isApplicable($query, $entity_type)) {
        return $plugin;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    // Fetch and sort definitions by weight.
    $definitions = $this->getCachedDefinitions();
    if (empty($definitions)) {
      $definitions = $this->findDefinitions();
      uasort($definitions, '\Drupal\Component\Utility\SortArray::sortByWeightElement');

      $this->setCachedDefinitions($definitions);
    }

    return $definitions;
  }

}
