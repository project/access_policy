<?php

namespace Drupal\access_policy;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for handling Access Policy query alter.
 *
 * @package Drupal\access_policy
 */
class AccessPolicyQueryAlter implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The access policy query plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyQueryPluginManager
   */
  protected $queryPluginManager;

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * The original query object.
   *
   * @var \Drupal\Core\Database\Query\AlterableInterface
   */
  protected $query;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Whether the current user is an admin.
   *
   * @var bool
   */
  protected $isAdminUser;

  /**
   * AccessPolicyQueryAlter constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\AccessPolicyQueryPluginManager $query_plugin_manager
   *   The access policy query plugin manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccessPolicyQueryPluginManager $query_plugin_manager, AccountInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->queryPluginManager = $query_plugin_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.access_policy_query'),
      $container->get('current_user'),
    );
  }

  /**
   * Alter a query.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The query.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see hook_query_alter()
   */
  public function queryAlter(AlterableInterface $query, EntityTypeInterface $entity_type) {
    $this->query = $query;
    $this->entityType = $entity_type;

    if (!$this->isAdminUser()) {
      $altered = $this->query->getMetaData('access_policy');
      if (!$altered) {
        $base_table = $this->getBaseTable($this->query, $this->entityType);

        // If the base table does not match the entity type then do not proceed
        // with query. This can happen in the case of books or forums which both
        // tag the query with node_access.
        if ($this->isEntityBaseTable($base_table, $this->query, $this->entityType)) {
          $handler = $this->queryPluginManager->getApplicableQueryPlugin($this->query, $this->entityType);

          $base_field = $entity_type->getKey('id');
          $select = new SqlQuery();
          $select->init($base_table, $base_field);
          $handler->init($select, $entity_type);
          $handler->query();
          $query_stub = $handler->getQuery();

          // Copy the joins and conditions to the main query.
          $this->joinTablesFromQueryStub($this->query, $query_stub);
          $this->addConditionsFromQueryStub($this->query, $query_stub);
        }
        $this->query->addMetaData('access_policy', TRUE);
      }
    }
  }

  /**
   * Determine whether the current user is an administrator.
   *
   * Administrators have access to all content. In that case we shouldn't
   * modify the query at all.
   *
   * @return bool
   *   TRUE if the user is an admin; FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function isAdminUser() {
    if (!isset($this->isAdminUser)) {
      $this->isAdminUser = $this->checkIfAdminUser();
    }

    return $this->isAdminUser;
  }

  /**
   * Check to see if the user is an administrator.
   *
   * @return bool
   *   TRUE if the user is an admin; FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function checkIfAdminUser() {
    $access_policies = $this->entityTypeManager->getStorage('access_policy')
      ->loadByProperties(['target_entity_type_id' => $this->entityType->id()]);

    foreach ($access_policies as $policy) {
      if (!$policy->isQueryEnabled()) {
        continue;
      }
      if (!$this->userCanView($policy)) {
        return FALSE;
      }
      if ($this->hasAccessRules($policy) && !$this->userCanBypassAccessRules($policy)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Determine whether the current user can view content with an access policy.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   *
   * @return bool
   *   TRUE if the user has access; FALSE otherwise.
   */
  protected function userCanView(AccessPolicyInterface $access_policy) {
    if ($access_policy->getOperationsHandler()->shouldValidatePermission('view')) {
      $entity_type = $access_policy->getTargetEntityType();
      $label = strtolower($entity_type->getLabel()->getUntranslatedString());
      $has_permission = $this->currentUser->hasPermission("view {$access_policy->id()} " . $label);

      if (!$has_permission) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Determine whether the access policy has rules that should be queried.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   *
   * @return bool
   *   TRUE if it has access rules; FALSE otherwise.
   */
  public function hasAccessRules(AccessPolicyInterface $access_policy) {
    $has_rules = $access_policy->getOperationsHandler()->shouldValidateAccessRules('view') && $access_policy->getAccessRules();
    if ($has_rules) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine whether the current user can bypass the access rules.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The current access policy.
   *
   * @return bool
   *   TRUE if the user can bypass the rules; FALSE otherwise.
   */
  public function userCanBypassAccessRules(AccessPolicyInterface $access_policy) {
    return $this->currentUser->hasPermission("bypass {$access_policy->id()} access rules");
  }

  /**
   * Get the current base table.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The query.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return string
   *   The current base table.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getBaseTable(AlterableInterface $query, EntityTypeInterface $entity_type) {
    $tables = $query->getTables();
    $base_table = $query->getMetaData('base_table');
    if (!$base_table) {
      /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
      $table_mapping = $this->entityTypeManager->getStorage($entity_type->id())->getTableMapping();
      $entity_base_tables = $table_mapping->getTableNames();

      foreach ($tables as $table_info) {
        if (!($table_info instanceof SelectInterface)) {
          $table = $table_info['table'];
          // If the table is either the base table or data table then use
          // one of those.
          if ($table == $entity_type->getBaseTable() || $table == $entity_type->getDataTable()) {
            // If the table has an alias then make sure and use that instead.
            $base_table = $table_info['alias'] ?? $table;
            break;
          }
          // If one of the node base tables are in the query, add it to the list
          // of possible base tables to join against.
          if (in_array($table, $entity_base_tables) && is_null($table_info['join type'])) {
            $base_table = $table_info['alias'] ?? $table;
            break;
          }
        }
      }
    }

    return $base_table;
  }

  /**
   * Determine whether the base table belongs to the entity.
   *
   * @param string $base_table
   *   The current base table.
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The query.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return bool
   *   TRUE if the base table belongs to the entity; FALSE otherwise.
   */
  private function isEntityBaseTable($base_table, AlterableInterface $query, EntityTypeInterface $entity_type) {
    // The base table can sometimes be an alias. Grab the actual table name.
    $tables = $query->getTables();
    if (isset($tables[$base_table])) {
      $base_table = $tables[$base_table]['table'];

      /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
      $table_mapping = $this->entityTypeManager->getStorage($entity_type->id())->getTableMapping();
      $entity_base_tables = $table_mapping->getTableNames();
      return in_array($base_table, $entity_base_tables);
    }

    return FALSE;
  }

  /**
   * Join tables from the query stub to the main query.
   *
   * This also ensures that any tables that have already been joined don't get
   * joined again as it could create duplicate results.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The main query.
   * @param \Drupal\Core\Database\Query\AlterableInterface $queryStub
   *   The access policy query object.
   */
  public function joinTablesFromQueryStub(AlterableInterface $query, AlterableInterface $queryStub) {
    $existing_tables = $query->getTables();
    $tables = $queryStub->getTables();
    $join_tables = array_filter($tables, function ($new_table) use ($existing_tables) {
      if (!is_null($new_table['join type'])) {
        $join_table = TRUE;
        // Make sure we don't join tables that have already been joined.
        foreach ($existing_tables as $table) {
          if ($new_table['table'] == $table['table'] && $new_table['alias'] == $table['alias']) {
            $join_table = FALSE;
            break;
          }
        }
        return $join_table;
      }
      return FALSE;
    });

    foreach ($join_tables as $table) {
      $query->addJoin($table['join type'], $table['table'], $table['alias'], $table['condition'], $table['arguments']);
    }
  }

  /**
   * Add conditions from a pseudo query.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The query.
   * @param \Drupal\Core\Database\Query\AlterableInterface $queryStub
   *   The access policy query object.
   */
  public function addConditionsFromQueryStub(AlterableInterface $query, AlterableInterface $queryStub) {
    $conditions = $queryStub->conditions();

    foreach ($conditions as $key => $condition) {
      if (is_int($key)) {
        $query->condition($condition['field'], $condition['value'], $condition['operator']);
      }
    }
  }

}
