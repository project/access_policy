<?php

namespace Drupal\access_policy\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for access policy 403 response.
 */
class Http403Controller extends ControllerBase {

  /**
   * The current route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a Http403Controller instance.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route_match service.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
    );
  }

  /**
   * The 403 response.
   *
   * @return array
   *   A render array containing the message to display for 403 pages.
   */
  public function on403() {
    // It's necessary to use route match because, for some reason, Drupal does
    // not provide parameters as function arguments for http4xx controllers.
    $policy = $this->routeMatch->getParameter('access_policy');
    $behavior = $policy->getHttp403Response();
    return $behavior->render();
  }

}
