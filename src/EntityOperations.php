<?php

namespace Drupal\access_policy;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface;
use Drupal\access_policy\Validation\ExecutionContext;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for reacting to entity events.
 */
class EntityOperations implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity storage handler.
   *
   * @var \Drupal\access_policy\ContentAccessPolicyManager
   */
  protected $storage;

  /**
   * The access policy selection service.
   *
   * @var \Drupal\access_policy\AccessPolicySelectionInterface
   */
  protected $selection;

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $information;

  /**
   * The access rule plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $accessRuleManager;

  /**
   * The selection rule manager.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $selectionRuleManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new EntityOperations object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\ContentAccessPolicyManager $storage
   *   The access policy content entity handler.
   * @param \Drupal\access_policy\AccessPolicySelectionInterface $selection
   *   The access policy selection service.
   * @param \Drupal\access_policy\AccessPolicyInformation $information
   *   The access policy information service.
   * @param \Drupal\access_policy\AccessPolicyHandlerManager $access_rule_manager
   *   The access rule manager.
   * @param \Drupal\access_policy\AccessPolicyHandlerManager $selection_rule_manager
   *   The selection rule manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ContentAccessPolicyManager $storage, AccessPolicySelectionInterface $selection, AccessPolicyInformation $information, AccessPolicyHandlerManager $access_rule_manager, AccessPolicyHandlerManager $selection_rule_manager, AccountInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->storage = $storage;
    $this->selection = $selection;
    $this->information = $information;
    $this->accessRuleManager = $access_rule_manager;
    $this->selectionRuleManager = $selection_rule_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('access_policy.content_policy_manager'),
      $container->get('access_policy.selection'),
      $container->get('access_policy.information'),
      $container->get('plugin.manager.access_policy.access_rule'),
      $container->get('plugin.manager.access_policy.selection_rule'),
      $container->get('current_user'),
    );
  }

  /**
   * Build the operations link.
   *
   * @see hook_entity_operation_alter();
   */
  public function entityOperationAlter(&$operations, EntityInterface $entity) {
    if ($this->information->isAccessControlledEntity($entity) && $this->isOperationLinkEnabled($entity)) {
      $operations['manage_access'] = [
        'title' => $this->t('Access'),
        'weight' => 50,
        'url' => $entity->toUrl('access-policy-form'),
      ];
    }
  }

  /**
   * Determine whether the operation link is enabled.
   *
   * @return bool
   *   TRUE if the operation link is enabled; FALSE otherwise.
   */
  private function isOperationLinkEnabled(EntityInterface $entity) {
    if ($entity->access('manage access') && $this->selection->isOperationsLinkEnabled($entity->getEntityTypeId())) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Assign an access policy to the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @see hook_entity_presave()
   */
  public function entityPresave(EntityInterface $entity) {
    if (!$this->storage->isPolicyUpdated($entity) && $this->information->hasEnabledForEntitiesOfEntityType($entity->getEntityType()) && $entity->language()->isDefault()) {
      $user = $this->currentUser;
      $do_update = FALSE;
      if ($entity->isNew()) {
        if ($this->storage->isEmpty($entity)) {
          $do_update = TRUE;
          $policy = $this->selection->getDefaultPolicy($entity, $user);
        }
      }
      elseif ($this->selection->assignPolicyOnUpdate($entity->getEntityTypeId())) {
        $do_update = $this->selection->shouldUpdateAccessPolicy($entity, $user);
        $policy = $this->selection->getApplicablePolicies($entity, $user);
      }

      if ($do_update) {
        // If no valid policy was found then remove any existing policy.
        if (empty($policy)) {
          $this->storage->clear($entity);
        }
        else {
          $this->storage->set($entity, $policy);
        }
      }
    }
  }

  /**
   * Add default operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @see hook_access_policy_presave()
   */
  public function accessPolicyPresave(EntityInterface $entity) {
    if ($entity->isNew()) {
      $operationPluginManager = \Drupal::service('plugin.manager.access_policy_operation');
      $definitions = $operationPluginManager->getApplicableDefinitions($entity->getTargetEntityType());
      if (empty($entity->getOperations())) {
        $operations = [];
        foreach ($definitions as $id => $definition) {
          $operations[$id]['permission'] = $definition['permission'] ?? FALSE;
          $operations[$id]['access_rules'] = $definition['access_rules'] ?? FALSE;
          $operations[$id]['show_column'] = $definition['show_column'] ?? FALSE;
        }
        $entity->setOperations($operations);
      }
    }
  }

  /**
   * Setting the default access policy on the entity form.
   *
   * This is primarily for supporting constraint validation during initial
   * creation of the entity. The access policy actually gets saved as part of
   * hook_entity_presave.
   *
   * @see hook_form_alter();
   */
  public function entityFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $form_obj = $form_state->getFormObject();
    $entity = $form_obj->getEntity();

    if ($entity->isNew()) {
      $policies = $this->information->getEnabledForEntitiesOfEntityType($entity->getEntityType());
      // This only works if the none of the policies have selection rules.
      // That's because there is no way to know beforehand what policy is
      // assigned before the field has a value.
      $has_rules = FALSE;
      foreach ($policies as $policy) {
        $selection_rules = $policy->getHandlers('selection_rule');
        if (!empty($selection_rules)) {
          $has_rules = TRUE;
          break;
        }
      }

      if (!empty($policies) && !$has_rules) {
        $default = $this->selection->getDefaultPolicy($entity, $this->currentUser);
        if (!empty($default)) {
          $this->storage->set($entity, $default);
        }
      }
    }
  }

  /**
   * Entity insert hook.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that was just saved.
   *
   * @see hook_entity_insert()
   */
  public function entityInsert(EntityInterface $entity) {
    if ($this->information->isAccessControlledEntity($entity)) {
      $this->updateOrCreateFromEntity($entity);
    }
  }

  /**
   * Entity update hook.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that was just saved.
   *
   * @see hook_entity_update()
   */
  public function entityUpdate(EntityInterface $entity) {
    if ($this->information->isAccessControlledEntity($entity)) {
      $this->updateOrCreateFromEntity($entity);
    }
  }

  /**
   * Creates or updates the access policy of an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to update or create an access policy for.
   */
  protected function updateOrCreateFromEntity(EntityInterface $entity) {
    // Clear the selection cache.
    $this->selection->resetCache();
  }

  /**
   * Hide the field on the edit form.
   *
   * @see hook_entity_field_access()
   */
  public function entityFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface|null $items = NULL) {
    if ($operation == 'edit') {
      $entity = $items->getEntity();
      // Get all access policies and see if they are controlling any fields on
      // this entity.
      $policies = $this->entityTypeManager->getStorage('access_policy')->loadByProperties([
        'target_entity_type_id' => $entity->getEntityTypeId(),
      ]);
      foreach ($policies as $policy) {
        if ($this->isFieldHiddenBySelectionRules($policy, $field_definition, $account)) {
          return AccessResult::forbidden();
        }
        if ($this->isFieldHiddenByEntityFieldAccessRuleWidget($policy, $field_definition, $account)) {
          return AccessResult::forbidden();
        }
      }
    }
    return AccessResult::neutral();
  }

  /**
   * Determine whether the field is being hidden by selection rules.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return bool
   *   TRUE if the field is being hidden by selection rules; FALSE otherwise.
   */
  protected function isFieldHiddenBySelectionRules(AccessPolicyInterface $access_policy, FieldDefinitionInterface $field_definition, AccountInterface $account) {
    $handlers = $access_policy->getHandlers('selection_rule');
    foreach ($handlers as $handler) {
      // Only check if this selection rule actually controls this field.
      if ($handler['field'] == $field_definition->getName() && isset($handler['settings']['field_access'])) {
        if ($handler['settings']['field_access']['type'] == 'permission') {
          $perm = $handler['settings']['field_access']['permission'];
          if (!$account->hasPermission($perm)) {
            return TRUE;
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * Determine whether the field is being hidden by an access rule widget.
   *
   * The entity_field widget has a settings for showing/hiding the field on the
   * entity edit form.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return bool
   *   TRUE if the field is being hidden by access rule; FALSE otherwise.
   */
  protected function isFieldHiddenByEntityFieldAccessRuleWidget(AccessPolicyInterface $access_policy, FieldDefinitionInterface $field_definition, AccountInterface $account) {
    $handlers = $this->accessRuleManager->getHandlersFromPolicy($access_policy);
    foreach ($handlers as $handler) {
      if ($handler->getDefinition()->getFieldName() == $field_definition->getName()) {
        if ($handler->getDefinition()->getWidget() == 'entity_field') {
          $widget_settings = $handler->getDefinition()->getSetting('widget');
          if (!empty($widget_settings['show']) && isset($widget_settings['settings']['hide_original_field'])) {
            if ($widget_settings['settings']['hide_original_field']) {
              return TRUE;
            }
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * Hide the field on the edit form.
   *
   * @see hook_entity_field_access()
   */
  public function userFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface|null $items = NULL) {
    if ($operation == 'edit') {
      $policies = $this->entityTypeManager->getStorage('access_policy')->loadMultiple();
      $is_observed = FALSE;
      $is_allowed = FALSE;
      foreach ($policies as $policy) {
        if ($this->isUserFieldObservedByAccessRule($policy, $field_definition)) {
          $is_observed = TRUE;
          if ($account->hasPermission('edit ' . $policy->id() . ' user information')) {
            // Multiple access policies could be observing the same field.
            // We don't want to exit prematurely if one of them fails.
            $is_allowed = TRUE;
            break;
          }
        }
      }
      if ($is_observed && !$is_allowed) {
        return AccessResult::forbidden();
      }
    }
    return AccessResult::neutral();
  }

  /**
   * Determine whether the field is a user field observed by an access rule.
   *
   * Only grant access if the user has permission.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return bool
   *   TRUE if the user field is being observed by access rules.
   */
  protected function isUserFieldObservedByAccessRule(AccessPolicyInterface $access_policy, FieldDefinitionInterface $field_definition) {
    $handlers = $this->accessRuleManager->getHandlersFromPolicy($access_policy);
    foreach ($handlers as $handler) {
      $handler_definition = $handler->getDefinition();

      if ($handler_definition->getEntityType() == 'user' && $handler->getDefinition()->getFieldName() == $field_definition->getName()) {
        if ($handler->getDefinition()->getFieldName() == $field_definition->getName()) {
          return TRUE;
        }
      }
      // @todo not all arguments will have getField().
      if ($handler->getArgument() && $handler->getArgument()->getPluginId() == 'current_user') {
        if ($handler->getArgument()->getField() == $field_definition->getName()) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Implements hook_field_widget_complete_form_alter().
   */
  public function fieldWidgetCompleteFormAlter(&$elements, FormStateInterface $form_state, $context) {
    $field_definition = $context['items']->getFieldDefinition();
    if ($handler = $this->getHandlerWithFiltering($field_definition)) {
      $handler = $this->initializeHandler($handler);

      // If the current user bypasses all access rule validation then do not
      // filter the field.
      $user = $handler->getContext()->get('user');
      $access_policy = $handler->getDefinition()->getAccessPolicy();
      if (!$user->hasPermission('bypass ' . $access_policy . ' access rules')) {
        // If the current field value is allowed then filter out all the others
        // that might not be. If the current value is not allowed then disable
        // the field entirely.
        if ($this->isCurrentValueAllowed($handler, $context['items'])) {
          $options = &$elements['widget']['#options'];
          $elements['widget']['#options'] = $this->filterAllowedOptions($options, $handler);
        }
        else {
          $elements['widget']['#disabled'] = TRUE;
        }
      }
    }
  }

  /**
   * Filter the allowed options.
   *
   * @param array $options
   *   The current allowed options.
   * @param \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface $handler
   *   The handler plugin.
   *
   * @return array
   *   Array of filtered options.
   */
  protected function filterAllowedOptions(array $options, AccessPolicyHandlerInterface $handler) {
    return array_filter($options, function ($option) use ($handler) {
      $allowed_values = $handler->getValue();
      if ($handler->validateSimple($allowed_values, [$option])) {
        return TRUE;
      }
      return FALSE;
    },
      ARRAY_FILTER_USE_KEY
    );
  }

  /**
   * Retrieve any access rule plugins with filtering.
   *
   * This is the access rule where the setting "filter allowed values" has been
   * enabled.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface|null
   *   The access rule plugin or NULL if not found.
   */
  private function getHandlerWithFiltering(FieldDefinitionInterface $field_definition) {
    $handlers = $this->accessRuleManager->getHandlersFromPolicies($field_definition->getTargetEntityTypeId(), function ($handler) use ($field_definition) {
      if ($handler->getDefinition()->getFieldName() == $field_definition->getName() && $handler->getDefinition()->getSetting('filter_allowed_values')) {
        return TRUE;
      }
      return FALSE;
    });

    if (!empty($handlers)) {
      return reset($handlers);
    }
  }

  /**
   * Get the initialized handler.
   *
   * @param \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface $handler
   *   The access rule plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function initializeHandler(AccessPolicyHandlerInterface $handler) {
    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    $context = ExecutionContext::create('edit', [
      'user' => $user,
    ]);
    $handler->setContext($context);
    return $handler;
  }

  /**
   * Determine whether the current field value is allowed for that user.
   *
   * If the current field value is not allowed for that user then the field
   * is disabled so that they can't modify it.
   *
   * @param \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface $handler
   *   The access rule plugin.
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The current field object.
   *
   * @return bool
   *   TRUE if it's allowed; FALSE otherwise.
   */
  private function isCurrentValueAllowed(AccessPolicyHandlerInterface $handler, FieldItemListInterface $field) {
    // First, check to see if that field has a value. If it doesn't then they
    // should have access.
    if ($field->isEmpty()) {
      return TRUE;
    }

    $storage = $field->getFieldDefinition()->getFieldStorageDefinition();
    $property_name = $storage->getMainPropertyName();
    $current_value = array_map(function ($item) use ($property_name) {
      return $item[$property_name];
    }, $field->getValue());

    $allowed_values = $handler->getValue();
    return $handler->validateSimple($allowed_values, $current_value);
  }

}
