<?php

namespace Drupal\access_policy;

use Drupal\access_policy\Entity\AccessPolicyInterface;

/**
 * Class AccessPolicyOperations.
 *
 * This is added to make it easier to transition away from access policy type
 * plugin.
 */
class AccessPolicyOperations {

  /**
   * Array of operations.
   *
   * @var array
   */
  protected $operations = [];

  /**
   * Constructs a new AccessPolicyOperations object.
   *
   * @param array $operations
   *   Array of operations from an access policy.
   */
  public function __construct(array $operations) {
    $this->operations = $operations;
  }

  /**
   * Load the operations from the access policy.
   *
   * @return static
   *   The access policy operations object.
   */
  public static function create(AccessPolicyInterface $access_policy) {
    return new static($access_policy->getOperations());
  }

  /**
   * Determine whether permissions should be validated.
   *
   * @param string $op
   *   The operation (e.g view, update, and delete).
   *
   * @return bool
   *   TRUE if permissions should be validated; FALSE otherwise.
   */
  public function shouldValidatePermission($op) {
    if (!empty($this->operations[$op]['permission'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine whether access rules are validated.
   *
   * @param string $op
   *   The operation (e.g view, update and delete).
   *
   * @return bool
   *   TRUE if rules should be validated; FALSE otherwise.
   */
  public function shouldValidateAccessRules($op) {
    if (!empty($this->operations[$op]['access_rules'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine whether the column should be shown for an operation.
   *
   * @param string $op
   *   The operation.
   *
   * @return bool
   *   TRUE if the column should be shown; FALSE otherwise.
   */
  public function showColumn($op) {
    if (!empty($this->operations[$op]['show_column'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Return the supported operations.
   *
   * @return string[]
   *   Array of operations (e.g view, update, delete, assign).
   */
  public function getAllOperations() {
    return array_keys($this->operations);
  }

  /**
   * Get all operations controlled by rules.
   *
   * Operations can be toggled per access rule so this returns an associative
   * array instead of indexed array.
   *
   * @return array
   *   Associative array of operations with flag indicating whether they are
   *   enabled or not. (e.g ['view' => TRUE, 'update' => FALSE, ...]).
   */
  public function getAccessRuleOperations() {
    $operations = [];
    foreach ($this->operations as $op => $restriction) {
      if (!empty($restriction['access_rules'])) {
        $operations[$op] = $restriction['access_rules'];
      }
    }
    return $operations;
  }

  /**
   * Get all operations controlled by permissions.
   *
   * @return array
   *   Array of operations.
   */
  public function getPermissionOperations() {
    $operations = [];
    foreach ($this->operations as $op => $restriction) {
      if (!empty($restriction['permission'])) {
        $operations[] = $op;
      }
    }
    return $operations;
  }

  /**
   * Determine whether this access policy supports permissions.
   *
   * @return bool
   *   TRUE if the access policy support permissions.
   */
  public function supportsPermissions() {
    foreach ($this->operations as $restriction) {
      if (!empty($restriction['permission'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Determine whether this access policy supports access rules.
   *
   * @return bool
   *   TRUE if the access policy support permissions.
   */
  public function supportsAccessRules() {
    foreach ($this->operations as $restriction) {
      if (!empty($restriction['access_rules'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
