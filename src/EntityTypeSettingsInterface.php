<?php

namespace Drupal\access_policy;

/**
 * Defines an Entity type settings interface.
 *
 * Each entity type (node, taxonomy term etc.) has its own access policy
 * settings.
 */
interface EntityTypeSettingsInterface {

  /**
   * Load the settings for a specific entity type.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return $this
   *   The entity type settings.
   */
  public function load($entity_type);

  /**
   * Gets data from this entity type settings.
   *
   * @param string $key
   *   A string that maps to a key within the entity type settings.
   *
   * @return mixed
   *   The data that was requested.
   */
  public function get($key);

  /**
   * Sets a value in entity type settings.
   *
   * @param string $key
   *   Identifier to store value in settings.
   * @param mixed $value
   *   Value to associate with identifier.
   *
   * @return $this
   *   The entity type settings.
   */
  public function set($key, $value);

  /**
   * Saves the entity type settings.
   *
   * @return $this
   */
  public function save();

}
