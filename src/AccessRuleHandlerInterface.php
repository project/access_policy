<?php

namespace Drupal\access_policy;

/**
 * Access rule handler interface.
 *
 * Base interface for access rule handlers.
 */
interface AccessRuleHandlerInterface {

  /**
   * Initialize the access rule handler.
   *
   * @param \Drupal\access_policy\AccessPolicyHandlerDefinition $definition
   *   The access policy handler definition.
   * @param string $operator
   *   The current operator.
   * @param mixed $value
   *   The current actual value.
   */
  public function initialize(AccessPolicyHandlerDefinition $definition, $operator, $value);

}
