<?php

namespace Drupal\access_policy;

/**
 * Access policy handler definition class.
 *
 * This class consolidates information about access policy handler plugin. For
 * example, the entity type and bundles are part of the access policy while the
 * field name is part of the access rule configuration. This consolidates that
 * information together so that it's easier to work with.
 */
class AccessPolicyHandlerDefinition {

  /**
   * The unique handler id.
   *
   * @var string
   */
  protected $id;

  /**
   * The group.
   *
   * This can be an entity type (node, media, user etc.) or environmental (env).
   *
   * @var string
   */
  protected $group;

  /**
   * The plugin id.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * The label of the handler.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the handler.
   *
   * @var string
   */
  protected $description;

  /**
   * The current access policy this handler is associated with.
   *
   * @var string
   */
  protected $accessPolicy;

  /**
   * The operator.
   *
   * If this value is set then it's immutable and the operator form will
   * be hidden. To allow it to be changed set the operator as part of settings.
   *
   * @var string
   */
  protected $operator;

  /**
   * The plugin settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * The target entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The field name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * The field type.
   *
   * @var string
   */
  protected $fieldType;

  /**
   * The access rule widget.
   *
   * @var string
   */
  protected $widget;

  /**
   * The contextual argument.
   *
   * @var array
   */
  protected $argument = [];

  /**
   * Flag indicating this handler is required.
   *
   * Required handlers can't be removed from the policy. This is mostly
   * used with policy types.
   *
   * @var bool
   */
  protected $required = FALSE;

  /**
   * Constructs a new access policy handler definition object.
   *
   * @param string $type
   *   The handler type.
   * @param string $group
   *   The access rule group.
   * @param string $id
   *   The unique access rule id.
   * @param array $config
   *   The access rule configuration values.
   */
  public function __construct($type, $group, $id, array $config) {
    // If there are handler type specific overrides then use that first.
    if (isset($config[$type])) {
      foreach ($config[$type] as $key => $value) {
        $config[$key] = $value;
      }
    }

    $this->id = $id;
    $this->group = $group;
    $this->pluginId = $config['plugin_id'];
    $this->label = $config['label'];
    $this->description = $config['description'] ?? '';
    $this->accessPolicy = $config['access_policy'] ?? NULL;
    // If no entity type is provided then default to the group. This helps
    // to keep the data definition brief. Also make sure that the group is
    // a property entity type.
    $group_is_entity_type = \Drupal::entityTypeManager()->getDefinition($group, FALSE);
    $entity_type = $group_is_entity_type ? $group : '';

    $this->entityType = $config['entity_type'] ?? $entity_type;
    $this->fieldName = $config['field'] ?? '';
    $this->widget = $config['widget'] ?? NULL;
    $this->required = $config['required'] ?? FALSE;
    $this->operator = $config['operator'] ?? NULL;
    $this->settings = $config['settings'] ?? [];
    $this->argument = $config['argument'] ?? NULL;

    if (!empty($this->fieldName)) {
      $fieldManager = \Drupal::service('entity_field.manager');
      $field_map = $fieldManager->getFieldMap();
      $this->fieldType = $field_map[$this->entityType][$this->fieldName]['type'];
    }
  }

  /**
   * Load the definition from existing config.
   *
   * @param string $type
   *   The handler type.
   * @param array $config
   *   The configuration.
   *
   * @return static
   *   The access policy handler definition object.
   */
  public static function create($type, array $config) {
    return new static($type, $config['group'], $config['id'], $config);
  }

  /**
   * Get the unique handler id.
   *
   * @return string
   *   The unique handler id.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Get the group that this handler is associated with.
   *
   * @return string
   *   The group name.
   */
  public function getGroup() {
    return $this->group;
  }

  /**
   * Get the handler id.
   *
   * @return string
   *   The plugin id.
   */
  public function getPluginId() {
    return $this->pluginId;
  }

  /**
   * Get the label of the handler.
   *
   * @return string
   *   The label.
   */
  public function getLabel() {
    if (!empty($this->settings['admin_label'])) {
      return $this->settings['admin_label'];
    }

    return $this->label;
  }

  /**
   * Get the description of handler.
   *
   * @return string
   *   The handler description from access policy data.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Get the access policy associated with handler rule.
   *
   * @return string
   *   The access policy id.
   */
  public function getAccessPolicy() {
    return $this->accessPolicy;
  }

  /**
   * Get the operator.
   *
   * @return string
   *   The operator.
   */
  public function getOperator() {
    return $this->operator;
  }

  /**
   * Get a plugin setting.
   *
   * @param string $setting_name
   *   The setting name.
   *
   * @return mixed
   *   The setting value.
   */
  public function getSetting($setting_name) {
    return $this->settings[$setting_name] ?? NULL;
  }

  /**
   * Get the plugin settings.
   *
   * @return array
   *   Array of handler configuration.
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * Get the field name.
   *
   * @return string
   *   The field name.
   */
  public function getFieldName() {
    return $this->fieldName;
  }

  /**
   * Get the field type.
   *
   * @return string
   *   The field type.
   */
  public function getFieldType() {
    return $this->fieldType;
  }

  /**
   * Get the target entity type id.
   *
   * @return string
   *   The entity type id.
   */
  public function getEntityType() {
    return $this->entityType;
  }

  /**
   * Determine whether this handler is required.
   *
   * @return bool
   *   TRUE if this handler is required; FALSE otherwise.
   */
  public function isRequired() {
    return $this->required;
  }

  /**
   * Get the widget (if supported).
   *
   * @return string
   *   The access rule widget plugin id.
   */
  public function getWidget() {
    return $this->widget;
  }

  /**
   * Get the current rule argument.
   *
   * @return array
   *   The access rule argument plugin id.
   */
  public function getArgument() {
    return $this->argument;
  }

  /**
   * Return array.
   *
   * This returns an array that can be saved to configuration. Note that label
   * and description are omitted.
   *
   * @return array
   *   Array of data.
   */
  public function getData() {
    return [
      'id' => $this->id,
      'group' => $this->group,
      'plugin_id' => $this->pluginId,
      'settings' => $this->settings,
      'entity_type' => $this->entityType,
      'field' => $this->fieldName,
      'field_type' => $this->fieldType,
      'required' => $this->required,
    ];
  }

}
