<?php

namespace Drupal\access_policy;

/**
 * Entity stub helper class.
 *
 * This creates a temporary entity stub that helps with validation selection
 * and queries.
 */
class EntityStubHelper {

  /**
   * Create a stub entity.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The stub entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function createStub($entity_type_id, $bundle = NULL) {
    // If no bundle is provided then grab any bundle.
    if (empty($bundle)) {
      /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundleInfo */
      $bundleInfo = \Drupal::service('entity_type.bundle.info');
      $bundles = $bundleInfo->getBundleInfo($entity_type_id);
      $bundle = key($bundles);
    }

    $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $bundle_key = $entity_type->getKey('bundle');
    return \Drupal::entityTypeManager()->getStorage($entity_type_id)->create([
      $bundle_key => $bundle,
    ]);
  }

}
