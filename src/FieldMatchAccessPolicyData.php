<?php

namespace Drupal\access_policy;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class to manage and lazy load cached access policy data.
 */
class FieldMatchAccessPolicyData implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $accessPolicyInfo;

  /**
   * Static cache of field definitions.
   *
   * @var array
   */
  protected $fieldDefinitions = [];

  /**
   * Constructs the FieldMatchAccessPolicyData access policy data set.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\access_policy\AccessPolicyInformation $access_policy_info
   *   The access policy information service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, AccessPolicyInformation $access_policy_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->accessPolicyInfo = $access_policy_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('access_policy.information'),
    );
  }

  /**
   * Defines the default access policy data.
   *
   * @see hook_access_policy_data()
   */
  public function accessPolicyData() {
    $data = [];
    $this->createDerivedAccessRules($data);
    return $data;
  }

  /**
   * Create derived access rules.
   *
   * @param array $data
   *   The current access rule data.
   */
  protected function createDerivedAccessRules(array &$data) {
    // It doesn't make sense to compare all entity types. Most of the time
    // users will only want to compare taxonomy terms or nodes.
    $allowed_entity_types = [
      'node',
      'taxonomy_term',
    ];
    $target_types = $this->entityTypeManager->getDefinitions();
    $entity_types = $this->accessPolicyInfo->getAllEnabledEntityTypes();
    foreach ($target_types as $target_type) {
      if (!in_array($target_type->id(), $allowed_entity_types)) {
        continue;
      }

      foreach ($entity_types as $entity_type) {
        $field_definitions = $this->getFieldDefinitions($entity_type, ['entity_reference']);
        foreach ($field_definitions as $definition) {
          $settings = $definition->getSetting('handler_settings');
          if ($definition instanceof FieldConfigInterface) {
            if ($this->userHasEntityReferenceField($target_type->id(), $settings)) {
              $data[$entity_type->id()]['match_' . $definition->getName()] = $this->createFieldMatchEntityReferenceAccessRule($entity_type, $definition);

              // Add support for term comparison with depth.
              if ($target_type->id() == 'taxonomy_term') {
                $data[$entity_type->id()]['match_depth_' . $definition->getName()] = $this->createTermReferenceWithDepthAccessRule($entity_type, $definition);
              }
            }
          }
        }
      }
    }

    $list_fields = [
      'list_string',
      'list_float',
      'list_integer',
    ];
    foreach ($list_fields as $field_type) {
      if ($this->userHasListField($field_type)) {
        foreach ($entity_types as $entity_type) {
          $field_definitions = $this->getFieldDefinitions($entity_type, [$field_type]);
          foreach ($field_definitions as $definition) {
            $data[$entity_type->id()]['match_' . $definition->getName()] = $this->createFieldMatchListAccessRule($entity_type, $definition);

            // Only add numeric match for numeric lists fields.
            if ($definition->getType() == 'list_float' || $definition->getType() == 'list_integer') {
              $data[$entity_type->id()]['numeric_match_' . $definition->getName()] = $this->createFieldMatchListNumericAccessRule($entity_type, $definition);
            }
          }
        }
      }
    }

    $entity_types = $this->accessPolicyInfo->getAllEnabledEntityTypes();
    foreach ($entity_types as $entity_type) {

      $has_author_field = $entity_type->hasKey('owner') && $this->userHasEntityReferenceField('user');
      if ($has_author_field) {
        $data[$entity_type->id()]['author_reference'] = $this->createAuthorReferenceAccessRule($entity_type);
      }

      if ($this->userHasEntityReferenceField($entity_type->id())) {
        $data[$entity_type->id()]['entity_reference'] = $this->createCurrentEntityReferenceAccessRule($entity_type);

        // This is primarily for supporting term hierarchy.
        if ($entity_type->id() == 'taxonomy_term') {
          $data[$entity_type->id()]['term_reference'] = $this->createCurrentTermReferenceAccessRule($entity_type);
        }
      }

      $field_definitions = $this->getFieldDefinitions($entity_type, ['entity_reference']);
      foreach ($field_definitions as $definition) {
        $field_storage = $definition->getFieldStorageDefinition();
        $is_user_ref_field = ($definition instanceof FieldConfigInterface && $field_storage->getSetting('target_type') == 'user');
        if ($is_user_ref_field) {
          $data[$entity_type->id()][$definition->getName() . '_user_reference'] = $this->createUserReferenceAccessRule($entity_type, $definition);
        }
      }
    }
  }

  /**
   * Create a field match list access rule.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   The field definition.
   *
   * @return array
   *   The access rule data definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createFieldMatchListAccessRule(EntityTypeInterface $entity_type, FieldDefinitionInterface $definition) {
    return [
      'label' => $this->t('Compare @entity_field with user', ['@entity_field' => $definition->getLabel()]),
      'description' => $this->t('Compare values between the @entity_type and current user.', [
        '@entity_type' => $entity_type->getSingularLabel(),
      ]),
      'plugin_id' => 'entity_field_list',
      'entity_type' => $entity_type->id(),
      'field' => $definition->getName(),
      'argument' => [
        'plugin' => 'current_user',
        'field_type' => $definition->getType(),
      ],
      'widget' => 'entity_field',
    ];
  }

  /**
   * Create a numeric field match list access rule.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   The field definition.
   *
   * @return array
   *   The access rule data definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createFieldMatchListNumericAccessRule(EntityTypeInterface $entity_type, FieldDefinitionInterface $definition) {
    return [
      'label' => $this->t('Compare @entity_field with user (numeric)', ['@entity_field' => $definition->getLabel()]),
      'description' => $this->t('Compare numeric values between the @entity_type and current user.', [
        '@entity_type' => $entity_type->getSingularLabel(),
      ]),
      'plugin_id' => 'entity_field_list_numeric',
      'entity_type' => $entity_type->id(),
      'field' => $definition->getName(),
      'argument' => [
        'plugin' => 'current_user',
        'field_type' => $definition->getType(),
      ],
      'widget' => 'entity_field',
    ];
  }

  /**
   * Create a field match entity reference access rule.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   The field definition.
   *
   * @return array
   *   The access rule data definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createFieldMatchEntityReferenceAccessRule(EntityTypeInterface $entity_type, FieldDefinitionInterface $definition) {
    $field_storage = $definition->getFieldStorageDefinition();
    $target_type_id = $field_storage->getSetting('target_type');
    $target_type = $this->entityTypeManager->getDefinition($target_type_id);
    return [
      'label' => $this->t('Compare @entity_field with user', ['@entity_field' => $definition->getLabel()]),
      'description' => $this->t('Compare @target_type between the @entity_type and current user.', [
        '@target_type' => $target_type->getPluralLabel(),
        '@entity_type' => $entity_type->getSingularLabel(),
      ]),
      'plugin_id' => 'entity_field_entity_reference',
      'entity_type' => $entity_type->id(),
      'field' => $definition->getName(),
      'argument' => [
        'plugin' => 'current_user',
        'field_type' => 'entity_reference',
      ],
      'widget' => 'entity_field',
    ];
  }

  /**
   * Create the user reference access rule.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   The field definition.
   *
   * @return array
   *   The access rule data definition.
   */
  protected function createUserReferenceAccessRule(EntityTypeInterface $entity_type, FieldDefinitionInterface $definition) {
    return [
      'label' => $this->t('@field_name field has reference to current user', [
        '@field_name' => $definition->getLabel(),
        '@entity_type' => $entity_type->getSingularLabel(),
      ]),
      'description' => $this->t('The @entity_type has a reference to the current user.', ['@entity_type' => $entity_type->getSingularLabel()]),
      'plugin_id' => 'entity_field_standard',
      'operator' => '=',
      'entity_type' => $entity_type->id(),
      'field' => $definition->getName(),
      'argument' => [
        'plugin' => 'current_user',
        'field' => 'uid',
      ],
      'widget' => 'entity_field',
    ];
  }

  /**
   * Create author reference access rule.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return array
   *   The access rule data definition.
   */
  protected function createAuthorReferenceAccessRule(EntityTypeInterface $entity_type) {
    $uid_field = $entity_type->getKey('owner');
    return [
      'label' => $this->t('Current user: Has reference to original author'),
      'description' => $this->t('The current user has a reference to the original author of the @entity_type.', ['@entity_type' => $entity_type->getSingularLabel()]),
      'plugin_id' => 'entity_field_entity_reference',
      'operator' => 'in',
      'entity_type' => $entity_type->id(),
      'field' => $uid_field,
      'argument' => [
        'plugin' => 'current_user',
        'field_type' => 'entity_reference',
      ],
    ];
  }

  /**
   * Create the entity reference access rule.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return array
   *   The access rule data definition.
   */
  public function createCurrentEntityReferenceAccessRule(EntityTypeInterface $entity_type) {
    $id_field = $entity_type->getKey('id');
    return [
      'label' => $this->t('Current user: Has reference to this @entity_type', ['@entity_type' => $entity_type->getSingularLabel()]),
      'description' => $this->t('The current user has a reference to this @entity_type.', ['@entity_type' => $entity_type->getSingularLabel()]),
      'plugin_id' => 'entity_field_standard',
      'operator' => '=',
      'entity_type' => $entity_type->id(),
      'field' => $id_field,
      'argument' => [
        'plugin' => 'current_user',
        'field_type' => 'entity_reference',
      ],
    ];
  }

  /**
   * Create the entity reference access rule.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return array
   *   The access rule data definition.
   */
  public function createCurrentTermReferenceAccessRule(EntityTypeInterface $entity_type) {
    $id_field = $entity_type->getKey('id');
    return [
      'label' => $this->t('Current user: Has reference to this @entity_type (with depth)', ['@entity_type' => $entity_type->getSingularLabel()]),
      'description' => $this->t('The current user has a reference to this @entity_type or its parent.', ['@entity_type' => $entity_type->getSingularLabel()]),
      'plugin_id' => 'term_reference_depth',
      'operator' => 'in',
      'entity_type' => $entity_type->id(),
      'field' => $id_field,
      'argument' => [
        'plugin' => 'current_user',
        'field_type' => 'entity_reference',
      ],
    ];
  }

  /**
   * Create the term reference with depth access rule.
   *
   * This access rule compares taxonomy terms while respecting the hierarchy.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   The term reference field access rule data definition.
   */
  protected function createTermReferenceWithDepthAccessRule(EntityTypeInterface $entity_type, FieldDefinitionInterface $field_definition) {
    $field_storage = $field_definition->getFieldStorageDefinition();
    $target_type_id = $field_storage->getSetting('target_type');
    $target_type = $this->entityTypeManager->getDefinition($target_type_id);
    return [
      'label' => $this->t('Compare @field with user (with depth)', ['@field' => $field_definition->getLabel()]),
      'description' => $this->t("Grant access to the @entity_type if its taxonomy terms match the terms in the user field, or are children of the user's terms.", [
        '@target_type' => $target_type->getPluralLabel(),
        '@entity_type' => $entity_type->getSingularLabel(),
      ]),
      'plugin_id' => 'term_reference_depth',
      'field' => $field_definition->getName(),
      'entity_type' => $entity_type->id(),
      'operator' => 'in',
      'argument' => [
        'plugin' => 'current_user',
        'field_type' => 'entity_reference',
      ],
      'widget' => 'entity_field',
    ];
  }

  /**
   * Determine whether the user has an list field.
   *
   * @return bool
   *   TRUE if the user has a list field; FALSE otherwise.
   */
  protected function userHasListField($type) {
    $fields = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    foreach ($fields as $field_definition) {
      $field_storage = $field_definition->getFieldStorageDefinition();
      if ($field_definition->getType() == $type && $field_storage instanceof FieldStorageConfigInterface && !$field_storage->isLocked()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Determine whether the user has an entity reference field.
   *
   * @param string $target_type
   *   The target type (e.g node, taxonomy term etc.).
   * @param array $settings
   *   The settings to compare against.
   *
   * @return bool
   *   TRUE if the user has an entity reference field; FALSE otherwise.
   */
  protected function userHasEntityReferenceField($target_type, array $settings = []) {
    $fields = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    foreach ($fields as $field_definition) {
      if ($field_definition->getType() == 'entity_reference') {
        $storage = $field_definition->getFieldStorageDefinition();
        $has_matching_settings = TRUE;
        if (!empty($settings)) {
          $handler_settings = $field_definition->getSetting('handler_settings');

          // If the node is using bundles and the user is using bundles then
          // the user must have matching bundles.
          if (isset($settings['target_bundles']) && isset($handler_settings['target_bundles'])) {
            $settings_bundles = $settings['target_bundles'] ?? [];
            $target_bundles = array_values($handler_settings['target_bundles']);
            $intersect = array_intersect($target_bundles, $settings_bundles);
            $has_matching_settings = !empty($intersect);
          }
        }
        if ($storage->getSetting('target_type') == $target_type && $has_matching_settings) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Get the field definition for a specific field type.
   *
   * Note that it only returns a definition for one supported bundle.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param array $field_types
   *   The field types array.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   Array of field definitions.
   */
  protected function getFieldDefinitions(EntityTypeInterface $entity_type, array $field_types) {
    $field_definitions = [];
    foreach ($field_types as $field_type) {
      $field_map = $this->entityFieldManager->getFieldMapByFieldType($field_type);
      $fields = $field_map[$entity_type->id()] ?? [];

      foreach ($fields as $field_name => $data) {
        $bundles = $data['bundles'];
        foreach ($bundles as $bundle) {
          if (empty($this->fieldDefinitions[$bundle])) {
            $this->fieldDefinitions[$bundle] = $this->entityFieldManager->getFieldDefinitions($entity_type->id(), $bundle);
          }
          if (isset($this->fieldDefinitions[$bundle][$field_name]) && !isset($field_definitions[$field_name])) {
            $field_definitions[$field_name] = $this->fieldDefinitions[$bundle][$field_name];
          }
        }
      }
    }
    return $field_definitions;
  }

}
