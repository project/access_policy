<?php

namespace Drupal\access_policy\Validation;

/**
 * Defines an execution context interface.
 *
 * Contexts can change for different handlers. For example, some access
 * rules only need the user entity while others need both entity and user
 * entity.
 */
interface ExecutionContextInterface {

  /**
   * Get the current operation.
   *
   * @return string
   *   The current operation (e.g view, update, delete etc.)
   */
  public function getOperation();

  /**
   * Get a specific context value.
   *
   * @param string $key
   *   The context key.
   *
   * @return mixed
   *   The execution context value.
   */
  public function get($key);

}
