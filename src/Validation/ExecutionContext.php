<?php

namespace Drupal\access_policy\Validation;

/**
 * Defines an execution context class.
 */
class ExecutionContext implements ExecutionContextInterface {

  /**
   * The current operation.
   *
   * @var string
   */
  protected $operation;

  /**
   * The current context array.
   *
   * @var array
   */
  protected $context = [];

  /**
   * Constructs a new ExecutionContext object.
   *
   * @param string $operation
   *   The current operation.
   * @param array $context
   *   The context array.
   */
  public function __construct($operation, array $context) {
    $this->operation = $operation;
    $this->context = $context;
  }

  /**
   * Create the execution context.
   *
   * @param string $operation
   *   The current operation.
   * @param array $context
   *   The context array.
   *
   * @return static
   *   The ExecutionContext object.
   */
  public static function create($operation, array $context) {
    return new static($operation, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function getOperation() {
    return $this->operation;
  }

  /**
   * {@inheritdoc}
   */
  public function get($key) {
    if (isset($this->context[$key])) {
      return $this->context[$key];
    }
    return NULL;
  }

}
