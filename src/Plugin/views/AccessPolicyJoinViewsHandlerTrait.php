<?php

namespace Drupal\access_policy\Plugin\views;

use Drupal\views\Views;

/**
 * Assist views handler plugins to join to the access_policy entity.
 */
trait AccessPolicyJoinViewsHandlerTrait {

  /**
   * {@inheritdoc}
   */
  public function ensureMyTable() {
    if (!isset($this->tableAlias)) {
      $table_alias = $this->query->ensureTable($this->table, $this->relationship);
      $storage = $this->entityTypeManager->getStorage($this->getEntityType());
      $table_name = $storage->getTableMapping()->getFieldTableName('access_policy');

      $left_entity_type = $this->entityTypeManager->getDefinition($this->getEntityType());
      $configuration = [
        'table' => $table_name,
        'field' => 'entity_id',
        'left_table' => $table_alias,
        'left_field' => $left_entity_type->getKey('id'),
      ];

      $join = Views::pluginManager('join')->createInstance('standard', $configuration);
      $this->tableAlias = $this->query->addRelationship($table_name, $join, $table_name);
    }

    return $this->tableAlias;
  }

}
