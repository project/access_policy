<?php

namespace Drupal\access_policy\Plugin\views\field;

use Drupal\access_policy\Plugin\views\AccessPolicyJoinViewsHandlerTrait;
use Drupal\views\Plugin\views\field\EntityField;

/**
 * A field handler for the computed access_policy field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("access_policy_field")
 */
class AccessPolicyField extends EntityField {

  use AccessPolicyJoinViewsHandlerTrait;

  /**
   * {@inheritdoc}
   */
  public function clickSort($order) {
    $this->ensureMyTable();

    $column_name = 'access_policy_target_id';
    $this->aliases[$column_name] = $this->tableAlias . '.' . $column_name;

    $this->query->addOrderBy(NULL, NULL, $order, $this->aliases[$column_name]);
  }

}
