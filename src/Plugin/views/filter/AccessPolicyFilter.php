<?php

namespace Drupal\access_policy\Plugin\views\filter;

use Drupal\access_policy\Plugin\views\AccessPolicyJoinViewsHandlerTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\filter\InOperator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter for Access policy.
 *
 * @ingroup access_policy
 *
 * @ViewsFilter("access_policy_filter")
 */
class AccessPolicyFilter extends InOperator {

  use AccessPolicyJoinViewsHandlerTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The storage handler of the access_policy entity type.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $accessPolicyStorage;

  /**
   * Constructs a new AccessPolicyFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->accessPolicyStorage = $entity_type_manager->getStorage('access_policy');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (isset($this->valueOptions)) {
      return $this->valueOptions;
    }
    $this->valueOptions = [];

    foreach ($this->accessPolicyStorage->loadByProperties(['target_entity_type_id' => $this->getEntityType()]) as $policy) {
      $this->valueOptions[$policy->id()] = $policy->label();
    }

    return $this->valueOptions;
  }

}
