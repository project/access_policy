<?php

namespace Drupal\access_policy\Plugin\Validation\Constraint;

use Drupal\access_policy\AccessPolicyInformation;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ValidAccessPolicyReference constraint.
 */
class ValidAccessPolicyReferenceConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $accessPolicyInfo;

  /**
   * Constructs a ValidAccessPolicyReferenceConstraintValidator object.
   *
   * @param \Drupal\access_policy\AccessPolicyInformation $access_policy_info
   *   The access policy info service.
   */
  public function __construct(AccessPolicyInformation $access_policy_info) {
    $this->accessPolicyInfo = $access_policy_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('access_policy.information'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    $subject = $entity->getEntity();
    $policies = $entity->referencedEntities();

    foreach ($policies as $delta => $policy) {
      if (!$this->accessPolicyInfo->isEnabledForEntitiesOfEntityType($policy, $subject->getEntityType())) {
        $this->context->buildViolation($constraint->supportedEntityType)
          ->setParameter('%label', $policy->label())
          ->atPath((string) $delta . '.entity')
          ->setInvalidValue($policy)
          ->addViolation();
      }
    }

  }

}
