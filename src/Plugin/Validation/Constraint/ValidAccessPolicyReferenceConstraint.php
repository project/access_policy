<?php

namespace Drupal\access_policy\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint for the access policy reference field.
 *
 * @Constraint(
 *   id = "ValidAccessPolicyReference",
 *   label = @Translation("Access policy reference valid reference", context = "Validation")
 * )
 */
class ValidAccessPolicyReferenceConstraint extends Constraint {

  /**
   * Violation message when the entity type does not support this access policy.
   *
   * @var string
   */
  public $supportedEntityType = 'The access policy (%label) cannot be referenced by this entity type.';

}
