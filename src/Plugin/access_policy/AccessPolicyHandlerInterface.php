<?php

namespace Drupal\access_policy\Plugin\access_policy;

use Drupal\access_policy\AccessPolicyHandlerDefinition;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Access policy handler interface.
 *
 * Access policy handler interface provides a generic interface for access rule
 * plugins.
 */
interface AccessPolicyHandlerInterface {

  /**
   * Initialize the access rule handler.
   *
   * @param \Drupal\access_policy\AccessPolicyHandlerDefinition $definition
   *   The access policy handler definition.
   */
  public function initialize(AccessPolicyHandlerDefinition $definition);

  /**
   * Checks whether the handler rule is applicable for the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return bool
   *   TRUE if the plugin is applicable; FALSE otherwise.
   */
  public function isApplicable(EntityInterface $entity);

  /**
   * Creates a generic configuration form for all access policy handler plugins.
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state);

  /**
   * Validate the access policy handler plugin.
   */
  public function validateSettingsForm(array &$form, FormStateInterface $form_state);

  /**
   * Submit the access policy handler plugin.
   */
  public function submitSettingsForm(array &$form, FormStateInterface $form_state);

  /**
   * The admin label.
   *
   * @return string
   *   The admin label of the handler.
   */
  public function adminLabel();

  /**
   * Render an admin summary of the current handler.
   *
   * @return string|null
   *   The admin summary of the current handler.
   */
  public function adminSummary();

}
