<?php

namespace Drupal\access_policy\Plugin\access_policy\Http403Response;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for 403 behavior.
 */
abstract class Http403ResponseBase extends PluginBase implements Http403ResponseInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setSettings($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [];
  }

  /**
   * The plugin settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($key) {
    return $this->settings[$key];
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings) {
    $this->settings = NestedArray::mergeDeepArray([
      $this->defaultSettings(),
      $settings,
    ], TRUE);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSetting($key, $value) {
    $this->settings[$key] = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, FormStateInterface $form_state) {
    return $form;
  }

}
