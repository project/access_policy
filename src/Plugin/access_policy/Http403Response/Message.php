<?php

namespace Drupal\access_policy\Plugin\access_policy\Http403Response;

use Drupal\Core\Form\FormStateInterface;

/**
 * Custom 403 message.
 *
 * @Http403Response(
 *   id = "message",
 *   label = @Translation("403 message"),
 *   description = @Translation("Shows a custom 403 message.")
 * )
 */
class Message extends Http403ResponseBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      'message' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, FormStateInterface $form_state) {
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Enter a custom message for the 403 page.'),
      '#default_value' => $this->settings['message'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      '#markup' => $this->settings['message'],
    ];
  }

}
