<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyQuery;

use Drupal\access_policy\SqlQuery;
use Drupal\access_policy\Validation\ExecutionContext;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Database\Query\ConditionInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Sql Sub query handler.
 *
 * This is a variant on the default Sql handler that uses subqueries. This can
 * be less performant but also helps to reduce duplicates. Especially when
 * multiple policies are assigned or access rules have multiple values.
 *
 * @AccessPolicyQuery(
 *   id = "sql_subquery",
 *   weight = 0,
 * )
 */
class SqlSubquery extends Sql {

  /**
   * The data table name.
   *
   * @var string
   */
  protected $dataTable;

  /**
   * The subquery.
   *
   * @var \Drupal\Core\Database\Query\AlterableInterface
   */
  protected $subQuery;

  /**
   * The base table of the subquery.
   *
   * @var string
   */
  protected $subBaseTable;

  /**
   * The base field of the subquery.
   *
   * @var string
   */
  protected $subBaseField;

  /**
   * {@inheritdoc}
   */
  public function isApplicable(AlterableInterface $query, EntityTypeInterface $entity_type) {
    // This is the last query plugin so, it will always be applicable if none
    // of the others are selected.
    return TRUE;
  }

  /**
   * Initialize the query handler.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The access policy query object.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   */
  public function init(AlterableInterface $query, EntityTypeInterface $entity_type) {
    parent::init($query, $entity_type);

    $this->subBaseTable = $entity_type->getDataTable();
    $this->subBaseField = $entity_type->getKey('id');
    $this->subQuery = $this->connection->select($this->subBaseTable)
      ->fields($this->subBaseTable, [$this->subBaseField]);
  }

  /**
   * Construct the query.
   */
  public function query() {

    $allowed_policies = $this->getAllowedAccessPolicies($this->entityType);
    $subquery_group = $this->query->orConditionGroup();

    // Make sure that the subquery also has the access policy table.
    $column_name = 'entity_id';
    $join_condition = "{$this->policyAlias}.{$column_name} = " . $this->getSubqueryBaseTablePlaceholder();
    $this->subQuery->leftJoin($this->policyTable, $this->policyAlias, $join_condition);

    foreach ($allowed_policies as $access_policy) {
      // If the user can't view it then ensure the data table so that we
      // can compare against the status column.
      if (!$this->userCanViewUnpublished($access_policy, $this->currentUser) && empty($this->dataTable)) {
        $this->dataTable = $this->ensureDataTable();
      }
      $this->accessPolicyQueryAlter($subquery_group, $access_policy);
    }
    $subquery_group->condition($this->getAccessPolicyColumn(), NULL, 'IS NULL');
    $subquery_group->condition($this->getAccessPolicyColumn(), '');
    $this->subQuery->condition($subquery_group);

    // If any policies exist then we need to add the OR group for
    // unassigned and empty policies. If no policies exist then we
    // shouldn't modify the query at all.
    $supported_policies = $this->getSupportedAccessPolicies($this->entityType);
    if (!empty($supported_policies)) {
      $or_group = $this->query->orConditionGroup();
      $or_group->condition($this->getBaseTablePlaceholder(), $this->subQuery, 'IN');
      $this->query->condition($or_group);
    }

  }

  /**
   * Get the base table placeholder unique to the subquery.
   */
  protected function getSubqueryBaseTablePlaceholder() {
    return $this->subBaseTable . '.' . $this->subBaseField;
  }

  /**
   * Ensure the data table.
   *
   * This is only joined if the user does not have access to unpublished content
   * and if it's not joined already.
   *
   * @return string
   *   The data table.
   */
  public function ensureDataTable() {
    $tables = $this->subQuery->getTables();

    $join = TRUE;
    $data_table = $this->entityType->getDataTable();
    foreach ($tables as $table_info) {
      $table = $table_info['table'];
      // If it's already joined then do not join it again.
      if ($table == $this->entityType->getDataTable()) {
        $join = FALSE;
        $data_table = $table_info['alias'] ?? $table;
      }
    }

    if ($join) {
      $this->subQuery->join($data_table, $data_table, $data_table . '.' . $this->subBaseTable . ' = ' . $this->subBaseTable . '.' . $this->subBaseField);
    }

    return $data_table;
  }

  /**
   * Alter a query based on Access Rules.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $condGroup
   *   The condition group.
   * @param array $access_rules
   *   The Access Rules.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function accessRulesQueryAlter(ConditionInterface $condGroup, array $access_rules) {
    foreach ($access_rules as $access_rule) {
      // In this context the access rule will never validate against a specific
      // entity, only the current user.
      $access_rule->setContext(ExecutionContext::create('view', ['user' => $this->currentUser]));

      // If a handler is provided then use that, otherwise fallback to the
      // query alter directly on the access rule.
      $handler = $access_rule->getHandler('query_alter');
      if ($handler) {
        $select = new SqlQuery();
        $select->init($this->subBaseTable, $this->subBaseField);
        $handler->setQuery($select);
        $handler->query();
        $select = $handler->getQuery();

        if ($select->isDistinct()) {
          $this->subQuery->distinct();
        }

        $this->joinTablesFromQueryStub($this->subQuery, $select);
        $this->addConditionsFromQueryStub($condGroup, $select);
      }
    }
  }

}
