<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyQuery;

use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Class for handling Access Policy query alter.
 *
 * This generates a dynamic query based on how access policies and access
 * rules are configured. The logic of the select query is always going to follow
 * this format.
 *
 * SELECT column FROM node_field_data
 * WHERE
 * access_policy = 'policy' AND (field = 0 AND/OR field2 = 3...) OR
 * access_policy = 'policy 2' AND (field = 1 AND/OR field2 = 3...)
 *
 * @package Drupal\access_policy
 */
interface AccessPolicyQueryInterface {

  /**
   * Initialize the query handler.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The access policy query object.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   */
  public function init(AlterableInterface $query, EntityTypeInterface $entity_type);

  /**
   * Construct the query.
   */
  public function query();

}
