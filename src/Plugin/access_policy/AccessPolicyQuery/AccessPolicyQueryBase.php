<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyQuery;

use Drupal\access_policy\AccessPolicyHandlerManager;
use Drupal\access_policy\AccessPolicyValidatorInterface;
use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\access_policy\EntityStubHelper;
use Drupal\access_policy\Plugin\access_policy\AccessRule\AccessRulePluginInterface;
use Drupal\access_policy\Validation\ExecutionContext;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Database\Query\ConditionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface definition for Access policy query plugins.
 */
abstract class AccessPolicyQueryBase extends PluginBase implements AccessPolicyQueryInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The access rule plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $accessRuleManager;

  /**
   * The access policy validator service.
   *
   * @var \Drupal\access_policy\AccessPolicyValidatorInterface
   */
  protected $accessPolicyValidator;

  /**
   * The current_user service.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * Array of access policies the current user has access to.
   *
   * @var array
   */
  protected $allowedAccessPolicies = NULL;

  /**
   * The query object.
   *
   * @var \Drupal\access_policy\QueryInterface
   */
  protected $query;

  /**
   * The base table name.
   *
   * @var string
   */
  protected $baseTable;

  /**
   * The base field name.
   *
   * @var string
   */
  protected $baseField;

  /**
   * Array of access rules grouped by access policy.
   *
   * @var array
   */
  protected $accessRules = [];

  /**
   * AccessPolicyQueryAlter constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\access_policy\AccessPolicyHandlerManager $access_rule_manager
   *   The access rule plugin manager.
   * @param \Drupal\access_policy\AccessPolicyValidatorInterface $access_policy_validator
   *   The access policy validator service.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current_user service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, AccessPolicyHandlerManager $access_rule_manager, AccessPolicyValidatorInterface $access_policy_validator, AccountProxy $current_user, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->accessRuleManager = $access_rule_manager;
    $this->accessPolicyValidator = $access_policy_validator;
    $this->currentUser = $this->entityTypeManager->getStorage('user')->load($current_user->id());
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.access_policy.access_rule'),
      $container->get('access_policy.validator'),
      $container->get('current_user'),
      $container->get('database')
    );
  }

  /**
   * Determine whether this query is applicable.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The query object.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return bool
   *   TRUE if this query is applicable; FALSE otherwise.
   */
  public function isApplicable(AlterableInterface $query, EntityTypeInterface $entity_type) {
    return TRUE;
  }

  /**
   * Initialize the query handler.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The access policy query object.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   */
  public function init(AlterableInterface $query, EntityTypeInterface $entity_type) {
    $this->query = $query;
    $this->entityType = $entity_type;
    $this->baseTable = $query->getBaseTable();
    $this->baseField = $query->getBaseField();
  }

  /**
   * Get the query object.
   *
   * @return \Drupal\access_policy\QueryInterface
   *   The access policy query object.
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * Construct the query.
   */
  public function query() {

    $this->ensureTable($this->query, $this->entityType);

    $allowed_policies = $this->getAllowedAccessPolicies($this->entityType);
    $or_group = $this->query->orConditionGroup();
    foreach ($allowed_policies as $access_policy) {
      $this->accessPolicyQueryAlter($or_group, $access_policy);
    }

    // If any policies exist then we need to add the OR group for
    // unassigned and empty policies. If no policies exist then we
    // shouldn't modify the query at all.
    $supported_policies = $this->getSupportedAccessPolicies($this->entityType);
    if (!empty($supported_policies)) {
      $column = $this->getAccessPolicyColumn();
      $or_group->condition($column, NULL, 'IS NULL');
      $or_group->condition($column, '');
      $this->query->condition($or_group);
    }
  }

  /**
   * Get all supported access policies for this entity type.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of access policies.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getSupportedAccessPolicies(EntityTypeInterface $entity_type) {
    return $this->entityTypeManager->getStorage('access_policy')
      ->loadByProperties(['target_entity_type_id' => $entity_type->id()]);
  }

  /**
   * Return the access policies that the user has access to.
   *
   * This will only return access policies that the user has access to and
   * that the user can view.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of access policy entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllowedAccessPolicies(EntityTypeInterface $entity_type) {
    if (!isset($this->allowedAccessPolicies)) {
      $access_policies = $this->getSupportedAccessPolicies($entity_type);
      $access_policies = array_filter($access_policies, function ($policy) {
        /** @var \Drupal\access_policy\Entity\AccessPolicyInterface $policy */
        if ($this->userCanView($policy, $this->currentUser)) {
          return TRUE;
        }
        return FALSE;
      });

      // Filter against any access rules that don't need to alter the query.
      // This will remove any access policies whose access rules fail.
      $access_policies = array_filter($access_policies, function ($policy) {
        if ($this->hasAccessRules($policy)) {
          return $this->validateNonQueryAccessRules($policy, $this->currentUser);
        }
        return TRUE;
      });

      $this->allowedAccessPolicies = $access_policies;
    }

    return $this->allowedAccessPolicies;
  }

  /**
   * Alter a query based on Access Policies.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $orGroup
   *   The condition group.
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The Access Policy.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  abstract protected function accessPolicyQueryAlter(ConditionInterface $orGroup, AccessPolicyInterface $access_policy);

  /**
   * Make sure that the access policy table has been joined properly.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The query.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   */
  abstract public function ensureTable(AlterableInterface $query, EntityTypeInterface $entity_type);

  /**
   * Get Access policy column name.
   *
   * @return string
   *   The column name.
   */
  abstract protected function getAccessPolicyColumn();

  /**
   * Determine whether the access policy has rules that should be queried.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   *
   * @return bool
   *   TRUE if it has access rules; FALSE otherwise.
   */
  protected function hasAccessRules(AccessPolicyInterface $access_policy) {
    $is_query_enabled = $access_policy->isQueryEnabled();
    $has_rules = $access_policy->getOperationsHandler()->shouldValidateAccessRules('view') && $access_policy->getAccessRules();
    $does_not_bypass = !$this->currentUser->hasPermission("bypass {$access_policy->id()} access rules");
    if ($is_query_enabled && $has_rules && $does_not_bypass) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine whether the current user can view content with an access policy.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return bool
   *   TRUE if the user has access; FALSE otherwise.
   */
  protected function userCanView(AccessPolicyInterface $access_policy, AccountInterface $account) {
    // If the query is enabled for the access policy then check to see if
    // they have access first. If it's not enabled then they always have
    // access.
    if ($access_policy->isQueryEnabled() && $access_policy->getOperationsHandler()->shouldValidatePermission('view')) {
      $entity_type = $access_policy->getTargetEntityType();
      $label = strtolower($entity_type->getLabel()->getUntranslatedString());
      $has_permission = $account->hasPermission("view {$access_policy->id()} " . $label);

      if (!$has_permission) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Validate the non query alter access rules.
   *
   * Access rules can validate against user/environmental attributes and entity
   * attributes. Only entity attributes actually need to alter the query. The
   * rest can go through normal access rule validation.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return bool
   *   TRUE if the access rule passes; FALSE otherwise.
   */
  protected function validateNonQueryAccessRules(AccessPolicyInterface $access_policy, AccountInterface $account) {
    // Validate the non query alter access rules.
    $plugins = $this->getAccessRulePlugins($access_policy, ['validate' => TRUE]);
    if (!empty($plugins)) {
      $entity = EntityStubHelper::createStub($access_policy->getTargetEntityTypeId());

      $context = ExecutionContext::create('view', [
        'entity' => $entity,
        'user' => $account,
      ]);
      $is_valid = $this->accessPolicyValidator->validateAccessRulePlugins($plugins, $context, $access_policy->getAccessRuleOperator());
      if (!$is_valid) {
        // Only exit if this is an AND operator or all the access rules have
        // been validated.
        $has_query_alter_plugins = $this->getAccessRulePlugins($access_policy, ['query_alter' => TRUE]);
        if ($access_policy->getAccessRuleOperator() == 'AND' || !$has_query_alter_plugins) {
          return FALSE;
        }
      }

      // If the validated access rule passed, and it's OR, then it's not
      // necessary for other rules to alter the query. Remove them from the
      // array.
      if ($is_valid && $access_policy->getAccessRuleOperator() == 'OR') {
        $this->accessRules[$access_policy->id()] = array_filter($this->accessRules[$access_policy->id()], function ($rule) {
          if (isset($rule['query_alter'])) {
            return FALSE;
          }
          return TRUE;
        });
      }
    }

    return TRUE;
  }

  /**
   * Get access rule plugins from a policy.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param array $filter
   *   Filter by query alter or validate.
   *
   * @return array
   *   Array of access rules.
   */
  protected function getAccessRulePlugins(AccessPolicyInterface $access_policy, array $filter = []) {
    if (!isset($this->accessRules[$access_policy->id()])) {
      $this->accessRules[$access_policy->id()] = [];

      $plugins = $this->accessRuleManager->getHandlersFromPolicy($access_policy);
      foreach ($plugins as $plugin) {
        $definition = $plugin->getDefinition();
        $settings = $definition->getSettings();

        // If query is enabled for this access rule then continue to fetch and
        // group them.
        if (isset($settings['query']) && $settings['query']) {
          $this->addAccessRule($access_policy, $plugin);
        }
      }
    }

    $access_rules = $this->accessRules[$access_policy->id()];

    if (!empty($filter)) {
      $access_rules = $this->filterAccessRulePlugins($access_rules, $filter);
    }

    $plugins = [];
    foreach ($access_rules as $access_rule) {
      $plugins[] = $access_rule['plugin'];
    }

    return $plugins;
  }

  /**
   * Store the access rule in the static cache.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param \Drupal\access_policy\Plugin\access_policy\AccessRule\AccessRulePluginInterface $plugin
   *   The access rule plugin.
   */
  protected function addAccessRule(AccessPolicyInterface $access_policy, AccessRulePluginInterface $plugin) {
    $definition = $plugin->getDefinition();
    $id = $definition->getId();
    $access_rule = [
      'plugin' => $plugin,
    ];

    if ($this->isQueryAlterAccessRule($access_policy, $plugin)) {
      $access_rule['query_alter'] = TRUE;
    }
    elseif ($this->isValidationAccessRule($access_policy, $plugin)) {
      $access_rule['validate'] = TRUE;
    }

    $this->accessRules[$access_policy->id()][$id] = $access_rule;
  }

  /**
   * Determine if the access rule performs a query alter.
   *
   * If the plugin group matches the current access policy entity type
   * then we can perform a query alter.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param \Drupal\access_policy\Plugin\access_policy\AccessRule\AccessRulePluginInterface $plugin
   *   The access rule plugin.
   *
   * @return bool
   *   TRUE if the access rule can perform query alter; FALSE otherwise.
   */
  protected function isQueryAlterAccessRule(AccessPolicyInterface $access_policy, AccessRulePluginInterface $plugin) {
    $definition = $plugin->getDefinition();
    if ($definition->getGroup() == $access_policy->getTargetEntityTypeId()) {
      $plugin_definition = $plugin->getPluginDefinition();
      if (isset($plugin_definition['handlers']['query_alter'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Determine if the access rule performs normal validation.
   *
   * If the group doesn't match the current access policy entity type
   * then we are going to perform normal access rule validation.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The current access policy.
   * @param \Drupal\access_policy\Plugin\AccessRule\AccessRulePluginInterface $plugin
   *   The access rule plugin.
   *
   * @return bool
   *   TRUE if the access rule should perform normal validation.
   */
  protected function isValidationAccessRule(AccessPolicyInterface $access_policy, AccessRulePluginInterface $plugin) {
    $definition = $plugin->getDefinition();
    if ($definition->getGroup() != $access_policy->getTargetEntityTypeId()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Filter access rules by property.
   *
   * @param array $access_rules
   *   Array of access rules.
   * @param array $filter
   *   Filter array.
   *
   * @return array
   *   Array of filtered access rules.
   */
  protected function filterAccessRulePlugins(array $access_rules, array $filter) {
    $filtered_rules = [];
    foreach ($access_rules as $item) {
      if (!empty($filter)) {
        $return = TRUE;
        foreach ($filter as $key => $value) {
          if (!isset($item[$key]) || (isset($item[$key]) && $item[$key] != $value)) {
            $return = FALSE;
          }
        }
        if ($return) {
          $filtered_rules[] = $item;
        }
      }
    }

    return $filtered_rules;
  }

  /**
   * Determine whether the current user can view unpublished content.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return bool
   *   TRUE if the user has access; FALSE otherwise.
   */
  protected function userCanViewUnpublished(AccessPolicyInterface $access_policy, AccountInterface $account) {
    if ($access_policy->isQueryEnabled() && $access_policy->getOperationsHandler()->shouldValidatePermission('view_unpublished')) {
      $entity_type = $access_policy->getTargetEntityType();
      $label = strtolower($entity_type->getLabel()->getUntranslatedString());
      return $account->hasPermission("view any {$access_policy->id()} unpublished " . $label);
    }

    return TRUE;
  }

  /**
   * Determine whether the query was actually altered.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $condGroup
   *   The condition group.
   *
   * @return bool
   *   TRUE if the query was altered, FALSE otherwise.
   */
  protected function queryIsAltered(ConditionInterface $condGroup) {
    foreach ($condGroup->conditions() as $key => $condition) {
      if (is_int($key)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
