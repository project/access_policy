<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyQuery;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\access_policy\QueryInterface;
use Drupal\access_policy\SqlQuery;
use Drupal\access_policy\Validation\ExecutionContext;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Database\Query\ConditionInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Sql query handler.
 *
 * This generates a dynamic query based on how access policies and access
 * rules are configured. The logic of the select query is always going to follow
 * this format.
 *
 * SELECT column FROM node_field_data
 * WHERE
 * access_policy = 'policy' AND (field = 0 AND/OR field2 = 3...) OR
 * access_policy = 'policy 2' AND (field = 1 AND/OR field2 = 3...)
 *
 * @AccessPolicyQuery(
 *   id = "sql",
 *   weight = -10,
 * )
 */
class Sql extends AccessPolicyQueryBase {

  /**
   * The access policy table name.
   *
   * @var string
   */
  protected $policyTable;

  /**
   * The access policy table alias.
   *
   * @var string
   */
  protected $policyAlias = 'cap';

  /**
   * The access policy column name.
   *
   * @var string
   */
  protected $policyColumn = 'access_policy_target_id';

  /**
   * The data table name.
   *
   * @var string
   */
  protected $dataTable;

  /**
   * {@inheritdoc}
   */
  public function isApplicable(AlterableInterface $query, EntityTypeInterface $entity_type) {
    $access_polices = $this->getAllowedAccessPolicies($entity_type);
    if ($this->hasPoliciesInSameSelectionSet($access_polices)) {
      return FALSE;
    }
    if ($this->hasPoliciesWithMultiValueAccessRules($access_polices)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Determine whether there are any policies in the same selection set.
   *
   * Selection sets allow for multiple access policies to be chosen. If they
   * are in the same set then we shouldn't use this query because it could
   * cause duplicates.
   *
   * @param array $access_policies
   *   Array of access policies.
   *
   * @return bool
   *   TRUE if any policies are in the same set; FALSE otherwise.
   */
  protected function hasPoliciesInSameSelectionSet(array $access_policies) {
    $selection_sets = [];
    foreach ($access_policies as $policy) {
      $set = $policy->getSelectionSet();
      $selection_sets = array_merge($selection_sets, $set);
    }
    $selection_sets = array_count_values($selection_sets);
    foreach ($selection_sets as $count) {
      if ($count > 1) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Determine whether there are any policies with multi-value access rules.
   *
   * Access policies with multi-value access rules can cause duplicate results
   * with this query. Therefore, if this returns TRUE then we shouldn't use this
   * query.
   *
   * @param array $access_policies
   *   Array of access policies.
   *
   * @return bool
   *   TRUE if any policies have multi-value access rules. FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function hasPoliciesWithMultiValueAccessRules(array $access_policies) {
    foreach ($access_policies as $policy) {
      if ($this->hasAccessRules($policy)) {
        $access_rules = $this->getAccessRulePlugins($policy, ['query_alter' => TRUE]);
        foreach ($access_rules as $plugin) {
          $field_name = $plugin->getDefinition()->getFieldName();
          if (!empty($field_name)) {
            $entity_type = $plugin->getDefinition()->getEntityType();
            $definition = $this->getFieldStorageDefinition($entity_type, $field_name);
            // If the field supports saving multiple values then return TRUE
            // and let the next query plugin handle it.
            if ($definition && $definition->isMultiple()) {
              return TRUE;
            }
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * Get the field storage definition.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $field_name
   *   The field name.
   *
   * @return \Drupal\Core\Field\FieldStorageDefinitionInterface|null
   *   The field storage definition or NULL if not found.
   */
  protected function getFieldStorageDefinition($entity_type, $field_name) {
    $definitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type);
    if (isset($definitions[$field_name])) {
      return $definitions[$field_name];
    }
    return NULL;
  }

  /**
   * Initialize the query handler.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The access policy query object.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   */
  public function init(AlterableInterface $query, EntityTypeInterface $entity_type) {
    parent::init($query, $entity_type);

    $storage = $this->entityTypeManager->getStorage($entity_type->id());
    $this->policyTable = $storage->getTableMapping()->getFieldTableName('access_policy');
  }

  /**
   * Construct the query.
   */
  public function query() {
    $access_policies = $this->getAllowedAccessPolicies($this->entityType);
    foreach ($access_policies as $access_policy) {
      // If the user can't view it then ensure the data table so that we
      // can compare against the status column.
      if (!$this->userCanViewUnpublished($access_policy, $this->currentUser) && empty($this->dataTable)) {
        $this->dataTable = $this->ensureDataTable();
      }
    }

    parent::query();
  }

  /**
   * Ensure the data table.
   *
   * This is only joined if the user does not have access to unpublished content
   * and if it's not joined already.
   *
   * @return string
   *   The data table.
   */
  public function ensureDataTable() {
    $tables = $this->query->getTables();

    $join = TRUE;
    $data_table = $this->entityType->getDataTable();
    foreach ($tables as $table_info) {
      $table = $table_info['table'];
      // If it's already joined then do not join it again.
      if ($table == $this->entityType->getDataTable()) {
        $join = FALSE;
        $data_table = $table_info['alias'] ?? $table;
      }
    }

    if ($join) {
      $this->query->join($data_table, $data_table, $data_table . '.' . $this->baseField . ' = ' . $this->baseTable . '.' . $this->baseField);
    }

    return $data_table;
  }

  /**
   * Make sure that the access policy table has been joined properly.
   */
  public function ensureTable(AlterableInterface $query, EntityTypeInterface $entity_type) {
    $tables = $this->query->getTables();
    $join = TRUE;
    foreach ($tables as $table) {
      // Ensure that we don't join the table twice. this can happen when
      // filtering on views.
      if ($table['table'] == $this->policyTable) {
        $join = FALSE;
        break;
      }
    }

    if ($join) {
      $column_name = 'entity_id';
      $join_condition = "{$this->policyAlias}.{$column_name} = " . $this->getBaseTablePlaceholder();

      $this->query->leftJoin($this->policyTable, $this->policyAlias, $join_condition);
    }
  }

  /**
   * Alter a query based on Access Policies.
   *
   * The access policy will always have the logic of OR access_policy = 'policy'
   * AND (access rules AND/OR).
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $orGroup
   *   The condition group.
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The Access Policy.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function accessPolicyQueryAlter(ConditionInterface $orGroup, AccessPolicyInterface $access_policy) {
    // Only alter the query if there are applicable access rules.
    $has_access_rules = $this->hasAccessRules($access_policy) && $this->getAccessRulePlugins($access_policy, ['query_alter' => TRUE]);
    if ($has_access_rules) {
      $operator = $access_policy->getAccessRuleOperator();
      switch ($operator) {
        case 'AND':
          $this->createAndGroupCondition($orGroup, $access_policy);
          break;

        case 'OR':
          $this->createOrGroupCondition($orGroup, $access_policy);
          break;
      }
    }

    // If no rules are supported or no query was altered then do a normal
    // condition without any AND group.
    if (!$has_access_rules || !$this->queryIsAltered($orGroup)) {
      $this->createConditionWithoutAccessRules($orGroup, $access_policy);
    }
  }

  /**
   * Get the base table placeholder.
   *
   * @return string
   *   The base table.
   */
  protected function getBaseTablePlaceholder() {
    return $this->baseTable . '.' . $this->baseField;
  }

  /**
   * Create the And group condition for access rule plugins.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $orGroup
   *   The condition group.
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The Access Policy.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  private function createAndGroupCondition(ConditionInterface $orGroup, AccessPolicyInterface $access_policy) {
    $access_rules = $this->getAccessRulePlugins($access_policy, ['query_alter' => TRUE]);
    $andGroup = $this->query->andConditionGroup();

    $this->accessRulesQueryAlter($andGroup, $access_rules);

    // If the query was actually altered then create the AND group and add the
    // conditions. This helps to prevent plugins without queries from breaking
    // the query.
    if ($this->queryIsAltered($andGroup)) {
      $andGroup->condition($this->getAccessPolicyColumn(), $access_policy->id());

      // If the user can't view unpublished content then require status=1.
      if (!$this->userCanViewUnpublished($access_policy, $this->currentUser)) {
        $status = $this->entityType->getKey('published');
        $andGroup->condition($this->dataTable . '.' . $status, TRUE);
      }

      $orGroup->condition($andGroup);
    }
  }

  /**
   * Create the OR group condition for access rule plugins.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $orGroup
   *   The condition group.
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The Access Policy.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  private function createOrGroupCondition(ConditionInterface $orGroup, AccessPolicyInterface $access_policy) {
    $access_rules = $this->getAccessRulePlugins($access_policy, ['query_alter' => TRUE]);
    $condGroup = $this->query->orConditionGroup();

    $this->accessRulesQueryAlter($condGroup, $access_rules);

    // If the query was actually altered then create the AND group and add the
    // OR conditions. This helps to prevent plugins without queries from
    // breaking the query.
    if ($this->queryIsAltered($condGroup)) {
      $andGroup = $this->query->andConditionGroup();
      $andGroup->condition($condGroup);
      $andGroup->condition($this->getAccessPolicyColumn(), $access_policy->id());

      // If the user can't view unpublished content then require status=1.
      if (!$this->userCanViewUnpublished($access_policy, $this->currentUser)) {
        $status = $this->entityType->getKey('published');
        $andGroup->condition($this->dataTable . '.' . $status, TRUE);
      }

      $orGroup->condition($andGroup);
    }
  }

  /**
   * Create condition for policy without access rules.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $orGroup
   *   The current or group.
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The current access policy.
   */
  private function createConditionWithoutAccessRules(ConditionInterface $orGroup, AccessPolicyInterface $access_policy) {
    // If the user can't view unpublished content then require status=1.
    if (!$this->userCanViewUnpublished($access_policy, $this->currentUser)) {
      $andGroup = $this->query->andConditionGroup();
      $andGroup->condition($this->getAccessPolicyColumn(), $access_policy->id());
      $status = $this->entityType->getKey('published');
      $andGroup->condition($this->dataTable . '.' . $status, TRUE);

      $orGroup->condition($andGroup);
    }
    else {
      $orGroup->condition($this->getAccessPolicyColumn(), $access_policy->id());
    }
  }

  /**
   * Alter a query based on Access Rules.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $condGroup
   *   The condition group.
   * @param array $access_rules
   *   The Access Rules.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function accessRulesQueryAlter(ConditionInterface $condGroup, array $access_rules) {
    foreach ($access_rules as $access_rule) {
      // In this context the access rule will never validate against a specific
      // entity, only the current user.
      $access_rule->setContext(ExecutionContext::create('view', ['user' => $this->currentUser]));

      // If a handler is provided then use that, otherwise fallback to the
      // query alter directly on the access rule.
      $handler = $access_rule->getHandler('query_alter');
      if ($handler) {
        $select = new SqlQuery();
        $select->init($this->baseTable, $this->baseField);
        $handler->setQuery($select);
        $handler->query();
        $select = $handler->getQuery();

        if ($select->isDistinct()) {
          $this->query->distinct();
        }

        $this->joinTablesFromQueryStub($this->query, $select);
        $this->addConditionsFromQueryStub($condGroup, $select);
      }
    }
  }

  /**
   * Join tables from the query stub to the main query.
   *
   * This also ensures that any tables that have already been joined don't get
   * joined again as it could create duplicate results.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The main query.
   * @param \Drupal\Core\Database\Query\AlterableInterface $queryStub
   *   The access policy query object.
   */
  public function joinTablesFromQueryStub(AlterableInterface $query, AlterableInterface $queryStub) {
    $existing_tables = $query->getTables();
    $tables = $queryStub->getTables();
    $join_tables = array_filter($tables, function ($new_table) use ($existing_tables) {
      if (!is_null($new_table['join type'])) {
        $join_table = TRUE;
        // Make sure we don't join tables that have already been joined.
        foreach ($existing_tables as $table) {
          if ($new_table['table'] == $table['table'] && $new_table['alias'] == $table['alias']) {
            $join_table = FALSE;
            break;
          }
        }
        return $join_table;
      }
      return FALSE;
    });

    foreach ($join_tables as $table) {
      $query->addJoin($table['join type'], $table['table'], $table['alias'], $table['condition'], $table['arguments']);
    }
  }

  /**
   * Add conditions from a pseudo query.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $condGroup
   *   The condition group.
   * @param \Drupal\access_policy\QueryInterface $queryStub
   *   The access policy query object.
   */
  public function addConditionsFromQueryStub(ConditionInterface $condGroup, QueryInterface $queryStub) {
    $conditions = $queryStub->conditions();

    foreach ($conditions as $key => $condition) {
      if (is_int($key)) {
        $condGroup->condition($condition['field'], $condition['value'], $condition['operator']);
      }
    }
  }

  /**
   * Get Access policy column name based on the table name.
   *
   * @return string
   *   The column name.
   */
  protected function getAccessPolicyColumn(): string {
    return $this->policyAlias . '.' . $this->policyColumn;
  }

}
