<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRuleArgument;

use Drupal\access_policy\AccessPolicyHandlerDefinition;
use Drupal\access_policy\Validation\ExecutionContextInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for access rule argument plugins.
 */
abstract class AccessRuleArgumentPluginBase extends PluginBase implements AccessRuleArgumentPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The access rule argument configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The access policy handler definition object.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerDefinition
   */
  protected $definition;

  /**
   * The execution context.
   *
   * @var \Drupal\access_policy\Validation\ExecutionContext
   */
  protected $context;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * Initialize the access rule argument.
   *
   * @param \Drupal\access_policy\AccessPolicyHandlerDefinition $definition
   *   The access policy handler definition.
   */
  public function initialize(AccessPolicyHandlerDefinition $definition) {
    $this->definition = $definition;
  }

  /**
   * Set the execution context.
   *
   * @param \Drupal\access_policy\Validation\ExecutionContextInterface $context
   *   The execution context.
   */
  public function setContext(ExecutionContextInterface $context) {
    $this->context = $context;
  }

  /**
   * {@inheritdoc}
   */
  public function valueForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

}
