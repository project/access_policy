<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRuleArgument;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Current user access rule argument.
 *
 * @AccessRuleArgument(
 *   id = "current_user",
 *   label = @Translation("Current user"),
 * )
 */
class CurrentUser extends AccessRuleArgumentPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The field type manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The current field.
   *
   * @var string
   */
  protected $field;

  /**
   * Constructs a new CurrentUser access rule argument.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, AccountInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration = $configuration;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->fieldTypeManager = $field_type_manager;
    $this->currentUser = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function valueForm(array $form, FormStateInterface $form_state) {
    if (isset($this->configuration['field'])) {
      // If configuration specifies a specific field then save its value.
      $form['field'] = [
        '#type' => 'value',
        '#value' => $this->getField(),
      ];
    }
    // If configuration specifies a field type then show a select list. If no
    // field type is specified then it will default to the field type  it's
    // being compared against.
    else {
      $field_type = $this->configuration['field_type'] ?? $this->definition->getFieldType();
      $form['field'] = [
        '#type' => 'select',
        '#title' => $this->t('User field'),
        '#options' => $this->getUserFieldOptions($field_type),
        '#description' => $this->t('Choose the field you want to compare with.'),
        '#default_value' => $this->getField(),
      ];
    }

    return $form;
  }

  /**
   * Get the current field.
   *
   * @return string
   *   The current field name.
   */
  public function getField() {
    if (!isset($this->field)) {
      if (isset($this->configuration['field'])) {
        $this->field = $this->configuration['field'];
      }

      $value = $this->definition->getSetting('value');
      if (isset($value['field'])) {
        $this->field = $value['field'];
      }
    }

    return $this->field;
  }

  /**
   * Get options array for user field.
   *
   * @return array
   *   The options array.
   */
  protected function getUserFieldOptions($field_type) {
    $user_field_definitions = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $definitions = $this->filterByApplicableFieldStorage($user_field_definitions, [$field_type]);

    $options = [];
    foreach ($definitions as $field_name => $definition) {
      $options[$field_name] = $definition->getLabel();
    }
    return $options;
  }

  /**
   * Filter the field definitions by field type.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $field_definitions
   *   Array of field definitions.
   * @param array $field_types
   *   Array of field type ids.
   *
   * @return array
   *   Array of filtered field definitions.
   */
  protected function filterByApplicableFieldStorage(array $field_definitions, array $field_types) {
    return array_filter($field_definitions, function ($definition) use ($field_types) {
      return $this->isFieldOptionApplicable($definition, $field_types);
    });
  }

  /**
   * Determine whether the field option is applicable.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The current field definition.
   * @param array $field_types
   *   Array of supported field types.
   *
   * @return bool
   *   TRUE to show it in options list; FALSE to remove it.
   */
  protected function isFieldOptionApplicable(FieldDefinitionInterface $field_definition, array $field_types) {
    $field_storage = $field_definition->getFieldStorageDefinition();
    $current_field_type = $field_storage->getType();

    // If the field type is an entity reference field make sure the target types
    // match.
    $is_compatible = TRUE;
    if ($current_field_type == 'entity_reference') {
      $is_compatible = ($this->getTargetType() == $field_storage->getSetting('target_type'));
    }

    if ($field_storage instanceof FieldStorageConfigInterface
      && !$field_storage->isLocked()
      && in_array($current_field_type, $field_types)
      && $is_compatible) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the current target type for entity reference fields.
   *
   * @return string
   *   The target type.
   */
  public function getTargetType() {
    if (isset($this->configuration['target_type'])) {
      return $this->configuration['target_type'];
    }

    if ($this->definition->getFieldType() == 'entity_reference') {
      return $this->getFieldStorageDefinition()->getSetting('target_type');
    }

    // Otherwise always default to the current entity type.
    return $this->definition->getEntityType();
  }

  /**
   * Get the field value from the user.
   *
   * @return array
   *   The value from the user.
   */
  public function getValue() {
    return $this->getEntityFieldValue($this->getCurrentUser(), $this->getField());
  }

  /**
   * Get the current user.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The user entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getCurrentUser() {
    // If the context contains the current user then use that.
    if (!empty($this->context) && $this->context->get('user')) {
      return $this->context->get('user');
    }
    // Otherwise load it from the container.
    return $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
  }

  /**
   * Get the values from a field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param string $field_name
   *   The field name.
   *
   * @return array
   *   Index array of field values.
   */
  protected function getEntityFieldValue(EntityInterface $entity, $field_name) {
    $field = $entity->get($field_name);
    $field_definition = $field->getFieldDefinition();
    $field_type = $field_definition->getType();

    // Get the main property name.
    $column_name = $this->getMainPropertyName($field_type);

    $values = $entity->get($field_name)->getValue();
    return array_map(function ($value) use ($column_name) {
      if (isset($value[$column_name])) {
        return $value[$column_name];
      }
    }, $values);
  }

  /**
   * Get the main property name of a field.
   *
   * @param string $field_type
   *   The field type.
   *
   * @return string
   *   The main property name.
   */
  public function getMainPropertyName($field_type) {
    $definition = $this->fieldTypeManager->getDefinition($field_type);
    $class = $definition['class'];
    return $class::mainPropertyName();
  }

  /**
   * Get the current field storage definition.
   *
   * @return \Drupal\Core\Field\FieldStorageDefinitionInterface|false
   *   The field storage definition.
   */
  public function getFieldStorageDefinition() {
    $fields = $this->entityFieldManager->getFieldStorageDefinitions($this->definition->getEntityType());
    if (isset($fields[$this->definition->getFieldName()])) {
      return $fields[$this->definition->getFieldName()];
    }
    return FALSE;
  }

}
