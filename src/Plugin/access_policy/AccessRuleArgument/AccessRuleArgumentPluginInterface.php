<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRuleArgument;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface AccessRuleArgument.
 *
 * Access rule arguments allow you to configure and pass in contextual arguments
 * as part of your access rules. For example, with an access rule argument you
 * can pass in details about the current user, system information etc.
 *
 * You use access rule arguments by adding them to your data definition.
 */
interface AccessRuleArgumentPluginInterface {

  /**
   * Create the argument value form.
   *
   * This will be loaded on the access rule configuration form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The form array.
   */
  public function valueForm(array $form, FormStateInterface $form_state);

  /**
   * Get the values from the argument.
   */
  public function getValue();

}
