<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRuleWidget;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface AccessRuleWidgetInterface.
 */
interface AccessRuleWidgetPluginInterface {

  /**
   * Get the widget settings.
   *
   * @return array
   *   The array of widget settings.
   */
  public function getWidgetSettings();

  /**
   * Set the widget settings.
   *
   * @param array $settings
   *   Array of widget settings.
   */
  public function setWidgetSettings(array $settings);

  /**
   * Set a widget setting.
   *
   * @param string $key
   *   The key.
   * @param mixed $value
   *   Teh value.
   *
   * @return $this
   */
  public function setWidgetSetting($key, $value);

  /**
   * Create the widget settings form.
   *
   * @param array $form
   *   The widget form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The settings form array.
   */
  public function settingsForm(array $form, FormStateInterface $form_state);

  /**
   * Builds form elements.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state);

  /**
   * Allow the access rule widget to act upon the entity when it's being built.
   *
   * Note that this gets called for all widgets across policies, regardless of
   * which one is currently enabled. This allows field widgets to perform
   * additional cleanup tasks if the policy they are associated with is removed.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function buildEntity(EntityInterface $entity, array $form, FormStateInterface $form_state);

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state);

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state);

}
