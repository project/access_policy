<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRuleWidget;

use Drupal\access_policy\AccessPolicyHandlerDefinition;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for configurable Access Rules that can show fields on Access Tab.
 */
abstract class AccessRuleWidgetBase extends PluginBase implements AccessRuleWidgetPluginInterface {

  use StringTranslationTrait;

  /**
   * The widget settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * The current entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The access policy handler definition object.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerDefinition
   */
  protected $definition;

  /**
   * Flag indicating whether default settings have been merged.
   *
   * @var bool
   */
  protected $defaultSettingsMerged;

  /**
   * Initialize the access rule widget.
   *
   * @param \Drupal\access_policy\AccessPolicyHandlerDefinition $definition
   *   The access policy handler definition.
   */
  public function initialize(AccessPolicyHandlerDefinition $definition) {
    $this->definition = $definition;
  }

  /**
   * Get the current entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The current entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Set the entity.
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultWidgetSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getWidgetSetting($key) {
    // Merge defaults if we have no value for the key.
    if (!$this->defaultSettingsMerged && !array_key_exists($key, $this->settings)) {
      $this->mergeDefaults();
    }
    return $this->settings[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getWidgetSettings() {
    // Merge defaults before returning the array.
    if (!$this->defaultSettingsMerged) {
      $this->mergeDefaults();
    }
    return $this->settings;
  }

  /**
   * Merges default settings values into $settings.
   */
  protected function mergeDefaults() {
    $this->settings += static::defaultWidgetSettings();
    $this->defaultSettingsMerged = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setWidgetSettings(array $settings) {
    $this->settings = $settings;
    $this->defaultSettingsMerged = FALSE;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setWidgetSetting($key, $value) {
    $this->settings[$key] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm($form, $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(EntityInterface $entity, $form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

}
