<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * The delete operation plugin.
 *
 * @AccessPolicyOperation(
 *   id = "delete",
 *   label = @Translation("Delete"),
 *   description = @Translation("Delete an entity"),
 *   operation = "delete",
 *   weight = 3,
 *   permission = true,
 *   access_rules = true,
 *   show_column = true,
 * )
 */
class Delete extends AccessPolicyOperationBase {

  /**
   * {@inheritdoc}
   */
  public static function isCurrent(EntityInterface $entity, $op) {
    return $op == 'delete';
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityTypeInterface $entity_type) {
    return !empty($entity_type->getFormClass('delete')) || $entity_type->hasLinkTemplate('delete-form');
  }

  /**
   * {@inheritdoc}
   */
  public function createPermission(AccessPolicyInterface $access_policy) {
    $entity_type_label = $this->getEntityTypeLabel($access_policy->getTargetEntityType());
    return [
      'delete ' . $access_policy->id() . ' ' . $entity_type_label => [
        'title' => $this->t("@access_policy: Delete any @entity_type assigned this access policy", [
          '@access_policy' => $access_policy->label(),
          '@entity_type' => $entity_type_label,
        ]),
      ],
    ];
  }

}
