<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Interface for access policy operations plugins.
 *
 * These plugins allow you to integrate access policy with any entity operation.
 */
interface AccessPolicyOperationInterface {

  /**
   * Determine whether this is the current operation for an entity.
   *
   * @return bool
   *   TRUE if this is the current operation; FALSE otherwise.
   */
  public static function isCurrent(EntityInterface $entity, $op);

  /**
   * Determine whether this operation is applicable for an entity type.
   *
   * @return bool
   *   TRUE if the operation is applicable; FALSE otherwise.
   */
  public static function isApplicable(EntityTypeInterface $entity_type);

  /**
   * Get the plugin label.
   *
   * @return string
   *   The plugin label.
   */
  public function getLabel();

  /**
   * Determine whether permissions are validated as part of this operation.
   *
   * @return bool
   *   TRUE if permissions should be validated; FALSE otherwise.
   */
  public function isPermissionEnabled();

  /**
   * Determine whether rules are validated as part of this operation.
   *
   * @return bool
   *   TRUE if rules should be validated; FALSE otherwise.
   */
  public function isAccessRulesEnabled($op);

  /**
   * Determine whether this operation supports permission validation.
   *
   * @return bool
   *   TRUE if it's supported; FALSE otherwise.
   */
  public function supportsPermissions();

  /**
   * Determine whether this operation supports access rule validation.
   *
   * @return bool
   *   TRUE if it's supported; FALSE otherwise.
   */
  public function supportsAccessRules();

  /**
   * Creates the permission for this operation.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The Access Policy.
   *
   * @return array
   *   The permission.
   */
  public function createPermission(AccessPolicyInterface $access_policy);

  /**
   * Retrieves the permission string for this operation.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   *
   * @return string
   *   The permission string.
   */
  public function getPermission(AccessPolicyInterface $access_policy);

}
