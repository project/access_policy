<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * The edit operation plugin.
 *
 * @AccessPolicyOperation(
 *   id = "edit",
 *   label = @Translation("Edit"),
 *   description = @Translation("Edit an entity"),
 *   operation = "update",
 *   weight = 2,
 *   permission = true,
 *   access_rules = true,
 *   show_column = true,
 * )
 */
class Edit extends AccessPolicyOperationBase {

  /**
   * {@inheritdoc}
   */
  public static function isCurrent(EntityInterface $entity, $op) {
    return $op == 'update';
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityTypeInterface $entity_type) {
    return !empty($entity_type->getFormClass('edit')) || $entity_type->hasLinkTemplate('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function createPermission(AccessPolicyInterface $access_policy) {
    $entity_type_label = $this->getEntityTypeLabel($access_policy->getTargetEntityType());
    return [
      'edit ' . $access_policy->id() . ' ' . $entity_type_label => [
        'title' => $this->t("@access_policy: Edt any @entity_type assigned this access policy", [
          '@access_policy' => $access_policy->label(),
          '@entity_type' => $entity_type_label,
        ]),
      ],
    ];
  }

}
