<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * The manage access operation plugin.
 *
 * @AccessPolicyOperation(
 *   id = "manage_access",
 *   label = @Translation("Manage access"),
 *   description = @Translation("Manage access for an entity"),
 *   operation = "manage access",
 *   weight = 4,
 *   permission = true,
 *   access_rules = true,
 *   show_column = false,
 * )
 */
class ManageAccess extends AccessPolicyOperationBase {

  /**
   * {@inheritdoc}
   */
  public static function isCurrent(EntityInterface $entity, $op) {
    return $op == 'manage access';
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityTypeInterface $entity_type) {
    return !empty($entity_type->getFormClass('manage access'));
  }

  /**
   * {@inheritdoc}
   *
   * The manage access permission is the same as the "Assign access policy"
   * permission, and it does not create its own. This will make the permission
   * required.
   */
  public function getPermission(AccessPolicyInterface $access_policy) {
    return 'assign ' . $access_policy->id() . ' access policy';
  }

  /**
   * {@inheritdoc}
   */
  public function createPermission(AccessPolicyInterface $access_policy) {
    return [];
  }

}
