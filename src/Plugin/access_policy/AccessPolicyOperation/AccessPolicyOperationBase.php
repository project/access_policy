<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 * Base class for access policy operations.
 */
abstract class AccessPolicyOperationBase extends PluginBase implements AccessPolicyOperationInterface {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityTypeInterface $entity_type) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function isPermissionEnabled() {
    return (!empty($this->pluginDefinition['permission']));
  }

  /**
   * {@inheritdoc}
   */
  public function isAccessRulesEnabled($op) {
    return (!empty($this->pluginDefinition['access_rules']));
  }

  /**
   * {@inheritdoc}
   */
  public function supportsPermissions() {
    return (isset($this->pluginDefinition['permission']));
  }

  /**
   * {@inheritdoc}
   */
  public function supportsAccessRules() {
    return (isset($this->pluginDefinition['access_rules']));
  }

  /**
   * Determine whether this operation is the original owner of the permission.
   *
   * It's possible to return a permission in getPermission() that was defined
   * somewhere else. In fact, it may be preferred in some cases. This checks
   * to see if this operation is where the permission was originally defined.
   *
   * @return bool
   *   TRUE if this is the owner of the permission; FALSE otherwise.
   */
  public function isPermissionOwner(AccessPolicyInterface $access_policy) {
    $permission = $this->createPermission($access_policy);
    if (!empty($permission)) {
      $permission_string = array_key_first($permission);
      return $this->getPermission($access_policy) == $permission_string;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function createPermission(AccessPolicyInterface $access_policy) {
    return [];
  }

  /**
   * Get the entity type label.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return string
   *   The entity type label.
   */
  protected function getEntityTypeLabel(EntityTypeInterface $entity_type) {
    return strtolower($entity_type->getLabel()->getUntranslatedString());
  }

  /**
   * {@inheritdoc}
   */
  public function getPermission(AccessPolicyInterface $access_policy) {
    $permission = $this->createPermission($access_policy);
    if (isset($permission)) {
      return array_key_first($permission);
    }
  }

}
