<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * The view operation plugin.
 *
 * @AccessPolicyOperation(
 *   id = "view",
 *   label = @Translation("View"),
 *   description = @Translation("View an entity"),
 *   operation = "view",
 *   weight = 0,
 *   permission = true,
 *   access_rules = true,
 *   show_column = true,
 * )
 */
class View extends AccessPolicyOperationBase {

  /**
   * {@inheritdoc}
   */
  public static function isCurrent(EntityInterface $entity, $op) {
    return $op == 'view';
  }

  /**
   * {@inheritdoc}
   */
  public function createPermission(AccessPolicyInterface $access_policy) {
    $entity_type_label = $this->getEntityTypeLabel($access_policy->getTargetEntityType());
    return [
      'view ' . $access_policy->id() . ' ' . $entity_type_label => [
        'title' => $this->t('@access_policy: View any @entity_type assigned this access policy', [
          '@access_policy' => $access_policy->label(),
          '@entity_type' => $entity_type_label,
        ]),
      ],
    ];
  }

}
