<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * The view unpublished operation plugin.
 *
 * @AccessPolicyOperation(
 *   id = "view_unpublished",
 *   label = @Translation("View unpublished"),
 *   description = @Translation("View an unpublished entity"),
 *   operation = "view",
 *   weight = -1,
 *   permission = true,
 *   access_rules = true,
 *   show_column = true,
 * )
 */
class ViewUnpublished extends AccessPolicyOperationBase {

  /**
   * {@inheritdoc}
   */
  public static function isCurrent(EntityInterface $entity, $op) {
    if ($op == 'view' && $entity instanceof EntityPublishedInterface && !$entity->isPublished()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityTypeInterface $entity_type) {
    if ($entity_type->entityClassImplements(EntityOwnerInterface::class)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function createPermission(AccessPolicyInterface $access_policy) {
    $entity_type_label = $this->getEntityTypeLabel($access_policy->getTargetEntityType());
    return [
      'view any ' . $access_policy->id() . ' unpublished ' . $entity_type_label => [
        'title' => $this->t('@access_policy: View any unpublished @entity_type assigned this access policy', [
          '@access_policy' => $access_policy->label(),
          '@entity_type' => $entity_type_label,
        ]),
      ],
    ];
  }

}
