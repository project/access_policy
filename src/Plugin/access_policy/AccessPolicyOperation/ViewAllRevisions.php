<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * The view all revisions operation plugin.
 *
 * @AccessPolicyOperation(
 *   id = "view_all_revisions",
 *   label = @Translation("View all revisions"),
 *   description = @Translation("View all revisions"),
 *   operation = "view all revisions",
 *   weight = 1,
 *   permission = true,
 *   access_rules = true,
 *   show_column = true,
 * )
 */
class ViewAllRevisions extends AccessPolicyOperationBase {

  /**
   * {@inheritdoc}
   */
  public static function isCurrent(EntityInterface $entity, $op) {
    $revision_operations = [
      'view all revisions',
      'view revision',
      'revert revision',
      'delete revision',
    ];
    return in_array($op, $revision_operations);
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityTypeInterface $entity_type) {
    if ($entity_type->isRevisionable() && $entity_type->hasLinkTemplate('revision')) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function createPermission(AccessPolicyInterface $access_policy) {
    $entity_type_label = $this->getEntityTypeLabel($access_policy->getTargetEntityType());
    return [
      'view all ' . $access_policy->id() . ' ' . $entity_type_label . ' revisions' => [
        'title' => $this->t('@access_policy: View all revisions of @entity_type assigned this access policy', [
          '@access_policy' => $access_policy->label(),
          '@entity_type' => $entity_type_label,
        ]),
        'description' => $this->t('To view the revisions, you still need permission to view all revisions.'),
      ],
    ];

  }

}
