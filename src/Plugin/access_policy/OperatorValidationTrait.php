<?php

namespace Drupal\access_policy\Plugin\access_policy;

/**
 * Basic trait for helping with validation.
 */
trait OperatorValidationTrait {

  /**
   * Simple equals comparison.
   *
   * @param mixed $expected
   *   The expected value.
   * @param mixed $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if the values are equal; FALSE otherwise.
   */
  public function equals($expected, $actual) {
    if ($expected == $actual) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Simple not equals comparison.
   *
   * @param mixed $expected
   *   The expected value.
   * @param mixed $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if the values are equal; FALSE otherwise.
   */
  public function notEquals($expected, $actual) {
    if ($expected != $actual) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Contains operator for comparing strings.
   *
   * @param string $needle
   *   The string needle.
   * @param string $haystack
   *   The string haystack.
   *
   * @return bool
   *   TRUE if the haystack contains the needle; FALSE otherwise.
   */
  public function contains($needle, $haystack) {
    if (substr_count($haystack, $needle) > 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Not contains operator for comparing strings.
   *
   * @param string $needle
   *   The string needle.
   * @param string $haystack
   *   The string haystack.
   *
   * @return bool
   *   TRUE if the haystack does not contain the needle; FALSE otherwise.
   */
  public function notContains($needle, $haystack) {
    if (substr_count($haystack, $needle) > 0) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Starts with comparison for strings.
   *
   * @param string $prefix
   *   The prefix.
   * @param string $string
   *   The string.
   *
   * @return bool
   *   TRUE if the string starts with the prefix; FALSE otherwise.
   */
  public function startsWith($prefix, $string) {
    $length = strlen($prefix);
    if (substr($string, 0, $length) == $prefix) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Not starts with comparison for strings.
   *
   * @param string $prefix
   *   The prefix.
   * @param string $string
   *   The string.
   *
   * @return bool
   *   TRUE if the string does not start with the prefix; FALSE otherwise.
   */
  public function notStartsWith($prefix, $string) {
    $length = strlen($prefix);
    if (substr($string, 0, $length) != $prefix) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Ends with comparison for strings.
   *
   * @param string $suffix
   *   The suffix.
   * @param string $string
   *   The string.
   *
   * @return bool
   *   TRUE if the string ends with the suffix; FALSE otherwise.
   */
  public function endsWith($suffix, $string) {
    $offset = strlen($suffix);
    if (substr($string, -$offset) == $suffix) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Not ends with comparison for strings.
   *
   * @param string $suffix
   *   The suffix.
   * @param string $string
   *   The string.
   *
   * @return bool
   *   TRUE if the string does not end with the suffix; FALSE otherwise.
   */
  public function notEndsWith($suffix, $string) {
    $offset = strlen($suffix);
    if (substr($string, -$offset) != $suffix) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Is one of for a set of data.
   *
   * @param array $options
   *   Array of options to compare against.
   * @param mixed $value
   *   The current value.
   *
   * @return bool
   *   TRUE if the item is found in the set; FALSE otherwise.
   */
  public function isOneOf(array $options, $value) {
    if (in_array($value, $options)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Is none of for a set of data.
   *
   * @param array $options
   *   Array of options to compare against.
   * @param mixed $value
   *   The current value.
   *
   * @return bool
   *   TRUE if the item not found in the set; FALSE otherwise.
   */
  public function isNoneOf(array $options, $value) {
    if (in_array($value, $options)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Is less than.
   *
   * @param int $expected
   *   The expected value.
   * @param int $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if actual value is less than the expected value; FALSE otherwise.
   */
  public function isLessThan($expected, $actual) {
    if ($actual < $expected) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Is less than or equal to.
   *
   * @param int $expected
   *   The expected value.
   * @param int $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if actual value is less than or equal to expected value; FALSE
   *   otherwise.
   */
  public function isLessThanOrEqualTo($expected, $actual) {
    if ($actual <= $expected) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Is greater than.
   *
   * @param int $expected
   *   The expected value.
   * @param int $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if the actual value is greater than the expected value; FALSE
   *   otherwise.
   */
  public function isGreaterThan($expected, $actual) {
    if ($actual > $expected) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Is greater than.
   *
   * @param int $expected
   *   The expected value.
   * @param int $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if actual value is greater or equal to expected value; FALSE
   *   otherwise.
   */
  public function isGreaterThanOrEqualTo($expected, $actual) {
    if ($actual >= $expected) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Is empty.
   *
   * Note: This is called isEmptyValue to not conflict with tests.
   *
   * @param mixed $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if the value is empty; FALSE otherwise.
   */
  public function isEmptyValue($actual) {
    if (empty($actual)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Is not empty.
   *
   * @param mixed $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if the value is not empty; FALSE otherwise.
   */
  public function isNotEmptyValue($actual) {
    if (!empty($actual)) {
      return TRUE;
    }
    return FALSE;
  }

}
