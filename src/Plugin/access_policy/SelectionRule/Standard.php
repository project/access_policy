<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionRule;

/**
 * Determine whether the entity field has a value.
 *
 * @SelectionRule(
 *   id = "standard",
 * )
 */
class Standard extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      "operator" => 'not empty',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'validateSimple',
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'validateSimple',
      ],
      'empty' => [
        'title' => $this->t('Is empty'),
        'method' => 'validateSimple',
        'hide_value' => TRUE,
      ],
      'not empty' => [
        'title' => $this->t('Is not empty'),
        'method' => 'validateSimple',
        'hide_value' => TRUE,
      ],
      'is applicable' => [
        'title' => $this->t('Is applicable'),
        'method' => 'validateIsApplicable',
        'hide_value' => TRUE,
        'values' => FALSE,
      ],
    ];

    return $operators;
  }

  /**
   * Validate simple operators.
   *
   * @param mixed $expected
   *   The expected value.
   * @param mixed $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateSimple($expected, $actual) {
    $op = $this->getOperator();
    switch ($op) {
      case '=':
        return $this->equals($expected, $actual);

      case '!=':
        return $this->notEquals($expected, $actual);

      case 'empty':
        return $this->isEmptyValue($actual);

      case 'not empty':
        return $this->isNotEmptyValue($actual);
    }

    return FALSE;
  }

}
