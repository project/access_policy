<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionRule;

use Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerBase;
use Drupal\access_policy\Validation\ExecutionContextInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Class SelectionRuleBase.
 *
 * This abstract class provides the generic selection rule configuration form
 * and validation.
 */
abstract class SelectionRuleBase extends AccessPolicyHandlerBase implements ContainerFactoryPluginInterface {

  /**
   * The current execution context.
   *
   * @var \Drupal\access_policy\Validation\ExecutionContext
   */
  protected $context;

  /**
   * Set the execution context.
   *
   * @param \Drupal\access_policy\Validation\ExecutionContextInterface $context
   *   The execution context.
   */
  public function setContext(ExecutionContextInterface $context) {
    $this->context = $context;
  }

  /**
   * Get the current execution context.
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * Returns generic default configuration for access rule plugins.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  protected function baseSettingsDefaults() {
    return [
      'admin_label' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state) {
    $description = $this->definition->getDescription();
    if ($description) {
      $form['help'] = [
        '#markup' => '<p>' . $description . '</p>',
      ];
    }

    $this->operatorForm($form, $form_state);
    $this->valueForm($form, $form_state);

    $form['admin_label'] = [
      '#type' => 'details',
      '#title' => $this->t('Administrative title'),
      '#open' => FALSE,
    ];

    $form['admin_label']['admin_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Administrative title'),
      '#default_value' => $this->settings['admin_label'],
      '#description' => $this->t('This will be displayed on the rules list. This is useful when you have the same selection rule used twice.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateSettingsForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitSettingsForm(array &$form, FormStateInterface $form_state) {
    // Process the handlers submission if no errors occurred only.
    if (!$form_state->getErrors()) {
      $form_state->cleanValues();
      $values = $form_state->getValues();
      $this->settings = $values;
    }
  }

}
