<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionRule;

use Drupal\access_policy\LabelHelper;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Control access policy selection by comparing list field values.
 *
 * @SelectionRule(
 *   id = "list",
 * )
 */
class FieldList extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      'value' => [],
      "operator" => 'not empty',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      'or' => [
        'title' => $this->t('Is one of'),
        'method' => 'validateSimple',
      ],
      'not' => [
        'title' => $this->t('Is none of'),
        'method' => 'validateSimple',
      ],
      'empty' => [
        'title' => $this->t('Is empty'),
        'method' => 'validateSimple',
        'hide_value' => TRUE,
      ],
      'not empty' => [
        'title' => $this->t('Is not empty'),
        'method' => 'validateSimple',
        'hide_value' => TRUE,
      ],
      'is applicable' => [
        'title' => $this->t('Is applicable'),
        'method' => 'validateIsApplicable',
        'hide_value' => TRUE,
        'values' => TRUE,
      ],
    ];

    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(array &$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'select',
      '#title' => $this->t('Value'),
      '#options' => $this->getAllowedValues(),
      '#default_value' => $this->settings['value'],
      '#multiple' => TRUE,
    ];
  }

  /**
   * Get allowed field value options.
   *
   * @return array
   *   Array of allowed values.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllowedValues() {
    $field_definition = $this->getFieldDefinition();
    $settings = $field_definition->getSettings();
    return $settings['allowed_values'] ?? [];
  }

  /**
   * Get the current field definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|false
   *   The field definition.
   */
  protected function getFieldDefinition() {
    $field_map = $this->entityFieldManager->getFieldMapByFieldType($this->definition->getFieldType());
    $entity_fields = $field_map[$this->definition->getEntityType()];
    $current_field = $entity_fields[$this->definition->getFieldName()] ?? [];

    $bundle = reset($current_field['bundles']);
    $definitions = $this->entityFieldManager->getFieldDefinitions($this->definition->getEntityType(), $bundle);
    if (isset($definitions[$this->definition->getFieldName()])) {
      return $definitions[$this->definition->getFieldName()];
    }

    return FALSE;
  }

  /**
   * Validate simple operators.
   *
   * @param mixed $expected
   *   The expected value.
   * @param mixed $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateSimple($expected, $actual) {
    $op = $this->getOperator();
    switch ($op) {
      case 'or':
        foreach ($actual as $value) {
          if ($this->isOneOf($expected, $value)) {
            return TRUE;
          }
        }
        break;

      case 'not':
        foreach ($actual as $value) {
          if ($this->isOneOf($expected, $value)) {
            return FALSE;
          }
        }
        return TRUE;

      case 'empty':
        return $this->isEmptyValue($actual);

      case 'not empty':
        return $this->isNotEmptyValue($actual);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function validateCallback(EntityInterface $entity, AccountInterface $account) {
    $operator = $this->getOperator();
    $info = $this->operators();

    if ($operator == 'is applicable') {
      return $this->validateIsApplicable($entity, $account);
    }

    $field_name = $this->definition->getFieldName();
    $field_values = $this->getEntityFieldValues($entity, $field_name);
    $options = $this->getValue();

    if (!empty($info[$operator]['method'])) {
      // If any of the field values match then return true.
      $valid = $this->{$info[$operator]['method']}($options, $field_values);
      if ($valid) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitSettingsForm(array &$form, FormStateInterface $form_state) {
    parent::submitSettingsForm($form, $form_state);
    $values = $form_state->getValues();
    $value = $values['value'] ?? [];
    $this->settings['value'] = array_filter($value);
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if ($this->isValueHidden()) {
      return $this->getOperator();
    }
    $allowed_values = $this->getAllowedValues();
    $labels = [];
    $selected = $this->settings['value'];
    foreach ($allowed_values as $key => $label) {
      if (in_array($key, $selected)) {
        $labels[] = $label;
      }
    }

    return $this->getOperator() . ' ' . LabelHelper::render($labels, [
      'limit' => 1,
      'empty_value' => $this->t('Unknown'),
    ]);
  }

}
