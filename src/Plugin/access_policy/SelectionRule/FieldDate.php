<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionRule;

/**
 * Assign access policy based on date.
 *
 * Extending empty since it's the only one that seems to make sense in this
 * case. If use case makes itself apparent we can update this without breaking
 * anything.
 *
 * @SelectionRule (
 *   id = "date",
 * )
 */
class FieldDate extends FieldEmpty {

}
