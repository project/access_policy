<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionRule;

/**
 * Assign policy if the field is empty or not empty.
 *
 * @SelectionRule(
 *   id = "empty",
 * )
 */
class FieldEmpty extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      "operator" => 'not empty',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      'empty' => [
        'title' => $this->t('Is empty'),
        'method' => 'validateSimple',
        'hide_value' => TRUE,
      ],
      'not empty' => [
        'title' => $this->t('Is not empty'),
        'method' => 'validateSimple',
        'hide_value' => TRUE,
      ],
      'is applicable' => [
        'title' => $this->t('Is applicable'),
        'method' => 'validateIsApplicable',
        'hide_value' => TRUE,
        'values' => FALSE,
      ],
    ];

    return $operators;
  }

  /**
   * Validate simple operators.
   *
   * @param mixed $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateSimple($actual) {
    $op = $this->getOperator();
    switch ($op) {
      case 'empty':
        return $this->isEmptyValue($actual);

      case 'not empty':
        return $this->isNotEmptyValue($actual);
    }

    return FALSE;
  }

}
