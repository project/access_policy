<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionRule;

use Drupal\access_policy\LabelHelper;
use Drupal\Core\Entity\EntityInterface;

/**
 * Control access policy selection by comparing moderation states.
 *
 * @SelectionRule(
 *   id = "moderation_state",
 * )
 */
class ModerationState extends FieldList {

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityInterface $entity) {
    $workflow_storage = $this->entityTypeManager->getStorage('workflow');
    $workflows = $workflow_storage->loadByProperties([
      'type' => 'content_moderation',
    ]);
    foreach ($workflows as $workflow) {
      /** @var \Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface $workflow_type */
      $workflow_type = $workflow->getTypePlugin();
      return $workflow_type->appliesToEntityTypeAndBundle($entity->getEntityTypeId(), $entity->bundle());
    }

    return FALSE;
  }

  /**
   * Get allowed field value options.
   *
   * @return array
   *   Array of allowed values.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllowedValues() {
    $allowed_values = [];
    $workflow_storage = $this->entityTypeManager->getStorage('workflow');
    foreach ($workflow_storage->loadByProperties(['type' => 'content_moderation']) as $workflow) {
      /** @var \Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface $workflow_type */
      $workflow_type = $workflow->getTypePlugin();
      if (in_array($this->getDefinition()->getEntityType(), $workflow_type->getEntityTypes(), TRUE)) {
        foreach ($workflow_type->getStates() as $state_id => $state) {
          $workflow_string = implode('-', [$workflow->id(), $state_id]);
          $allowed_values[$workflow->label()][$workflow_string] = $state->label();
        }
      }
    }

    return $allowed_values;
  }

  /**
   * Get all allowed values in a flattened associative array.
   *
   * The moderation states are grouped by workflow. This flattens that
   * the multi-dimensional array into flat associative array.
   *
   * @return array
   *   Array of moderation states.
   */
  protected function getAllowedValuesFlattened() {
    $allowed_values = $this->getAllowedValues();
    $all_states = [];
    foreach ($allowed_values as $moderation_states) {
      foreach ($moderation_states as $name => $state) {
        $all_states[$name] = $state;
      }
    }
    return $all_states;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if ($this->isValueHidden()) {
      return $this->getOperator();
    }

    $allowed_values = $this->getAllowedValuesFlattened();
    $labels = [];
    $selected = $this->settings['value'];
    foreach ($allowed_values as $key => $label) {
      if (in_array($key, $selected)) {
        $labels[] = $label;
      }
    }

    return $this->getOperator() . ' ' . LabelHelper::render($labels, [
      'limit' => 2,
      'empty_value' => $this->t('Unknown'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $value = parent::getValue();
    $options = [];
    foreach ($value as $option) {
      $moderation_state = explode('-', $option)[1];
      $options[] = $moderation_state;
    }

    return $options;
  }

}
