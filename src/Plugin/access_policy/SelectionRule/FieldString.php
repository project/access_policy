<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionRule;

/**
 * Assign policy based on string values.
 *
 * Most of the time empty and not empty will be used. However, there can be use
 * cases where content should be assigned a policy if it contains certain words.
 *
 * @SelectionRule(
 *   id = "string",
 * )
 */
class FieldString extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      "operator" => 'not empty',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'validateSimple',
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'validateSimple',
      ],
      'contains' => [
        'title' => $this->t('Contains'),
        'method' => 'validateString',
      ],
      'not contains' => [
        'title' => $this->t('Does not contain'),
        'method' => 'validateString',
      ],
      'starts with' => [
        'title' => $this->t('Starts with'),
        'method' => 'validateString',
      ],
      'not starts with' => [
        'title' => $this->t('Does not start with'),
        'method' => 'validateString',
      ],
      'ends with' => [
        'title' => $this->t('Ends with'),
        'method' => 'validateString',
      ],
      'not ends with' => [
        'title' => $this->t('Does not end with'),
        'method' => 'validateString',
      ],
      'empty' => [
        'title' => $this->t('Is empty'),
        'method' => 'validateSimple',
        'hide_value' => TRUE,
      ],
      'not empty' => [
        'title' => $this->t('Is not empty'),
        'method' => 'validateSimple',
        'hide_value' => TRUE,
      ],
      'is applicable' => [
        'title' => $this->t('Is applicable'),
        'method' => 'validateIsApplicable',
        'hide_value' => TRUE,
        'values' => FALSE,
      ],
    ];

    return $operators;
  }

  /**
   * Validate operators.
   *
   * @param mixed $expected
   *   The expected value.
   * @param mixed $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateSimple($expected, $actual) {
    $op = $this->getOperator();
    switch ($op) {
      case '=':
        return $this->equals($expected, $actual);

      case '!=':
        return $this->notEquals($expected, $actual);

      case 'empty':
        return $this->isEmptyValue($actual);

      case 'not empty':
        return $this->isNotEmptyValue($actual);
    }

    return FALSE;
  }

  /**
   * Validate string values.
   *
   * @param string $substring
   *   The substring.
   * @param string $string
   *   The full string.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateString($substring, $string) {
    if (empty($string) && is_array($string)) {
      $string = '';
    }

    $op = $this->getOperator();
    switch ($op) {
      case 'contains':
        return $this->contains($substring, $string);

      case 'not contains':
        return $this->notContains($substring, $string);

      case 'starts with':
        return $this->startsWith($substring, $string);

      case 'not starts with':
        return $this->notStartsWith($substring, $string);

      case 'ends with':
        return $this->endsWith($substring, $string);

      case 'not ends with':
        return $this->notEndsWith($substring, $string);
    }

    return FALSE;
  }

}
