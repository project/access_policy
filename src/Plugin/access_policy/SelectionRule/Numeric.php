<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionRule;

/**
 * Assign policy based on numeric values.
 *
 * @SelectionRule (
 *   id = "numeric",
 * )
 */
class Numeric extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      "operator" => 'not empty',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      '<' => [
        'title' => $this->t('Is less than'),
        'method' => 'validateSimple',
      ],
      '<=' => [
        'title' => $this->t('Is less than or equal to'),
        'method' => 'validateSimple',
      ],
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'validateSimple',
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'validateSimple',
      ],
      '>' => [
        'title' => $this->t('Is greater than'),
        'method' => 'validateSimple',
      ],
      '>=' => [
        'title' => $this->t('Is greater than or equal to'),
        'method' => 'validateSimple',
      ],
      'empty' => [
        'title' => $this->t('Is empty'),
        'method' => 'validateSimple',
        'hide_value' => TRUE,
      ],
      'not empty' => [
        'title' => $this->t('Is not empty'),
        'method' => 'validateSimple',
        'hide_value' => TRUE,
      ],
      'is applicable' => [
        'title' => $this->t('Is applicable'),
        'method' => 'validateIsApplicable',
        'hide_value' => TRUE,
        'values' => FALSE,
      ],
    ];

    return $operators;
  }

  /**
   * Validate operators.
   *
   * @param int $expected
   *   The expected value.
   * @param int $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateSimple($expected, $actual) {
    $op = $this->getOperator();

    switch ($op) {
      case '=':
        return $this->equals($expected, $actual);

      case '!=':
        return $this->notEquals($expected, $actual);

      case '<':
        return $this->isLessThan($expected, $actual);

      case '<=':
        return $this->isLessThanOrEqualTo($expected, $actual);

      case '>':
        return $this->isGreaterThan($expected, $actual);

      case '>=':
        return $this->isGreaterThanOrEqualTo($expected, $actual);

      case 'empty':
        return $this->isEmptyValue($actual);

      case 'not empty':
        return $this->isNotEmptyValue($actual);
    }

    return FALSE;
  }

}
