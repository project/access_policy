<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionRule;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Is authored by the current user.
 *
 * Checks to see if the entity was created by the current user.
 *
 * @SelectionRule(
 *   id = "is_own",
 * )
 */
class IsOwn extends SelectionRuleBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new IsOwn selection rule.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityInterface $entity) {
    // If the user is the original owner then allow the policy to be selected.
    if ($entity instanceof EntityOwnerInterface) {
      return $this->isOriginalOwner($entity, $this->currentUser);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(EntityInterface $entity, AccountInterface $account) {
    return $this->isOriginalOwner($entity, $account);
  }

  /**
   * Determine whether the user is the original owner.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return bool
   *   TRUE if the user is the original owner; FALSE otherwise.
   */
  protected function isOriginalOwner(EntityInterface $entity, AccountInterface $account) {
    if ($entity->getOwnerId() == $account->id()) {
      return TRUE;
    }

    return FALSE;
  }

}
