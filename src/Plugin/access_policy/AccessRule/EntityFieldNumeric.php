<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

/**
 * Restrict content by comparing field values.
 *
 * @AccessRule(
 *   id = "entity_field_numeric",
 *   handlers = {
 *     "query_alter" = "\Drupal\access_policy\AccessRuleQueryHandler\EntityField"
 *   }
 * )
 */
class EntityFieldNumeric extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      "operator" => '=',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      '<' => [
        'title' => $this->t('Is less than'),
        'method' => 'validateSimple',
      ],
      '<=' => [
        'title' => $this->t('Is less than or equal to'),
        'method' => 'validateSimple',
      ],
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'validateSimple',
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'validateSimple',
      ],
      '>' => [
        'title' => $this->t('Is greater than'),
        'method' => 'validateSimple',
      ],
      '>=' => [
        'title' => $this->t('Is greater than or equal to'),
        'method' => 'validateSimple',
      ],
    ];

    return $operators;
  }

  /**
   * Validate operators.
   *
   * @param int $expected
   *   The expected value.
   * @param int $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateSimple($expected, $actual) {
    $op = $this->getOperator();

    switch ($op) {
      case '=':
        return $this->equals($expected, $actual);

      case '!=':
        return $this->notEquals($expected, $actual);

      case '<':
        return $this->isLessThan($expected, $actual);

      case '<=':
        return $this->isLessThanOrEqualTo($expected, $actual);

      case '>':
        return $this->isGreaterThan($expected, $actual);

      case '>=':
        return $this->isGreaterThanOrEqualTo($expected, $actual);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return $this->getOperator() . ' ' . $this->settings['value'];
  }

}
