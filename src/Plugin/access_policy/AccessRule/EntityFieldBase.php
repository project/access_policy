<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field value base class.
 */
class EntityFieldBase extends AccessRuleBase {

  use OperatorValidationTrait;
  use AccessRuleFieldTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a EntityFieldBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityFieldManager = $entity_field_manager;
    $this->fieldTypeManager = $field_type_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityInterface $entity) {
    // If the definition is a user then it will always return true.
    // @todo come back to this.
    if ($this->getDefinition()->getEntityType() == 'user') {
      return TRUE;
    }

    if ($entity->hasField($this->definition->getFieldName())) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      'value' => '',
      'empty_behavior' => 'allow',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function accessRuleForm(array $form, FormStateInterface $form_state) {
    $form = parent::accessRuleForm($form, $form_state);

    $this->extraOptionsForm($form, $form_state);

    $form['empty'] = [
      '#type' => 'details',
      '#title' => $this->t('Empty behavior'),
    ];
    $form['empty']['empty_behavior'] = [
      '#type' => 'select',
      '#title' => $this->t('Empty behavior'),
      '#options' => [
        'allow' => $this->t('Allow'),
        'deny' => $this->t('Deny'),
        'ignore' => $this->t('Ignore'),
      ],
      '#default_value' => $this->settings['empty_behavior'],
      '#description' => $this->t('Allow or deny access if the field does not have a value. Ignore to skip this rule entirely.'),
    ];

    return $form;
  }

  /**
   * Build the extra options form.
   *
   * This form is used for EntityField access rules that want to add additional
   * options between value form and empty behavior.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  protected function extraOptionsForm(array &$form, FormStateInterface $form_state) {
    // Intentionally left blank.
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(array &$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $this->settings['value'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function accessRuleFormSubmit(array &$form, FormStateInterface $form_state) {
    parent::accessRuleFormSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->settings['empty_behavior'] = $values['empty_behavior'];
  }

  /**
   * {@inheritdoc}
   */
  public function validate(EntityInterface $entity, AccountInterface $account) {
    $entity_values = $this->getEntityValues($entity, $account);

    if (empty($entity_values)) {
      switch ($this->settings['empty_behavior']) {
        case 'deny':
          return FALSE;

        case 'ignore':
          return NULL;

        default:
          return TRUE;
      }
    }

    return $this->validateCallback($entity, $account);

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    if ($this->getArgument() && $this->getArgument()->getPluginId() == 'current_user' || $this->getDefinition()->getEntityType() == 'user') {
      return ['user.field_values'];
    }

    return parent::getCacheContexts();
  }

  /**
   * Get the values to compare against.
   *
   * @return array
   *   The indexed array of values to compare against.
   */
  public function getValue() {
    $value = parent::getValue();

    // If the value is an array check to see if it's a field value.
    if (is_array($value)) {
      $column_name = $this->getMainPropertyName($this->definition->getFieldType());
      return array_map(function ($value) use ($column_name) {
        // If the column name exists then return that. Otherwise just return
        // the value.
        if (isset($value[$column_name])) {
          return $value[$column_name];
        }
        return $value;
      }, $value);
    }
    return $value;

  }

  /**
   * Validate method callback.
   *
   * For multi-value fields, validation callbacks operate on each value. This
   * is to align with how queries match content with conditions.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return bool
   *   TRUE if valid; FALSE otherwise.
   */
  public function validateCallback(EntityInterface $entity, AccountInterface $account) {
    $operator = $this->getOperator();
    $info = $this->operators();

    // If the values should not be passed then pass in the original entity and
    // account.
    if (isset($info[$operator]['values']) && $info[$operator]['values'] === FALSE) {
      $valid = $this->{$info[$operator]['method']}($entity, $account);
      if ($valid) {
        return TRUE;
      }
      return FALSE;
    }

    $field_values = $this->getEntityValues($entity, $account);
    $options = $this->getValue();

    if (!empty($info[$operator]['method'])) {
      // If field values are empty we should still validate against it.
      if (empty($field_values)) {
        foreach ($options as $option) {
          $valid = $this->{$info[$operator]['method']}($option, $field_values);
          if ($valid) {
            return TRUE;
          }
        }
      }
      else {
        // If any of the field values match then return true.
        foreach ($field_values as $value) {
          foreach ($options as $option) {
            $valid = $this->{$info[$operator]['method']}($option, $value);
            if ($valid) {
              return TRUE;
            }
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * Get the current entity values.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return array
   *   The values.
   */
  protected function getEntityValues(EntityInterface $entity, AccountInterface $account) {
    if ($this->getDefinition()->getEntityType() == 'user') {
      $entity = $this->entityTypeManager->getStorage('user')
        ->load($account->id());
    }

    return $this->getEntityFieldValues($entity, $this->definition->getFieldName());
  }

}
