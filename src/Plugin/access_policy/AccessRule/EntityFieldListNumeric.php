<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\access_policy\LabelHelper;
use Drupal\Core\Form\FormStateInterface;

/**
 * Restrict content by comparing field on the entity.
 *
 * @AccessRule(
 *   id = "entity_field_list_numeric",
 *   handlers = {
 *     "query_alter" = "\Drupal\access_policy\AccessRuleQueryHandler\EntityField"
 *   }
 * )
 */
class EntityFieldListNumeric extends EntityFieldList {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      'value' => [],
      "operator" => '=',
      'filter_allowed_values' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function accessRuleFormSubmit(array &$form, FormStateInterface $form_state) {
    parent::accessRuleFormSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->settings['value'] = $values['value'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(array &$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'select',
      '#title' => $this->t('Value'),
      '#options' => $this->getAllowedValues(),
      '#default_value' => $this->settings['value'],
    ];
  }

  /**
   * Get allowed field value options.
   *
   * @return array
   *   Array of allowed values.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllowedValues() {
    $field_definition = $this->getFieldDefinition();
    $settings = $field_definition->getSettings();
    // Append the numeric value.
    $allowed_values = $settings['allowed_values'] ?? [];
    foreach ($allowed_values as $integer => $label) {
      $allowed_values[$integer] = $label . ' (' . $integer . ')';
    }
    return $allowed_values;
  }

  /**
   * Get the current field definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|false
   *   The field definition.
   */
  protected function getFieldDefinition() {
    $field_map = $this->entityFieldManager->getFieldMapByFieldType($this->definition->getFieldType());
    $entity_fields = $field_map[$this->definition->getEntityType()];
    $current_field = $entity_fields[$this->definition->getFieldName()] ?? [];

    $bundle = reset($current_field['bundles']);
    $definitions = $this->entityFieldManager->getFieldDefinitions($this->definition->getEntityType(), $bundle);
    if (isset($definitions[$this->definition->getFieldName()])) {
      return $definitions[$this->definition->getFieldName()];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      '<' => [
        'title' => $this->t('Is less than'),
        'method' => 'validateSimple',
      ],
      '<=' => [
        'title' => $this->t('Is less than or equal to'),
        'method' => 'validateSimple',
      ],
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'validateSimple',
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'validateSimple',
      ],
      '>' => [
        'title' => $this->t('Is greater than'),
        'method' => 'validateSimple',
      ],
      '>=' => [
        'title' => $this->t('Is greater than or equal to'),
        'method' => 'validateSimple',
      ],
    ];

    return $operators;
  }

  /**
   * Validate operators.
   *
   * @param int $expected
   *   The expected value.
   * @param int $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateSimple($expected, $actual) {
    $op = $this->getOperator();

    switch ($op) {
      case '=':
        return $this->equals($expected, $actual);

      case '!=':
        return $this->notEquals($expected, $actual);

      case '<':
        return $this->isLessThan($expected, $actual);

      case '<=':
        return $this->isLessThanOrEqualTo($expected, $actual);

      case '>':
        return $this->isGreaterThan($expected, $actual);

      case '>=':
        return $this->isGreaterThanOrEqualTo($expected, $actual);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if (!$this->getArgument()) {
      $allowed_values = $this->getAllowedValues();
      $selected = $this->settings['value'];
      $label = [];
      if (isset($allowed_values[$selected])) {
        $label = [$allowed_values[$selected]];
      }

      return $this->getOperator() . ' ' . LabelHelper::render($label, [
        'limit' => 1,
        'empty_value' => $this->t('Unknown'),
      ]);
    }
  }

}
