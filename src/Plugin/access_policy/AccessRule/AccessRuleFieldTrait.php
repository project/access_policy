<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\Core\Entity\EntityInterface;

/**
 * Basic trait for helping with fields.
 */
trait AccessRuleFieldTrait {

  /**
   * The entity type manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManager
   *   The entity type manager.
   */
  public function entityTypeManager() {
    return \Drupal::service('entity_type.manager');
  }

  /**
   * The entity field manager.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   *   The entity field manager.
   */
  public function entityFieldManager() {
    return \Drupal::service('entity_field.manager');
  }

  /**
   * The field type manager.
   *
   * @return \Drupal\Core\Field\FieldTypePluginManagerInterface
   *   The field type manager.
   */
  public function fieldTypeManager() {
    return \Drupal::service('plugin.manager.field.field_type');
  }

  /**
   * Get the values from a field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param string $field_name
   *   The field name.
   *
   * @return array
   *   Index array of field values.
   */
  public function getEntityFieldValues(EntityInterface $entity, $field_name) {
    $field = $entity->get($field_name);
    $field_definition = $field->getFieldDefinition();
    $field_type = $field_definition->getType();

    // Get the main property name.
    $column_name = $this->getMainPropertyName($field_type);

    $values = $entity->get($field_name)->getValue();
    return array_map(function ($value) use ($column_name) {
      if (isset($value[$column_name])) {
        return $value[$column_name];
      }
    }, $values);
  }

  /**
   * Get the field table name.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $field_name
   *   The field name.
   *
   * @return string
   *   The field table name.
   */
  public function getFieldTableName($entity_type_id, $field_name) {
    $storage = $this->entityTypeManager()->getStorage($entity_type_id);
    return $storage->getTableMapping()->getFieldTableName($field_name);
  }

  /**
   * Get the field column name.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $field_name
   *   The field name.
   * @param string $property
   *   The optional property. If blank it will retrieve the main property.
   *
   * @return string
   *   The field column name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getFieldColumnName($entity_type_id, $field_name, $property = NULL) {
    $storage = $this->entityTypeManager()->getStorage($entity_type_id);
    $storage_definition = $this->entityFieldManager()->getActiveFieldStorageDefinitions($entity_type_id)[$field_name];

    // If property is empty then get the main property.
    if (empty($property)) {
      $property = $this->getMainPropertyName($storage_definition->getType());
    }

    // Finally, return the full column name.
    return $storage->getTableMapping()->getFieldColumnName($storage_definition, $property);
  }

  /**
   * Get the main property name of a field.
   *
   * @param string $field_type
   *   The field type.
   *
   * @return string
   *   The main property name.
   */
  public function getMainPropertyName($field_type) {
    $definition = $this->fieldTypeManager()->getDefinition($field_type);
    $class = $definition['class'];
    return $class::mainPropertyName();
  }

}
