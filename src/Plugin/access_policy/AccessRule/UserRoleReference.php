<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\access_policy\Plugin\access_policy\OperatorValidationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Grant access to users found in an entity reference field.
 *
 * @AccessRule(
 *   id = "user_role_reference",
 *   handlers = {
 *     "query_alter" = "\Drupal\access_policy\AccessRuleQueryHandler\UserRoleReference"
 *   }
 * )
 */
class UserRoleReference extends AccessRuleBase {

  use OperatorValidationTrait;
  use AccessRuleFieldTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a UserReference access rule object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityFieldManager = $entity_field_manager;
    $this->fieldTypeManager = $field_type_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityInterface $entity) {
    if ($entity->hasField($this->definition->getFieldName())) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(EntityInterface $entity, AccountInterface $account) {
    $entity_field = $this->definition->getFieldName();
    // If it's a multi-value field then compare all the values. If any of them
    // match then return true.
    $roles = array_map(function ($field) {
      if (isset($field['target_id'])) {
        return $field['target_id'];
      }
    }, $entity->get($entity_field)->getValue());

    foreach ($roles as $role) {
      if ($account->hasRole($role)) {
        return TRUE;
      }
    }

    return FALSE;

  }

}
