<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\access_policy\AccessPolicyHandlerDefinition;
use Drupal\access_policy\Validation\ExecutionContextInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;

/**
 * Access rule plugin trait.
 *
 * @package Drupal\access_policy\Plugin\AccessRule
 */
trait AccessRulePluginTrait {

  /**
   * The access policy handler definition object.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerDefinition
   */
  protected $definition;

  /**
   * The current execution context.
   *
   * @var \Drupal\access_policy\Validation\ExecutionContext
   */
  protected $context;

  /**
   * Plugin configuration.
   *
   * This stores both mutable and immutable configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The plugin settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * The current contextual argument plugin (if any).
   *
   * @var \Drupal\access_policy\Plugin\access_policy\AccessRuleArgument\AccessRuleArgumentPluginInterface
   */
  protected $argument;

  /**
   * Get the current contextual argument plugin.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessRuleArgument\AccessRuleArgumentPluginInterface
   *   The access rule argument plugin.
   */
  public function getArgument() {
    $config = $this->definition->getArgument();
    if (!empty($config) && !isset($this->argument)) {
      $this->argument = \Drupal::service('plugin.manager.access_rule_argument')->createInstance($config['plugin'], $config);
      $this->argument->initialize($this->getDefinition());

      // If execution context exists then pass it in.
      if ($this->getContext()) {
        $this->argument->setContext($this->getContext());
      }
    }
    return $this->argument;
  }

  /**
   * Initialize the access rule plugin.
   *
   * @param \Drupal\access_policy\AccessPolicyHandlerDefinition $definition
   *   The access policy handler definition object.
   */
  public function initialize(AccessPolicyHandlerDefinition $definition) {
    $this->definition = $definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition() {
    // @todo the definition isn't always in sync with settings.
    return $this->definition;
  }

  /**
   * {@inheritdoc}
   */
  public function setContext(ExecutionContextInterface $context) {
    $this->context = $context;
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings) {
    $this->settings = NestedArray::mergeDeepArray([
      $this->baseSettingsDefaults(),
      $this->defaultSettings(),
      $settings,
    ], TRUE);
  }

  /**
   * Returns generic default configuration for access rule plugins.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  protected function baseSettingsDefaults() {
    return [
      'admin_label' => '',
      'query' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setSettingValue($key, $value) {
    $this->settings[$key] = $value;
  }

  /**
   * {@inheritdoc}
   *
   * Creates a generic configuration form for all access rules. Individual
   * access rule plugins can add elements to this form by overriding
   * AccessRule::accessRuleForm(). Most access rule plugins should not override
   * this method unless they need to alter the generic form elements.
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state) {
    $description = $this->definition->getDescription();
    if ($description) {
      $form['help'] = [
        '#markup' => '<p>' . $description . '</p>',
      ];
    }

    // Add plugin-specific settings for this access rule type.
    $form += $this->accessRuleForm($form, $form_state);

    $form['query_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Query settings'),
      '#open' => FALSE,
    ];

    $form['query_settings']['query'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->settings['query'],
      '#title' => $this->t('Apply access rule to queries'),
      '#description' => $this->t('This will hide @entity_type on listing pages and entity reference fields.', [
        '@entity_type' => $this->getAccessPolicy()->getTargetEntityType()->getPluralLabel(),
      ]),
    ];

    $form['operations'] = [
      '#type' => 'details',
      '#title' => $this->t('Operation constraints'),
      '#open' => FALSE,
    ];

    $form['operations']['help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Choose which operations you want this access rule to constrain. Note that this does not bypass permission access.'),
    ];
    $form['operations']['use_default_ops'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use default'),
      '#default_value' => !isset($this->settings['operations']),
    ];

    $form['operations']['operations'] = [
      '#type' => 'checkboxes',
      '#options' => $this->getSupportedOperations(),
      '#title' => $this->t('Operations'),
      '#default_value' => (isset($this->settings['operations'])) ? $this->settings['operations'] : $this->getDefaultOperations(),
      '#states' => [
        'disabled' => [':input[name="use_default_ops"]' => ['checked' => TRUE]],
      ],
    ];

    // Show the widget settings if supported by the plugin.
    if ($this->definition->getWidget() && $this->isSelectionPageEnabled()) {
      $form['widget'] = [
        '#type' => 'details',
        '#title' => $this->t('Access tab settings'),
        '#open' => FALSE,
        '#tree' => TRUE,
      ];
      $form['widget']['show'] = [
        '#type' => 'checkbox',
        '#default_value' => $this->settings['widget']['show'] ?? FALSE,
        '#title' => $this->t('Show widget'),
        '#description' => $this->t('Show the widget on the Access tab.'),
      ];
      $form['widget']['settings'] = [
        '#type' => 'container',
        '#states' => [
          'visible' => [':input[name="widget[show]"]' => ['checked' => TRUE]],
        ],
      ];
      $form['widget']['settings'] = $this->buildWidgetSettingsForm($this->definition->getWidget(), $form['widget']['settings'], $form_state);
    }

    $form['admin_label'] = [
      '#type' => 'details',
      '#title' => $this->t('Administrative title'),
      '#open' => FALSE,
    ];

    $form['admin_label']['admin_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Administrative title'),
      '#default_value' => $this->settings['admin_label'],
      '#description' => $this->t('This will be displayed on the rules list. This is useful when you have the same access rule used twice.'),
    ];

    return $form;
  }

  /**
   * Determine if the selection page is enabled.
   *
   * The access rule widget should only appear if the selection page has been
   * enabled.
   *
   * @return bool
   *   TRUE if the selection page is enabled; FALSE otherwise.
   */
  protected function isSelectionPageEnabled() {
    $entity_type = $this->definition->getEntityType();
    return \Drupal::service('access_policy.selection')->isSelectionPageEnabled($entity_type);
  }

  /**
   * Build the access rule widget settings form.
   *
   * @param string $plugin_id
   *   The widget plugin id.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The access rule widget settings form.
   */
  public function buildWidgetSettingsForm($plugin_id, array $form, FormStateInterface $form_state) {
    $accessRuleWidgetPluginManager = \Drupal::service('plugin.manager.access_rule_widget');
    $widget = $accessRuleWidgetPluginManager->createInstance($plugin_id);
    $widget->initialize($this->getDefinition());
    $settings = $this->settings['widget']['settings'] ?? [];
    $widget->setWidgetSettings($settings);
    return $widget->settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * Most access rule plugins should not override this method. To add validation
   * for a specific access rule, override AccessRule::accessRuleFormValidate().
   */
  public function validateSettingsForm(array &$form, FormStateInterface $form_state) {
    $this->accessRuleFormValidate($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * Most access rule plugins should not override this method. To add submission
   * handling for a specific access rule, override
   * AccessRule::accessRuleFormSubmit().
   */
  public function submitSettingsForm(array &$form, FormStateInterface $form_state) {
    // Process the access rule's submission handling if no errors occurred only.
    if (!$form_state->getErrors()) {
      $form_state->cleanValues();
      if (!empty($form_state->getValue('widget'))) {
        $this->settings['widget'] = $form_state->getValue('widget');
      }

      if (isset($this->settings['operations'])) {
        unset($this->settings['operations']);
      }

      // For some reason 'use_default_ops' does not get properly sent in
      // getValue during initial creation of the access rule.
      $input = $form_state->getUserInput();
      if (empty($input['use_default_ops'])) {
        $override = $this->getOperationOverrides($form_state->getValue('operations'));
        if ($override) {
          $this->settings['operations'] = $override;
        }
      }

      $this->settings['admin_label'] = $form_state->getValue('admin_label');
      $this->settings['query'] = (bool) $form_state->getValue('query');
      $this->accessRuleFormSubmit($form, $form_state);
    }
  }

  /**
   * Load any handlers on this plugin.
   *
   * @param string $name
   *   The handler name.
   *
   * @return \Drupal\access_policy\AccessRuleHandlerInterface|false
   *   The access rule handler.
   */
  public function getHandler($name) {
    $definition = $this->getPluginDefinition();
    if (!empty($definition['handlers'][$name])) {
      $class = $definition['handlers'][$name];
      $handler = \Drupal::service('class_resolver')->getInstanceFromDefinition($class);
      $handler->initialize($this->getDefinition(), $this->getOperator(), $this->getValue());
      return $handler;
    }
    return FALSE;
  }

  /**
   * Normalize the submitted operations values.
   *
   * If the operations are identical to the default as defined by the access
   * policy type then it's not necessary to store the operation override.
   *
   * @param array $operations
   *   The current operations.
   *
   * @return array|false
   *   Array of operations or FALSE if its default.
   */
  public function getOperationOverrides(array $operations) {
    $op_sorted = array_keys(array_filter($operations));
    $default = $this->getDefaultOperations();
    sort($op_sorted);
    sort($default);
    if ($op_sorted === $default) {
      return FALSE;
    }

    return array_keys(array_filter($operations));
  }

  /**
   * Get the default operations.
   *
   * @return string[]
   *   Array of default operations.
   */
  protected function getDefaultOperations() {
    $operations = $this->getAccessRuleOperations();
    $options = [];
    foreach ($operations as $op => $enabled) {
      if ($enabled) {
        $options[] = $op;
      }
    }

    return $options;
  }

  /**
   * Get the supported operations.
   *
   * @return array
   *   Array of supported operations.
   */
  protected function getSupportedOperations() {
    $operations = $this->getAccessRuleOperations();
    $options = [];
    foreach ($operations as $op => $enabled) {
      $options[$op] = $this->getOperationLabel($op);
    }

    return $options;
  }

  /**
   * Get the access policy associated with this handler.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The access policy
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAccessPolicy() {
    return \Drupal::entityTypeManager()->getStorage('access_policy')->load($this->definition->getAccessPolicy());
  }

  /**
   * Get all rules operations.
   *
   * @return array
   *   The rules operations.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAccessRuleOperations() {
    $policy = $this->getAccessPolicy();
    return $policy->getOperationsHandler()->getAccessRuleOperations();
  }

  /**
   * Generate an operation label.
   *
   * @param string $id
   *   The operation id.
   *
   * @return string
   *   The operation label.
   */
  protected function getOperationLabel($id) {
    $operationPluginManager = \Drupal::service('plugin.manager.access_policy_operation');
    $definition = $operationPluginManager->getDefinition($id);
    return $definition['label'];
  }

}
