<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\Core\Form\FormStateInterface;

/**
 * Restrict content by comparing field on the entity.
 *
 * @AccessRule(
 *   id = "entity_field_date",
 *   handlers = {
 *     "query_alter" = "\Drupal\access_policy\AccessRuleQueryHandler\EntityFieldDate"
 *   }
 * )
 */
class EntityFieldDate extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      "operator" => '=',
      'type' => 'now',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function accessRuleFormSubmit(array &$form, FormStateInterface $form_state) {
    parent::accessRuleFormSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->settings['type'] = $values['value']['type'];
    $this->settings['value'] = $values['value']['value'];
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(array &$form, FormStateInterface $form_state) {
    $form['value']['#tree'] = TRUE;
    $form['value']['type'] = [
      '#type' => 'radios',
      '#options' => [
        'now' => $this->t('Now'),
        'date' => $this->t('A date in any machine readable format. CCYY-MM-DD HH:MM:SS is preferred.'),
        'relative' => $this->t('An offset from the current time such as "+1 day" or "-2 hours -30 minutes"'),
      ],
      '#default_value' => $this->settings['type'],
    ];
    $form['value']['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $this->settings['value'],
      '#states' => [
        'visible' => [
          ':input[name="value[type]"]' => [
            ['value' => 'date'],
            ['value' => 'relative'],
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      '<' => [
        'title' => $this->t('Is less than'),
        'method' => 'validateSimple',
      ],
      '<=' => [
        'title' => $this->t('Is less than or equal to'),
        'method' => 'validateSimple',
      ],
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'validateSimple',
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'validateSimple',
      ],
      '>' => [
        'title' => $this->t('Is greater than'),
        'method' => 'validateSimple',
      ],
      '>=' => [
        'title' => $this->t('Is greater than or equal to'),
        'method' => 'validateSimple',
      ],
    ];

    return $operators;
  }

  /**
   * Validate operators.
   *
   * @param int $expected
   *   The expected value.
   * @param int $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateSimple($expected, $actual) {
    $op = $this->getOperator();

    $actual = $this->convertToTimestamp($actual);

    switch ($this->settings['type']) {
      case 'now':
        $expected = time();
        break;

      case 'date':
        $expected = $this->convertToTimestamp($expected);
        break;

      case 'relative':
        $expected = time() + $this->convertToTimestamp($expected);
        break;
    }

    switch ($op) {
      case '=':
        return $this->equals($expected, $actual);

      case '!=':
        return $this->notEquals($expected, $actual);

      case '<':
        return $this->isLessThan($expected, $actual);

      case '<=':
        return $this->isLessThanOrEqualTo($expected, $actual);

      case '>':
        return $this->isGreaterThan($expected, $actual);

      case '>=':
        return $this->isGreaterThanOrEqualTo($expected, $actual);
    }

    return FALSE;
  }

  /**
   * Convert a date to a timestamp for comparison.
   *
   * @param string $date
   *   The string formatted date.
   *
   * @return int
   *   The timestamp.
   */
  protected function convertToTimestamp($date) {
    // Only convert to timestamp if the value is not an integer.
    if (!is_numeric($date)) {
      return intval(strtotime($date, 0));
    }

    return intval($date);
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    switch ($this->settings['type']) {
      case 'now':
        $value = $this->t('Now');
        break;

      default:
        $value = $this->settings['value'];
        break;
    }
    return $this->getOperator() . ' ' . $value;
  }

}
