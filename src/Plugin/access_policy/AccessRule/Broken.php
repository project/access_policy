<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Broken access rule.
 *
 * If an access rule data definition is invalid or does not exist it will
 * fall back to this access rule plugin. Broken access rules do not restrict
 * access.
 *
 * @AccessRule(
 *   id = "broken",
 * )
 */
class Broken extends AccessRuleBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityInterface $entity) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state) {
    $description_top = $this->t('The handler for this access rule is broken or missing. The following details are available:');

    $items = [];
    foreach ($this->configuration['original_configuration'] as $key => $value) {
      if (is_scalar($value)) {
        $items[] = new FormattableMarkup('@key: @value', [
          '@key' => $key,
          '@value' => $value,
        ]);
      }
    }

    $form['description'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['js-form-item', 'form-item', 'description'],
      ],
      'description_top' => [
        '#markup' => '<p>' . $description_top . '</p>',
      ],
      'detail_list' => [
        '#theme' => 'item_list',
        '#items' => $items,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(EntityInterface $entity, AccountInterface $account) {
    return TRUE;
  }

}
