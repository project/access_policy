<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * The access rule plugin interface.
 */
interface AccessRulePluginInterface extends ContainerFactoryPluginInterface, PluginInspectionInterface {

  /**
   * Get the full handler definition.
   *
   * This should not be confused with the plugin definition. This includes
   * settings as well as the data definition.
   *
   * @return \Drupal\access_policy\AccessPolicyHandlerDefinition
   *   The access policy handler definition object.
   */
  public function getDefinition();

  /**
   * Checks whether the access rule is applicable for the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return bool
   *   TRUE if the plugin is applicable; FALSE otherwise.
   */
  public function isApplicable(EntityInterface $entity);

  /**
   * Validate the access rule.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user session for which to check access.
   *
   * @return bool
   *   TRUE if the validation passes; FALSE otherwise.
   */
  public function validate(EntityInterface $entity, AccountInterface $account);

  /**
   * Get the access rule cache contexts.
   *
   * If the access rule is comparing values between the current user and entity
   * then use 'user.field_values', if it is comparing against the uid then use
   * 'user', otherwise use 'user.permissions'.
   *
   * @return array
   *   Array of cache contexts.
   */
  public function getCacheContexts();

  /**
   * Returns the configuration form elements specific to this rule plugin.
   *
   * Access rules that need to add form elements to the normal rule
   * configuration form should implement this method.
   *
   * @param array $form
   *   The form definition array for the access rule configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The renderable form array representing the entire configuration form.
   */
  public function accessRuleForm(array $form, FormStateInterface $form_state);

  /**
   * Adds access rule type-specific validation for the access rule form.
   *
   * Note that this method takes the form structure and form state for the full
   * access rule configuration form as arguments, not just the elements defined
   * in AccessRulePluginInterface::accessRuleForm().
   *
   * @param array $form
   *   The form definition array for the full access rule configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\Core\Block\BlockPluginInterface::blockForm()
   * @see \Drupal\Core\Block\BlockPluginInterface::blockSubmit()
   */
  public function accessRuleFormValidate(array &$form, FormStateInterface $form_state);

  /**
   * Adds access rule type-specific submission handling for the block form.
   *
   * Note that this method takes the form structure and form state for the full
   * access rule configuration form as arguments, not just the elements defined
   * in AccessRulePluginInterface::accessRuleForm().
   *
   * @param array $form
   *   The form definition array for the full access rule configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\Core\Block\BlockPluginInterface::blockForm()
   * @see \Drupal\Core\Block\BlockPluginInterface::blockValidate()
   */
  public function accessRuleFormSubmit(array &$form, FormStateInterface $form_state);

}
