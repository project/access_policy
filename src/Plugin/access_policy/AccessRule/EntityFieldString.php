<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

/**
 * Restrict content by comparing field values.
 *
 * @AccessRule(
 *   id = "entity_field_string",
 *   handlers = {
 *     "query_alter" = "\Drupal\access_policy\AccessRuleQueryHandler\EntityField"
 *   }
 * )
 */
class EntityFieldString extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      "operator" => '=',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'validateSimple',
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'validateSimple',
      ],
      'contains' => [
        'title' => $this->t('Contains'),
        'method' => 'validateContains',
      ],
      'not contains' => [
        'title' => $this->t('Does not contain'),
        'method' => 'validateContains',
      ],
      'starts with' => [
        'title' => $this->t('Starts with'),
        'method' => 'validateStartsWith',
      ],
      'not starts with' => [
        'title' => $this->t('Does not start with'),
        'method' => 'validateStartsWith',
      ],
      'ends with' => [
        'title' => $this->t('Ends with'),
        'method' => 'validateEndsWith',
      ],
      'not ends with' => [
        'title' => $this->t('Does not end with'),
        'method' => 'validateEndsWith',
      ],
    ];

    return $operators;
  }

  /**
   * Validate operators.
   *
   * @param mixed $expected
   *   The expected value.
   * @param mixed $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateSimple($expected, $actual) {
    $op = $this->getOperator();
    switch ($op) {
      case '=':
        return $this->equals($expected, $actual);

      case '!=':
        return $this->notEquals($expected, $actual);
    }

    return FALSE;
  }

  /**
   * Validate contains and not contains operators.
   *
   * @param string $needle
   *   The needle.
   * @param string $haystack
   *   The haystack.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateContains($needle, $haystack) {
    $op = $this->getOperator();
    switch ($op) {
      case 'contains':
        return $this->contains($needle, $haystack);

      case 'not contains':
        return $this->notContains($needle, $haystack);
    }

    return FALSE;
  }

  /**
   * Validate starts with and not starts with operators.
   *
   * @param string $prefix
   *   The string prefix.
   * @param string $string
   *   The string.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateStartsWith($prefix, $string) {
    $op = $this->getOperator();

    switch ($op) {
      case 'starts with':
        return $this->startsWith($prefix, $string);

      case 'not starts with':
        return $this->notStartsWith($prefix, $string);
    }

    return FALSE;
  }

  /**
   * Validate starts with and not starts with operators.
   *
   * @param string $suffix
   *   The string suffix.
   * @param string $string
   *   The string.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateEndsWith($suffix, $string) {
    $op = $this->getOperator();

    switch ($op) {
      case 'ends with':
        return $this->endsWith($suffix, $string);

      case 'not ends with':
        return $this->notEndsWith($suffix, $string);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return $this->getOperator() . ' ' . $this->settings['value'];
  }

}
