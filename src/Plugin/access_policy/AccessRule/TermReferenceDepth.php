<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\access_policy\TermHierarchy;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Restrict content by comparing term references with depth support.
 *
 * @AccessRule(
 *   id = "term_reference_depth",
 *   handlers = {
 *     "query_alter" = "\Drupal\access_policy\AccessRuleQueryHandler\EntityField"
 *   }
 * )
 */
class TermReferenceDepth extends EntityFieldEntityReference {

  /**
   * The access policy term hierarchy service.
   *
   * @var \Drupal\access_policy\TermHierarchy
   */
  protected $termHierarchy;

  /**
   * Constructs a TermReferenceDepth object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\TermHierarchy $term_hierarchy
   *   The access policy term hierarchy service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, EntityTypeManagerInterface $entity_type_manager, TermHierarchy $term_hierarchy) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_field_manager, $field_type_manager, $entity_type_manager);
    $this->termHierarchy = $term_hierarchy;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('entity_type.manager'),
      $container->get('access_policy.term_hierarchy'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      'value' => [],
      'depth' => 1,
      "operator" => 'in',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function extraOptionsForm(array &$form, FormStateInterface $form_state) {
    $form['depth'] = [
      '#title' => $this->t('Depth'),
      '#type' => 'select',
      '#default_value' => $this->settings['depth'],
      '#options' => $this->getDepthOptions(10),
      '#description' => $this->t('The depth will match entities tagged with terms in the hierarchy.'),
    ];

    return $form;
  }

  /**
   * Get the depth options.
   *
   * @param int $limit
   *   The depth limit.
   *
   * @return array
   *   Array of depth options.
   */
  protected function getDepthOptions($limit) {
    $options = [];
    for ($i = 0; $i < $limit; $i++) {
      $options[] = $i;
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function accessRuleFormSubmit(array &$form, FormStateInterface $form_state) {
    parent::accessRuleFormSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->settings['value'] = $values['value'] ?? [];
    $this->settings['depth'] = $values['depth'] ?? 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $value = parent::getValue();
    // Fetch all the supported bundles. Most of the time this should
    // only return one bundle.
    $terms = $this->entityTypeManager()->getStorage('taxonomy_term')->loadMultiple($value);
    foreach ($terms as $term) {
      $term_objs = $this->termHierarchy->loadTree($term->bundle(), $term->id(), $this->settings['depth']);
      foreach ($term_objs as $term_obj) {
        $value[] = $term_obj->tid;
      }
    }

    return array_unique($value);
  }

}
