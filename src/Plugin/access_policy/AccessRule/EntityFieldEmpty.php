<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Restrict content based on whether a field has a value.
 *
 * @AccessRule(
 *   id = "entity_field_empty",
 *   handlers = {
 *     "query_alter" = "\Drupal\access_policy\AccessRuleQueryHandler\EntityField"
 *   }
 * )
 */
class EntityFieldEmpty extends EntityFieldBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      "operator" => 'not empty',
      'empty_behavior' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      'empty' => [
        'title' => $this->t('Is empty'),
        'method' => 'validateEmpty',
        'hide_value' => TRUE,
      ],
      'not empty' => [
        'title' => $this->t('Is not empty'),
        'method' => 'validateEmpty',
        'hide_value' => TRUE,
      ],
    ];

    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  public function accessRuleForm(array $form, FormStateInterface $form_state) {
    $form = parent::accessRuleForm($form, $form_state);
    unset($form['empty']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(EntityInterface $entity, AccountInterface $account) {
    $entity_values = $this->getEntityValues($entity, $account);

    return $this->validateEmpty($entity_values);
  }

  /**
   * Validate simple operators.
   *
   * @param mixed $actual
   *   The actual value.
   *
   * @return bool
   *   TRUE if operator passes; FALSE otherwise.
   */
  public function validateEmpty($actual) {
    $op = $this->getOperator();
    switch ($op) {
      case 'empty':
        return $this->isEmptyValue($actual);

      case 'not empty':
        return $this->isNotEmptyValue($actual);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return $this->getOperator() . ' ' . $this->settings['value'];
  }

}
