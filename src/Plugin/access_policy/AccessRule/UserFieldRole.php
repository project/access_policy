<?php

namespace Drupal\access_policy\Plugin\access_policy\AccessRule;

use Drupal\access_policy\LabelHelper;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Restrict content for users with specific roles.
 *
 * @AccessRule(
 *   id = "user_field_role",
 * )
 */
class UserFieldRole extends EntityFieldEntityReference {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      "operator" => 'in',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function accessRuleForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => $this->t("<strong>Warning:</strong> This does not bypass permission access. This is best used with rules-only policy types."),
    ];
    return parent::accessRuleForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(array &$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Value'),
      '#options' => $this->getRoles(),
      '#default_value' => $this->settings['value'],
      '#description' => $this->t('Choose which roles can access this content.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function valueSubmit($form, FormStateInterface $form_state) {
    $values = array_filter($form_state->getValues());
    if (!empty($values['value'])) {
      $value = array_filter($values['value']);
      $form_state->setValue('value', $value);
    }
  }

  /**
   * Get all available roles.
   *
   * @return array
   *   Array of roles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getRoles() {
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $options = [];
    foreach ($roles as $role) {
      $options[$role->id()] = $role->label();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function validateCallback(EntityInterface $entity, AccountInterface $account) {
    $operator = $this->getOperator();
    $info = $this->operators();

    $field_values = $this->getEntityValues($entity, $account);

    if (!empty($info[$operator]['method'])) {
      // If any of the field values match then return true.
      // Roles are stored on the access policy with underscores "_" however they
      // are stored in Drupal with a space. We need to replace that underscore
      // with the space.
      $rids = array_keys($this->settings['value']);
      $options = array_map(function ($role_id) {
        return str_replace('_', ' ', $role_id);
      }, $rids);

      $valid = $this->{$info[$operator]['method']}($options, $field_values);
      if ($valid) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    $roles = $this->getRoles();
    $roles = array_filter($roles, function ($role) {
      if (in_array($role, $this->settings['value'])) {
        return TRUE;
      }
      return FALSE;
    }, ARRAY_FILTER_USE_KEY);

    return $this->getOperator() . ' ' . LabelHelper::render($roles, [
      'limit' => 2,
      'empty_value' => $this->t('Unknown'),
    ]);
  }

}
