<?php

namespace Drupal\access_policy\Plugin\access_policy;

use Drupal\access_policy\AccessPolicyHandlerDefinition;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AccessPolicyHandlerBase.
 *
 * This abstract class provides the generic access policy handler configuration
 * form and validation.
 */
abstract class AccessPolicyHandlerBase extends PluginBase implements AccessPolicyHandlerInterface {

  /**
   * The access policy handler definition object.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerDefinition
   */
  protected $definition;

  /**
   * The actual values that will be compared against.
   *
   * @var mixed
   */
  protected $value = NULL;

  /**
   * Contains the operator which is used for validation.
   *
   * @var string
   */
  protected $operator = '=';

  /**
   * Plugin configuration.
   *
   * This stores both mutable and immutable configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The plugin settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration = $configuration;
    $settings = $configuration['settings'] ?? [];
    $this->setSettings($settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings) {
    $this->settings = NestedArray::mergeDeepArray([
      $this->baseSettingsDefaults(),
      $this->defaultSettings(),
      $settings,
    ], TRUE);
  }

  /**
   * Returns generic default configuration for handler plugins.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  protected function baseSettingsDefaults() {
    return [
      'admin_label' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setSettingValue($key, $value) {
    $this->settings[$key] = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition() {
    return $this->definition;
  }

  /**
   * Initialize the access policy handler plugin.
   *
   * @param \Drupal\access_policy\AccessPolicyHandlerDefinition $definition
   *   The access policy handler definition object.
   */
  public function initialize(AccessPolicyHandlerDefinition $definition) {
    $this->definition = $definition;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function isApplicable(EntityInterface $entity);

  /**
   * {@inheritdoc}
   */
  abstract public function validate(EntityInterface $entity, AccountInterface $account);

  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state) {
    $this->operatorForm($form, $form_state);
    $this->valueForm($form, $form_state);

    return $form;
  }

  /**
   * Show the operator form if options exist.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  protected function operatorForm(array &$form, FormStateInterface $form_state) {
    $options = $this->getOperatorOptions();
    if (!empty($options)) {
      $form['operator'] = [
        '#type' => 'select',
        '#title' => $this->t('Operator'),
        '#options' => $this->getOperatorOptions(),
        '#default_value' => $this->getOperator(),
      ];
    }
  }

  /**
   * Build the value form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  protected function valueForm(array &$form, FormStateInterface $form_state) {
    $form['value'] = [];
  }

  /**
   * Create array of operator options.
   *
   * @return array
   *   Array of operators.
   */
  public function getOperatorOptions() {
    $options = [];
    $operators = $this->operators();
    foreach ($operators as $op => $item) {
      $options[$op] = $item['title'];
    }

    return $options;
  }

  /**
   * Array of operators.
   *
   * @return array
   *   The array of operators.
   */
  public function operators() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function validateSettingsForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitSettingsForm(array &$form, FormStateInterface $form_state) {
    $this->operatorSubmit($form, $form_state);
    $this->valueSubmit($form, $form_state);

    $values = $form_state->getValues();
    if (isset($values['operator'])) {
      $this->settings['operator'] = $values['operator'];
    }
    // Saving the settings from which we retrieve the value, not necessarily
    // the actual value.
    if (isset($values['value'])) {
      $this->settings['value'] = $values['value'];
    }
  }

  /**
   * Perform any changes to the submitted values.
   */
  protected function operatorSubmit($form, FormStateInterface $form_state) {

  }

  /**
   * Perform any changes to the submitted values.
   */
  protected function valueSubmit($form, FormStateInterface $form_state) {

  }

  /**
   * Return the actual value that is being validated against.
   *
   * This can be static values or computed values such as from a field.
   *
   * @return mixed
   *   The current actual values.
   */
  public function getValue() {
    if (!isset($this->value)) {
      if (isset($this->settings['value'])) {
        $value = $this->settings['value'];
        if (!is_array($value)) {
          $value = [$value];
        }
        $this->value = $value;
      }
    }

    return $this->value;
  }

  /**
   * Set the computed value.
   *
   * @param mixed $value
   *   The computed value.
   */
  public function setValue($value) {
    $this->value = $value;
  }

  /**
   * Get the current operator.
   *
   * The operator can be set in multiple places, either in the data definition,
   * the settings or in the class itself. If the operator is set in the data
   * definition then it can't be changed.
   *
   * @return string
   *   The current operator.
   */
  public function getOperator() {
    if (isset($this->settings['operator'])) {
      $this->operator = $this->settings['operator'];
    }
    return $this->operator;
  }

  /**
   * {@inheritdoc}
   */
  public function adminLabel() {
    if ($this->adminSummary() && empty($this->getDefinition()->getSetting('admin_label'))) {
      return $this->getDefinition()->getLabel() . ' (' . $this->adminSummary() . ')';
    }

    return $this->getDefinition()->getLabel();
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {

  }

}
