<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionStrategy;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Manual selection strategy.
 *
 * @SelectionStrategy(
 *   id = "manual",
 *   label = @Translation("Manual"),
 *   description = @Translation("Users assign policies from an Access tab. Similar to traditional document-based access control."),
 *   weight = 10,
 * )
 */
class ManualSelection extends SelectionStrategyBase {

  /**
   * {@inheritdoc}
   */
  public function defaultOptions() {
    return [
      'dynamic_assignment' => 'never',
      'enable_selection_page' => TRUE,
      'show_operations_link' => FALSE,
      'enable_policy_field' => TRUE,
      'allow_empty' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      'allow_empty' => TRUE,
      'default_policy' => 'empty',
      'show_operations_link' => FALSE,
      'show_confirmation_form' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityTypeInterface $entity_type) {
    $path = $entity_type->getLinkTemplate('canonical');
    if (empty($path)) {
      return FALSE;
    }

    // Confirm that the corresponding route has a local task associated with it.
    /** @var \Symfony\Component\Routing\RouteCollection $route_collection */
    $route_collection = \Drupal::service('router.route_provider')->getRoutesByPattern($path);
    foreach ($route_collection->all() as $name => $route) {
      $local_tasks = \Drupal::service('plugin.manager.menu.local_task')->getLocalTasksForRoute($name);
      if (!empty($local_tasks[0])) {
        /** @var \Drupal\Core\Menu\LocalTaskInterface $task */
        foreach ($local_tasks[0] as $task) {
          if ($task->getRouteName() == $name) {
            return TRUE;
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state) {

    $form['show_operations_link'] = [
      '#title' => $this->t('Show operations link'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show a link to the Access tab on listing pages.'),
      '#default_value' => $this->settings['show_operations_link'],
    ];

    $form['show_confirmation_form'] = [
      '#title' => $this->t('Show confirmation form'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show a confirmation form when changing access policies.'),
      '#default_value' => $this->settings['show_confirmation_form'],
    ];

    $form['allow_empty'] = [
      '#title' => $this->t('Allow empty'),
      '#type' => 'checkbox',
      '#default_value' => $this->settings['allow_empty'],
      '#description' => $this->t('Allow authors to create and edit @entity_type without an access policy.', ['@entity_type' => $this->entityType->getPluralLabel()]),
    ];

    $empty_allowed = [':input[name="strategy_settings[allow_empty]"]' => ['checked' => FALSE]];

    $form['default_policy'] = [
      '#title' => $this->t('Default policy'),
      '#type' => 'radios',
      '#options' => [
        'empty' => $this->t('Do not assign a policy'),
        'first_available' => $this->t('First available'),
      ],
      '#default_value' => $this->settings['default_policy'],
      '#states' => [
        'disabled' => $empty_allowed,
      ],
      '#description' => $this->t('Set the default policy when new @entity_type are created.', ['@entity_type' => $this->entityType->getPluralLabel()]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareOptions(array $options) {
    $options = parent::prepareOptions($options);

    if ($this->settings['default_policy'] == 'first_available') {
      $options['dynamic_assignment'] = 'on_create';
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitSettingsForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $form_state->cleanValues();
      $values = $form_state->getValue('strategy_settings');
      $this->settings['show_operations_link'] = (bool) $values['show_operations_link'];
      $this->settings['show_confirmation_form'] = (bool) $values['show_confirmation_form'];
      $this->settings['allow_empty'] = (bool) $values['allow_empty'];

      if (!$this->settings['allow_empty']) {
        $this->settings['default_policy'] = 'first_available';
      }
      else {
        $this->settings['default_policy'] = $values['default_policy'];
      }
    }
  }

}
