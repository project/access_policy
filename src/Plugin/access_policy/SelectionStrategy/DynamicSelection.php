<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionStrategy;

use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Dynamic selection strategy.
 *
 * @SelectionStrategy(
 *   id = "dynamic",
 *   label = @Translation("Dynamic"),
 *   description = @Translation("Access policies are dynamically assigned to entities based on selection rules."),
 *   weight = 0,
 * )
 */
class DynamicSelection extends SelectionStrategyBase {

  /**
   * {@inheritdoc}
   */
  public function defaultOptions() {
    return [
      'dynamic_assignment' => 'on_change',
      'enable_selection_page' => FALSE,
      'show_operations_link' => FALSE,
      'enable_policy_field' => FALSE,
      'allow_empty' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityTypeInterface $entity_type) {
    return TRUE;
  }

}
