<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionStrategy;

use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Interface SelectionStrategyInterface.
 *
 * Selection strategy plugins allow you to customize various options of the
 * assignment experience. For example, you can disable the access tab, specify
 * the dynamic assignment behavior and more. This is useful in cases where one
 * assignment experience may be applicable for one entity type but not another.
 *
 * Available options include:
 * - dynamic_assignment: When to trigger dynamic assignment. Values include:
 *   'on_save', 'on_change', 'on_create' and 'never'.
 * - enable_selection_page: Show the Access tab on entities.
 * - show_operations_link: Show an operations link on listing pages.
 * - enable_policy_field: Enable or disable the access policy field.
 * - allow_empty: Allow entities with no access policy.
 */
interface SelectionStrategyInterface {

  /**
   * Determine whether a strategy is applicable for a given entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return bool
   *   TRUE if applicable; FALSE otherwise.
   */
  public static function isApplicable(EntityTypeInterface $entity_type);

  /**
   * The default strategy options.
   *
   * The selection strategy controls various features through an associative
   * array of options.
   *
   * @return array
   *   Array of default options.
   */
  public function defaultOptions();

  /**
   * Prepare the final options based on configuration values.
   *
   * This normalizes any settings that you may have defined into the options
   * array that the system recognizes.
   *
   * @param array $options
   *   The default options.
   *
   * @return array
   *   Array of strategy options.
   */
  public function prepareOptions(array $options);

}
