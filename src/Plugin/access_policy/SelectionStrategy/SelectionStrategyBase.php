<?php

namespace Drupal\access_policy\Plugin\access_policy\SelectionStrategy;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class SelectionStrategyBase.
 *
 * This abstract class provides the generic configuration form and validation.
 */
abstract class SelectionStrategyBase extends PluginBase implements SelectionStrategyInterface {

  use StringTranslationTrait;

  /**
   * The entity type associated with this selection strategy.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * Array of selection strategy options.
   *
   * The options provided here control what features are available and how
   * they are used.
   *
   * @var array
   */
  protected $options = [];

  /**
   * The selection strategy settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setSettings($configuration);
  }

  /**
   * Set the entity type for this selection strategy.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   */
  public function setEntityType(EntityTypeInterface $entity_type) {
    $this->entityType = $entity_type;
  }

  /**
   * Returns the array of settings, including defaults for missing settings.
   *
   * @return array
   *   The array of settings.
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * Returns the value of a setting, or its default value if absent.
   *
   * @param string $key
   *   The setting name.
   *
   * @return mixed
   *   The setting value.
   */
  public function getSetting($key) {
    if (isset($this->settings[$key])) {
      return $this->settings[$key];
    }
  }

  /**
   * Sets the settings for the plugin.
   *
   * @param array $settings
   *   The array of settings, keyed by setting names. Missing settings will be
   *   assigned their default values.
   *
   * @return $this
   */
  public function setSettings(array $settings) {
    $this->settings = NestedArray::mergeDeepArray([
      $this->baseSettingsDefaults(),
      $this->defaultSettings(),
      $settings,
    ], TRUE);

    return $this;
  }

  /**
   * Sets the value of a setting for the plugin.
   *
   * @param string $key
   *   The setting name.
   * @param mixed $value
   *   The setting value.
   *
   * @return $this
   */
  public function setSetting($key, $value) {
    $this->settings[$key] = $value;

    return $this;
  }

  /**
   * Defines the default settings for this plugin.
   *
   * @return array
   *   A list of default settings, keyed by the setting name.
   */
  public function defaultSettings() {
    return [];
  }

  /**
   * Returns generic default configuration for access rule plugins.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  protected function baseSettingsDefaults() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateSettingsForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitSettingsForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('strategy_settings') ?? [];
    $this->settings = $values;
  }

  /**
   * Get an individual selection strategy option.
   *
   * @param string $key
   *   The option key.
   *
   * @return mixed
   *   The selection option value.
   */
  public function getOption($key) {
    $options = $this->getOptions();
    if (isset($options[$key])) {
      return $options[$key];
    }
  }

  /**
   * Get all selection strategy options.
   *
   * @return array
   *   The selection option array.
   */
  public function getOptions() {
    if (empty($this->options)) {
      $options = NestedArray::mergeDeepArray([
        $this->defaultOptions(),
      ], TRUE);
      $this->options = $this->prepareOptions($options);
    }

    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareOptions(array $options) {
    // If settings have matching keys then replace the option values
    // with those.
    if (!empty($this->settings)) {
      foreach ($this->settings as $key => $value) {
        if (isset($options[$key])) {
          $options[$key] = $value;
        }
      }
    }
    return $options;
  }

}
