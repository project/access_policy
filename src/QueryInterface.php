<?php

namespace Drupal\access_policy;

/**
 * Access policy query interface.
 *
 * This is a simplified query interface that makes it easier to integrate access
 * policy queries with different contexts. This is not used with an actual query
 * object. Instead, you implement this interface with a custom query stub object
 * and then merge the properties in the query stub to the actual query object.
 *
 * @see \Drupal\access_policy\SqlQuery
 */
interface QueryInterface {

  /**
   * Determine if query is distinct.
   *
   * @return bool
   *   Distinct status.
   */
  public function setDistinct();

  /**
   * Determine if query is distinct.
   *
   * @return bool
   *   Distinct status.
   */
  public function isDistinct();

  /**
   * Get the base table.
   *
   * @return string
   *   The base table.
   */
  public function getBaseTable();

  /**
   * Get the base field.
   *
   * @return string
   *   The base field.
   */
  public function getBaseField();

  /**
   * Returns the database connection.
   *
   * @return \Drupal\Core\Database\Connection
   *   The database connection.
   */
  public function getConnection();

  /**
   * Get all the tables that have been joined.
   *
   * @return array
   *   Array of joined tables.
   */
  public function getTables();

  /**
   * Join against another table in the database.
   *
   * This is an exact copy of the Drupal\Core\Database\Query::addJoin().
   *
   * @param string $type
   *   The type of join.
   * @param string $table
   *   The table against which to join.
   * @param string $alias
   *   The alias for the table.
   * @param string $condition
   *   The condition on which to join this table.
   * @param array $arguments
   *   An array of arguments to replace into the $condition of this join.
   *
   * @return string
   *   The unique alias that was assigned for this table.
   */
  public function addJoin($type, $table, $alias = NULL, $condition = NULL, array $arguments = []);

  /**
   * Get all the conditions that have been added.
   *
   * @return array
   *   The array of conditions.
   */
  public function conditions();

  /**
   * Add a condition.
   *
   * @param string $field
   *   The field name.
   * @param mixed $value
   *   The value.
   * @param string $operator
   *   The operator.
   */
  public function condition($field, $value = NULL, $operator = NULL);

  /**
   * Creates a new group of conditions ANDed together.
   *
   * @return \Drupal\Core\Database\Query\ConditionInterface
   *   The AND condition group.
   */
  public function andConditionGroup();

  /**
   * Creates a new group of conditions ORed together.
   *
   * @return \Drupal\Core\Database\Query\ConditionInterface
   *   The OR condition group.
   */
  public function orConditionGroup();

}
