<?php

namespace Drupal\access_policy;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class to manage and lazy load cached access policy data.
 */
class EntityFieldAccessPolicyData implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $accessPolicyInfo;

  /**
   * The access rule manager.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $accessRuleManager;

  /**
   * Constructs the EntityField access policy data set.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\access_policy\AccessPolicyInformation $access_policy_info
   *   The access policy information service.
   * @param \Drupal\access_policy\AccessPolicyHandlerManager $access_rule_manager
   *   The access rule manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $bundle_info, AccessPolicyInformation $access_policy_info, AccessPolicyHandlerManager $access_rule_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->bundleInfo = $bundle_info;
    $this->accessPolicyInfo = $access_policy_info;
    $this->accessRuleManager = $access_rule_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('access_policy.information'),
      $container->get('plugin.manager.access_policy.access_rule'),
    );
  }

  /**
   * Defines the default access policy data.
   *
   * @see hook_access_policy_data()
   */
  public function accessPolicyData() {
    $data = [];
    $this->createBaseFieldAccessRules($data);
    $this->createDerivedAccessRules($data);
    return $data;
  }

  /**
   * Create base field access rules.
   *
   * @param array $data
   *   The current access policy data.
   */
  protected function createBaseFieldAccessRules(array &$data) {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if ($this->accessPolicyInfo->isAccessControlledEntityType($entity_type)) {
        $field_definitions = $this->getBaseFieldDefinitions($entity_type);
        foreach ($field_definitions as $field_definition) {
          $data[$entity_type->id()][$field_definition->getName()] = [
            'label' => $this->t('@label', ['@label' => $field_definition->getLabel()]),
            'plugin_id' => $this->getFieldValuePluginId($field_definition),
            'field' => $field_definition->getName(),
          ];

          // Override the "type" field access rule.
          $key = $entity_type->getKey('bundle');
          if ($key == $field_definition->getName()) {
            $data[$entity_type->id()][$key]['plugin_id'] = 'entity_field_type';
            $data[$entity_type->id()][$key]['selection_rule'] = [
              'label' => $this->t('@field', ['@field' => $field_definition->getLabel()]),
              'plugin_id' => 'bundle',
              'description' => $this->t('The bundle'),
            ];
          }

          if ($field_definition->getName() == 'moderation_state') {
            $data[$entity_type->id()][$field_definition->getName()]['plugin_id'] = 'entity_field_moderation_state';
            $data[$entity_type->id()][$field_definition->getName()]['selection_rule'] = [
              'label' => $this->t('@field', ['@field' => $field_definition->getLabel()]),
              'plugin_id' => 'moderation_state',
              'description' => $this->t('The moderation state'),
            ];
          }
        }

      }
    }
  }

  /**
   * Get base field definitions.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return array
   *   Array of field definitions.
   */
  protected function getBaseFieldDefinitions(EntityTypeInterface $entity_type) {
    $base_fields = [
      'id',
      'label',
      'bundle',
      'created',
      'changed',
      'published',
      'moderation_state',
    ];
    $base_definitions = $this->entityFieldManager->getBaseFieldDefinitions($entity_type->id());

    $field_definitions = [];
    foreach ($base_fields as $field) {
      $key = $entity_type->getKey($field);
      $field_name = (!empty($key)) ? $key : $field;
      if ($field_name && isset($base_definitions[$field_name])) {
        $field_definitions[$field_name] = $base_definitions[$field_name];
      }
    }
    return $field_definitions;
  }

  /**
   * Create derived entity field access rules.
   *
   * @param array $data
   *   The current access policy data.
   */
  protected function createDerivedAccessRules(array &$data) {
    $field_definitions = $this->getAllFieldDefinitions();
    if (!empty($field_definitions)) {
      foreach ($field_definitions as $entity_type => $fields) {
        foreach ($fields as $field_name => $definition) {
          $type = $definition->getType();
          switch ($type) {
            case 'entity_reference':
              $storage = $definition->getFieldStorageDefinition();
              $target_type = $storage->getSetting('target_type');
              switch ($target_type) {
                case 'user_role':
                  $data[$entity_type][$field_name] = $this->createUserRoleReferenceAccessRule($definition);
                  break;

                case 'taxonomy_term':
                  $data[$entity_type]['term_depth_' . $field_name] = $this->createTermReferenceWithDepthAccessRule($definition);
                  // Make sure that the default plugin also gets created for
                  // taxonomy terms.
                  $data[$entity_type][$field_name] = $this->createEntityReferenceFieldValueAccessRule($definition);
                  break;

                default:
                  $data[$entity_type][$field_name] = $this->createEntityReferenceFieldValueAccessRule($definition);
                  break;
              }
              break;

            // This will create both the default list access rule and unique
            // numeric access rules for list fields.
            case 'list_integer':
            case 'list_float':
              $data[$entity_type]['numeric_' . $field_name] = $this->createFieldListNumericFieldValueAccessRule($definition);
              $data[$entity_type][$field_name] = $this->createFieldValueAccessRule($definition);
              break;

            default:
              $data[$entity_type][$field_name] = $this->createFieldValueAccessRule($definition);
              break;
          }

        }
      }
    }

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if ($this->accessPolicyInfo->isSupportedEntityType($entity_type)) {
        $implements = class_implements($entity_type->getOriginalClass());
        if (in_array('Drupal\user\EntityOwnerInterface', $implements)) {
          $data[$entity_type->id()]['is_own'] = $this->createIsOwnAccessRule($entity_type);
        }
      }
    }
  }

  /**
   * Create field value access rule data definition.
   *
   * @param \Drupal\field\FieldConfigInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   The field value access rule data definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createFieldValueAccessRule(FieldConfigInterface $field_definition) {
    return [
      'label' => $this->t('@field', ['@field' => $field_definition->getLabel()]),
      'plugin_id' => $this->getFieldValuePluginId($field_definition),
      'field' => $field_definition->getName(),
      'widget' => 'entity_field',
      'selection_rule' => [
        'label' => $this->t('@field', ['@field' => $field_definition->getLabel()]),
        'plugin_id' => $this->getSelectionRulePluginId($field_definition),
      ],
    ];
  }

  /**
   * Get default plugin ids based on field type.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return string
   *   The default plugin id.
   */
  protected function getFieldValuePluginId(FieldDefinitionInterface $field_definition) {
    switch ($field_definition->getType()) {
      case 'integer':
      case 'float':
      case 'decimal':
      case 'timestamp':
        return 'entity_field_numeric';

      case 'changed':
      case 'created':
      case 'datetime':
        return 'entity_field_date';

      case 'list_string':
      case 'list_integer':
      case 'list_float':
        return 'entity_field_list';

      case 'string':
      case 'string_long':
      case 'text':
      case 'text_long':
      case 'text_with_summary':
      case 'email':
        return 'entity_field_string';

      case 'boolean':
        return 'entity_field_boolean';

      case 'image':
      case 'file':
      case 'link':
      case 'comment':
        return 'entity_field_empty';

      default:
        return 'entity_field_standard';
    }
  }

  /**
   * Create entity reference field value access rule.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   The entity reference access rule data definition.
   */
  protected function createEntityReferenceFieldValueAccessRule(FieldDefinitionInterface $field_definition) {
    return [
      'label' => $this->t('@field', ['@field' => $field_definition->getLabel()]),
      'plugin_id' => 'entity_field_entity_reference',
      'field' => $field_definition->getName(),
      'widget' => 'entity_field',
      'selection_rule' => [
        'label' => $this->t('@field', ['@field' => $field_definition->getLabel()]),
        'plugin_id' => $this->getSelectionRulePluginId($field_definition),
      ],
    ];
  }

  /**
   * Create the term reference with depth access rule.
   *
   * This access rule compares taxonomy terms while respecting the hierarchy.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   The term reference field access rule data definition.
   */
  protected function createTermReferenceWithDepthAccessRule(FieldDefinitionInterface $field_definition) {
    return [
      'label' => $this->t('@field (with depth)', ['@field' => $field_definition->getLabel()]),
      'plugin_id' => 'term_reference_depth',
      'field' => $field_definition->getName(),
      'operator' => 'in',
      'widget' => 'entity_field',
    ];
  }

  /**
   * Create numeric list field access rule.
   *
   * This access rule compares values in list_integer and list_float fields
   * numerically. This allows for features like mandatory access control.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   The numeric list field access rule data definition.
   */
  protected function createFieldListNumericFieldValueAccessRule(FieldDefinitionInterface $field_definition) {
    return [
      'label' => $this->t('@field (numeric)', ['@field' => $field_definition->getLabel()]),
      'plugin_id' => 'entity_field_list_numeric',
      'field' => $field_definition->getName(),
      'widget' => 'entity_field',
    ];
  }

  /**
   * Get the selection rule plugin id.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return string
   *   The default plugin id.
   */
  protected function getSelectionRulePluginId(FieldDefinitionInterface $field_definition) {
    switch ($field_definition->getType()) {
      case 'integer':
      case 'float':
      case 'decimal':
      case 'timestamp':
        return 'numeric';

      case 'changed':
      case 'created':
      case 'datetime':
        return 'date';

      case 'list_string':
      case 'list_integer':
      case 'list_float':
        return 'list';

      case 'string':
      case 'string_long':
      case 'text':
      case 'text_long':
      case 'text_with_summary':
      case 'email':
        return 'string';

      case 'boolean':
        return 'boolean';

      case 'image':
      case 'file':
      case 'link':
      case 'comment':
        return 'empty';

      case 'entity_reference':
        return 'entity_reference';

      default:
        return 'standard';
    }
  }

  /**
   * Create user reference access rule data definition.
   *
   * @param \Drupal\field\FieldConfigInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   The user reference access rule data definition.
   */
  protected function createUserRoleReferenceAccessRule(FieldConfigInterface $field_definition) {
    return [
      'label' => $this->t('Has reference to role on current user: @field', ['@field' => $field_definition->getLabel()]),
      'description' => $this->t('Grant access to users with a given role.'),
      'plugin_id' => 'user_role_reference',
      'field' => $field_definition->getName(),
      'widget' => 'entity_field',
    ];
  }

  /**
   * Create the is_own access rule data definition.
   *
   * @return array
   *   The access rule data definition.
   */
  protected function createIsOwnAccessRule(EntityTypeInterface $entity_type) {
    return [
      'label' => $this->t('Authored by current user'),
      'description' => $this->t('Grant access if the @entity_type is authored by the current user.', ['@entity_type' => $entity_type->getSingularLabel()]),
      'plugin_id' => 'is_own',
      'field' => 'uid',
      'selection_rule' => [
        'plugin_id' => 'is_own',
        'description' => $this->t('Assign this access policy if the @entity_type is authored by the current user.', ['@entity_type' => $entity_type->getSingularLabel()]),
      ],
    ];
  }

  /**
   * Get all field definitions.
   *
   * @return array
   *   Array of field definitions.
   */
  protected function getAllFieldDefinitions() {
    $fields = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if ($this->accessPolicyInfo->isAccessControlledEntityType($entity_type)) {
        $bundles = $this->bundleInfo->getBundleInfo($entity_type->id());
        $bundle_options = [];
        foreach ($bundles as $bundle_id => $bundle) {
          $bundle_options[] = $bundle_id;
        }
        $field_definitions = $this->getFieldDefinitions($entity_type, $bundle_options);
        if (!empty($field_definitions)) {
          $fields[$entity_type->id()] = $this->filterByApplicableFieldStorage($entity_type->id(), $field_definitions);
        }
      }
    }

    return $fields;
  }

  /**
   * Aggregate of field definitions for supported content entity types/bundles.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param array $bundles
   *   The array of bundles.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   Array of field definitions.
   */
  private function getFieldDefinitions(EntityTypeInterface $entity_type, array $bundles) {
    $field_definitions = [];
    foreach ($bundles as $bundle) {
      $field_definitions = array_merge($field_definitions, $this->entityFieldManager->getFieldDefinitions($entity_type->id(), $bundle));
    }

    return $field_definitions;
  }

  /**
   * Array of applicable entity reference fields found on an entity type.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $field_definitions
   *   Array of field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   Array of filtered field definitions.
   */
  private function filterByApplicableFieldStorage($entity_type_id, array $field_definitions) {
    $options = [];
    foreach ($this->entityFieldManager->getFieldStorageDefinitions($entity_type_id) as $field_name => $field_storage) {
      if ($field_storage instanceof FieldStorageConfigInterface
        && !$field_storage->isLocked()) {
        if (isset($field_definitions[$field_name])) {
          $options[$field_name] = $field_definitions[$field_name];
        }
      }
    }
    return $options;
  }

}
