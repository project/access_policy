<?php

namespace Drupal\access_policy;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class AccessPolicyOperationPluginManger.
 */
class AccessPolicyOperationPluginManager extends DefaultPluginManager {

  /**
   * AccessPolicyTypePluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/access_policy/AccessPolicyOperation', $namespaces, $module_handler, 'Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation\AccessPolicyOperationInterface', 'Drupal\access_policy\Annotation\AccessPolicyOperation');

    $this->alterInfo('access_policy_operation');
    $this->setCacheBackend($cache_backend, 'access_policy_operation');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    // Fetch and sort definitions by weight.
    $definitions = $this->getCachedDefinitions();
    if (empty($definitions)) {
      $definitions = $this->findDefinitions();
      uasort($definitions, '\Drupal\Component\Utility\SortArray::sortByWeightElement');

      $this->setCachedDefinitions($definitions);
    }

    return $definitions;
  }

  /**
   * Get the applicable access policy operation.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation\AccessPolicyOperationInterface[]
   *   The array of operation plugins.
   */
  public function getApplicable(EntityTypeInterface $entity_type) {
    $definitions = $this->getDefinitions();
    $applicable = [];
    foreach ($definitions as $id => $definition) {
      $class = $definition['class'];
      if ($class::isApplicable($entity_type)) {
        $applicable[$id] = $this->createInstance($definition['id']);
      }
    }
    return $applicable;
  }

  /**
   * Get the applicable access policy operation.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return array
   *   The array of plugin definitions.
   */
  public function getApplicableDefinitions(EntityTypeInterface $entity_type) {
    $definitions = $this->getDefinitions();

    $applicable = [];
    foreach ($definitions as $id => $definition) {
      $class = $definition['class'];
      if ($class::isApplicable($entity_type)) {
        $applicable[$id] = $definition;
      }
    }
    return $applicable;
  }

  /**
   * Get the applicable access policy operation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $op
   *   The operation.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyOperation\AccessPolicyOperationInterface|null
   *   The access policy operation plugin.
   */
  public function getCurrentOperation(EntityInterface $entity, $op) {
    $definitions = $this->getDefinitions();
    foreach ($definitions as $definition) {
      $class = $definition['class'];
      if ($class::isCurrent($entity, $op)) {
        return $this->createInstance($definition['id']);
      }
    }
  }

}
