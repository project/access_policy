<?php

namespace Drupal\access_policy;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Service for validating access policies on entities.
 */
interface AccessPolicyValidatorInterface {

  /**
   * Validate the current access policy on the entity (if any).
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The content entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param string $operation
   *   The operation plugin id.
   *
   * @return bool
   *   TRUE if they have access; FALSE otherwise.
   */
  public function validate(EntityInterface $entity, AccountInterface $account, string $operation);

  /**
   * Validate any access policy against an entity.
   *
   * This is useful if you want to validate an access policy against an entity
   * without actually assigning it.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The current access policy.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   * @param string $operation
   *   The operation plugin id.
   *
   * @return bool
   *   TRUE if they have access; FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validateAccessPolicy(AccessPolicyInterface $access_policy, EntityInterface $entity, AccountInterface $account, $operation);

  /**
   * Reset the validation static cache.
   */
  public function resetCache();

}
