<?php

namespace Drupal\access_policy_ui\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for access policy ui routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Change the title of the edit form.
    if ($collection->get('entity.access_policy.edit_form')) {
      $collection->get('entity.access_policy.edit_form')->setDefault('_title_callback', '\Drupal\access_policy_ui\Form\AccessPolicyEditForm::getTitle');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -210];
    return $events;
  }

}
