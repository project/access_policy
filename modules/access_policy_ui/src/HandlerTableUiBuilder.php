<?php

namespace Drupal\access_policy_ui;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Build the handler table.
 *
 * @internal
 *   This service is an internal part of the access policy access rule/selection
 *   rule table and does not provide any extension points.
 */
class HandlerTableUiBuilder {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The handler type.
   *
   * @var string
   */
  protected $handlerType;

  /**
   * Constructs a new handler table ui builder.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * Build the handler table for an access policy.
   *
   * @param string $handler_type
   *   The handler type.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The access policy.
   *
   * @return array
   *   The renderable table.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function build($handler_type, EntityInterface $entity) {
    $this->handlerType = $handler_type;

    $build['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'access-rules',
      ],
    ];
    $header = [
      'type' => $this->t('Items'),
      'operations' => $this->t('Operations'),
    ];
    $selected_rules_list = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('No handlers selected.'),
    ];
    $selected_handlers = $this->getHandlerManager()->getHandlersFromPolicy($entity);
    foreach ($selected_handlers as $id => $plugin) {
      $selected_rules_list[$id] = [
        '#attributes' => [
          'id' => 'rule--' . $id,
        ],
        'type' => [
          '#type' => 'markup',
          '#markup' => $plugin->adminLabel(),
        ],
        'operations' => [
          '#type' => 'operations',
          '#links' => $this->buildOperationsLinks($entity, $id),
        ],
      ];
    }

    $build['container']['table'] = $selected_rules_list;
    return $build;
  }

  /**
   * Build the operations links.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $accessPolicy
   *   The access policy entity.
   * @param string $id
   *   The access rule id.
   *
   * @return array
   *   Array of operations links.
   */
  protected function buildOperationsLinks(AccessPolicyInterface $accessPolicy, $id) {
    $operations['configure'] = [
      'title' => $this->t('Configure'),
      'url' => Url::fromRoute('access_policy_ui.access_policy_configure_rule_form', [
        'access_policy' => $accessPolicy->id(),
        'type' => $this->handlerType,
        'id' => $id,
      ]),
      'attributes' => self::getDialogOptions(),
    ];

    // If the plugin is required then don't allow it to be removed.
    // For some reason it's not enforcing route access on the operation link.
    // We have to do it ourselves.
    $delete_url = Url::fromRoute('access_policy_ui.access_policy_remove_rule_form', [
      'access_policy' => $accessPolicy->id(),
      'type' => $this->handlerType,
      'id' => $id,
    ]);
    if ($delete_url->access($this->currentUser)) {
      $operations['remove'] = [
        'title' => $this->t('Remove'),
        'url' => $delete_url,
        'attributes' => self::getDialogOptions(['width' => '880px']),
      ];
    }

    return $operations;
  }

  /**
   * Get the dialog options.
   *
   * @return array
   *   The modal attributes.
   */
  public static function getDialogOptions($options = ['width' => '75%']) {
    return [
      'class' => ['use-ajax', 'button', 'button--small', 'field-add-more-submit'],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode($options),
    ];
  }

  /**
   * Get the access policy handler manager.
   *
   * @return \Drupal\access_policy\AccessPolicyHandlerManager
   *   The access policy handler manager.
   */
  protected function getHandlerManager() {
    return \Drupal::service('plugin.manager.access_policy.' . $this->handlerType);
  }

}
