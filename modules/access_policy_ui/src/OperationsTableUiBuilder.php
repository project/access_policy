<?php

namespace Drupal\access_policy_ui;

use Drupal\access_policy\AccessPolicyOperationPluginManager;
use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\user\RoleInterface;

/**
 * Build the role operations table.
 *
 * @internal
 *   This service is an internal part of the access policy operations table
 *   does not provide any extension points.
 */
class OperationsTableUiBuilder {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The operation plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyOperationPluginManager
   */
  protected $operationPluginManager;

  /**
   * Constructs a new operations table ui builder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\AccessPolicyOperationPluginManager $operation_plugin_manager
   *   The operation plugin manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccessPolicyOperationPluginManager $operation_plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->operationPluginManager = $operation_plugin_manager;
  }

  /**
   * Build the role operations table for an access policy.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The access policy.
   *
   * @return array
   *   The renderable table.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function build(EntityInterface $entity) {
    $entity_type = $entity->getTargetEntityType();
    $build = [];
    $build['op_container'] = [
      '#type' => 'container',
      '#id' => 'role-operations',
    ];
    $build['op_container']['help'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('This table shows a summary of operation access accounting for permissions and access rules. ✔ means the role has access to all @entity_type with this policy. Limited means that the role has access but may be constrained by one or more access rules. Blank means the role has no access.', ['@entity_type' => $entity_type->getPluralLabel()]) . '</p>',
    ];
    $build['op_container']['manage_operations'] = [
      '#type' => 'link',
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#title' => $this->t('Manage operations'),
      '#url' => $entity->toUrl('operations-form'),
      '#attributes' => $this->getDialogOptions(),
    ];

    $table = [
      '#type' => 'table',
      '#header' => $this->buildHeader($entity),
      '#empty' => $this->t('No roles found.'),
      '#sticky' => TRUE,
    ];

    $roles = $this->getAllRoles();
    foreach ($roles as $role) {
      $table[$role->id()] = $this->buildRow($entity, $role);
    }

    $build['op_container']['table'] = $table;

    return $build;
  }

  /**
   * Build the table row.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\user\RoleInterface $role
   *   The role.
   *
   * @return array[]
   *   The operations row.
   */
  protected function buildRow(EntityInterface $entity, RoleInterface $role) {
    $definitions = $this->getDefinitions($entity);
    $row['type'] = [
      '#type' => 'markup',
      '#markup' => $role->label(),
    ];
    foreach ($definitions as $id => $definition) {
      $row[$definition['id']] = [
        '#type' => 'markup',
        '#markup' => $this->getOperationAccess($entity, $role, $id),
      ];
    }
    $row['operations'] = [
      '#type' => 'operations',
      '#links' => $this->buildRoleOperationsLink($entity, $role->id()),
    ];

    return $row;
  }

  /**
   * Build the table header.
   *
   * @return array[]
   *   The table header array.
   */
  protected function buildHeader(EntityInterface $entity) {
    $header['type'] = $this->t('Role');
    $definitions = $this->getDefinitions($entity);
    foreach ($definitions as $definition) {
      $header[$definition['id']] = $definition['label'];
    }
    $header['operations'] = $this->t('Operations');
    return $header;
  }

  /**
   * Get the definitions.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   Array of definitions.
   */
  protected function getDefinitions(EntityInterface $entity) {
    $definitions = $this->operationPluginManager->getApplicableDefinitions($entity->getTargetEntityType());
    return array_filter($definitions, function ($definition) use ($entity) {
      return $entity->getOperationsHandler()->showColumn($definition['id']);
    });
  }

  /**
   * Get the operations access string.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param \Drupal\user\RoleInterface $role
   *   The user role.
   * @param string $operation
   *   The operation string.
   *
   * @return string
   *   The access string.
   */
  protected function getOperationAccess(AccessPolicyInterface $access_policy, RoleInterface $role, $operation) {
    $operations_handler = $access_policy->getOperationsHandler();
    if ($operations_handler->shouldValidatePermission($operation)) {
      $perm = $this->operationPluginManager->createInstance($operation)->getPermission($access_policy, $access_policy->getTargetEntityType());
      // @todo add test for this, the anonymous user should get its own logic.
      if ($role->id() == 'anonymous' && !$role->hasPermission($perm)) {
        return '';
      }

      if (!$this->authenticatedRoleHasAccess($perm) && !$role->hasPermission($perm)) {
        return '';
      }
    }

    if ($operations_handler->shouldValidateAccessRules($operation)) {
      $has_bypass_perm = $role->hasPermission('bypass ' . $access_policy->id() . ' access rules');
      $rules = $access_policy->getHandlers('access_rule');
      if (!empty($rules) && !$has_bypass_perm) {
        foreach ($rules as $rule) {
          if ($this->isOperationEnabled($rule, $operation)) {
            return $this->t('Limited');
          }
        }
      }
    }

    return '✔';
  }

  /**
   * Determine whether the authenticated role has access.
   *
   * @param string $permission
   *   The permission.
   *
   * @return bool
   *   TRUE if the role has access; FALSE otherwise.
   */
  protected function authenticatedRoleHasAccess($permission) {
    $role = $this->entityTypeManager->getStorage('user_role')->load('authenticated');
    return $role->hasPermission($permission);
  }

  /**
   * Determine whether the operation is being excluded from the access rule.
   *
   * @param array $rule
   *   The access rule.
   * @param string $operation
   *   The operation.
   *
   * @return bool
   *   TRUE if the operation is disabled; FALSE otherwise.
   */
  protected function isOperationEnabled(array $rule, $operation) {
    $settings = $rule['settings'] ?? [];

    if (!isset($settings['operations'])) {
      return TRUE;
    }

    if (isset($settings['operations']) && in_array($operation, $settings['operations'])) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Get all the roles.
   *
   * @return array
   *   Array of roles keyed by role id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllRoles() {
    $role_options = [];
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    foreach ($roles as $role) {
      $role_options[$role->id()] = $role;
    }
    return $role_options;

  }

  /**
   * Build the role operations link.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The access policy.
   * @param string $id
   *   The role id.
   *
   * @return array
   *   The operations link.
   */
  public function buildRoleOperationsLink(EntityInterface $entity, $id) {
    $operations['configure'] = [
      'title' => $this->t('Edit permissions'),
      'url' => Url::fromRoute('entity.access_policy.permissions_form', [
        'access_policy' => $entity->id(),
        'user_role' => $id,
      ]),
      'attributes' => $this->getDialogOptions(),
    ];

    return $operations;
  }

  /**
   * Get the dialog options.
   *
   * @return array
   *   The modal attributes.
   */
  public static function getDialogOptions() {
    return [
      'class' => ['use-ajax'],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode([
        'width' => '75%',
      ]),
    ];
  }

}
