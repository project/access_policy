<?php

namespace Drupal\access_policy_ui;

use Drupal\access_policy\LabelHelper;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a trait for rendering arrays as labels.
 */
trait AccessPolicyTableTrait {

  /**
   * Get a list of selection sets.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param int $limit
   *   Set limit of how many labels to show.
   *
   * @return string
   *   The list of selection set labels.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSelectionSetLabels(EntityInterface $entity, $limit = NULL) {
    $selection_sets = $entity->getSelectionSet();
    $labels = [];
    foreach ($selection_sets as $name) {
      $set = $this->getSelectionManager()->getSelectionSet($entity->getTargetEntityTypeId(), $name);
      $labels[] = $set['label'];
    }

    return LabelHelper::render($labels, ['limit' => $limit]);
  }

  /**
   * Get the selection manager service.
   *
   * @return \Drupal\access_policy\AccessPolicySelectionInterface
   *   The access policy selection manager.
   */
  protected function getSelectionManager() {
    return \Drupal::service('access_policy.selection');
  }

}
