<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\access_policy\EntityTypeSettingsInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The form for adding selection sets.
 */
abstract class SelectionSetFormBase extends FormBase {

  use SelectionSetTableTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type settings.
   *
   * @var \Drupal\access_policy\EntityTypeSettingsInterface
   */
  protected $entityTypeSettings;

  /**
   * The target entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $targetEntityType;

  /**
   * Base class for selection set form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\EntityTypeSettingsInterface $entity_type_settings
   *   The entity type settings.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeSettingsInterface $entity_type_settings) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeSettings = $entity_type_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('access_policy.entity_type_settings'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $target_entity_type_id = NULL, $name = NULL) {
    $this->targetEntityType = $this->entityTypeManager->getDefinition($target_entity_type_id);

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
      '#submit' => ['::submitForm'],
      '#ajax' => [
        'callback' => [$this, 'ajaxSubmitCallback'],
        'disable-refocus' => TRUE,
      ],
    ];

    $form['actions']['submit']['#validate'][] = [
      $form_state->getFormObject(), 'validateForm',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'button',
      '#value' => $this->t('Cancel'),
      '#ajax' => [
        'callback' => [$this, 'ajaxCancelCallback'],
      ],
      '#validate' => [],
      '#limit_validation_errors' => [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Overridden in other methods.
  }

  /**
   * Ajax callback to close the modal and update the selected rules.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response object.
   */
  public function ajaxSubmitCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if (!$form_state::hasAnyErrors()) {
      $selection_set_table = $this->buildSelectionSetTable();
      $response->addCommand(new CloseDialogCommand());
      $response->addCommand(new HtmlCommand('#selection-sets', $selection_set_table));
    }
    // If there were errors then rebuild the form with the error messages.
    else {
      $response->addCommand(new ReplaceCommand('.selection-set-modal-form', $form));
    }
    return $response;
  }

  /**
   * Ajax callback to close the modal without changing any rules.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response object.
   */
  public function ajaxCancelCallback() {
    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand());
    return $response;
  }

  /**
   * Get the current entity type settings.
   */
  protected function getConfig() {
    return $this->entityTypeSettings->load($this->targetEntityType->id());
  }

}
