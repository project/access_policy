<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\access_policy\AccessPolicyInformation;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AccessPolicyCreateForm.
 *
 * The AccessPolicy create form.
 *
 * @package Drupal\access_policy\Form
 */
class AccessPolicyCreateForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $accessPolicyInfo;

  /**
   * Constructs a AccessPolicyCreateForm object.
   *
   * @param \Drupal\access_policy\AccessPolicyInformation $access_policy_info
   *   The access policy information service.
   */
  public function __construct(AccessPolicyInformation $access_policy_info) {
    $this->accessPolicyInfo = $access_policy_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('access_policy.information'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("The human-readable name of this access policy. This name must be unique."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\access_policy\Entity\AccessPolicy::load',
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $form['target_entity_type_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#default_value' => 'node',
      '#options' => $this->getContentEntityTypeOptions(),
      '#description' => $this->t('Choose the entity type that this policy is for.'),
    ];

    return $form;
  }

  /**
   * Get all the supported content entity types.
   *
   * @return array
   *   Array of content entity types options, keyed by entity type id.
   */
  private function getContentEntityTypeOptions() {
    $entity_types = $this->entityTypeManager->getDefinitions();
    $content_entity_types = array_filter($entity_types, function (EntityTypeInterface $entity_type) {
      return $this->accessPolicyInfo->isSupportedEntityType($entity_type);
    });
    $options = [];
    foreach ($content_entity_types as $entity_type) {
      $options[$entity_type->id()] = $entity_type->getLabel();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the @label Access Policy.', [
          '@label' => $entity->label(),
        ]));
        break;
    }
    $form_state->setRedirectUrl($entity->toUrl('edit-form'));
    return $status;
  }

}
