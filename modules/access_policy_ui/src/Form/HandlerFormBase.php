<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\access_policy\AccessPolicyHandlerManager;
use Drupal\access_policy_ui\HandlerTableUiBuilder;
use Drupal\access_policy_ui\OperationsTableUiBuilder;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base form for configuring access rules.
 */
abstract class HandlerFormBase extends FormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The class resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The access rules plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $accessRulePluginManager;

  /**
   * The operations table ui builder.
   *
   * @var \Drupal\access_policy_ui\OperationsTableUiBuilder
   */
  protected $operationsTableUiBuilder;

  /**
   * The handler table ui builder.
   *
   * @var \Drupal\access_policy_ui\HandlerTableUiBuilder
   */
  protected $handlerTableUiBuilder;

  /**
   * The access policy entity object.
   *
   * @var \Drupal\access_policy\Entity\AccessPolicyInterface
   */
  protected $accessPolicy;

  /**
   * The current handler type.
   *
   * @var string
   */
  protected $handlerType;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ClassResolverInterface $class_resolver, AccessPolicyHandlerManager $access_rule_manager, OperationsTableUiBuilder $ui_builder, HandlerTableUiBuilder $handler_table_builder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->classResolver = $class_resolver;
    $this->accessRulePluginManager = $access_rule_manager;
    $this->operationsTableUiBuilder = $ui_builder;
    $this->handlerTableUiBuilder = $handler_table_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('class_resolver'),
      $container->get('plugin.manager.access_policy.access_rule'),
      $container->get('access_policy.operations_table_ui_builder'),
      $container->get('access_policy.handler_table_ui_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
      '#submit' => ['::submitForm'],
      '#ajax' => [
        'callback' => [$this, 'ajaxSubmitCallback'],
        'disable-refocus' => TRUE,
      ],
    ];

    $form['actions']['submit']['#validate'][] = [
      $form_state->getFormObject(), 'validateForm',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'button',
      '#value' => $this->t('Cancel'),
      '#ajax' => [
        'callback' => [$this, 'ajaxCancelCallback'],
      ],
      '#validate' => [],
      '#limit_validation_errors' => [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Overridden in other methods.
  }

  /**
   * Ajax callback to close the modal and update the selected rules.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response object.
   */
  public function ajaxSubmitCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if (!$form_state::hasAnyErrors()) {
      // Make sure that the access policy is up-to-date before rebuilding.
      $access_policy = $this->entityTypeManager->getStorage('access_policy')->load($this->accessPolicy->id());
      $selected_rules_list = $this->handlerTableUiBuilder->build($this->handlerType, $access_policy);
      $response->addCommand(new CloseDialogCommand());
      $response->addCommand(new HtmlCommand('#access-rules', $selected_rules_list));

      // If this is an access rule then also rebuild the operations table.
      if ($this->handlerType == 'access_rule') {
        $role_operations = $this->operationsTableUiBuilder->build($access_policy);
        $response->addCommand(new HtmlCommand('#role-operations', $role_operations));
      }
    }
    // If there were errors then rebuild the form with the error messages.
    else {
      $response->addCommand(new ReplaceCommand('.handler-modal-form', $form));
    }
    return $response;
  }

  /**
   * Ajax callback to close the modal without changing any rules.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response object.
   */
  public function ajaxCancelCallback() {
    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand());
    return $response;
  }

  /**
   * Get the access policy handler manager.
   *
   * @return \Drupal\access_policy\AccessPolicyHandlerManager
   *   The access policy handler manager.
   */
  protected function getHandlerManager() {
    return \Drupal::service('plugin.manager.access_policy.' . $this->handlerType);
  }

}
