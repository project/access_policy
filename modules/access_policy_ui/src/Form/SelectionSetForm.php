<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * The form for adding selection sets.
 */
class SelectionSetForm extends SelectionSetFormBase {

  use SelectionSetTableTrait;

  /**
   * The target entity type.
   *
   * @var string
   */
  protected $targetEntityType;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'selection_set_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $target_entity_type_id = NULL, $name = NULL) {
    $this->targetEntityType = $this->entityTypeManager->getDefinition($target_entity_type_id);

    $selection_set = $this->getSelectionSet($name);

    // Show any validation errors in the modal.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    // This is necessary for modal validation.
    $form['#attributes']['class'][] = 'selection-set-modal-form';

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $selection_set['label'] ?? '',
      '#description' => $this->t("The human-readable name of this selection set. This name must be unique."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $selection_set['id'] ?? '',
      '#machine_name' => [
        'exists' => [$this, 'exists'],
      ],
      '#disabled' => (!empty($selection_set['id'])) ?? FALSE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
      '#submit' => ['::submitForm'],
      '#ajax' => [
        'callback' => [$this, 'ajaxSubmitCallback'],
        'disable-refocus' => TRUE,
      ],
    ];

    $form['actions']['submit']['#validate'][] = [
      $form_state->getFormObject(), 'validateForm',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'button',
      '#value' => $this->t('Cancel'),
      '#ajax' => [
        'callback' => [$this, 'ajaxCancelCallback'],
      ],
      '#validate' => [],
      '#limit_validation_errors' => [],
    ];

    return $form;
  }

  /**
   * Determines if the selection set already exists.
   *
   * @param string $name
   *   The selection set name.
   *
   * @return bool
   *   TRUE if the selection set exists, FALSE otherwise.
   */
  public function exists($name) {
    return (!empty($this->getSelectionSet($name)));
  }

  /**
   * Get the current selection set.
   *
   * @param string $name
   *   The selection set machine name.
   *
   * @return array|false
   *   The selection set array or FALSE if not found.
   */
  public function getSelectionSet($name) {
    $selection_sets = $this->getConfig()->get('selection_sets');
    if (isset($selection_sets[$name])) {
      return $selection_sets[$name];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selection_sets = $this->getConfig()->get('selection_sets');
    $id = $form_state->getValue('id');

    $set = [
      'id' => $form_state->getValue('id'),
      'label' => $form_state->getValue('label'),
    ];
    $selection_sets[$id] = $set;

    $this->getConfig()->set('selection_sets', $selection_sets);
    $this->getConfig()->save();
  }

}
