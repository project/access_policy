<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;

/**
 * The form for deleting handlers associated with an access policy.
 */
class HandlerRemoveForm extends HandlerFormBase {

  /**
   * The handler id.
   *
   * @var string
   */
  protected $id;

  /**
   * The handler type.
   *
   * @var string
   */
  protected $handlerType;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'access_policy_remove_rule_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, AccessPolicyInterface|null $access_policy = NULL, $type = NULL, $id = NULL) {
    $this->accessPolicy = $access_policy;
    $this->handlerType = $type;
    $this->id = $id;

    $form = parent::buildForm($form, $form_state);

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Are you sure you want to remove this?'),
    ];

    $form['actions']['submit']['#value'] = $this->t('Remove');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->accessPolicy->removeHandler($this->handlerType, $this->id);
    $this->accessPolicy->save();
  }

  /**
   * Checks that the rule is not required and can be deleted.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface|null $access_policy
   *   The access policy entity.
   * @param string $type
   *   The handler type.
   * @param string $id
   *   The handler unique id.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccessPolicyInterface|null $access_policy = NULL, $type = NULL, $id = NULL) {
    $plugins = $access_policy->getHandlers($type);
    if (isset($plugins[$id])) {
      $plugin = $plugins[$id];
    }

    if (!empty($plugin['required'])) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

  /**
   * Get the form title.
   *
   * @param string $access_policy
   *   The access policy id.
   * @param string $type
   *   The handler type.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  public function getTitle($access_policy = NULL, $type = NULL) {
    switch ($type) {
      case 'access_rule':
        return $this->t('Remove access rule');

      case 'selection_rule':
        return $this->t('Remove selection rule');

      default:
        return $this->t('Remove handler');
    }
  }

}
