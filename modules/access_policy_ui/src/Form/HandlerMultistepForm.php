<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * The form for deleting rules associated with an access policy.
 */
class HandlerMultistepForm extends HandlerFormBase {

  /**
   * The access policy entity object.
   *
   * @var \Drupal\access_policy\Entity\AccessPolicyInterface
   */
  protected $accessPolicy;

  /**
   * The handler rule type (e.g access or selection rule).
   *
   * @var string
   */
  protected $handlerType;

  /**
   * The current access rule group.
   *
   * @var string
   */
  protected $group;

  /**
   * The current access rule id.
   *
   * @var string
   */
  protected $id;

  /**
   * The current handler.
   *
   * @var \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface
   */
  protected $handler;

  /**
   * The current step in the access rule form.
   *
   * @var int
   */
  protected $currentStep = 1;

  /**
   * The current stored form values.
   *
   * @var array
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'access_rule_multistep_form';
  }

  /**
   * Define the steps of the multi-step form.
   *
   * @return array
   *   Mapping of steps and corresponding method.
   */
  protected function getSteps() {
    return [
      1 => 'addAccessRuleForm',
      2 => 'configureAccessRuleForm',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, AccessPolicyInterface|null $access_policy = NULL, $type = NULL) {
    $form = parent::buildForm($form, $form_state);

    $this->accessPolicy = $access_policy;
    $this->handlerType = $type;

    $method = $this->getFormBuilderAtStep($this->currentStep);
    $this->$method($form, $form_state);

    $form['#prefix'] = '<div id="access-rule-multistep">';
    $form['#suffix'] = '</div>';

    return $form;
  }

  /**
   * Get the form at a particular step.
   *
   * @param int $step
   *   The current step.
   */
  public function getFormBuilderAtStep($step) {
    $steps = $this->getSteps();
    return $steps[$step];
  }

  /**
   * Build the Add access rule form.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function addAccessRuleForm(array &$form, FormStateInterface $form_state) {
    $form = $this->classResolver->getInstanceFromDefinition(HandlerAddForm::class)
      ->buildForm($form, $form_state, $this->accessPolicy, $this->handlerType);

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Add and configure'),
      // Only validate access rules.
      '#limit_validation_errors' => [['access_rules']],
      '#submit' => ['::nextStepSubmit'],
      '#ajax' => [
        'callback' => [$this, 'ajaxNextStepCallback'],
        'wrapper' => 'access-rule-multistep',
        'disable-refocus' => TRUE,
      ],
    ];
  }

  /**
   * Add rule submission handler.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function nextStepSubmit(array &$form, FormStateInterface $form_state) {
    // Preserve the submitted values.
    $this->storeSubmittedValues($form, $form_state);

    if ($this->currentStep == 1) {
      [$this->group, $this->id] = $this->parseSelectedRuleId();
    }

    $this->currentStep++;

    // Rebuild the form and load the next form.
    $form_state->setRebuild();
  }

  /**
   * Store submitted form values.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state values.
   */
  public function storeSubmittedValues(array $form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      $this->storage[$key] = $value;
    }
  }

  /**
   * Next step ajax callback.
   */
  public function ajaxNextStepCallback($form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Build the configuration form.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function configureAccessRuleForm(array &$form, FormStateInterface $form_state) {
    $rule = $this->getGroup() . ':' . $this->getId();
    $form = $this->classResolver->getInstanceFromDefinition(HandlerEditForm::class)
      ->buildForm($form, $form_state, $this->accessPolicy, $rule, $this->handlerType, TRUE);

    // Override the submission and validation handlers because we're adding the
    // rule for the first time.
    $form['actions']['submit']['#validate'][] = [
      $form_state->getFormObject(), 'addHandlerValidate',
    ];
    $form['actions']['submit']['#submit'] = ['::addHandlerSubmit'];
  }

  /**
   * Validate the new access rule plugin settings.
   */
  public function addHandlerValidate(array &$form, FormStateInterface $form_state) {
    $this->storeSubmittedValues($form, $form_state);

    $plugin = $this->getHandler();
    $plugin->validateSettingsForm($form, $form_state);
  }

  /**
   * Save the new access rule.
   */
  public function addHandlerSubmit($form, FormStateInterface $form_state) {
    $plugin = $this->getHandler();
    $plugin->submitSettingsForm($form, $form_state);
    $this->getAccessPolicy()->addHandler($this->handlerType, $plugin);
    $this->getAccessPolicy()->save();
  }

  /**
   * Parse the current selected rule id.
   */
  protected function parseSelectedRuleId() {
    $access_rules = array_filter($this->storage['access_rules']);
    if (!empty($access_rules)) {
      $rule = array_pop($access_rules);

      // @todo Set it up for multiple rules in the future.
      return explode(':', $rule);
    }
  }

  /**
   * Get the current group.
   *
   * @return string
   *   The group associated with this access rule.
   */
  public function getGroup() {
    return $this->group;
  }

  /**
   * Get the access rule id.
   *
   * @return string
   *   The current unique access rule id.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Get the current access policy.
   *
   * @return \Drupal\access_policy\Entity\AccessPolicyInterface
   *   The access policy associated with this access rule.
   */
  public function getAccessPolicy() {
    return $this->accessPolicy;
  }

  /**
   * Get the current handler plugin.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface
   *   The handler plugin.
   */
  public function getHandler() {
    // If the access rule is new instantiate a new plugin.
    if (empty($this->handler)) {
      $this->handler = $this->getHandlerManager()->getHandler($this->getGroup(), $this->getId(), ['access_policy' => $this->accessPolicy->id()]);
    }

    return $this->handler;
  }

  /**
   * Get the form title.
   *
   * @param string $access_policy
   *   The access policy id.
   * @param string $type
   *   The handler type.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  public function getTitle($access_policy = NULL, $type = NULL) {
    switch ($type) {
      case 'access_rule':
        return $this->t('Add access rule');

      case 'selection_rule':
        return $this->t('Add selection rule');

      default:
        return $this->t('Add handler');
    }
  }

}
