<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\access_policy\Entity\AccessPolicyInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * The form for configuring access rules on an access policy.
 */
class HandlerEditForm extends HandlerFormBase {

  /**
   * The Access Rule id.
   *
   * @var string
   */
  protected $id;

  /**
   * Flag to determine whether we're configuring a new access rule.
   *
   * @var bool
   */
  protected $isNew;

  /**
   * The access policy entity object.
   *
   * @var \Drupal\access_policy\Entity\AccessPolicyInterface
   */
  protected $accessPolicy;

  /**
   * The access policy handler type.
   *
   * @var string
   */
  protected $handlerType;

  /**
   * The current handler.
   *
   * @var \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface
   */
  protected $handler;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'access_policy_configure_rule_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, AccessPolicyInterface|null $access_policy = NULL, $id = NULL, $type = NULL, $is_new = NULL) {
    $this->accessPolicy = $access_policy;
    $this->id = $id;
    $this->handlerType = $type;
    $this->isNew = $is_new;

    // We need to ensure that the plugin id is preserved during validation.
    // This is passed up to the add rule form when we're configuring an access
    // rule for the first time.
    $form['id'] = [
      '#type' => 'hidden',
      '#value' => $id,
    ];

    // Show any validation errors in the modal.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    // This is necessary for modal validation.
    $form['#attributes']['class'][] = 'handler-modal-form';

    $plugin_object = $this->getHandler();
    $form += $plugin_object->buildSettingsForm($form, $form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_obj = $form_state->getFormObject();
    $plugin = $this->getHandler();
    $plugin->submitSettingsForm($form, $form_state);

    $this->accessPolicy->updateHandler($this->handlerType, $form_obj->id, $plugin->getSettings());
    $this->accessPolicy->save();
  }

  /**
   * Validate the access rule plugin.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $plugin = $this->getHandler();
    $plugin->validateSettingsForm($form, $form_state);
  }

  /**
   * Get the current access rule plugin.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\AccessPolicyHandlerInterface
   *   The access policy handler plugin.
   */
  protected function getHandler() {
    // If the access rule is new instantiate a new plugin.
    if (empty($this->handler)) {
      if ($this->isNew) {
        [$group, $name] = explode(':', $this->id);
        $this->handler = $this->getHandlerManager()->getHandler($group, $name, ['access_policy' => $this->accessPolicy->id()]);
      }
      // Otherwise use the existing access rule.
      else {
        $this->handler = $this->getHandlerManager()->getHandlerFromPolicy($this->accessPolicy, $this->id);
      }
    }

    return $this->handler;
  }

  /**
   * Route title callback.
   *
   * @param \Drupal\access_policy\Entity\AccessPolicyInterface $access_policy
   *   The access policy.
   * @param string $type
   *   The handler type.
   * @param string $id
   *   The access rule plugin id.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The route title.
   */
  public function getTitle(AccessPolicyInterface $access_policy, $type, $id) {
    $this->handlerType = $type;
    $plugin = $this->getHandlerManager()->getHandlerFromPolicy($access_policy, $id);
    return $this->t('Configure: @label', ['@label' => $plugin->getDefinition()->getLabel()]);
  }

}
