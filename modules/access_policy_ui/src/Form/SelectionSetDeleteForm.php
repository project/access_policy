<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * The form for deleting selection sets.
 */
class SelectionSetDeleteForm extends SelectionSetFormBase {

  /**
   * The selection set name.
   *
   * @var string
   */
  protected $name;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'selection_set_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $target_entity_type_id = NULL, $name = NULL) {
    $this->name = $name;

    $form = parent::buildForm($form, $form_state, $target_entity_type_id, $name);

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Are you sure you want to delete this selection set?'),
    ];

    $form['actions']['submit']['#value'] = $this->t('Delete');

    if ($this->isUsed($name)) {
      $form['message'] = [
        '#type' => 'markup',
        '#markup' => $this->t('This selection set is being used and cannot be deleted.'),
      ];
      unset($form['actions']['submit']);
    }

    return $form;
  }

  /**
   * Determine whether the selection set is being used.
   *
   * @param string $name
   *   The selection set machine name.
   *
   * @return bool
   *   TRUE if the selection set is being used; FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function isUsed($name) {
    $access_policies = $this->entityTypeManager->getStorage('access_policy')->loadByProperties([
      'target_entity_type_id' => $this->targetEntityType->id(),
    ]);
    foreach ($access_policies as $policy) {
      if (in_array($name, $policy->getSelectionSet())) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selection_sets = $this->getConfig()->get('selection_sets');
    if (isset($selection_sets[$this->name])) {
      unset($selection_sets[$this->name]);
    }

    $this->getConfig()->set('selection_sets', $selection_sets);
    $this->getConfig()->save();
  }

}
