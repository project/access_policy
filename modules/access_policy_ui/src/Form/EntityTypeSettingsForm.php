<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\access_policy\EntityTypeSettingsInterface;
use Drupal\access_policy\SelectionStrategyPluginManager;
use Drupal\access_policy_ui\AccessPolicyTableTrait;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for managing entity type specific settings.
 */
class EntityTypeSettingsForm extends DraggableListBuilder implements FormInterface, ContainerInjectionInterface {

  use AccessPolicyTableTrait;
  use SelectionSetTableTrait;

  /**
   * The target entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $targetEntityType;

  /**
   * The route builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The access policy settings service.
   *
   * @var \Drupal\access_policy\EntityTypeSettingsInterface
   */
  protected $entityTypeSettings;

  /**
   * The selection strategy plugin manager.
   *
   * @var \Drupal\access_policy\SelectionStrategyPluginManager
   */
  protected $selectionStrategyManager;

  /**
   * The current selection strategy plugin.
   *
   * @var \Drupal\access_policy\Plugin\access_policy\SelectionStrategy\SelectionStrategyInterface
   */
  protected $selectionStrategy;

  /**
   * Constructs a new EntityTypeSettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route builder.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\EntityTypeSettingsInterface $settings
   *   The access policy settings.
   * @param \Drupal\access_policy\SelectionStrategyPluginManager $selection_strategy_manager
   *   The selection strategy plugin manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, RouteBuilderInterface $route_builder, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, EntityTypeSettingsInterface $settings, SelectionStrategyPluginManager $selection_strategy_manager) {
    parent::__construct($entity_type, $storage);
    $this->routeBuilder = $route_builder;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeSettings = $settings;
    $this->selectionStrategyManager = $selection_strategy_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getDefinition('access_policy'),
      $container->get('entity_type.manager')->getStorage('access_policy'),
      $container->get('router.builder'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('access_policy.entity_type_settings'),
      $container->get('plugin.manager.selection_strategy'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'access_policy_sort_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $target_entity_type_id = NULL) {
    $this->targetEntityType = $this->entityTypeManager->getDefinition($target_entity_type_id);

    $current_strategy = $this->getCurrentStrategy($form, $form_state);

    $form['selection_strategy'] = [
      '#type' => 'details',
      '#title' => $this->t('Selection mode'),
      '#open' => TRUE,
    ];

    $strategies = $this->selectionStrategyManager->getApplicableDefinitions($this->targetEntityType);
    foreach ($strategies as $definition) {
      $form['selection_strategy'][$definition['id']] = [
        '#title' => $definition['label'],
        '#parents' => ['selection_strategy'],
        '#type' => 'radio',
        '#return_value' => $definition['id'],
        '#default_value' => $current_strategy->getPluginId(),
        '#description' => $definition['description'],
        '#ajax' => [
          'callback' => '::loadStrategySettings',
          'wrapper' => 'selection-strategy-settings',
          'event' => 'change',
          'progress' => ['type' => 'none'],
        ],
      ];
    }

    $form['strategy_settings_container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="selection-strategy-settings">',
      '#suffix' => '</div>',
    ];

    if (!empty($current_strategy->defaultSettings())) {

      $form['strategy_settings_container']['strategy_settings'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Selection mode settings'),
        '#tree' => TRUE,
      ];

      $form['strategy_settings_container']['strategy_settings'] += $current_strategy->buildSettingsForm($form['strategy_settings_container']['strategy_settings'], $form_state);
    }

    $form['sort_policies'] = [
      '#type' => 'details',
      '#title' => $this->t('Sort access policies'),
      '#open' => TRUE,
    ];

    $form['sort_policies']['rearrange'] = [
      '#type' => 'container',
    ];

    $this->buildSortTable($form, $form_state);

    $form['selection_sets'] = [
      '#type' => 'details',
      '#title' => $this->t('Selection sets'),
      '#open' => TRUE,
    ];

    $form['selection_sets']['items'] = $this->buildSelectionSetTable();

    $form['selection_sets']['add_set'] = [
      '#type' => 'link',
      '#title' => $this->t('Add selection set'),
      '#url' => Url::fromRoute('access_policy_ui.selection_set_add_form', ['target_entity_type_id' => $this->targetEntityType->id()]),
      '#attributes' => $this->getModalAttributes(),
      // This addresses a core bug introduced in 10.1 that causes buttons in
      // modals to render incorrectly.
      // @see https://www.drupal.org/project/drupal/issues/3372594
      '#attached' => [
        'library' => ['core/drupal.dialog'],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['button']],
      '#url' => Url::fromRoute("entity.access_policy.collection"),
      '#cache' => [
        'contexts' => [
          'url.query_args:destination',
        ],
      ],
    ];

    return $form;
  }

  /**
   * Get the current selection strategy.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\access_policy\Plugin\access_policy\SelectionStrategy\SelectionStrategyInterface
   *   The selection strategy plugin.
   */
  protected function getCurrentStrategy(array $form, FormStateInterface $form_state) {
    if ($form_state->getValue('selection_strategy')) {
      $strategy = $form_state->getValue('selection_strategy');
    }
    else {
      $strategy = $this->getSetting('selection_strategy');
    }

    $settings = $this->getSetting('selection_strategy_settings') ?? [];
    $this->selectionStrategy = $this->selectionStrategyManager->createInstance($strategy, $settings);
    $this->selectionStrategy->setEntityType($this->targetEntityType);
    return $this->selectionStrategy;
  }

  /**
   * Ajax callback for updating Access Rules.
   *
   * @param array &$form
   *   The built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The updated content.
   */
  public static function loadStrategySettings(array &$form, FormStateInterface $form_state) {
    return $form['strategy_settings_container'];
  }

  /**
   * Build the policy sort table.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function buildSortTable(array &$form, FormStateInterface $form_state) {
    $label = $this->targetEntityType->getPluralLabel();
    $form['sort_policies']['rearrange'][$this->entitiesKey] = [
      '#type' => 'table',
      '#prefix' => $this->t('Access policies are assigned to @label_plural in the order defined here. The first policy available to a user will be assigned on create or update.', ['@label_plural' => $label]),
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'weight',
        ],
      ],
    ];

    $this->entities = $this->load();
    $delta = 10;
    // Change the delta of the weight field if have more than 20 entities.
    if (!empty($this->weightKey)) {
      $count = count($this->entities);
      if ($count > 20) {
        $delta = ceil($count / 2);
      }
    }
    foreach ($this->entities as $entity) {
      $row = $this->buildRow($entity);
      if (isset($row['label'])) {
        $row['label'] = ['#plain_text' => $row['label']];
      }
      if (isset($row['weight'])) {
        $row['weight']['#delta'] = $delta;
      }
      $form['sort_policies']['rearrange'][$this->entitiesKey][$entity->id()] = $row;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Rebuild the routes if the selection strategy has changed.
    if ($this->hasSelectionStrategyChanged($form, $form_state)) {
      $this->routeBuilder->setRebuildNeeded();
    }

    $this->selectionStrategy->submitSettingsForm($form, $form_state);
    $settings = $this->selectionStrategy->getSettings();
    $this->setSetting('selection_strategy', $form_state->getValue('selection_strategy'));
    $this->setSetting('selection_strategy_settings', $settings);
    $this->saveSettings();

    $form_state->setRedirect('entity.access_policy.collection');
  }

  /**
   * Determine whether the selection strategy has changed.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if the selection strategy has changed; FALSE otherwise.
   */
  public function hasSelectionStrategyChanged($form, FormStateInterface $form_state) {
    $current = $this->getSetting('selection_strategy');
    $new_strategy = $form_state->getValue('selection_strategy');
    if ($current != $new_strategy) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->selectionStrategy->validateSettingsForm($form, $form_state);
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->condition('target_entity_type_id', $this->targetEntityType->id())
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['label'] = $this->t('Name');
    $header['selection_set'] = $this->t('Selection set');
    if (!empty($this->weightKey)) {
      $header['weight'] = $this->t('Weight');
    }
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['label'] = $entity->label();
    $row['selection_set'] = ['#markup' => $this->getSelectionSetLabels($entity)];
    if (!empty($this->weightKey)) {
      // Override default values to markup elements.
      $row['#attributes']['class'][] = 'draggable';
      $row['#weight'] = $entity->get($this->weightKey);
      // Add weight column.
      $row['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $entity->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $entity->get($this->weightKey),
        '#attributes' => ['class' => ['weight']],
      ];
    }
    return $row;
  }

  /**
   * Get the form title.
   *
   * @param string $target_entity_type_id
   *   The entity type id.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  public function getTitle($target_entity_type_id = NULL) {
    $this->targetEntityType = $this->entityTypeManager->getDefinition($target_entity_type_id);
    return $this->t('@entity_type settings', ['@entity_type' => $this->targetEntityType->getLabel()]);
  }

  /**
   * Get a setting value.
   *
   * @param string $key
   *   The setting key.
   *
   * @return mixed
   *   The settings value.
   */
  protected function getSetting($key) {
    return $this->getConfig()->get($key);
  }

  /**
   * Set a setting value.
   *
   * @param string $key
   *   The setting key.
   * @param mixed $value
   *   The setting value.
   */
  protected function setSetting($key, $value) {
    $this->getConfig()->set($key, $value);
  }

  /**
   * Save the settings.
   */
  protected function saveSettings() {
    $this->getConfig()->save();
  }

  /**
   * Get the current entity type settings.
   *
   * @return \Drupal\access_policy\EntityTypeSettingsInterface
   *   The entity type settings object.
   */
  protected function getConfig() {
    return $this->entityTypeSettings->load($this->targetEntityType->id());
  }

}
