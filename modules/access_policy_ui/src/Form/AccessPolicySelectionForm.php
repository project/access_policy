<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\access_policy\AccessPolicyHandlerManager;
use Drupal\access_policy\AccessPolicyInformation;
use Drupal\access_policy\EntityTypeSettingsInterface;
use Drupal\access_policy\Http403ResponsePluginManager;
use Drupal\access_policy_ui\HandlerTableUiBuilder;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AccessPolicySelectionForm.
 *
 * The Access policy selection form.
 *
 * @package Drupal\access_policy\Form
 */
class AccessPolicySelectionForm extends EntityForm {

  /**
   * The selection rule plugin manager.
   *
   * @var \Drupal\access_policy\AccessPolicyHandlerManager
   */
  protected $selectionRuleManager;

  /**
   * The access denied behavior manager.
   *
   * @var \Drupal\access_policy\Http403ResponsePluginManager
   */
  protected $http403ResponsePluginManager;

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $accessPolicyInformation;

  /**
   * The access policy entity type settings.
   *
   * @var \Drupal\access_policy\EntityTypeSettingsInterface
   */
  protected $entityTypeSettings;

  /**
   * The handler table ui builder.
   *
   * @var \Drupal\access_policy_ui\HandlerTableUiBuilder
   */
  protected $handlerTableUiBuilder;

  /**
   * Constructs a AccessPolicySelectionForm object.
   *
   * @param \Drupal\access_policy\AccessPolicyHandlerManager $selection_rule_manager
   *   The selection rule plugin manager.
   * @param \Drupal\access_policy\Http403ResponsePluginManager $http_403_manager
   *   The access denied behavior manager.
   * @param \Drupal\access_policy\AccessPolicyInformation $access_policy_information
   *   The access policy information service.
   * @param \Drupal\access_policy\EntityTypeSettingsInterface $settings
   *   The access policy entity type settings.
   * @param \Drupal\access_policy_ui\HandlerTableUiBuilder $handler_table_builder
   *   The handler type ui builder.
   */
  public function __construct(AccessPolicyHandlerManager $selection_rule_manager, Http403ResponsePluginManager $http_403_manager, AccessPolicyInformation $access_policy_information, EntityTypeSettingsInterface $settings, HandlerTableUiBuilder $handler_table_builder) {
    $this->selectionRuleManager = $selection_rule_manager;
    $this->http403ResponsePluginManager = $http_403_manager;
    $this->accessPolicyInformation = $access_policy_information;
    $this->entityTypeSettings = $settings;
    $this->handlerTableUiBuilder = $handler_table_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.access_policy.selection_rule'),
      $container->get('plugin.manager.http_403_response'),
      $container->get('access_policy.information'),
      $container->get('access_policy.entity_type_settings'),
      $container->get('access_policy.handler_table_ui_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $entity = $this->entity;

    $form['selection_rule_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Selection rules'),
    ];

    $form['selection_rule_container']['help'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('Selection rules allow you to define which policies are available based on different attributes. For example: original author, bundle, field value, etc.') . '</p>',
    ];
    $form['selection_rule_container']['access_rules_table'] = $this->handlerTableUiBuilder->build('selection_rule', $entity);

    $form['selection_rule_container']['add_rule'] = [
      '#type' => 'link',
      '#title' => $this->t('Add selection rule'),
      '#url' => Url::fromRoute('access_policy_ui.access_policy_add_rule_form', [
        'access_policy' => $entity->id(),
        'type' => 'selection_rule',
      ]),
      '#attributes' => HandlerTableUiBuilder::getDialogOptions(),
      // This addresses a core bug introduced in 10.1 that causes buttons in
      // modals to render incorrectly.
      // @see https://www.drupal.org/project/drupal/issues/3372594
      '#attached' => [
        'library' => ['core/drupal.dialog'],
      ],
    ];

    $form['selection_rule_container']['selection_rule_operator'] = [
      '#type' => 'select',
      '#title' => $this->t('Validation operator'),
      '#description' => $this->t('How many rules must be true in order to assign this policy?'),
      '#default_value' => $entity->getSelectionRuleOperator(),
      '#options' => [
        'AND' => 'All',
        'OR' => 'Any',
      ],
    ];

    $form['selection_set_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Multiple policy support'),
    ];
    $form['selection_set_container']['help'] = [
      '#markup' => '<p>' . $this->t('By default, only one access policy can be assigned at a time. To use this policy with others, please add it to a selection set.') . '</p>',
    ];
    if (empty($this->getSelectionSetOptions())) {
      $form['selection_set_container']['selection_set'] = [
        '#markup' => "<p>" . $this->t('No selection sets available. Add selection sets from <a href = ":link">:entity_type settings</a>.', [
          ':entity_type' => $entity->getTargetEntityType()->getLabel(),
          ':link' => Url::fromRoute('access_policy_ui.entity_type_settings_form', ['target_entity_type_id' => $entity->getTargetEntityTypeId()])->toString(),
        ]) . "</p>",
      ];
    }
    else {
      $form['selection_set_container']['selection_set'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Selection set'),
        '#options' => $this->getSelectionSetOptions(),
        '#default_value' => $entity->getSelectionSet(),
      ];
    }

    return $form;
  }

  /**
   * Get the array of available selection sets.
   *
   * @return array
   *   Array of selection sets.
   */
  public function getSelectionSetOptions() {
    $settings = $this->entityTypeSettings->load($this->entity->getTargetEntityTypeId());
    $selection_sets = $settings->get('selection_sets') ?? [];
    $options = [];
    foreach ($selection_sets as $set) {
      $options[$set['id']] = $set['label'];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $values = $form_state->getValues();

    $selection_set = [];
    if (!empty($values['selection_set'])) {
      $selection_set = array_keys(array_filter($values['selection_set']));
    }
    $entity->setSelectionSet($selection_set);
    $status = $entity->save();

    $this->messenger()->addMessage($this->t('Saved the %label Access Policy.', [
      '%label' => $entity->label(),
    ]));

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    if (isset($actions['delete'])) {
      unset($actions['delete']);
    }

    return $actions;
  }

  /**
   * Get the modal attributes.
   *
   * @return array
   *   The modal attributes.
   */
  protected function getModalAttributes() {
    return [
      'class' => ['use-ajax', 'button button--small', 'field-add-more-submit'],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode([
        'width' => '75%',
      ]),
    ];
  }

}
