<?php

namespace Drupal\access_policy_ui\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;

/**
 * Provides a trait for handling the selection set table.
 */
trait SelectionSetTableTrait {

  /**
   * Build the full selection set table.
   *
   * @return array
   *   The fully formed table of selection sets.
   */
  public function buildSelectionSetTable() {
    $selection_sets = $this->getConfig()->get('selection_sets') ?? [];

    $build['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'selection-sets',
      ],
    ];
    $build['container']['help'] = [
      '#markup' => '<p>' . $this->t('Selection sets allow you to assign multiple access policies to an entity.') . '</p>',
    ];
    $header = [
      'type' => $this->t('Items'),
      'operations' => $this->t('Operations'),
    ];
    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('No selection sets available.'),
    ];
    foreach ($selection_sets as $id => $plugin) {
      $table[$id] = [
        'type' => [
          '#type' => 'markup',
          '#markup' => $plugin['label'],
        ],
        'operations' => [
          '#type' => 'operations',
          '#links' => $this->buildOperationsLinks($id),
        ],
      ];
    }

    $build['container']['table'] = $table;
    return $build;
  }

  /**
   * Build the operations links.
   *
   * @param string $name
   *   The selection set machine name.
   *
   * @return array
   *   Array of operations links.
   */
  protected function buildOperationsLinks($name) {
    $operations['configure'] = [
      'title' => $this->t('Configure'),
      'url' => Url::fromRoute('access_policy_ui.selection_set_form', [
        'target_entity_type_id' => $this->targetEntityType->id(),
        'name' => $name,
      ]),
      'attributes' => $this->getModalAttributes(),
    ];

    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'url' => Url::fromRoute('access_policy_ui.selection_set_delete_form', [
        'target_entity_type_id' => $this->targetEntityType->id(),
        'name' => $name,
      ]),
      'attributes' => $this->getModalAttributes(['width' => '880px']),
    ];

    return $operations;
  }

  /**
   * Get the modal attributes.
   *
   * @return array
   *   The modal attributes.
   */
  protected function getModalAttributes($options = ['width' => '75%']) {
    return [
      'class' => ['use-ajax', 'button', 'button--small', 'field-add-more-submit'],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode($options),
    ];
  }

}
