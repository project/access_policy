<?php

namespace Drupal\access_policy_ui;

use Drupal\access_policy\AccessPolicyInformation;
use Drupal\access_policy\AccessPolicySelectionInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of AccessPolicy type entities.
 */
class AccessPolicyListBuilder extends ConfigEntityListBuilder {

  use AccessPolicyTableTrait;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The access policy information service.
   *
   * @var \Drupal\access_policy\AccessPolicyInformation
   */
  protected $accessPolicyInfo;

  /**
   * The access policy selection manager.
   *
   * @var \Drupal\access_policy\AccessPolicySelectionInterface
   */
  protected $selectionManager;

  /**
   * Array of supported entity types.
   *
   * @var array
   */
  protected $entityTypes;

  /**
   * Constructs a new AccessPolicyListBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\access_policy\AccessPolicyInformation $information
   *   Access policy information service.
   * @param \Drupal\access_policy\AccessPolicySelectionInterface $selection_manager
   *   The access policy selection manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, AccessPolicyInformation $information, AccessPolicySelectionInterface $selection_manager) {
    parent::__construct($entity_type, $storage);
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->accessPolicyInfo = $information;
    $this->selectionManager = $selection_manager;
    $this->entityTypes = $this->accessPolicyInfo->getAllEnabledEntityTypes();
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('access_policy.information'),
      $container->get('access_policy.selection'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'access_policy_admin_overview';
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = [];
    foreach ($this->load() as $entity_type => $entities) {
      if (!isset($this->entityTypes[$entity_type])) {
        continue;
      }

      $build[$entity_type] = [
        '#type' => 'container',
        '#prefix' => '<h2>' . $this->entityTypes[$entity_type]->getLabel() . '</h2>',
      ];

      // Move content at the top.
      if ($entity_type == 'node') {
        $build[$entity_type]['#weight'] = -10;
      }

      $build[$entity_type]['settings'] = [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#type' => 'link',
        '#url' => Url::fromRoute('access_policy_ui.entity_type_settings_form', ['target_entity_type_id' => $entity_type]),
        '#title' => $this->t('@entity_type settings', ['@entity_type' => $this->entityTypes[$entity_type]->getLabel()]),
        '#weight' => -10,
      ];
      $build[$entity_type]['table'] = $this->renderTable($this->entityTypes[$entity_type], $entities);
    }
    if (empty($build)) {
      $build = $this->renderEmptyTable();
    }
    return $build;
  }

  /**
   * Render an empty table when no results found.
   *
   * @return array
   *   Empty table render array.
   */
  public function renderEmptyTable() {
    $table = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#rows' => [],
      '#empty' => $this->t('No Access policies yet'),
    ];
    return $table;

  }

  /**
   * Render the access policy table.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The current entity type.
   * @param array $entities
   *   The array of access policies.
   *
   * @return array
   *   The fully formed table render array.
   */
  public function renderTable(EntityTypeInterface $entity_type, array $entities) {
    $table = [
      '#prefix' => '<div id = "policies-' . $entity_type->id() . '">',
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#rows' => [],
      '#suffix' => '</div>',
    ];
    foreach ($entities as $entity) {
      if ($row = $this->buildRow($entity)) {
        $table['#rows'][$entity->id()] = $row;
      }
    }

    return $table;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['selection_set'] = $this->t('Selection set');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['selection_set'] = $this->getSelectionSetLabels($entity, 2);
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entities = [];
    foreach (parent::load() as $entity) {
      $entities[$entity->getTargetEntityTypeId()][] = $entity;
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\field\FieldConfigInterface $entity */
    $operations = parent::getDefaultOperations($entity);

    if (isset($operations['edit'])) {
      $operations['edit']['weight'] = 30;
      $operations['edit']['title'] = $this->t('Configure');
    }

    if ($entity->hasLinkTemplate('selection-settings-form')) {
      $operations['selection'] = [
        'title' => $this->t('Manage selection'),
        'weight' => 31,
        'url' => $this->ensureDestination($entity->toUrl('selection-settings-form')),
      ];
    }

    return $operations;
  }

}
