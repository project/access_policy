<?php

namespace Drupal\Tests\access_policy_ui\Kernel;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Tests\access_policy\Kernel\AccessPolicyKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests operations table ui builder.
 *
 * @group access_policy_ui
 */
class OperationsTableUiBuilderTest extends AccessPolicyKernelTestBase {

  use UserCreationTrait;
  use StringTranslationTrait;

  /**
   * The user role storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $roleStorage;

  /**
   * The operations table UI builder.
   *
   * @var \Drupal\access_policy_ui\OperationsTableUiBuilder
   */
  protected $operationsTableUiBuilder;

  /**
   * The authenticated role.
   *
   * @var \Drupal\user\RoleInterface
   */
  protected $authenticated;

  /**
   * The modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'filter',
    'node',
    'options',
    'system',
    'text',
    'user',
    'datetime',
    'access_policy',
    'access_policy_test',
    'access_policy_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['access_policy_test']);

    $this->roleStorage = $this->container->get('entity_type.manager')->getStorage('user_role');
    $this->operationsTableUiBuilder = $this->container->get('access_policy.operations_table_ui_builder');

    // Create the authenticated role. If this role has a permission then all
    // other roles also have that permission (except for anonymous).
    $authenticated = $this->roleStorage->create(['id' => 'authenticated', 'label' => 'Authenticated']);
    $authenticated->save();
    $this->authenticated = $authenticated;
  }

  /**
   * Basic tests for the operation table ui builder.
   *
   * @dataProvider providerOperationsTableUiBuilder
   */
  public function testOperationsTableUiBuilder($roles, $access_rule, $expected) {
    $policy = AccessPolicy::create([
      'id' => 'basic',
      'label' => 'Basic policy',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
    ]);
    $policy->save();

    if ($access_rule) {
      $handler = $this->accessRuleManager->getHandler('node', $access_rule['handler']);
      if (isset($access_rule['settings'])) {
        $handler->setSettings($access_rule['settings']);
      }
      $policy->addHandler('access_rule', $handler);
      $policy->save();
    }

    foreach ($roles as $role_id => $permissions) {
      if ($role_id == 'authenticated') {
        $this->grantPermissions($this->authenticated, $permissions);
      }
      else {
        $this->createRole($permissions, $role_id);
      }
    }

    $table = $this->operationsTableUiBuilder->build($policy);

    if (!isset($expected['authenticated'])) {
      $expected_authenticated = [
        'authenticated' => [
          'view' => '',
          'view_unpublished' => '',
          'view_all_revisions' => '',
          'edit' => '',
          'delete' => '',
        ],
      ];

      $expected = array_merge($expected_authenticated, $expected);
    }

    $this->assertMatchingTableRows($expected, $table['op_container']['table']);
  }

  /**
   * Data provider for operations table ui builder.
   */
  public function providerOperationsTableUiBuilder() {
    return [
      'role with all operations' => [
        'roles' => [
          'content_admin' => [
            'view basic content',
            'view any basic unpublished content',
            'view all basic content revisions',
            'edit basic content',
            'delete basic content',
          ],
        ],
        'access_rule' => FALSE,
        'expected' => [
          'content_admin' => [
            'view' => '✔',
            'view_unpublished' => '✔',
            'view_all_revisions' => '✔',
            'edit' => '✔',
            'delete' => '✔',
          ],
        ],
      ],
      'role with view operations' => [
        'roles' => [
          'content_admin' => [
            'view basic content',
          ],
        ],
        'access_rule' => FALSE,
        'expected' => [
          'content_admin' => [
            'view' => '✔',
            'view_unpublished' => '',
            'view_all_revisions' => '',
            'edit' => '',
            'delete' => '',
          ],
        ],
      ],
      'role with no operations' => [
        'roles' => [
          'content_admin' => [],
        ],
        'access_rule' => FALSE,
        'expected' => [
          'content_admin' => [
            'view' => '',
            'view_unpublished' => '',
            'view_all_revisions' => '',
            'edit' => '',
            'delete' => '',
          ],
        ],
      ],
      'policy with access rules' => [
        'roles' => [
          'content_admin' => [
            'view basic content',
            'view any basic unpublished content',
            'view all basic content revisions',
            'edit basic content',
            'delete basic content',
          ],
        ],
        'access_rule' => [
          'handler' => 'is_own',
        ],
        'expected' => [
          'content_admin' => [
            'view' => new TranslatableMarkup('Limited'),
            'view_unpublished' => new TranslatableMarkup('Limited'),
            'view_all_revisions' => new TranslatableMarkup('Limited'),
            'edit' => new TranslatableMarkup('Limited'),
            'delete' => new TranslatableMarkup('Limited'),
          ],
        ],
      ],
      'policy with access rules and disabled operation constraints' => [
        'roles' => [
          'content_admin' => [
            'view basic content',
            'edit basic content',
            'delete basic content',
          ],
        ],
        'access_rule' => [
          'handler' => 'is_own',
          'settings' => [
            'operations' => ['view'],
          ],
        ],
        'expected' => [
          'content_admin' => [
            'view' => new TranslatableMarkup('Limited'),
            'view_unpublished' => '',
            'view_all_revisions' => '',
            'edit' => '✔',
            'delete' => '✔',
          ],
        ],
      ],
      'policy with access rules and bypass permission' => [
        'roles' => [
          'content_admin' => [
            'view basic content',
            'edit basic content',
            'delete basic content',
            'bypass basic access rules',
          ],
        ],
        'access_rule' => [
          'handler' => 'is_own',
        ],
        'expected' => [
          'content_admin' => [
            'view' => '✔',
            'view_unpublished' => '',
            'view_all_revisions' => '',
            'edit' => '✔',
            'delete' => '✔',
          ],
        ],
      ],
      'authenticated with multiple operation' => [
        'roles' => [
          'authenticated' => [
            'view basic content',
            'edit basic content',
          ],
          'content_admin' => [],
        ],
        'access_rule' => FALSE,
        'expected' => [
          'authenticated' => [
            'view' => '✔',
            'view_unpublished' => '',
            'view_all_revisions' => '',
            'edit' => '✔',
            'delete' => '',
          ],
          'content_admin' => [
            'view' => '✔',
            'view_unpublished' => '',
            'view_all_revisions' => '',
            'edit' => '✔',
            'delete' => '',
          ],
        ],
      ],

    ];
  }

  /**
   * Assert that the table rows match.
   *
   * @param array $expected
   *   The expected rows; keyed by role id.
   * @param array $actual
   *   The actual rows.
   */
  public function assertMatchingTableRows($expected, $actual) {
    $actual_values = [];
    foreach ($expected as $role_id => $columns) {
      if (isset($actual[$role_id])) {
        foreach ($columns as $name => $value) {
          $actual_values[$role_id][$name] = $actual[$role_id][$name]['#markup'];
        }
      }
    }

    $this->assertEquals($expected, $actual_values);
  }

}
