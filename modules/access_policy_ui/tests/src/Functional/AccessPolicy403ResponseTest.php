<?php

namespace Drupal\Tests\access_policy_ui\Functional;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\Tests\access_policy\Functional\AccessPolicyTestBase;

/**
 * Tests access policy with custom 403 response.
 *
 * @group access_policy_ui
 */
class AccessPolicy403ResponseTest extends AccessPolicyTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'access_policy',
    'access_policy_ui',
    'access_policy_test',
    'filter',
    'node',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests viewing a custom 403 response message.
   */
  public function testViewContentWithCustom403Response() {
    $policy = AccessPolicy::create([
      'id' => 'policy_403_message',
      'label' => 'Test policy with 403 message',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'http_403_response' => [
        'plugin_id' => 'message',
        'settings' => [
          'message' => 'Foo bar',
        ],
      ],
    ]);
    $policy->save();

    // This user is not allowed to view content with this policy.
    $author = $this->drupalCreateUser();

    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
      'access_policy' => ['policy_403_message'],
    ]);

    $this->drupalLogin($author);

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Foo bar');

    // Create a node without an access policy. Make sure it uses the default.
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 0,
    ]);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');

    // View an entity type that does not support access policy. Make sure it
    // uses the default 403 error.
    $web_user = $this->drupalCreateUser();
    $this->drupalGet('user/' . $web_user->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');

    // Make sure that we're not overriding any other response.
    $this->drupalGet('node/' . $node->id() . '/empty');
    $this->assertSession()->statusCodeEquals(404);
    $this->assertSession()->pageTextContains('The requested page could not be found.');

    // Also make sure that custom 404 pages still work.
    $node_404 = $this->drupalCreateNode([
      'title' => 'Custom 404',
      'type' => 'page',
    ]);
    $this->config('system.site')->set('page.404', '/node/' . $node_404->id())->save();
    $this->drupalGet('node/' . $node->id() . '/empty');
    $this->assertSession()->statusCodeEquals(404);
    $this->assertSession()->pageTextContains('Custom 404');
  }

  /**
   * Tests creating an access policy with a custom 403 response.
   */
  public function testCreatePolicyWithCustom403Response() {

    // Create an access policy without custom 403 response.
    $policy = AccessPolicy::create([
      'id' => 'policy_403_message',
      'label' => 'Test policy with 403 message',
      'access_rules' => [],
      'target_entity_type_id' => 'node',
      'http_403_behavior' => [],
    ]);
    $policy->save();

    $rid = $this->drupalCreateRole([
      'access administration pages',
      'administer access policy entities',
    ]);
    $web_user = $this->drupalCreateUser();
    $web_user->addRole($rid);
    $web_user->save();

    $this->drupalLogin($web_user);

    $this->drupalGet('admin/people/access-policies');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/admin/people/access-policies/policy_403_message');
    $edit = [
      '403_behavior[plugin_id]' => 'message',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->statusCodeEquals(200);

    // Change the message text.
    $this->drupalGet('/admin/people/access-policies/policy_403_message');
    $edit = [
      '403_behavior[plugin_id]' => 'message',
      '403_behavior[settings][message]' => 'Foo bar',
    ];
    $this->submitForm($edit, 'Save');

    // Confirm that it saved the values.
    $this->drupalGet('/admin/people/access-policies/policy_403_message');
    $this->assertSession()->fieldValueEquals('403_behavior[plugin_id]', 'message');
    $this->assertSession()->fieldValueEquals('403_behavior[settings][message]', 'Foo bar');
  }

}
