<?php

namespace Drupal\Tests\access_policy_ui\Functional;

use Drupal\access_policy\Entity\AccessPolicy;
use Drupal\Tests\access_policy\Functional\AccessPolicyTestBase;

/**
 * Tests updating the entity type settings.
 *
 * @group access_policy_ui
 */
class AccessPolicySettingsFormTest extends AccessPolicyTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'access_policy',
    'access_policy_ui',
    'access_policy_test',
    'filter',
    'node',
    'datetime',
    'options',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests updating the entity type settings form.
   */
  public function testEntityTypeSettingsForm() {
    $policy = AccessPolicy::create([
      'id' => 'basic_policy',
      'label' => 'Basic policy',
      'target_entity_type_id' => 'node',
      'weight' => -10,
    ]);
    $policy->save();

    $rid = $this->drupalCreateRole([
      'access administration pages',
      'administer access policy entities',
    ]);
    $web_user = $this->drupalCreateUser();
    $web_user->addRole($rid);
    $web_user->save();

    $this->drupalLogin($web_user);

    $this->drupalGet('admin/people/access-policies/node/settings');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Basic policy');

    $this->assertSession()->elementExists('css', "[name='selection_strategy'][value='dynamic']");
    $this->assertSession()->elementExists('css', "[name='selection_strategy'][value='manual']");
    $this->assertSession()->fieldValueEquals('selection_strategy', 'dynamic');

    $edit = [
      'selection_strategy' => 'manual',
    ];
    $this->submitForm($edit, 'Save');

    $this->clickLink('Content settings');
    $this->assertSession()->fieldValueEquals('selection_strategy', 'manual');

    $this->clickLink('Add selection set');
    $this->assertSession()->statusCodeEquals(200);
    $edit = [
      'label' => 'Foo bar',
      'id' => 'foo_bar',
    ];
    $this->submitForm($edit, 'Save');
    $this->drupalGet('admin/people/access-policies/node/settings');
    $this->assertSession()->pageTextContains('Foo bar');

    $this->drupalGet('admin/people/access-policies/node/settings/set/foo_bar/delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([], 'Delete');
    $this->drupalGet('admin/people/access-policies/node/settings');
    $this->assertSession()->pageTextNotContains('Foo bar');

  }

}
