<?php

/**
 * @file
 * Hooks provided by the Access Policy module.
 */

/**
 * Describe access policy data.
 *
 * This maps the entity fields with the access policy handlers like access
 * rules abd selection rules.
 *
 * @return array
 *   An associative array describing the structure of entities and fields
 *   (and their equivalents) provided for use in Access policy.
 *
 * @see hook_access_policy_data_alter()
 */
function hook_access_policy_data() {

  // Define the return array.
  $data = [];

  // If validating directly against the fields of an entity the outermost keys
  // of $data are that entity type.
  $data['node'] = [];

  // If validating against something else (e.g. the current time), you can
  // provide a custom key.
  $data['system'] = [];

  // The first key inside the entity key represents the unique id of the
  // handler. The key can be anything you like, however a good convention is to
  // use the field name.
  $data['node']['field_example'] = [];

  // The minimum access policy data.
  // The label, field and plugin id are the only required fields to register
  // your access rule.
  $data['node']['field_example'] = [
    'label' => t('Example access rule.'),
    // The real field name.
    'field' => 'field_example',
    // The access rule plugin id.
    'plugin_id' => 'entity_field_string',
    // Alternative format.
    'access_rule' => [
      'plugin_id' => 'entity_field_string',
    ],
  ];

  // Recommended minimum access policy data.
  $data['node']['field_example'] = [
    'label' => t('Example access rule.'),
    // Provide a helpful description of your handler.
    'description' => t('The access rule description'),
    // Provide the entity type (otherwise it will default to the first key).
    // For example, in this case it would be 'node'.
    'entity_type' => 'node',
    'field' => 'field_example',
    'plugin_id' => 'entity_field_string',
  ];

  // Handler settings.
  // Each handler rule can define its own settings. You can provide default
  // values for those settings by providing the 'settings' key.
  $data['node']['field_example'] = [
    'label' => t('Example access rule.'),
    'field' => 'field_example',
    'plugin_id' => 'entity_field_string',
    // Set some default settings for the access rule. Administrators can still
    // change these values.
    'settings' => [
      'operator' => '=',
      'empty_behavior' => 'deny',
    ],
  ];

  // Static access rule operator.
  // All the core EntityField access rule plugins support operators (e.g. equal
  // to, not equal to) which can be changed from the access rule configuration
  // form. In some cases you might not want that. You can prevent it by
  // providing an 'operator' key.
  $data['node']['field_example'] = [
    'label' => t('Example access rule.'),
    'field' => 'field_example',
    'plugin_id' => 'entity_field_string',
    // Set an operator key and the access rule will always validate against
    // this.
    'operator' => '=',
  ];

  // Access rule widget plugin.
  // Access policy uses Drupal fields to control access. However, you
  // may not want to expose those fields to all users. You can "move" those
  // fields to the Access policy selection tab by setting the "widget" key.
  // This can be configured from the access rule configuration page.
  $data['node']['field_example'] = [
    'label' => t('The example access rule.'),
    'field' => 'field_example',
    'plugin_id' => 'entity_field_string',
    // The access rule widget plugin id.
    'widget' => 'entity_field',
  ];

  // Validating against contextual data.
  // In some cases you'll want to pass in some contextual data to your access
  // rule plugin so that you can validate against it. A common use case is when
  // comparing the entity to the current user.
  $data['node']['field_tags'] = [
    'label' => t('Has matching tags'),
    'field' => 'field_tags',
    'plugin_id' => 'entity_field_entity_reference',
    // To provide contextual information you use the 'argument' key.
    'argument' => [
      // The CurrentUser access rule argument plugin id.
      'plugin' => 'current_user',
      // Specify the field you want to compare against.
      'field' => 'field_tags',
      // Or you can set a field type instead.
      'field_type' => 'entity_reference',
    ],
  ];

  return $data;
}

/**
 * Alter the access policy handler information from hook_access_policy_data().
 *
 * @param array $data
 *   An array of all information about handlers, collected from
 *   hook_access_policy_data(), passed by reference.
 *
 * @see \hook_access_policy_data()
 */
function hook_access_policy_data_alter(array &$data) {
  // Alter the label of the field_tags access rule.
  $data['node']['field_tags']['label'] = t('Has matching tags with current user');
}
