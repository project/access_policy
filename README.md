# Access Policy

The Access policy module combines Drupal's role based access control (RBAC)
architecture with an Attribute Based Access Control (ABAC) architecture to
create an incredibly powerful and flexible system for managing access to your
content.

To achieve this, Access Policy leverages Drupal fields as the attributes.
For example, to restrict content by department all you need to do is add a
"Department" taxonomy term entity reference field to a content type and user
and then set up your access policy to use it. No code necessary.

With Access Policy you can do things like:

- Restrict content to members in a particular group or department.
- Only allow users authors to edit content from 9:00 AM - 5:00 PM, Monday
  through Friday.
- Restrict content by priority or security level such as Secret, confidential,
  public etc.
- Make content private while granting some access to specific users. Very
  useful when complimented with content moderation.
- Grant access to content by assigning it to individual users.
- Soft-delete content with an “Add to trash” checkbox.
- Grant access to users with a specific domain name in their email address.
- Create gated content with a custom access denied message.
- Allow authors to see any unpublished content except for content that has
  been archived.

Any many more use cases.

- For a full description of the module, visit the [project page].
- To submit bug reports and feature suggestions, or to track changes, use the
  [issue queue].

[Project page]: https://www.drupal.org/project/access_policy
[issue queue]: https://www.drupal.org/project/issues/access_policy


## Table of contents

- Requirements
- Installation
- Configuration
- Information for developers

## Requirements

No other modules are required.

## Installation

- Install as you would normally install a contributed Drupal module. For further
  information, see _[Installing Drupal Modules]_.

[Installing Drupal Modules]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules

## Configuration

After installation, enable the access_policy_ui sub-module to start creating access policies. To
create your first policy go to `/admin/people/access-policies` and add a policy.
From there you can configure permissions, access rules, selection rules, and
set a custom 403 message.

After your policy has been created, decide what selection mode you want to use.
Access policy supports two selection modes:
- Dynamic: This follows a traditional ABAC model where access policies are
  dynamically assigned based on selection rules when an entity is updated.
  Note that some entity types (like paragraphs), only support the Dynamic
  selection mode.
- Manual: This mode is more of document style experience where you can choose
  the access policy from an Access tab. If you're unfamiliar with ABAC, this
  is a good place to start.

You can choose the mode you prefer from the entity type settings section of
the Access policies page.

[Getting started]: https://www.drupal.org/docs/8/modules/access-policy/getting-started

## Information for developers

### Hooks

Access policy comes with two hooks.

| Hook                          | Description                                      |
|-------------------------------|--------------------------------------------------|
| hook_access_policy_data       | Defines access policy handler data definition.   |
| hook_access_policy_data_alter | Alter the access policy handler data definition. |

For more details please see access_policy.api.php.

### Plugins

Access policy is highly extensible with custom plugins for entity operations, Access rules, Access rule
widgets, Access rule arguments, Selection rules, Selection strategies and 403
responses. This gives developers complete control of how to define their access scheme.

| Plugin type             | Directory                                        | Description                                                             |
|-------------------------|--------------------------------------------------|-------------------------------------------------------------------------|
| Access policy operation | `src/Plugin/access_policy/AccessPolicyOperation` | Integrates entity operations with access policy.                        |
| Access policy query     | `src/Plugin/access_policy/AccessPolicyQuery`     | Database query integration.                                             |
| Access rule             | `src/Plugin/access_policy/AccessRule`            | Define custom validation rules based on attributes.                     |
| Access rule argument    | `src/Plugin/access_policy/AccessRuleArgument`    | Provides contextual arguments for access rules.                         |
| Access rule widget      | `src/Plugin/access_policy/AccessRuleWidget`      | Displays a widget from the Access policy selection tab.                 |
| Selection rule          | `src/Plugin/access_policy/SelectionRule`         | Define custom policy selection rules based on attributes.               |
| Selection strategy      | `src/Plugin/access_policy/SelectionStrategy`     | Define custom policy selection strategies.                              |
| 403 response behavior   | `src/Plugin/access_policy/Http403Response`       | Render a custom 403 message.                                            |

### Drush

Access policy comes with a helpful command to diagnose access issues.

```
drush access-policy:check-access [entity type] [id] [username]
```

This will render a table with details about why a user might not be able to
access an entity. This will also include any messages provided by Drupal core.

```
Access policies assigned to this entity: Me only

-------------------- -------- ----------------------------------------------------
Operation            Access   Message
-------------------- -------- ----------------------------------------------------
view                 Yes
view all revisions   No       The 'Authored by current user' access rule failed.
update               No       The 'Authored by current user' access rule failed.
delete               No       The 'Authored by current user' access rule failed.
-------------------- -------- ----------------------------------------------------
```
