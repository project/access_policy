<?php

/**
 * @file
 * Provide views data for access_policy.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function access_policy_views_data_alter(array &$data) {
  $access_controlled_entity_types = \Drupal::service('access_policy.information')->getAllEnabledEntityTypes();

  foreach ($access_controlled_entity_types as $entity_type) {
    $table = $entity_type->id() . '__access_policy';
    if (isset($data[$table])) {
      $data[$table]['access_policy_target_id']['field'] = [
        'id' => 'field',
        'default_formatter' => 'content_access_policy',
        'field_name' => 'access_policy_target_id',
      ];
      $data[$table]['access_policy_target_id']['filter'] = [
        'id' => 'access_policy_filter',
        'allow empty' => TRUE,
      ];
    }
  }

}
